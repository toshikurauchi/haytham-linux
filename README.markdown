## Documentation

Documentation is available at [http://toshikurauchi.bitbucket.org/Haytham-Linux/](http://toshikurauchi.bitbucket.org/Haytham-Linux/).

## QT and OpenCV integration

QT and OpenCV integration was based on http://code.google.com/p/qt-opencv-multithreaded/

## Running tests

Run on a terminal:

	$ ./runtests.sh

In order to run all tests (showing all image results) run:

	$ ./runtests.sh -A

Or

	$ ./runtests.sh --all
	
### Python tests

To copy Python tests to the HaythamPython build directory add the following build step:

	make install

You will need [nose](http://nose.readthedocs.org/en/latest/) to run the Python wrapper tests:

	easy_install nose
	
or

	pip install nose
	
After installing nose you just have to run:

	$ nosetests
	
To see the test output images run nose in verbose mode:

	$ nosetests -v

## Generating documentation

Run on a terminal:

	$ doxygen Doxyfile

## IDE

For some reason the project isn't working with QT Eclipse plugin. We are currently using QT Creator as main IDE.

## Requirements

### All

_Boost_, _VRPN_ and [_ArUco_](http://www.uco.es/investiga/grupos/ava/node/26) libraries are required for all OS.

### Linux

In order to run Haytham on Linux you will need _uvcdynctrl_ installed.
