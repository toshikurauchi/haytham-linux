# -------------------------------------------------
# Project created by QtCreator 2012-07-18T15:58:29
# -------------------------------------------------

TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS = HaythamLib \
          HaythamGUI \
          HaythamPython
HaythamGUI.depends = HaythamLib
HaythamPython.depends = HaythamLib
