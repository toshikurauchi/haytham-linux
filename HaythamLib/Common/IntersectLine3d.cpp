/*
 * IntersectLine3d.cpp
 *
 *  Created on: 16/10/2013
 *      Author: Andrew Kurauchi
 */

#include "IntersectLine3d.h"

namespace haytham {

IntersectLine3d::IntersectLine3d()
{
}

cv::Mat IntersectLine3d::intersectLines(cv::Mat &PA, cv::Mat &PB) {
    // N lines described as vectors
    cv::Mat Si = PA - PB;

    // Normalize vectors
    cv::Mat ni;
    cv::multiply(Si, Si, ni); // ni = Si^2
    cv::reduce(ni, ni, 1, CV_REDUCE_SUM); // Sum columns
    cv::sqrt(ni, ni);
    ni = ni.t(); // make matrix with 3 columns of norms to normalize each component of Si
    cv::Mat niRow;
    ni.copyTo(niRow);
    ni.push_back(niRow);
    ni.push_back(niRow);
    ni = ni.t();
    cv::divide(Si, ni, ni);

    cv::Mat nx = ni.col(0);
    cv::Mat ny = ni.col(1);
    cv::Mat nz = ni.col(2);

    cv::Mat nxSquare = nx.mul(nx)-1;
    cv::Mat nySquare = ny.mul(ny)-1;
    cv::Mat nzSquare = nz.mul(nz)-1;
    cv::Mat nxny = nx.mul(ny);
    cv::Mat nxnz = nx.mul(nz);
    cv::Mat nynz = ny.mul(nz);

    double SXX = cv::sum(nxSquare)[0];
    double SYY = cv::sum(nySquare)[0];
    double SZZ = cv::sum(nzSquare)[0];
    double SXY = cv::sum(nxny)[0];
    double SXZ = cv::sum(nxnz)[0];
    double SYZ = cv::sum(nynz)[0];
    cv::Mat S(3, 3, CV_64FC1);
    S.at<double>(0, 0) = SXX;
    S.at<double>(0, 1) = SXY;
    S.at<double>(0, 2) = SXZ;
    S.at<double>(1, 0) = SXY;
    S.at<double>(1, 1) = SYY;
    S.at<double>(1, 2) = SYZ;
    S.at<double>(2, 0) = SXZ;
    S.at<double>(2, 1) = SYZ;
    S.at<double>(2, 2) = SZZ;

    cv::Mat PAx = PA.col(0);
    cv::Mat PAy = PA.col(1);
    cv::Mat PAz = PA.col(2);
    double CX = cv::sum(PAx.mul(nxSquare) + PAy.mul(nxny) + PAz.mul(nxnz))[0];
    double CY = cv::sum(PAx.mul(nxny) + PAy.mul(nySquare) + PAz.mul(nynz))[0];
    double CZ = cv::sum(PAx.mul(nxnz) + PAy.mul(nynz) + PAz.mul(nzSquare))[0];
    cv::Mat C(3, 1, CV_64FC1);
    C.at<double>(0, 0) = CX;
    C.at<double>(1, 0) = CY;
    C.at<double>(2, 0) = CZ;

    cv::Mat intersection;
    if(cv::solve(S, C, intersection, cv::DECOMP_SVD)) {
        if(intersection.type() != PA.type()) {
            intersection.convertTo(intersection, PA.type());
        }
        return intersection;
    }
    return cv::Mat();
}

cv::Mat IntersectLine3d::intersectPlane(cv::Mat &line, cv::Mat &plane) {
    // Calculate normal vector to plane
    cv::Mat v1 = plane.col(1) - plane.col(0);
    cv::Mat v2 = plane.col(2) - plane.col(0);
    cv::Mat normal = v1.cross(v2);

    // Calculate vector of the line direction
    cv::Mat lineDir = line.col(1) - line.col(0);

    // Calculate d such that d*lineDir is at the plane
    double d = (plane.col(0) - line.col(0)).dot(normal)/(lineDir.dot(normal));

    return line.col(0) + d*lineDir;
}

} // namespace haytham
