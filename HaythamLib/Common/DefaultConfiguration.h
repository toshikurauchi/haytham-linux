/*
 * DefaultConfiguration.h
 *
 *  Created on: 15/01/2013
 *      Author: Andrew Kurauchi
 */

#ifndef DEFAULTCONFIGURATION_H
#define DEFAULTCONFIGURATION_H

namespace haytham {

// Pupil detection
const int IRIS_SIZE = 225;
const bool SHOW_PUPIL = true;
const bool SHOW_GLINT = true;
const int PUPIL_THRESHOLD = 75;
const int GLINT_THRESHOLD = 230;
const int DARKEST_REGION_IMAGE_WIDTH = 200;
const int DARKEST_REGION_MAVG_N = 5;
const int DARKEST_REGION_PUPIL_IRIS_RATIO = 4; // Relation between pupil and iris diameters: diamIris = pupilIrisRatio*pupilDiameter
const int DARKEST_REGION_THRESHOLD_VICINITY = 30; // How many pixels around the pupil point are used to calculate the threshold value for pupil segmentation

// Screen detection
const int SCENE_THRESHOLD = 200; // Old value: 450
const int SCREEN_MIN_SIZE = 30;
const int SHOW_SCREEN = false;
const int SHOW_FILTERED_SCENE_IMAGE = false;

// Gaze
const bool SHOW_GAZE = true;
const bool SMOOTH_GAZE = true;

// Calibration Points Detection
const int MIN_CALIB_POINTS_THRESH = 40;
const int MAX_CALIB_POINTS_THRESH = 250;
const int INIT_CALIB_POINTS_THRESH = 100;

} // namespace haytham

#endif // DEFAULTCONFIGURATION_H
