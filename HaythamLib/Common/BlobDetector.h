/*
 * BlobDetector.h
 *
 *  Created on: 30/01/2014
 *      Author: Nghia Ho
 *      Source: http://nghiaho.com/?p=1102
 */

#ifndef HAYTHAM_BLOBDETECTOR_H
#define HAYTHAM_BLOBDETECTOR_H

#include <vector>
#include <opencv2/opencv.hpp>

#include "Common/Blob.h"

namespace haytham {

class BlobDetector
{
public:
    BlobDetector();
    void findBlobs(const cv::Mat &binary, std::vector <Blob> &blobs);
};

} // namespace haytham

#endif // HAYTHAM_BLOBDETECTOR_H
