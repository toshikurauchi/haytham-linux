/*
 * Timer.cpp
 *
 *  Created on: 25/10/2012
 *      Author: Andrew Kurauchi
 */

#include "Timer.h"

namespace haytham {

/**
 * \brief Starts the timer.
 */
Timer::Timer() {
    this->start = boost::chrono::system_clock::now();
}

/**
 * \returns The time elapsed time since the timer was started (in milliseconds).
 */
double Timer::now() {
    boost::chrono::system_clock::time_point end;
    boost::chrono::duration<double, boost::milli> duration;

    end = boost::chrono::system_clock::now();
    duration = end - this->start;
    return duration.count();
}

} /* namespace haytham */
