/*
 * IntersectLine3d.h
 *
 *  Created on: 16/10/2013
 *      Author: Andrew Kurauchi
 */


#ifndef HAYTHAM_INTERSECTLINE3D_H
#define HAYTHAM_INTERSECTLINE3D_H

#include <opencv2/opencv.hpp>

namespace haytham {

class IntersectLine3d
{
public:
    IntersectLine3d();

    /**
     * Based on: http://www.mathworks.com/matlabcentral/fileexchange/37192-intersection-point-of-lines-in-3d-space/content/lineIntersect3D.m
     *
     * \brief Find intersection point of lines in 3D space, in the least squares sense.
     *
     * \param PA Nx3-matrix containing starting point of N lines
     * \param PB Nx3-matrix containing end point of N lines
     *
     * \returns A column vector representing the 3D best intersection point of the N lines, in least squares sense.
     */
    cv::Mat intersectLines(cv::Mat &PA, cv::Mat &PB);

    /**
     * Based on: http://en.wikipedia.org/wiki/Line-plane_intersection
     *
     * \brief Find intersection point of line and plane in 3D space.
     *
     * \param line 3x2-matrix containing two points at the line as column vectors
     * \param plane 3x3-matrix containing three points at the plane as column vectors
     *
     * \returns A column vector representing the plane-line 3D intersection point.
     */
    cv::Mat intersectPlane(cv::Mat &line, cv::Mat &plane);
};

} // namespace haytham

#endif // HAYTHAM_INTERSECTLINE3D_H
