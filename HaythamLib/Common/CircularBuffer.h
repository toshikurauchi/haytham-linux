#ifndef HAYTHAM_CIRCULARBUFFER_H
#define HAYTHAM_CIRCULARBUFFER_H

#include "Exceptions.h"

namespace haytham {

template <class T>
class CircularBuffer
{
public:
    CircularBuffer(int capacity) : capacity(capacity), nextFreePos(0), count(0) {
        this->buffer = new T[capacity];
    }

    void add(T elem) {
        this->buffer[this->nextFreePos] = elem;
        this->nextFreePos = (this->nextFreePos + 1) % this->capacity;
        if(this->count < this->capacity) {
            this->count++;
        }
    }

    int size() {
        return this->count;
    }

    T& get(int idx) {
        if(idx >= 0 && idx < this->count)
            return this->buffer[idx];
        throw IndexOutOfBoundsException();
    }

    class CircularBufferIterator {
    public:
        CircularBufferIterator(CircularBuffer *buffer, int maxPos, int startPos, int curPos = -1, bool passed = false) :
            buffer(buffer), startPos(startPos), maxPos(maxPos), passed(passed) {
            if(curPos == -1) {
                this->curPos = this->startPos;
            }
            else {
                this->curPos = curPos;
            }
        }

        bool hasNext() {
            return curPos != startPos || !passed;
        }

        CircularBufferIterator next() {
            int nextPos = (curPos - 1);
            if(nextPos < 0)
                nextPos = maxPos - 1;
            return CircularBufferIterator(buffer, maxPos, startPos, nextPos, true);
        }

        T& val() {
            return buffer->get(curPos);
        }

    private:
        CircularBuffer *buffer;
        int curPos;
        int startPos;
        int maxPos;
        bool passed;
    };

    CircularBufferIterator iterator() {
        int firstPos = this->nextFreePos - 1;
        if(firstPos < 0)
            firstPos = this->count - 1;
        return CircularBufferIterator(this, this->count, firstPos);
    }

private:
    T *buffer;
    int capacity;
    int nextFreePos;
    int count;
};

} // namespace haytham

#endif // HAYTHAM_CIRCULARBUFFER_H
