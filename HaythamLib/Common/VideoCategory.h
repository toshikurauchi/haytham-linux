/*
 * VideoCategory.h
 *
 *  Created on: 02/10/2012
 *      Author: Andrew Kurauchi
 */

#ifndef VIDEOCATEGORY_H
#define VIDEOCATEGORY_H

#include <QImage>
#include <QMetaType>

namespace haytham {

enum VideoCategory {
    EYE,
    SCENE
};

class Frame {
public:
    Frame() {}
    Frame(VideoCategory category, QImage frame);
    VideoCategory getCategory() const;
    QImage getFrame() const;
private:
    VideoCategory category;
    QImage frame;
};

} /* namespace haytham */

#endif // VIDEOCATEGORY_H
