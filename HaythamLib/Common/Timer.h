/*
 * Timer.h
 *
 *  Created on: 25/10/2012
 *      Author: Andrew Kurauchi
 */

#ifndef TIMER_H
#define TIMER_H

#include <boost/chrono/chrono.hpp>

namespace haytham {

class Timer
{
public:
    Timer();
    double now();
private:
    boost::chrono::system_clock::time_point start;
};

} /* namespace haytham */

#endif // TIMER_H
