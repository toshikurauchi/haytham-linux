/*
 * Point.h
 *
 *  Created on: 15/01/2013
 *      Author: Andrew Kurauchi
 */

#ifndef POINT_H
#define POINT_H

#include <opencv2/core.hpp>
#include <QPointF>
#include <iostream>

namespace haytham {

class Point {
public:
    double x;
    double y;

    Point();
    Point(double x, double y);
    Point(cv::Point);
    Point(cv::Point2f);
    Point(QPointF qPoint);
    Point(QPoint qPoint);
    QPointF asQPointF();
    cv::Point asCVPoint();
    cv::Point2f asCVPoint2f();
    const Point operator+(const Point &other) const;
    const Point operator-(const Point &other) const;
    const Point operator/(const double &divisor) const;
    Point operator+=(const Point &other);
    Point operator-=(const Point &other);
    bool operator ==(const Point &other) const;
    bool operator !=(const Point &other) const;
    double &operator [](int idx);
    double norm();
};

std::ostream &operator<<(std::ostream &ostr, const Point &point);

} // namespace haytham

#endif // POINT_H
