#ifndef HAYTHAM_EXCEPTIONS_H
#define HAYTHAM_EXCEPTIONS_H

#include <exception>

namespace haytham {

class IndexOutOfBoundsException : public std::exception
{
public:
    IndexOutOfBoundsException();

    virtual const char* what() const throw();
};

class MethodNotImplementedException : public std::exception
{
public:
    MethodNotImplementedException();

    virtual const char* what() const throw();
};

} // namespace haytham

#endif // HAYTHAM_EXCEPTIONS_H
