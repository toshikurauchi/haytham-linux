/*
 * METState.cpp
 *
 *  Created on: 06/08/2012
 *      Author: Andrew Kurauchi
 */

#include "METState.h"
#include "EyeDetector.h"
#include "Calibration.h"
#include "Server.h"
#include "DefaultConfiguration.h"

namespace haytham {

METState::~METState() {
    delete(this->leftEyeToSceneMapping);
    delete(this->rightEyeToSceneMapping);
    for(std::vector<SceneVisualization*>::iterator it = this->sceneVisualizations.begin(); it != this->sceneVisualizations.end(); it++) {
        delete(*it);
    }
    for(std::map<std::string, std::pair<Calibration*, Calibration*>* >::iterator it = this->calibrationMethods.begin(); it != this->calibrationMethods.end(); it++) {
        delete(it->second->first);
        delete(it->second->second);
        delete(it->second);
    }
}

// TODO pupilAdaptive -> true
// TODO sceneThreshold default value
METState::METState() : recordManager(this),
                       dilateErode(true),
                       pupilThreshold(0),
                       maxPupilScale(0.7),
                       minPupilScale(0.1),
					   showPupil(true),
                       pupilAdaptive(false),
					   removeGlint(false),
					   glintThreshold(100),
                       useBinocular(false),
                       settingLeftEyeROI(false),
                       settingRightEyeROI(false),
                       irisDiameter(10),
                       showIris(false),
                       showGaze(SHOW_GAZE),
                       smoothGaze(SMOOTH_GAZE),
                       leftCalibrationCorrectionX(0),
                       rightCalibrationCorrectionX(0),
                       leftCalibrationCorrectionY(0),
                       rightCalibrationCorrectionY(0),
                       usePupilGlint(true),
                       calibPointDetectThresh(INIT_CALIB_POINTS_THRESH),
                       sceneThreshold(70),
                       processingEye(false),
                       processingScene(false) {
    this->leftEyeToSceneMapping = 0;
    this->rightEyeToSceneMapping = 0;
    this->sceneVisualizations.push_back(new SceneVisualizationPOR);
    this->sceneVisualizations.push_back(new SceneVisualizationPath);
    this->sceneVisualizations.push_back(new SceneVisualizationGaussian);
    this->sceneVisualizations.push_back(new SceneVisualizationGaussianCumulative);
}

METState* METState::current = 0;

METState* METState::getCurrent() {
	if(!current) {
		current = new METState();
	}
	return current;
}

EyeDetector *METState::getEyeDetector() {
    return this->detector;
}

/**
 * \param data The eye data to be added.
 *
 * \brief Adds a new eye data to the buffer.
 */
void METState::addGazeData(BinocularGazeData data) {
    this->shiftGazeData();
    this->gazeData[0] = data;
}

/**
 * \returns The latest eye data acquired.
 */
BinocularGazeData &METState::getLatestGazeData() {
    return this->gazeData[0];
}

/**
 * \param n The number of samples to be returned.
 *
 * \returns The n latest eye data acquired.
 */
std::vector<BinocularGazeData> METState::getLatestGazeData(int n) {
    std::vector<BinocularGazeData> data;

    if(n <= 0) return data;
    if(n > EYE_DATA_ARRAY_LENGTH) n = EYE_DATA_ARRAY_LENGTH;

    for(int i = 0; i < n; i++) {
        data.push_back(this->gazeData[i]);
    }
    return data;
}

/**
 * \param position The position of the required data. Must be between 0 and 500.
 *
 * \returns The eye data at the required position. If position is outside
 * limits the latest eye data is returned.
 */
BinocularGazeData &METState::getGazeData(int position) {
    if(position >= 0 && position < EYE_DATA_ARRAY_LENGTH)
        return this->gazeData[position];
    return this->gazeData[0];
}

/**
 * \returns Eye feature being used (pupil center of pupil-glint vector).
 */
Point METState::getEyeFeature(bool left) {
    if(this->getUsePupilGlint()){
        if(left)
            return this->getLatestGazeData().getLeftData().getPupilGlintVector();
        else
            return this->getLatestGazeData().getRightData().getPupilGlintVector();
    }
    else {
        if(left)
            return this->getLatestGazeData().getLeftData().getPupilCenter();
        else
            return this->getLatestGazeData().getRightData().getPupilCenter();
    }
}

/**
 * \param frames The number of frames to be considered by the median.
 *
 * \returns Pupil center median of the last frames.
 */
Point METState::getEyeFeatureMedian(int frames, bool left) {
    std::vector<float> featureX;
    std::vector<float> featureY;

    if(frames > EYE_DATA_ARRAY_LENGTH) frames = EYE_DATA_ARRAY_LENGTH;
    else if(frames < 0) frames = 0;

    for(int i = 0; i < frames; i++) {
        GazeData data;
        if(left)
            data = this->gazeData[i].getLeftData();
        else
            data = this->gazeData[i].getRightData();
        if(data.isPupilFound()) {
            Point featurePosition;
            if(this->usePupilGlint) {
                featurePosition = data.getPupilGlintVector();
            }
            else {
                featurePosition = data.getPupilCenter();
            }
            featureX.push_back(featurePosition.x);
            featureY.push_back(featurePosition.y);
        }
    }

    ImageProcessing processor;
    float medianX = processor.findMedian(featureX);
    float medianY = processor.findMedian(featureY);

    return Point(medianX, medianY);
}

MonitorDetector *METState::getMonitorDetector() {
    return this->monitorDetector;
}

void METState::setMonitorDetector(MonitorDetector *monitorDetector) {
    this->monitorDetector = monitorDetector;
}

void METState::setActiveMonitor(Monitor monitor) {
    this->activeMonitor = monitor;
}

Monitor METState::getActiveMonitor() {
    return this->activeMonitor;
}

Calibration *METState::getLeftEyeToSceneMapping() {
    return this->leftEyeToSceneMapping;
}

Calibration *METState::getRightEyeToSceneMapping() {
    return this->rightEyeToSceneMapping;
}

Point METState::getCurrentSceneCalibrationPoint() {
    return this->currentSceneCalibrationPoint;
}

void METState::setCurrentSceneCalibrationPoint(Point point) {
    this->currentSceneCalibrationPoint = point;
}

void METState::setEyeToSceneMapping(std::pair<Calibration*, Calibration*> *calibrations) {
    this->rightEyeToSceneMapping = calibrations->first;
    this->leftEyeToSceneMapping = calibrations->second;
}

Timer &METState::getTimer() {
    return this->timer;
}

CameraCalibration &METState::getCameraCalibration() {
    return this->cameraCalibration;
}

bool METState::getUndistortScene() {
    return this->undistortScene;
}

cv::Mat &METState::getEyeImageOriginal() {
	return this->eyeImageOrginal;
}

cv::Mat &METState::getEyeImageForShow() {
	return this->eyeImageForShow;
}

cv::Mat &METState::getSceneImageOriginal() {
    return this->sceneImageOrginal;
}

cv::Mat &METState::getSceneImageForShow() {
    return this->sceneImageForShow;
}

RecordManager &METState::getRecordManager() {
    return this->recordManager;
}

VideoRecorder &METState::getSceneVideoRecorder() {
    return this->recordManager.getSceneVideoRecorder();
}

VideoRecorder &METState::getEyeVideoRecorder() {
    return this->recordManager.getEyeVideoRecorder();
}

GazeDataCsvRecorder &METState::getGazeDataRecorder() {
    return this->recordManager.getGazeDataRecorder();
}

bool METState::getDilateErode() {
	return this->dilateErode;
}

int METState::getPupilThreshold() {
    return this->pupilThreshold;
}

double METState::getMaxPupilScale() {
	return this->maxPupilScale;
}

double METState::getMinPupilScale() {
	return this->minPupilScale;
}

bool METState::getShowPupil() {
	return this->showPupil;
}

bool METState::getPupilAdaptive() {
	return this->pupilAdaptive;
}

bool METState::getRemoveGlint() {
	return this->removeGlint;
}

int METState::getGlintThreshold() {
	return this->glintThreshold;
}

bool METState::getShowGlint() {
    return this->showGlint;
}

int METState::getNumberOfGlints() {
    return this->numberOfGlints;
}

bool METState::getUseBinocular() {
    return this->useBinocular;
}

BinocularROIDetector &METState::getBinocularROIDetector() {
    return this->binocularROIDetector;
}

bool METState::getSettingLeftEyeROI() {
    return this->settingLeftEyeROI;
}

void METState::setSettingLeftEyeROI(bool settingLeftEyeROI) {
    this->settingLeftEyeROI = settingLeftEyeROI;
}

bool METState::getSettingRightEyeROI() {
    return this->settingRightEyeROI;
}

void METState::setSettingRightEyeROI(bool settingRightEyeROI) {
    this->settingRightEyeROI = settingRightEyeROI;
}

int METState::getIrisDiameter() {
	return this->irisDiameter;
}

bool METState::getShowIris() {
    return this->showIris;
}

bool METState::getShowGaze() {
    return this->showGaze;
}

void METState::setShowGaze(bool showGaze) {
    this->showGaze = showGaze;
}

bool METState::getSmoothGaze() {
    return this->smoothGaze;
}

void METState::setSmoothGaze(bool smoothGaze) {
    this->smoothGaze = smoothGaze;
}

int METState::getLeftCalibrationCorrectionX() {
    return this->leftCalibrationCorrectionX;
}

int METState::getRightCalibrationCorrectionX() {
    return this->rightCalibrationCorrectionX;
}

void METState::setLeftCalibrationCorrectionX(int x) {
    this->leftCalibrationCorrectionX = x;
}

void METState::setRightCalibrationCorrectionX(int x) {
    this->rightCalibrationCorrectionX = x;
}

int METState::getLeftCalibrationCorrectionY() {
    return this->leftCalibrationCorrectionY;
}

int METState::getRightCalibrationCorrectionY() {
    return this->rightCalibrationCorrectionY;
}

void METState::setLeftCalibrationCorrectionY(int y) {
    this->leftCalibrationCorrectionY = y;
}

void METState::setRightCalibrationCorrectionY(int y) {
    this->rightCalibrationCorrectionY = y;
}

bool METState::getUsePupilGlint() {
    return this->usePupilGlint;
}

void METState::setUsePupilGlint(bool usePupilGlint) {
    this->usePupilGlint = usePupilGlint;
}

std::map<std::string, std::pair<Calibration*, Calibration*>* > &METState::getCalibrationMethods() {
    return this->calibrationMethods;
}

int METState::getCalibPointDetectThresh() {
    return this->calibPointDetectThresh;
}

void METState::setCalibPointDetectThresh(int calibPointDetectThresh) {
    this->calibPointDetectThresh = calibPointDetectThresh;
}

int METState::getSceneThreshold() {
    return this->sceneThreshold;
}
bool METState::getShowScreenSize() {
    return this->showScreenSize;
}

float METState::getScreenMinSize() {
    return this->screenMinSize;
}

bool METState::getShowScreen() {
    return this->showScreen;
}

bool METState::getShowFilteredSceneImg() {
    return this->showFilteredSceneImg;
}

std::vector<SceneVisualization*> METState::getSelectedSceneVisualizations() {
    std::vector<SceneVisualization*> visualizations;
    for(std::vector<int>::iterator it = this->selectedVisualizationPos.begin(); it != this->selectedVisualizationPos.end(); it++) {
        visualizations.push_back(this->sceneVisualizations.at(*it));
    }
    return visualizations;
}

std::vector<SceneVisualization*> METState::getSceneVisualizations() {
    return this->sceneVisualizations;
}

void METState::setSelectedVisualizations(std::vector<int> selectedPos) {
    this->selectedVisualizationPos = selectedPos;
}

void METState::setProcessingVideo(VideoCategory category) {
    switch(category) {
    case EYE:
        this->processingEye = true;
        break;
    case SCENE:
        this->processingScene = true;
        break;
    default:
        break;
    }
}

void METState::setStoppedProcessingVideo(VideoCategory category) {
    switch(category) {
    case EYE:
        this->processingEye = false;
        break;
    case SCENE:
        this->processingScene = false;
        break;
    default:
        break;
    }
}

bool METState::isProcessingEye() {
    return this->processingEye;
}

bool METState::isProcessingScene() {
    return this->processingScene;
}

void METState::shiftGazeData() {
    for (int i = EYE_DATA_ARRAY_LENGTH - 1; i > 0; i--) {
        gazeData[i] = gazeData[i - 1];
    }
}

/**
  * \brief Adds a calibration method to be available to the user. This function must be called
  * for the calibration method to be listed in the GUI.
  *
  * \returns true if method was successfuly added, false otherwise (there was already a method with the same name).
  */
bool METState::addCalibrationMethod(Calibration *leftCalibration, Calibration *rightCalibration) {
    if(this->calibrationMethods.find(leftCalibration->getMethodName()) != this->calibrationMethods.end()) return false;
    this->calibrationMethods.insert(std::pair<std::string, std::pair<Calibration*, Calibration*>* >(leftCalibration->getMethodName(), new std::pair<Calibration*, Calibration*>(leftCalibration, rightCalibration)));
    return true;
}

} /* namespace haytham */
