#include "MatAlgebra.h"

namespace haytham {

MatAlgebra::MatAlgebra()
{
}

cv::Mat MatAlgebra::cumsum(cv::Mat &mat, bool sumCols) {
    cv::Mat result = cv::Mat::zeros(mat.size(), mat.type());
    int limit;
    if(sumCols) {
        limit = mat.cols;
    }
    else {
        limit = mat.rows;
    }
    for(int i = 0; i < limit; i++) {
        for(int j = i; j < limit; j++) {
            this->addLine(mat, result, i, j, sumCols);
        }
    }
    return result;
}

void MatAlgebra::addLine(cv::Mat &src, cv::Mat &dest, int srcIdx, int destIdx, bool sumCol) {
    if(sumCol) {
        dest.col(destIdx) += src.col(srcIdx);
    }
    else {
        dest.row(destIdx) += src.row(srcIdx);
    }
}

} // namespace haytham
