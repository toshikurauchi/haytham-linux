#ifndef HAYTHAM_GAUSSIANMAT_H
#define HAYTHAM_GAUSSIANMAT_H

#include <opencv2/opencv.hpp>

namespace haytham {

class GaussianMat
{
public:
    GaussianMat();
    cv::Mat getMat(int w, int h, float meanX, float meanY, float sigmaX = 40.0f, float sigmaY = 25.0f);
private:
    cv::Mat xValues;
    cv::Mat yValues;
};

} // namespace haytham

#endif // HAYTHAM_GAUSSIANMAT_H
