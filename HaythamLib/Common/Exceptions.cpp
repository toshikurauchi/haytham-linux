#include "Exceptions.h"

namespace haytham {

IndexOutOfBoundsException::IndexOutOfBoundsException()
{
}

const char* IndexOutOfBoundsException::what() const throw() {
    return "Index out of bounds";
}

MethodNotImplementedException::MethodNotImplementedException()
{
}

const char* MethodNotImplementedException::what() const throw() {
    return "Method not implemented by subclass";
}

} // namespace haytham
