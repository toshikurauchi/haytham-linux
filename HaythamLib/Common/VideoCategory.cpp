/*
 * VideoCategory.cpp
 *
 *  Created on: 02/10/2012
 *      Author: Andrew Kurauchi
 */

#include "VideoCategory.h"

namespace haytham {

Frame::Frame(VideoCategory category, QImage frame) : category(category), frame(frame) {
}

VideoCategory Frame::getCategory() const {
    return this->category;
}

QImage Frame::getFrame() const {
    return this->frame;
}

} /* namespace haytham */
