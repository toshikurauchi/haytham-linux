/*
 * ContourUtil.cpp
 *
 *  Created on: 08/10/2013
 *      Author: Andrew Kurauchi
 */

#include <algorithm>
#include <opencv2/imgproc.hpp>

#include "ContourUtil.h"

namespace haytham {

ContourUtil::ContourUtil()
{
}

bool decArea(std::vector<cv::Point> cont1, std::vector<cv::Point> cont2) {
    double area1 = cv::contourArea(cont1);
    double area2 = cv::contourArea(cont2);
    return area1 > area2;
}

void ContourUtil::sortDecreasingArea(std::vector<std::vector<cv::Point> > &contours) {
    std::sort(contours.begin(), contours.end(), decArea);
}

} // namespace haytham
