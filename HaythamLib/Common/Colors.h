/*
 * Colors.h
 *
 *  Created on: 25/04/2014
 *      Author: Andrew Kurauchi
 */

#ifndef COLORS_H
#define COLORS_H

#include <opencv2/opencv.hpp>

namespace haytham {
    namespace color {
        static const cv::Scalar blue(102, 51, 0);
        static const cv::Scalar orange(0, 153, 255);
        static const cv::Scalar white(255, 255, 255);
        static const cv::Scalar black(0, 0, 0);
        static const cv::Scalar green(84, 112, 9);
        static const cv::Scalar red(0, 0, 255);
        static const cv::Scalar yellowgreen(153, 204, 204);
    } // namespace color
} // namespace haytham

#endif // COLORS_H
