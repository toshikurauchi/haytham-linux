/*
 * METState.h
 *
 *  Created on: 06/08/2012
 *      Author: Andrew Kurauchi
 */

#ifndef METSTATE_H_
#define METSTATE_H_

#include <opencv2/opencv.hpp>
#include <QObject>
#include <map>
#include <vector>

#include "../IO/VideoRecorder.h"
#include "../IO/DataRecorder.h"
#include "../Common/Timer.h"
#include "VideoCategory.h"
#include "../MonitorDetection/MonitorDetector.h"
#include "../MonitorDetection/Monitor.h"
#include "ImageProcessing/SceneVisualization.h"
#include "../Camera/CameraCalibration.h"
#include "../EyeDetection/BinocularROIDetector.h"

namespace haytham {

class Calibration;
class EyeDetector;
class Server;

class METState {
public:
    ~METState();
	static METState* getCurrent();

    EyeDetector *getEyeDetector();
    void addGazeData(BinocularGazeData data);
    BinocularGazeData& getLatestGazeData();
    std::vector<BinocularGazeData> getLatestGazeData(int n);
    BinocularGazeData& getGazeData(int position);
    Point getEyeFeature(bool left);
    Point getEyeFeatureMedian(int frames, bool left);
    MonitorDetector *getMonitorDetector();
    void setMonitorDetector(MonitorDetector *monitorDetector);
    void setActiveMonitor(Monitor monitor);
    Monitor getActiveMonitor();
    Calibration *getLeftEyeToSceneMapping();
    Calibration *getRightEyeToSceneMapping();
    Point getCurrentSceneCalibrationPoint();
    void setCurrentSceneCalibrationPoint(Point point);
    void setEyeToSceneMapping(std::pair<Calibration*, Calibration*> *calibrations);
    Timer &getTimer();
    CameraCalibration &getCameraCalibration();
    bool getUndistortScene();
	cv::Mat &getEyeImageOriginal();
    cv::Mat &getEyeImageForShow();
    cv::Mat &getSceneImageOriginal();
    cv::Mat &getSceneImageForShow();
    RecordManager &getRecordManager();
    VideoRecorder &getSceneVideoRecorder();
    VideoRecorder &getEyeVideoRecorder();
    GazeDataCsvRecorder &getGazeDataRecorder();
    bool isSceneRecording();
    bool isEyeRecording();
	bool getDilateErode();
    int getPupilThreshold();
	double getMinPupilScale();
	double getMaxPupilScale();
	bool getShowPupil();
	bool getPupilAdaptive();
	bool getRemoveGlint();
	int getGlintThreshold();
    bool getShowGlint();
    int getNumberOfGlints();
    bool getUseBinocular();
    BinocularROIDetector &getBinocularROIDetector();
    bool getSettingLeftEyeROI();
    void setSettingLeftEyeROI(bool settingLeftEyeROI);
    bool getSettingRightEyeROI();
    void setSettingRightEyeROI(bool settingRightEyeROI);
	int getIrisDiameter();
    bool getShowIris();
    bool getShowGaze();
    void setShowGaze(bool showGaze);
    bool getSmoothGaze();
    void setSmoothGaze(bool smoothGaze);
    int getLeftCalibrationCorrectionX();
    int getRightCalibrationCorrectionX();
    void setLeftCalibrationCorrectionX(int x);
    void setRightCalibrationCorrectionX(int x);
    int getLeftCalibrationCorrectionY();
    int getRightCalibrationCorrectionY();
    void setLeftCalibrationCorrectionY(int y);
    void setRightCalibrationCorrectionY(int y);
    bool getUsePupilGlint();
    void setUsePupilGlint(bool usePupilGlint);
    std::map<std::string, std::pair<Calibration*, Calibration*>* > &getCalibrationMethods();
    int getCalibPointDetectThresh();
    void setCalibPointDetectThresh(int calibPointDetectThresh);
    int getSceneThreshold();
    bool getShowScreenSize();
    float getScreenMinSize();
    bool getShowScreen();
    bool getShowFilteredSceneImg();
    std::vector<SceneVisualization*> getSelectedSceneVisualizations();
    std::vector<SceneVisualization*> getSceneVisualizations();
    void setSelectedVisualizations(std::vector<int> selectedPos);
    void setProcessingVideo(VideoCategory category);
    void setStoppedProcessingVideo(VideoCategory category);
    bool isProcessingEye();
    bool isProcessingScene();

    void shiftGazeData();
    bool addCalibrationMethod(Calibration *leftCalibration, Calibration *rightCalibration);
private:
	METState();

	static METState* current;
    static const int EYE_DATA_ARRAY_LENGTH = 500;

	//Classes
    EyeDetector *detector;
    BinocularGazeData gazeData[EYE_DATA_ARRAY_LENGTH];
    MonitorDetector *monitorDetector;
	// METCore METCoreObject = new METCore();
	// Eye eye = Eye();
    Monitor activeMonitor;

    Calibration *rightEyeToSceneMapping;
    Calibration *leftEyeToSceneMapping;
    Point currentSceneCalibrationPoint;
    // Calibration SceneToLaser_Mapping = new Calibration();

	// ProcessTime ProcessTimeEyeBranch = new ProcessTime();
	// ProcessTime ProcessTimeSceneBranch = new ProcessTime();
    Timer timer;

    CameraCalibration cameraCalibration;
	// FindCamera Devices;
	// TextFile TextFileDataExport;


	//Cameras
	// VideoCaptureDevice EyeCamera;
	// VideoCaptureDevice SceneCamera;
	// FileVideoSource EyeVideoFile = null;
	// FileVideoSource SceneVideoFile = null;
	// bool syncCameras;
	// AutoResetEvent camera1Acquired = null;
	// bool eye_VFlip;
	// bool scene_VFlip;

	//Scene Camera Calibration
	// int cameraCalibrationSamples = 10;
	// int ChessBoard_W = 9;
	// int ChessBoard_H = 6;

	// int cameraCalibrationSamplesCount = 1000;
    bool undistortScene;
	// bool sceneCameraCalibrating;

	// PointF[] corners;
	// MCvPoint3D32f[][] object_points;
	// PointF[][] image_points;
	// IntrinsicCameraParameters intrinsic_param;
	// ExtrinsicCameraParameters[] extrinsic_param;


	//Images
	/**
	 * Original BGR eye image
	 */
	cv::Mat eyeImageOrginal;
	cv::Mat eyeImageForShow;
	// Image<Gray, Byte> EyeImageTest;
	// Image<Bgr, Byte> EyeImageFlowForShow;


    cv::Mat sceneImageOrginal;
    cv::Mat sceneImageForShow;
	// Image<Bgr, Byte> SceneImageProcessed;//Showing detected objects

	//Recording
    RecordManager recordManager;

	//Pupil
	// Boolean detectPupil;
	bool dilateErode;
	// int MaxPupilDiameter;
	// int MinPupilDiameter;
    int pupilThreshold;

	double maxPupilScale;
	double minPupilScale;
	// bool firstPupilDetection = true;
	// int CounterBeforeDrawingIrisCircle ;
	bool showPupil;
	//      Adaptive Threshold
	bool pupilAdaptive;
	// int PAdaptive_Constant;
	// int PAdaptive_blockSize;
	// Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE PAdaptive_type;
   // public bool PAdaptive_new;


	//Glint
	// Boolean detectGlint;
	bool removeGlint;
	int glintThreshold;
    bool showGlint;
    int numberOfGlints;

	//      Adaptive Threshold
	// bool GAdaptive = true;
	// int GAdaptive_Constant;
	// int GAdaptive_blockSize;
	// Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE GAdaptive_type;

    //Binocular
    bool useBinocular;
    BinocularROIDetector binocularROIDetector;
    bool settingLeftEyeROI;
    bool settingRightEyeROI;

	//Iris
	int irisDiameter;
    bool showIris;

	//Gaze
    bool showGaze;
    bool smoothGaze;
	// Point Gaze;
    int leftCalibrationCorrectionX;
    int rightCalibrationCorrectionX;
    int leftCalibrationCorrectionY;
    int rightCalibrationCorrectionY;

	//calibration
    bool usePupilGlint;
    std::map<std::string, std::pair<Calibration*, Calibration*>* > calibrationMethods;
    int calibPointDetectThresh;

	// Point eyeFeature;

	//Chart
	//          Define some variables
	// bool enablePlot;
	// int numberOfPointsInChart = 200;
	// int numberOfPointsAfterRemoval = 200;



	//Scene
    int sceneThreshold;
    bool showScreenSize;
    float screenMinSize;
	// Boolean SceneDetection ;
    bool showScreen;
    bool showFilteredSceneImg;
    std::vector<SceneVisualization*> sceneVisualizations;
    std::vector<int> selectedVisualizationPos;


	//Server & Client
	// Boolean ControlClientMouse;
	// Boolean clientMouseSmoothing;

	//Others
	// int testslider = 1;
	// Boolean TimerEnable = true;

	// double  sens = 0;

    //Processing
    bool processingEye;
    bool processingScene;

friend class MockMETState;
friend class EyeTab;
friend class SceneTab;
friend class VideoProcessingThread;
};

} /* namespace haytham */
#endif /* METSTATE_H_ */
