#ifndef HAYTHAM_MATALGEBRA_H
#define HAYTHAM_MATALGEBRA_H

#include <opencv2/opencv.hpp>

namespace haytham {

class MatAlgebra
{
public:
    MatAlgebra();
    cv::Mat cumsum(cv::Mat &mat, bool sumCols);
private:
    void addLine(cv::Mat &src, cv::Mat &dest, int srcIdx, int destIdx, bool sumCol);
};

} // namespace haytham

#endif // HAYTHAM_MATALGEBRA_H
