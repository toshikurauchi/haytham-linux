/*
 * Point.cpp
 *
 *  Created on: 15/01/2013
 *      Author: Andrew Kurauchi
 */

#include "Point.h"
#include "Exceptions.h"

namespace haytham {

Point::Point() : x(0), y(0) {
}

Point::Point(double x, double y) : x(x), y(y) {
}

Point::Point(cv::Point cvpoint) : x(cvpoint.x), y(cvpoint.y) {
}

Point::Point(cv::Point2f cvpoint) : x(cvpoint.x), y(cvpoint.y) {
}

Point::Point(QPointF qPoint) : x(qPoint.x()), y(qPoint.y()) {
}

Point::Point(QPoint qPoint) : x(qPoint.x()), y(qPoint.y()) {
}

cv::Point Point::asCVPoint() {
    return cv::Point(this->x, this->y);
}

cv::Point2f Point::asCVPoint2f() {
    return cv::Point2f(this->x, this->y);
}

QPointF Point::asQPointF() {
    return QPointF(this->x, this->y);
}

const Point Point::operator+(const Point &other) const {
    Point result = *this;
    result.x += other.x;
    result.y += other.y;
    return result;
}

const Point Point::operator-(const Point &other) const {
    Point result = *this;
    result.x -= other.x;
    result.y -= other.y;
    return result;
}

const Point Point::operator/(const double &divisor) const {
    Point result = *this;
    result.x /= divisor;
    result.y /= divisor;
    return result;
}

Point Point::operator+=(const Point &other) {
    this->x += other.x;
    this->y += other.y;
    return (*this);
}

Point Point::operator-=(const Point &other) {
    this->x -= other.x;
    this->y -= other.y;
    return (*this);
}

bool Point::operator ==(const Point &other) const {
    return this->x == other.x && this->y == other.y;
}

bool Point::operator !=(const Point &other) const {
    return !(*this == other);
}

double &Point::operator [](int idx) {
    if(idx == 0) {
        return this->x;
    }
    if(idx == 1) {
        return this->y;
    }
    throw IndexOutOfBoundsException();
}

double Point::norm() {
    return sqrt(this->x*this->x+this->y*this->y);
}

std::ostream &operator<<(std::ostream &ostr, const Point &point) {
    ostr << "(" << point.x << "," << point.y << ")";
    return ostr;
}

} // namespace haytham
