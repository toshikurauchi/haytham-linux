/*
 * Blob.cpp
 *
 *  Created on: 01/08/2012
 *      Author: Andrew Kurauchi
 */

#include "Blob.h"
#include <opencv2/imgproc.hpp>

namespace haytham {

/**
 * \brief Creates an empty blob.
 */
Blob::Blob() {
}

/**
 * \param blobPoints Points on the image that compose this blob.
 */
Blob::Blob(std::vector<cv::Point> blobPoints, bool filled) : points(blobPoints) {
    if(filled) {
        this->calculateHull();
        points = this->hull;
        blobPoints = points;
    }
    this->area = cv::contourArea(blobPoints);
    this->boundingBox = cv::boundingRect(blobPoints);
    int width = this->boundingBox.width;
    int height = this->boundingBox.height;
    this->fullness = this->area / (width * height);
    cv::Moments M = cv::moments(blobPoints);
    this->centerOfGravity = cv::Point2f(int(M.m10/M.m00), int(M.m01/M.m00));
}

/**
 * \returns The number of points in the blob's border.
 */
int Blob::size() const {
    return this->points.size();
}

/**
 * \brief Calculates and stores the convex hull for this blob's points.
 */
void Blob::calculateHull() {
    cv::convexHull(this->points, this->hull);
}

/**
 * \returns This blob's bounding box.
 */
cv::Rect const &Blob::getBoundingBox() const {
    return this->boundingBox;
}

/**
 * \returns The points that compose this blobs border.
 */
std::vector<cv::Point>const &Blob::getPoints() const {
    return this->points;
}

/**
 * \returns Blob's convex hull.
 */
std::vector<cv::Point>const &Blob::getHull() {
    if(this->hull.empty()) {
        this->calculateHull();
    }
    return this->hull;
}

/**
 * \returns This blob's fullness.
 *
 * Fullness is defined as follows:
 * \f[
 * 		\frac{\text{Blob area}}{\text{Blob bounding box area}}
 * \f]
 *
 */
double Blob::getFullness() const {
    return this->fullness;
}

/**
 * \returns The blob's center of gravity.
 */
cv::Point2f Blob::getCenterOfGravity() {
    return this->centerOfGravity;
}

/**
 * \returns The blob's area.
 */
double Blob::getArea() const {
    return this->area;
}

/**
 * \returns If true if blob is empty.
 */
bool Blob::empty() {
    return this->points.empty();
}

} /* namespace haytham */
