/*
 * Blob.h
 *
 *  Created on: 01/08/2012
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_BLOB_H
#define HAYTHAM_BLOB_H

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <vector>

namespace haytham {

class Blob {
public:
    Blob();
    Blob(std::vector<cv::Point> blobPoints, bool filled=false);
    int size() const;
    void calculateHull();

    cv::Rect const &getBoundingBox() const;
    std::vector<cv::Point> const &getPoints() const;
    std::vector<cv::Point> const &getHull();
    double getFullness() const;
    cv::Point2f getCenterOfGravity();
    double getArea() const;
    bool empty();
private:
    std::vector<cv::Point> points;
    std::vector<cv::Point> hull;
    double area;
    double fullness;
    cv::Rect boundingBox;
    cv::Point2f centerOfGravity;
};

} // namespace haytham

#endif // HAYTHAM_BLOB_H
