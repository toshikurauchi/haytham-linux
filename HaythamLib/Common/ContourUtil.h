/*
 * ContourUtil.h
 *
 *  Created on: 08/10/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_CONTOURUTIL_H
#define HAYTHAM_CONTOURUTIL_H

#include <vector>
#include <opencv2/opencv.hpp>

namespace haytham {

class ContourUtil
{
public:
    ContourUtil();
    void sortDecreasingArea(std::vector<std::vector<cv::Point> > &contours);
};

} // namespace haytham

#endif // HAYTHAM_CONTOURUTIL_H
