#include <opencv2/opencv.hpp>

#include "GaussianMat.h"

namespace haytham {

GaussianMat::GaussianMat()
{
}

cv::Mat GaussianMat::getMat(int w, int h, float meanX, float meanY, float sigmaX, float sigmaY) {
    cv::Mat mat = cv::Mat(h, w, CV_32FC1);
    cv::Mat xCopy;
    cv::Mat yCopy;

    if(this->xValues.empty() || this->xValues.size().width < w) {
        this->xValues = cv::Mat(1, w, CV_32FC1);
        for(int i = 0; i < w; i++) {
            this->xValues.at<float>(0,i) = i;
        }
    }
    if(this->yValues.empty() || this->yValues.size().height < h) {
        this->yValues = cv::Mat(1, h, CV_32FC1);
        for(int i = 0; i < h; i++) {
            this->yValues.at<float>(0,i) = i;
        }
    }

    this->xValues.copyTo(xCopy);
    this->yValues.copyTo(yCopy);
    xCopy -= meanX;
    yCopy -= meanY;
    cv::multiply(xCopy, xCopy, xCopy);
    cv::multiply(yCopy, yCopy, yCopy);
    xCopy /= 2*sigmaX*sigmaX;
    yCopy /= 2*sigmaY*sigmaY;

    for(int i = 0; i < w; i++) {
        for(int j = 0; j < h; j++) {
            mat.at<float>(j, i) = -(xCopy.at<float>(0,i) + yCopy.at<float>(0,j));
        }
    }
    cv::exp(mat, mat);
    cv::cvtColor(mat, mat, cv::COLOR_GRAY2BGR);

    return mat;
}

} // namespace haytham
