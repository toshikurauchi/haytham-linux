# -------------------------------------------------
# Project created by QtCreator 2012-07-18T15:58:29
# -------------------------------------------------

include(./EyeDetection/eyeDetection.pri)
include(./MonitorDetection/monitorDetection.pri)
include(./Calibration/calibration.pri)
include(./Common/common.pri)
include(./ImageProcessing/imageProcessing.pri)
include(./Network/network.pri)
include(./IO/io.pri)
include(./HeadGesture/headGesture.pri)
include(./Camera/camera.pri)

CONFIG += qt thread staticlib
QT += core \
    gui \
    network
TEMPLATE = lib
DEPENDPATH += .
INCLUDEPATH += /usr/local/include/opencv2 \
    /opt/local/include
macx {
CONFIG += objective_c
}

# TODO Found this at http://stackoverflow.com/questions/17127762/cvmat-to-qimage-and-back test it later
## add open CV
#unix {
#    CONFIG += link_pkgconfig
#    PKGCONFIG += opencv
#}
