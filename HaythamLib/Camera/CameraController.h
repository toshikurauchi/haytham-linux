/*
 * CameraController.h
 *
 *  Created on: 21/09/2012
 *      Author: Andrew Kurauchi
 */

#ifndef CAMERACONTROLLER_H
#define CAMERACONTROLLER_H

#include <string>
#include <vector>
#include <iostream>
#include <QMetaType>

namespace haytham {

class CameraInfo {
public:
    CameraInfo() {}
    CameraInfo(int id, std::string name);
    int getId();
    std::string &getName();
private:
    int id;
    std::string name;

friend std::ostream& operator<<(std::ostream& out, const CameraInfo& cameraInfo);
};

class CameraController
{
public:
    static CameraController *newController();

    virtual void turnLEDOn(int deviceId) = 0;
    virtual void turnLEDOff(int deviceId) = 0;
    std::vector<CameraInfo> getCameraInfoList();
    std::string exec(std::string cmd);
protected:
    /**
     * \brief Acquires camera description list.
     *
     * This function is OS dependant so all subclasses must implement it.
     */
    virtual void acquireCameraInfoList() = 0;
    std::vector<CameraInfo> cameraInfoList;
};

class LinuxCameraController : public CameraController
{
public:
    virtual void turnLEDOn(int deviceId);
    virtual void turnLEDOff(int deviceId);
protected:
    virtual void acquireCameraInfoList();
};

class WindowsCameraController : public CameraController
{
public:
    virtual void turnLEDOn(int deviceId);
    virtual void turnLEDOff(int deviceId);
protected:
    virtual void acquireCameraInfoList();
};

class MacOSCameraController : public CameraController
{
public:
    virtual void turnLEDOn(int deviceId);
    virtual void turnLEDOff(int deviceId);
protected:
    virtual void acquireCameraInfoList();
};

} /* namespace haytham */

Q_DECLARE_METATYPE(haytham::CameraInfo)

#endif // CAMERACONTROLLER_H
