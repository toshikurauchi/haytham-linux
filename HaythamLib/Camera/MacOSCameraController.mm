#import <QTKit/QTKit.h>
#import "UVCCameraControl.h"

#include <QDebug>
#include <string>

#include "CameraController.h"

namespace haytham {

/**
 * \param deviceId Target camera device ID.
 *
 * \brief Turns camera infrared LEDs on.
 */
void MacOSCameraController::turnLEDOn(int deviceId) {

    UVCCameraControl * cameraControl = [[UVCCameraControl alloc]
        initWithVendorID:0x1112 productID:0x28767];

    [cameraControl setAbsoluteFocus:1];

    [cameraControl release];
}

/**
 * \param deviceId Target camera device ID.
 *
 * \brief Turns camera infrared LEDs off.
 */
void MacOSCameraController::turnLEDOff(int deviceId) {}

void MacOSCameraController::acquireCameraInfoList() {
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    NSArray* devices = [QTCaptureDevice inputDevicesWithMediaType: QTMediaTypeVideo];

    for (int i = 0; i < [devices count]; i++) {
        std::string cameraName([[[devices objectAtIndex: i] localizedDisplayName] UTF8String]);
        //std::string cameraId([[[devices objectAtIndex: i] attributeForKey: QTCaptureDeviceInputSourceIdentifierKey] UTF8String]);
        //qDebug() << cameraName.c_str() << " ID: " << cameraId.c_str();

        this->cameraInfoList.push_back(CameraInfo(i, cameraName));
    }

    [pool drain];
}

}
