/*
 * CameraCalibration.h
 *
 *  Created on: 16/09/2013
 *      Author: Andrew Kurauchi
 */


#ifndef HAYTHAM_CAMERACALIBRATION_H
#define HAYTHAM_CAMERACALIBRATION_H

#include <opencv2/opencv.hpp>
#include <string>

namespace haytham {

class CalibrationFile
{
public:
    CalibrationFile();
    CalibrationFile(std::string filename);
    bool save(std::string filename, cv::Mat &cameraMatrix, cv::Mat &distCoeffs, std::vector<cv::Mat> &rvecs, std::vector<cv::Mat> &tvecs, double rms);

    bool isReady();
    cv::Mat getCameraMatrix();
    cv::Mat getDistCoeffs();
    std::vector<cv::Mat> getRvecs();
    std::vector<cv::Mat> getTvecs();
private:
    std::string filename;
    bool ready;
    cv::Mat cameraMatrix;
    cv::Mat distCoeffs;
    std::vector<cv::Mat> rvecs;
    std::vector<cv::Mat> tvecs;
};

class CameraCalibration
{
public:
    CameraCalibration(cv::Size patternSize = cv::Size(10, 7), float squareSize = 0.025);

    bool isCalibrating();
    void startCalibrating(std::string calibrationFilename);
    int addCalibrationImage(const cv::Mat &sceneImg);
    bool loadCalibration(std::string calibrationFilename);
    cv::Mat undistort(const cv::Mat &sceneImg);
    std::vector<cv::Point2f> findCorners(const cv::Mat &img);

    std::string getShortFilename();
    cv::Size getPatternSize();
    int getRemainingFrames();
    cv::Mat &getCameraMatrix();
    cv::Mat &getDistCoeffs();
private:
    void calibrate();

    cv::Size patternSize;
    std::vector<cv::Point3f> realCorners;
    std::vector<std::vector<cv::Point2f> > imagePoints;
    int count; // To store how many calibration images have been stored
    cv::Size imgSize;
    bool calibrated;

    // Camera parameters
    cv::Mat cameraMatrix;
    cv::Mat distCoeffs;
    std::vector<cv::Mat> rvecs;
    std::vector<cv::Mat> tvecs;
    std::string calibrationFilename;
};

} // namespace haytham

#endif // HAYTHAM_CAMERACALIBRATION_H
