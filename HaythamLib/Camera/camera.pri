DEPENDPATH += $$PWD
INCLUDEPATH += $$PWD

HEADERS += $$PWD/*.h
SOURCES += $$PWD/*.cpp

mac {
    OBJECTIVE_SOURCES += $$PWD/*.mm \
        $$PWD/*.m

    LIBS += -framework QTKit \
        -framework Cocoa \
        -framework IOKit
}
