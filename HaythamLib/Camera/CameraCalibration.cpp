/*
 * CameraCalibration.cpp
 *
 *  Created on: 16/09/2013
 *      Author: Andrew Kurauchi
 */

#include <QDebug>
#include <opencv2/opencv.hpp>
#include <opencv2/calib3d.hpp>

#include "CameraCalibration.h"

namespace haytham {

CameraCalibration::CameraCalibration(cv::Size patternSize, float squareSize) : patternSize(patternSize), calibrated(false)
{
    for(int i = 0; i < patternSize.height; ++i) {
        for(int j = 0; j < patternSize.width; ++j) {
            this->realCorners.push_back(cv::Point3f(float(j*squareSize), float(i*squareSize), 0));
        }
    }
    this->count = 0;
}

bool CameraCalibration::isCalibrating() {
    return (this->count > 0);
}

void CameraCalibration::startCalibrating(std::string calibrationFilename) {
    this->count = 15;
    this->calibrated = false;
    this->imagePoints.erase(this->imagePoints.begin(), this->imagePoints.end());
    this->calibrationFilename = calibrationFilename;
    qDebug() << "Started camera calibration";
}

int CameraCalibration::addCalibrationImage(const cv::Mat &sceneImg) {
    std::vector<cv::Point2f> corners = this->findCorners(sceneImg);
    if(corners.empty()) {
        qDebug() << "Couldn't find chessboard";
        return this->count;
    }
    this->imagePoints.push_back(corners);
    this->count--;
    if(this->count == 0) {
        this->imgSize = sceneImg.size();
        this->calibrate();
    }
    return this->count;
}

bool CameraCalibration::loadCalibration(std::string calibrationFilename) {
    CalibrationFile file(calibrationFilename);
    if(file.isReady()) {
        this->cameraMatrix = file.getCameraMatrix();
        this->distCoeffs = file.getDistCoeffs();
        this->rvecs = file.getRvecs();
        this->tvecs = file.getTvecs();
        this->calibrationFilename = calibrationFilename;
        this->calibrated = true;
    }
    return file.isReady();
}

cv::Mat CameraCalibration::undistort(const cv::Mat &sceneImg) {
    if(this->calibrated) {
        cv::Mat undistorted;
        cv::undistort(sceneImg, undistorted, this->cameraMatrix, this->distCoeffs);
        return undistorted;
    }
    return sceneImg;
}

std::vector<cv::Point2f> CameraCalibration::findCorners(const cv::Mat &img) {
    std::vector<cv::Point2f> corners;
    cv::findChessboardCorners(img, this->patternSize, corners);
    return corners;
}

void CameraCalibration::calibrate() {
    if(this->calibrationFilename.empty()) {
        return;
    }

    // Initialize objectPoints and imagePoints
    std::vector<std::vector<cv::Point3f> > objectPoints;
    objectPoints.push_back(realCorners);
    objectPoints.resize(this->imagePoints.size(), realCorners);

    // Initialize other parameters
    this->cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
    // to use CV_CALIB_FIX_ASPECT_RATIO
    this->cameraMatrix.at<double>(0,0) = 1.0;
    this->distCoeffs = cv::Mat::zeros(8, 1, CV_64F);

    this->rvecs.erase(this->rvecs.begin(), this->rvecs.end());
    this->tvecs.erase(this->tvecs.begin(), this->tvecs.end());
    double rms = cv::calibrateCamera(objectPoints, this->imagePoints, this->imgSize, this->cameraMatrix,
                                     this->distCoeffs, this->rvecs, this->tvecs, cv::CALIB_FIX_ASPECT_RATIO);

    CalibrationFile file;
    file.save(this->calibrationFilename, this->cameraMatrix, this->distCoeffs, this->rvecs, this->tvecs, rms);

    qDebug() << "Camera calibrated: rms = " << rms;
    this->calibrated = true;
}

std::string CameraCalibration::getShortFilename() {
    return this->calibrationFilename.substr(this->calibrationFilename.rfind('/') + 1);
}

cv::Size CameraCalibration::getPatternSize() {
    return this->patternSize;
}

int CameraCalibration::getRemainingFrames() {
    return this->count;
}

cv::Mat &CameraCalibration::getCameraMatrix() {
    return this->cameraMatrix;
}

cv::Mat &CameraCalibration::getDistCoeffs() {
    return this->distCoeffs;
}

// ************ CALIBRATION FILE ****************** //

CalibrationFile::CalibrationFile() : ready(false) {
}

CalibrationFile::CalibrationFile(std::string filename) : filename(filename) {
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    this->ready = fs.isOpened();
    if(this->ready) {
        fs["cameraMatrix"] >> this->cameraMatrix;
        fs["distCoeffs"] >> this->distCoeffs;
        fs["rvecs"] >> this->rvecs;
        fs["tvecs"] >> this->tvecs;
    }
}

bool CalibrationFile::save(std::string filename, cv::Mat &cameraMatrix, cv::Mat &distCoeffs, std::vector<cv::Mat> &rvecs, std::vector<cv::Mat> &tvecs, double rms) {
    cv::FileStorage fs(filename, cv::FileStorage::WRITE);
    if(!fs.isOpened()) {
        return false;
    }

    fs << "cameraMatrix" << cameraMatrix;
    fs << "distCoeffs" << distCoeffs;
    fs << "rvecs" << rvecs;
    fs << "tvecs" << tvecs;
    fs << "rms" << rms;

    fs.release();

    this->filename = filename;
    this->cameraMatrix = cameraMatrix;
    this->distCoeffs = distCoeffs;
    this->rvecs = rvecs;
    this->tvecs = tvecs;

    return true;
}

bool CalibrationFile::isReady() {
    return this->ready;
}

cv::Mat CalibrationFile::getCameraMatrix() {
    return this->cameraMatrix;
}

cv::Mat CalibrationFile::getDistCoeffs() {
    return this->distCoeffs;
}

std::vector<cv::Mat> CalibrationFile::getRvecs() {
    return this->rvecs;
}

std::vector<cv::Mat> CalibrationFile::getTvecs() {
    return this->tvecs;
}

} // namespace haytham
