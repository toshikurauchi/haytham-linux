/*
 * CameraController.cpp
 *
 *  Created on: 21/09/2012
 *      Author: Andrew Kurauchi
 */

#include <qglobal.h>
#include <cstdlib>
#include <sstream>
#include <string>
#include <iostream>
#include <cstdio>
#include <boost/regex.hpp>

#include "CameraController.h"

namespace haytham {

/**
 * \brief Constructor.
 *
 * \param id Camera index.
 * \param name Camera name.
 */
CameraInfo::CameraInfo(int id, std::string name) : id(id), name(name) {
}

/**
 * \returns Camera index.
 */
int CameraInfo::getId() {
    return this->id;
}

/**
 * \returns Camera name.
 */
std::string &CameraInfo::getName() {
    return this->name;
}

/**
 * \brief Writes camera description to an output stream.
 */
std::ostream& operator<<(std::ostream& out, const CameraInfo& info) {
        out << "Camera index: " << info.id << " name: " << info.name << std::endl;
        return out;
}

/**
 * \brief Instantiates a new CameraController for the current operating system.
 *
 * \returns The new CameraController.
 *
 * Different CameraControllers will be instantiated for different operating systems.
 */
CameraController *CameraController::newController() {
#ifdef Q_OS_LINUX
    return new LinuxCameraController;
#elif defined Q_OS_WIN
    return new WindowsCameraController;
#elif defined Q_OS_MAC
    return new MacOSCameraController;
#else
    return 0;
#endif
}

/**
 * \returns Camera description list.
 *
 * If camera description list hasn't been acquired from the OS yet acquireCameraInfoList is called.
 */
std::vector<CameraInfo> CameraController::getCameraInfoList() {
    if(this->cameraInfoList.empty()) {
        this->acquireCameraInfoList();
    }
    return this->cameraInfoList;
}

/**
 * \param cmd Command to be executed in terminal.
 *
 * \brief Executes cmd in a terminal.
 */
std::string CameraController::exec(std::string cmd) {
    FILE* pipe = popen(cmd.c_str(), "r");
    if (!pipe) return "ERROR";
    const int bufferSize = 1024;
    char buffer[bufferSize];
    std::string result = "";
    while(!feof(pipe)) {
        if(fgets(buffer, bufferSize, pipe) != NULL)
                result += buffer;
    }
    pclose(pipe);
    return result;
}

/**
 * \param deviceId Target camera device ID.
 *
 * \brief Turns camera infrared LEDs on.
 */
void LinuxCameraController::turnLEDOn(int deviceId) {
    // TODO What to do if uvcdynctrl is not installed or an error occurs?
    // The following command can be used to list available camera controls
    // uvcdynctrl -c -d /dev/video0

    std::stringstream command;
    // This initial whitespace is needed
    command << " uvcdynctrl -s \"Focus (absolute)\" 1 -d /dev/video" << deviceId;
    system(command.str().c_str());
}

/**
 * \param deviceId Target camera device ID.
 *
 * \brief Turns camera infrared LEDs off.
 */
void LinuxCameraController::turnLEDOff(int deviceId) {
    // TODO What to do if uvcdynctrl is not installed or an error occurs?
    std::stringstream command;
    // This initial whitespace is needed
    command << " uvcdynctrl -s \"Focus (absolute)\" 0 -d /dev/video" << deviceId;
    system(command.str().c_str());
}

void LinuxCameraController::acquireCameraInfoList() {
    // TODO What to do if uvcdynctrl is not installed or an error occurs?
    std::string listString = exec("uvcdynctrl -l");
    boost::regex regex("video(\\d)\\s+?(.*?)\n");
    boost::sregex_iterator matchIterator(listString.begin(), listString.end(), regex);
    boost::sregex_iterator end;

    for( ; matchIterator != end; ++matchIterator ) {
        int id;
        std::stringstream convert((*matchIterator)[1].str());
        convert >> id;
        this->cameraInfoList.push_back(CameraInfo(id, (*matchIterator)[2].str()));
    }
}

/**
 * \param deviceId Target camera device ID.
 *
 * \brief Turns camera infrared LEDs on.
 */
void WindowsCameraController::turnLEDOn(int deviceId) {}
/**
 * \param deviceId Target camera device ID.
 *
 * \brief Turns camera infrared LEDs off.
 */
void WindowsCameraController::turnLEDOff(int deviceId) {}
void WindowsCameraController::acquireCameraInfoList() {}


} /* namespace haytham */
