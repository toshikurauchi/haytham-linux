/*
 * DataRecorder.cpp
 *
 *  Created on: 25/10/2012
 *      Author: Andrew Kurauchi
 */

#include <QTextStream>

#include "DataRecorder.h"
#include "METState.h"

namespace haytham {

DataRecorder::DataRecorder() : isRecordingData(false)
{
}

bool DataRecorder::startRecording(QString &filename) {
    this->file = new QFile(filename);
    this->isRecordingData = this->file->open(QIODevice::WriteOnly | QIODevice::Text);
    return this->isRecordingData;
}

void DataRecorder::write(QString &text) {
    if(this->isRecording()) {
        this->write(text.toStdString());
    }
}

void DataRecorder::write(const std::string &text) {
    if(this->isRecording()) {
        this->file->write(text.c_str());
    }
}

bool DataRecorder::isRecording() {
    return this->isRecordingData;
}

void DataRecorder::stopRecording() {
    if(this->isRecording()) {
        /* this line must be executed before closing the
        file so write is not executed inconsistently */
        this->isRecordingData = false;
        this->file->close();
        delete this->file;
    }
}

// ************ GazeDataCsvRecorder *********** //

/**
 * Possible values:
 * - f, timestamp, pupil found, nothing
 * - p, timestamp, pupil center x, pupil center y
 * - s, timestamp, scene gaze x, scene gaze y
 * - m, timestamp, monitor gaze x, monitor gaze y
 * - g, timestamp, horizontal value (-1 for left and 1 for right), vertical value (-1 for down and 1 for up)
 */
const std::string GazeDataCsvRecorder::CSV_HEADER = "type, timestamp, value1, value2\n";

bool GazeDataCsvRecorder::startRecording(QString &filename) {
    bool recording;
    recording = DataRecorder::startRecording(filename);
    if(recording) {
        DataRecorder::write(CSV_HEADER);
    }
    return recording;
}

void GazeDataCsvRecorder::write(BinocularGazeData &data, bool printLeft) {
    if(this->isRecording()) {
        DataRecorder::write(data.asCsvString(printLeft));
    }
}

void GazeDataCsvRecorder::write(HeadGesture &gesture) {
    if(this->isRecording()) {
        DataRecorder::write(gesture.asCsvString());
    }
}

// *********** RecordManager ****************** //

RecordManager::RecordManager(METState *state) : state(state) {
}

bool RecordManager::startRecording(QString eyeVideoFilename, QString sceneVideoFilename, QString gazeDataFilename) {
    if(!eyeVideoFilename.isEmpty() &&
       !this->eyeVideoRecorder.startRecording(eyeVideoFilename, this->state->getEyeImageForShow().size())) {
        this->stopRecording();
        return false;
    }
    if(!sceneVideoFilename.isEmpty() &&
       !this->sceneVideoRecorder.startRecording(sceneVideoFilename, this->state->getSceneImageForShow().size())) {
        this->stopRecording();
        return false;
    }
    if(!gazeDataFilename.isEmpty() &&
       !this->gazeDataRecorder.startRecording(gazeDataFilename)) {
        this->stopRecording();
        return false;
    }

    return true;
}

void RecordManager::stopRecording() {
    this->sceneVideoRecorder.stopRecording();
    this->eyeVideoRecorder.stopRecording();
    this->gazeDataRecorder.stopRecording();
}

VideoRecorder &RecordManager::getSceneVideoRecorder() {
    return this->sceneVideoRecorder;
}

VideoRecorder &RecordManager::getEyeVideoRecorder() {
    return this->eyeVideoRecorder;
}

GazeDataCsvRecorder &RecordManager::getGazeDataRecorder() {
    return this->gazeDataRecorder;
}

} /* namespace haytham */
