/*
 * DataManager.h
 *
 *  Created on: 18/01/2013
 *      Author: Andrew Kurauchi
 */

#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include "Common/METState.h"
#include "EyeDetection/GazeData.h"
#include "HeadGesture/HeadGesture.h"

namespace haytham {

class DataManager {
public:
    DataManager(METState *state);
    void handleGazeData(BinocularGazeData &data);
    bool handleHeadGesture(HeadGesture &gesture);
private:
    METState *state;
    HeadGesture previousGesture;
};

} // namespace haytham

#endif // DATAMANAGER_H
