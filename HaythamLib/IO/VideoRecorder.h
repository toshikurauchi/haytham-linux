/*
 * VideoRecorder.h
 *
 *  Created on: 18/10/2012
 *      Author: Andrew Kurauchi
 */

#ifndef VIDEORECORDER_H
#define VIDEORECORDER_H

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <QString>

namespace haytham {

class VideoRecorder
{
public:
    VideoRecorder();
    ~VideoRecorder();
    bool startRecording(QString &filename, cv::Size frameSize);
    void addFrame(cv::Mat &frame);
    void stopRecording();
    bool isRecording();
private:
    cv::VideoWriter *videoWriter;
    long frameNumber;
    bool videoIsRecording;
};

} /* namespace haytham */

#endif // VIDEORECORDER_H
