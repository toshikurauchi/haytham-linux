/*
 * DataRecorder.h
 *
 *  Created on: 25/10/2012
 *      Author: Andrew Kurauchi
 */

#ifndef DATARECORDER_H
#define DATARECORDER_H

#include <iostream>
#include <QFile>
#include <string>

#include "../EyeDetection/GazeData.h"
#include "../HeadGesture/HeadGesture.h"
#include "../IO/VideoRecorder.h"

namespace haytham {

class METState;

class DataRecorder {
public:
    DataRecorder();
    virtual bool startRecording(QString &filename);
    void write(QString &text);
    void write(const std::string &text);
    bool isRecording();
    void stopRecording();
private:
    QFile *file;
    bool isRecordingData;
};

class GazeDataCsvRecorder : public DataRecorder {
public:
    static const std::string CSV_HEADER;

    virtual bool startRecording(QString &filename);
    void write(BinocularGazeData &data, bool printLeft);
    void write(HeadGesture &gesture);
};

class RecordManager {
public:
    RecordManager(METState *state);
    bool startRecording(QString eyeVideoFilename = QString(), QString sceneVideoFilename = QString(), QString gazeDataFilename = QString());
    void stopRecording();
    VideoRecorder &getSceneVideoRecorder();
    VideoRecorder &getEyeVideoRecorder();
    GazeDataCsvRecorder &getGazeDataRecorder();
private:
    METState *state;
    VideoRecorder sceneVideoRecorder;
    VideoRecorder eyeVideoRecorder;
    GazeDataCsvRecorder gazeDataRecorder;
};

} /* namespace haytham */

#endif // DATARECORDER_H
