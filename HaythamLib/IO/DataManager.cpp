/*
 * DataManager.cpp
 *
 *  Created on: 18/01/2013
 *      Author: Andrew Kurauchi
 */

#include "DataManager.h"
#include "Server.h"

namespace haytham {

// TODO I think we can remove the METState dependency here...
DataManager::DataManager(METState *state) : state(state) {
}

void DataManager::handleGazeData(BinocularGazeData &data) {
    // Record data
    GazeDataCsvRecorder &dataRecorder = this->state->getGazeDataRecorder();
    if(dataRecorder.isRecording()) {
        dataRecorder.write(data, this->state->getUseBinocular());
    }
}

bool DataManager::handleHeadGesture(HeadGesture &gesture) {
    if(gesture != this->previousGesture) {
        GazeDataCsvRecorder &dataRecorder = this->state->getGazeDataRecorder();
        if(dataRecorder.isRecording()) {
            dataRecorder.write(gesture);
        }
        this->previousGesture = gesture;
        return true;
    }
    return false;
}

} // namespace haytham
