/*
 * VideoRecorder.cpp
 *
 *  Created on: 18/10/2012
 *      Author: Andrew Kurauchi
 */

#include <QDebug>
#include <QMutex>
#include <QWaitCondition>

#include "VideoRecorder.h"
#include <opencv2/opencv.hpp>

namespace haytham {

VideoRecorder::VideoRecorder() : videoWriter(0), videoIsRecording(false)
{
}

VideoRecorder::~VideoRecorder() {
    if(this->videoWriter) {
        delete (this->videoWriter);
    }
}

bool VideoRecorder::startRecording(QString &filename, cv::Size frameSize) {
    qDebug() << "Starting video recording for: " << filename;
    try {
        this->videoWriter = new cv::VideoWriter(filename.toStdString(), cv::VideoWriter::fourcc('M','J','P','G'), 30, frameSize);
        this->frameNumber = 0;
        this->videoIsRecording = true;
    }
    catch(cv::Exception exception) {
        return false;
    }
    return true;
}

void VideoRecorder::addFrame(cv::Mat &frame) {
    if(this->videoIsRecording) {
        this->videoWriter->write(frame);
    }
}

void VideoRecorder::stopRecording() {
    if(this->videoIsRecording) {
        /* this line must be executed before releasing the
        videoWriter so addFrame is not executed inconsistently */
        this->videoIsRecording = false;
        // TODO sometimes videoWriter is released while a frame is being written to it (program crashes)
        QMutex mutex;
        mutex.lock();
        QWaitCondition waitCondition;
        waitCondition.wait(&mutex, 500);
        mutex.unlock();
        // TODO fix issue without the above workaround (sleeps for 500ms before releasing the videoWriter)
        this->videoWriter->release();
        delete (this->videoWriter);
        this->videoWriter = 0;
        this->frameNumber++;
    }
}

bool VideoRecorder::isRecording() {
    return this->videoIsRecording;
}

} /* namespace haytham */
