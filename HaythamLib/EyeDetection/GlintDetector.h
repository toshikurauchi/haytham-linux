/*
 * GlintDetector.h
 *
 *  Created on: 08/10/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_GLINTDETECTOR_H
#define HAYTHAM_GLINTDETECTOR_H

#include <vector>

#include "Common/Point.h"

namespace haytham {

class GlintDetector
{
public:
    GlintDetector();
    std::vector<Point> detectGlint(cv::Mat eyeImg, int threshold, int nGlints, Point offset);
};

} // namespace haytham

#endif // HAYTHAM_GLINTDETECTOR_H
