/*
 * BinocularROIDetector.h
 *
 *  Created on: 02/12/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_BINOCULARROIDETECTOR_H
#define HAYTHAM_BINOCULARROIDETECTOR_H

#include <opencv2/opencv.hpp>

namespace haytham {

class BinocularROIDetector
{
public:
    BinocularROIDetector();
    bool leftReady();
    bool rightReady();
    void setLeftROI(cv::Rect leftROI);
    cv::Rect &getLeftROI();
    void setRightROI(cv::Rect rightROI);
    cv::Rect &getRightROI();
    virtual cv::Mat extractLeftROIImage(const cv::Mat &fullImage);
    virtual cv::Mat extractRightROIImage(const cv::Mat &fullImage);
private:
    cv::Rect leftROI;
    cv::Rect rightROI;
    bool leftIsSet;
    bool rightIsSet;

    cv::Rect cropROI(cv::Rect &origROI, cv::Size imgSize);
};

} // namespace haytham

#endif // HAYTHAM_BINOCULARROIDETECTOR_H
