/*
 * EyeDetector.cpp
 *
 *  Created on: 26/07/2012
 *      Author: Andrew Kurauchi
 */

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/photo/photo.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>
#include <algorithm>
#include <cmath>
#include <QDebug>

#include "EyeDetector.h"
#include "EyeBlob.h"
#include "DefaultConfiguration.h"
#include "MatAlgebra.h"

namespace haytham {

// ********************* Eye Detector ***************************** //

EyeDetector::EyeDetector(std::string methodName) : methodName(methodName) {
}

std::string &EyeDetector::getMethodName() {
    return this->methodName;
}

// ********************* Threshold Eye Detector ***************************** //

ThresholdEyeDetector::ThresholdEyeDetector() : EyeDetector("Threshold Eye Detection") {
}

ThresholdEyeDetector::~ThresholdEyeDetector() {
}

/**
 * \param eyeImg Target BGR eye image.
 * \param threshold Threshold to be used for eye image binarization.
 * \param irisDiameter Value in pixels for the diameter of the iris in the image (defining a ROI).
 *
 * \brief Main function for finding a pupil in an eye image.
 *
 * \returns An GazeData with the detected pupil.
 */
GazeData ThresholdEyeDetector::findPupil(cv::Mat const &eyeImg, int threshold, int irisDiameter,
                                         double minPupilScale, double maxPupilScale, int glintThreshold) {
    cv::Rect roi = this->getPupilROI(eyeImg);
    cv::Mat croppedEyeImg;
    eyeImg.copyTo(croppedEyeImg);
    if(roi.width > 0 && roi.height > 0) {
        croppedEyeImg = croppedEyeImg(roi);
    }

    cv::Mat grayImg = this->imageProcessor.bgr2gray(croppedEyeImg);
    std::vector<cv::Rect> glintRegions = this->findGlintRegions(grayImg, glintThreshold);
    this->fillGaps(grayImg, glintRegions);
    GazeData data = detectPupil(grayImg, threshold, eyeImg.size(), roi, irisDiameter,
                                minPupilScale, maxPupilScale, glintRegions);
    data.addOffset(Point(roi.x, roi.y));
    this->lastAcquiredData = data;
    return data;
}

GazeData ThresholdEyeDetector::detectPupil(cv::Mat &grayImg, int threshold, cv::Size fullsize, cv::Rect roi,
                                           int irisDiameter, double minPupilScale, double maxPupilScale,
                                           std::vector<cv::Rect> ignoreRegions) {
    cv::Mat bwImg = this->imageProcessor.filterThreshold(grayImg, threshold, true);

    return this->detectPupilBlob(bwImg, fullsize, roi, irisDiameter, minPupilScale,
                                 maxPupilScale, ignoreRegions);
}

/**
 * \param eyeImg BGR eye image in which ROI will be set
 * \param changeWidthByDiameter If true ROI width will be based on last pupil diameter found,
 * default value will be used otherwise
 *
 * \returns The pupil ROI based on the parameters
 *
 * If the pupil hasn't been found on the previous frame ROI is set to the whole image. If
 * changeWidthByDiameter is true ROI dimensions is based on the last frame's pupil diameter.
 * A default ROI width is used otherwise.
 */
cv::Rect ThresholdEyeDetector::getPupilROI(const cv::Mat &eyeImg) {
    double factor = 2;
    cv::Rect roi;

    if (this->lastAcquiredData.isPupilFound()) {
        Point pupilCenter = this->lastAcquiredData.getPupilCenter();

        int size = static_cast<int>(this->lastAcquiredData.getPupilDiameter() * factor);
        roi = this->imageProcessor.createROIForSize(eyeImg.size(), pupilCenter.x - (size/2),
                                                    pupilCenter.y - (size/2),
                                                    size, size);
    }
    else {
        roi = cv::Rect(0, 0, eyeImg.cols, eyeImg.rows);
    }

    return roi;
}

void ThresholdEyeDetector::fillGaps(cv::Mat &grayImg, std::vector<cv::Rect> glintRegions) {
    cv::Mat ker = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3,3));
    cv::morphologyEx(grayImg, grayImg, cv::MORPH_OPEN, ker, cv::Point(-1, -1), 4);
    cv::Mat inpaintMask(grayImg.size(), CV_8UC1, cv::Scalar::all(0));
    int meanRadius = 0;
    for(std::vector<cv::Rect>::iterator it = glintRegions.begin(); it != glintRegions.end(); it++) {
        if(it->area() < grayImg.size().area() * 0.1) {
            cv::rectangle(inpaintMask, *it, cv::Scalar::all(255), CV_FILLED);
            meanRadius += (it->width + it->height)/4;
        }
    }
    if(glintRegions.size() > 0) {
        meanRadius /= glintRegions.size();
        cv::inpaint(grayImg, inpaintMask, grayImg, meanRadius, cv::INPAINT_NS);
    }
    cv::pyrDown(grayImg, grayImg);
    cv::pyrUp(grayImg, grayImg);
}

std::vector<cv::Rect> ThresholdEyeDetector::findGlintRegions(cv::Mat &grayImg, int threshold) {
    if(threshold == -1) {
        return std::vector<cv::Rect>();
    }
    cv::Mat bwImg = this->imageProcessor.filterThreshold(grayImg, threshold, false);
    // Find maximum area contours
    std::vector<std::vector<cv::Point> > glintContours;
    cv::findContours(bwImg, glintContours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
    std::vector<cv::Rect> glintRegions(glintContours.size());
    for(std::vector<std::vector<cv::Point> >::iterator it = glintContours.begin(); it != glintContours.end(); it++) {
        glintRegions.push_back(cv::boundingRect(*it));
    }
    return glintRegions;
}

/**
 * \param grayImage A filtered gray image (ideally a black and white image)
 * \param fullSizeImgWidth Original image width
 * \param fullSizeImgHeight Original image height
 * \param irisDiameter Value in pixels for the diameter of the pupil so a ROI is defined.
 *
 * \brief Detects pupil blob and fits an ellipse to it
 *
 * \returns The eye data of the detected pupil
 */
GazeData ThresholdEyeDetector::detectPupilBlob(cv::Mat &grayImg, cv::Size fullsize, cv::Rect roi,
                                               int irisDiameter, double minPupilScale, double maxPupilScale,
                                               std::vector<cv::Rect> ignoreRegions) {
    GazeData gazeData;
    EyeBlob pupilBlob(grayImg,
            (int) (minPupilScale * irisDiameter),
            (int) (maxPupilScale * irisDiameter),
            (int) (minPupilScale * irisDiameter),
            (int) (maxPupilScale * irisDiameter), 0.55, 4);
    // Pupil found
    if(pupilBlob.getFilteredBlobs().size() > 0) {
        Point imgCenter(fullsize.width/2-roi.x, fullsize.height/2-roi.y);
        pupilBlob.selectClosestBlobFromPoint(imgCenter);
        Blob *blob = pupilBlob.getSelectedBlob();

        std::vector<cv::Point> filteredPoints = this->filterPoints(blob->getPoints(), ignoreRegions);
        if(!pupilBlob.borderIntersectsROIBorder(roi, fullsize) && filteredPoints.size() >= 5) {
            cv::RotatedRect ellipse = this->imageProcessor.ellipseLeastSquareFitting(filteredPoints);
            gazeData.setPupilCenter(Point(ellipse.center));
            gazeData.setPupilDiameter(std::max(ellipse.size.width, ellipse.size.height));
            gazeData.setPupilEllipse(ellipse);
            gazeData.setPupilFound(true);
        }
    }
    return gazeData;
}

std::vector<cv::Point> ThresholdEyeDetector::filterPoints(const std::vector<cv::Point> &points, std::vector<cv::Rect> &ignoreRegions) {
    std::vector<cv::Point> filteredPoints;
    for(std::vector<cv::Point>::const_iterator it = points.begin(); it != points.end(); it++) {
        bool valid = true;
        for(std::vector<cv::Rect>::iterator itRect = ignoreRegions.begin(); itRect != ignoreRegions.end(); itRect++) {
            if(itRect->contains(*it)) {
                valid = false;
                break;
            }
        }
        if(valid) {
            filteredPoints.push_back(*it);
        }
    }
    return filteredPoints;
}

// ********************* Template Eye Detector ***************************** //

TemplateEyeDetector::TemplateEyeDetector(METState *state) : EyeDetector("Template Eye Detector"), state(state) {
}

GazeData TemplateEyeDetector::findPupil(cv::Mat const &eyeImg, int threshold, int irisDiameter, double minPupilScale, double maxPupilScale, int glintThreshold) {
    ImageProcessing processor;

    // Convert to gray, resize and invert image
    cv::Mat grayImgFull = processor.bgr2gray(eyeImg);
    int fullWidth = grayImgFull.size().width;
    int fullHeight = grayImgFull.size().height;
    int imageWidth = DARKEST_REGION_IMAGE_WIDTH;
    int imageHeight = imageWidth * fullHeight / fullWidth;
    cv::Mat grayImg;
    grayImgFull.copyTo(grayImg);
    cv::resize(grayImg, grayImg, cv::Size(imageWidth, imageHeight));
    grayImg = -grayImg + 255;

    // Creating Structuring Elements for morphology operations
    int fullPupilDiameter = int(maxPupilScale * irisDiameter);
    if(fullPupilDiameter % 2 == 0)
        fullPupilDiameter += 1;
    int pupilDiameter = int(fullPupilDiameter * imageWidth/fullWidth);
    if(pupilDiameter % 2 == 0)
        pupilDiameter += 1;
    // TODO Remove magic number!
    int reflexDiameter = 15;
    cv::Mat pupilSE = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(pupilDiameter, pupilDiameter));
    cv::Mat reflexSE = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(reflexDiameter, reflexDiameter));

    // Clear image glints and noise
    cv::morphologyEx(grayImg, grayImg, cv::MORPH_CLOSE, reflexSE);
    cv::morphologyEx(grayImg, grayImg, cv::MORPH_TOPHAT, pupilSE);

    // Calculate number of slices to consider
    int rowSlices = imageHeight/pupilDiameter;
    if(imageHeight % pupilDiameter != 0) {
        rowSlices += 1;
    }
    Point pupilCenter = this->maxProjection(grayImg, rowSlices, 3, pupilDiameter);
    pupilCenter.x *= fullWidth/imageWidth;
    pupilCenter.y *= fullHeight/imageHeight;
    processor.drawPoint(this->state->getEyeImageForShow(), pupilCenter);

    return this->segmentPupil(grayImgFull, pupilCenter, fullPupilDiameter, DARKEST_REGION_THRESHOLD_VICINITY);
}

// TODO Remove this and clear code
void draw(cv::Mat &values, cv::Mat &dest) {
    for(int i = 0; i < values.cols; i++) {
        dest.at<unsigned char>(int(values.at<float>(0, i)/100), i) = 255; // PROJ
        //dest.at<unsigned char>(int(values.at<float>(0, i)*10+500), i) = 255; // CORRELATION
        //dest.at<unsigned char>(int(values.at<float>(0, i)*100+100), i) = 255; // TEMPLATE
    }
}

Point TemplateEyeDetector::maxProjection(cv::Mat &eyeImg, int rowSlices, int verticalStep, int pupilDiameter) {
    int height = eyeImg.size().height;
    int maxIndex = height;
    cv::Mat projSum;

    MatAlgebra algebra;
    cv::Mat eyeImgFloat;
    eyeImg.convertTo(eyeImgFloat, CV_32F);
    cv::Mat cumsum = algebra.cumsum(eyeImgFloat, false);
    cumsum = algebra.cumsum(cumsum, true);
    int countList = 0;

    // Init return value
    Point retVal;
    float globalMax = 0;

    cv::Mat data;
    cv::Mat templ;
    cv::Mat correlation;
    int dataLength = cumsum.cols + 1 - DARKEST_REGION_MAVG_N;
    templ = this->createCircleTemplate(pupilDiameter, dataLength);
    int templLength = templ.cols;
    correlation.create(1, dataLength - templLength + 1, CV_32F);
    //cv::Mat plot = cv::Mat::zeros(200, maxIndex, CV_8U); // TEMPLATE
    //cv::Mat plot = cv::Mat::zeros(1000, dataLength - templLength + 1, CV_8U); // CORRELATION
    //cv::Mat plot = cv::Mat::zeros(500, maxIndex, CV_8U); // PROJ
    for(int i = 0; i < maxIndex - pupilDiameter; i += verticalStep) {
        int lastI = std::min(maxIndex, i + pupilDiameter) - 1;
        // Calculate windowed projected sum
        if(i == 0) {
            projSum = cumsum.row(lastI);
        }
        else {
            projSum = cumsum.row(lastI) - cumsum.row(i-1);
        }
        projSum /= float(lastI - i + 1);

        // Filter data using moving average
        data = this->movingAverage(projSum, DARKEST_REGION_MAVG_N);
        cv::matchTemplate(data, templ, correlation, cv::TM_CCORR);
        double minLocal, maxLocal;
        int indexMinVec[2], indexMaxVec[2];
        cv::minMaxIdx(correlation, &minLocal, &maxLocal, indexMinVec, indexMaxVec);
        //draw(correlation, plot);
        //draw(projSum, plot);

        // TODO Simplify: while -> if, remove ct, le, countList
        //while(ct < le && (maxLocal > globalMax || countList < 1)) {
        if(maxLocal > globalMax || countList < 1) {
            //ct++;
            int maxCol = indexMaxVec[1] + templLength/2 + DARKEST_REGION_MAVG_N/2;
            globalMax = maxLocal;
            retVal = Point(maxCol, (i + lastI)/2);
            if(countList < 1) {
                countList ++;
            }
            //correlation.at<float>(indexMaxVec[0], indexMaxVec[1]) = 0;
            //cv::minMaxIdx(correlation, &minLocal, &maxLocal, indexMinVec, indexMaxVec);
        }
    }
    //draw(templ, plot);
    //plot.copyTo(this->state->getEyeImageForShow());

    return retVal;
}

cv::Mat TemplateEyeDetector::movingAverage(cv::Mat a, int n) {
    int end = a.size().width;
    cv::Mat retVal = (a.colRange(cv::Range(n-1, end)) - a.colRange(cv::Range(0, end + 1 - n)))/n;
    retVal.convertTo(retVal, CV_32FC1);
    return retVal;
}

cv::Mat TemplateEyeDetector::createCircleTemplate(int pupilDiameter, int maxLength) {
    int offset = int(pupilDiameter*(DARKEST_REGION_PUPIL_IRIS_RATIO - 1) / 2.0);
    int length = pupilDiameter + 2*offset;
    if(length > maxLength) {
        if(pupilDiameter > maxLength) {
            pupilDiameter = maxLength;
        }
        length = maxLength;
        offset = (maxLength - pupilDiameter)/2;
    }
    cv::Mat templ(1, length, CV_32F);
    //templ.setTo(127.0f*pupilDiameter);
    templ.setTo(0);
    float R = (pupilDiameter - 1)/2;
    for(int i = 0; i < pupilDiameter; i++) {
        float x = i - R;
        float sqr = R*R - x*x;
        float y = std::sqrt(sqr)*127;
        templ.at<float>(0, i + offset) += 2*y;
    }
    cv::Scalar mean, std;
    cv::meanStdDev(templ, mean, std);
    templ = (templ - mean[0])/(std[0] * length);
    templ /= cv::norm(templ);
    return templ;
}

GazeData TemplateEyeDetector::segmentPupil(cv::Mat &grayImg, Point pupilCenter, int pupilDiameter, int thresholdVicinity) {
    int x = int(pupilCenter.x);
    int y = int(pupilCenter.y);
    int height = grayImg.size().height;
    int width = grayImg.size().width;

    // ROI limits
    int x0_p = std::max(0, x - thresholdVicinity);
    int x1_p = std::min(width, x + thresholdVicinity);
    int y0_p = std::max(0, y - thresholdVicinity);
    int y1_p = std::min(height, y + thresholdVicinity);

    // Calculate values for threshold in the pupil point vicinity
    cv::Mat pupil(grayImg, cv::Rect(x0_p, y0_p, x1_p - x0_p, y1_p - y0_p));
    cv::Scalar pupilPxMean, pupilPxStd;
    cv::meanStdDev(pupil, pupilPxMean, pupilPxStd);

    // Get pupil ROI
    x0_p = std::max(0, x - pupilDiameter);
    x1_p = std::min(width, x + pupilDiameter);
    y0_p = std::max(0, y - pupilDiameter);
    y1_p = std::min(height, y + pupilDiameter);
    cv::Mat oriPupil(grayImg, cv::Rect(x0_p, y0_p, x1_p - x0_p, y1_p - y0_p));

    // Find candidates
    cv::threshold(oriPupil, pupil, pupilPxMean[0], 255, cv::THRESH_BINARY_INV);
    std::vector<std::vector<cv::Point> > contours;
    cv::findContours(pupil, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_TC89_KCOS);

    // Find contour with maximum area
    double maxArea = 0;
    std::vector<cv::Point> bestCnt;
    for(std::vector<std::vector<cv::Point> >::iterator it = contours.begin(); it != contours.end(); it++) {
        double area = cv::contourArea(*it);
        if(area > maxArea) {
            maxArea = area;
            bestCnt = *it;
        }
    }
    GazeData data;
    if(!bestCnt.empty()) {
        cv::convexHull(bestCnt, bestCnt);
    }
    if(!bestCnt.empty()) {
        for(std::vector<cv::Point>::iterator it = bestCnt.begin(); it != bestCnt.end(); it++) {
            it->x += x0_p;
            it->y += y0_p;
        }
        cv::Moments M = cv::moments(bestCnt);
        Point center(int(M.m10/M.m00) + x0_p, int(M.m01/M.m00) + y0_p);
        data.setPupilCenter(center);

        if(bestCnt.size() >= 5) {
            data.setPupilEllipse(cv::fitEllipse(bestCnt));
        }
        data.setPupilFound(true);
    }
    return data;
}

// ********************* Threshold Eye Detector ***************************** //

ColorEyeDetector::ColorEyeDetector(METState *state) : EyeDetector("Color Eye Detection"), state(state) {
}

GazeData ColorEyeDetector::findPupil(cv::Mat const &eyeImg, int threshold, int irisDiameter, double minPupilScale, double maxPupilScale, int glintThreshold) {
    GazeData data;
    ImageProcessing processor;

    cv::Mat hsvImg = processor.bgr2hsv(eyeImg);
    std::vector<cv::Mat> hsv;
    cv::split(hsvImg, hsv);
    cv::Mat grayImg = hsv[1];

    cv::Mat bwImg = processor.filterThreshold(grayImg, threshold, true);

    int elemSize = int(2*minPupilScale*irisDiameter);
    cv::Mat elem = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(elemSize, elemSize));
    //cv::morphologyEx(bwImg, bwImg, cv::MORPH_OPEN, elem);
    cv::Sobel(bwImg, grayImg, grayImg.depth(), 2, 2, 5);

    grayImg.copyTo(this->state->getEyeImageForShow());

    EyeBlob pupilBlob(bwImg,
            (int) (minPupilScale * irisDiameter),
            (int) (maxPupilScale * irisDiameter),
            (int) (minPupilScale * irisDiameter),
            (int) (maxPupilScale * irisDiameter), 0.25, 4);

    Point pupilBlobCenter(0, 0);
    // Pupil not found
    if (pupilBlob.getFilteredBlobs().size() == 0) {
        data.setPupilFound(false);
    }
    // Pupil found
    else {
        pupilBlob.selectClosestBlobFromPoint(
                    Point(bwImg.size().width / 2,
                          bwImg.size().height / 2));
        Blob *blob = pupilBlob.getSelectedBlob();
        pupilBlobCenter.x = blob->getCenterOfGravity().x;
        pupilBlobCenter.y = blob->getCenterOfGravity().y;

        cv::RotatedRect ellipse = processor.ellipseLeastSquareFitting(blob->getPoints());
        data.setPupilEllipse(ellipse);
        data.setPupilCenter(ellipse.center);
        data.setPupilFound(true);
    }

    return data;
}

// ********************* Starburst Eye Detector ***************************** //

StarburstEyeDetector::StarburstEyeDetector() : EyeDetector("Starburst Eye Detection") {
}

GazeData StarburstEyeDetector::findPupil(const cv::Mat &eyeImg, int threshold, int irisDiameter, double minPupilScale, double maxPupilScale, int glintThreshold) {
    ImageProcessing processor;
    // Convert to gray, resize and invert image
    cv::Mat grayImg = processor.bgr2gray(eyeImg);

    return this->starburst.processImage(grayImg, threshold, 18, 10);
}

}
