/*
 * GazeData.cpp
 *
 *  Created on: 25/10/2012
 *      Author: Andrew Kurauchi
 */

#include <iostream>

#include "GazeData.h"

namespace haytham {

// GazeData

GazeData::GazeData() : pupilFound(false), pupilCenter(-1,-1), pupilDiameter(0), gazePosition(-1, -1), monitorGazePosition(-1,-1) {
}

/**
 * \returns Glint center vector.
 */
const std::vector<Point>& GazeData::getGlintCenters() const {
    return glintCenters;
}

/**
 * \brief Sets current glint centers.
 */
void GazeData::setGlintCenters(const std::vector<Point> glintCenters) {
    this->glintCenters = glintCenters;
}

/**
 * \returns Pupil center.
 */
Point& GazeData::getPupilCenter() {
    return pupilCenter;
}

/**
 * \brief Sets current pupil center.
 */
void GazeData::setPupilCenter(Point pupilCenter) {
    this->pupilCenter = pupilCenter;
}

/**
 * \return Pupil-glint vector
 */
const Point GazeData::getPupilGlintVector() const {
    float sumX = 0;
    float sumY = 0;
    for(std::vector<Point>::const_iterator it = this->glintCenters.begin(); it != this->glintCenters.end(); it++) {
        sumX += it->x;
        sumY += it->y;
    }
    Point glintCenter(sumX/this->glintCenters.size(), sumY/this->glintCenters.size());
    return this->pupilCenter - glintCenter;
}

/**
 * \returns Pupil diameter.
 */
int GazeData::getPupilDiameter() const {
    return pupilDiameter;
}

/**
 * \brief Sets current pupil diameter.
 */
void GazeData::setPupilDiameter(int pupilDiameter) {
    this->pupilDiameter = pupilDiameter;
}

/**
 * \returns Pupil ellipse.
 */
cv::RotatedRect& GazeData::getPupilEllipse() {
    return pupilEllipse;
}

/**
 * \brief Sets current pupil ellipse.
 */
void GazeData::setPupilEllipse(const cv::RotatedRect& pupilEllipse) {
    this->pupilEllipse = pupilEllipse;
    this->measurePupilDiameter();
}

/**
 * \returns true if pupil was found, false otherwise.
 */
bool GazeData::isPupilFound() const {
    return pupilFound;
}

/**
 * \brief Sets if the pupil was found in the current frame.
 */
void GazeData::setPupilFound(bool pupilFound) {
    this->pupilFound = pupilFound;
}

/**
 * \returns Tag.
 */
const std::string& GazeData::getTag() const {
    return tag;
}

/**
 * \brief Sets current tag.
 */
void GazeData::setTag(const std::string& tag) {
    this->tag = tag;
}

/**
 * \returns If pupil-glint vector is being used.
 */
const bool GazeData::getUsePupilGlint() {
    return this->usePupilGlint;
}

/**
 * \brief Sets if pupil-glint vector is being used.
 */
void GazeData::setUsePupilGlint(bool usePupilGlint) {
    this->usePupilGlint = usePupilGlint;
}

/**
 * \returns Eye data timestamp.
 */
double GazeData::getTime() const {
    return time;
}

/**
 * \brief Sets current timestamp.
 */
void GazeData::setTime(double time) {
    this->time = time;
}

void GazeData::setIsLeft(bool left) {
    this->left = left;
    if(left)
        this->side = "l";
    else
        this->side = "r";
}

bool GazeData::getIsLeft() {
    return this->left;
}

const Point& GazeData::getGazePosition() const {
    return this->gazePosition;
}

void GazeData::setGazePosition(const Point gazePosition) {
    this->gazePosition = gazePosition;
}

const Point& GazeData::getMonitorGazePosition() const {
    return this->monitorGazePosition;
}

void GazeData::setMonitorGazePosition(const Point monitorGazePosition) {
    this->monitorGazePosition = monitorGazePosition;
}

void GazeData::addOffset(Point offset) {
    this->pupilCenter += offset;
    this->pupilEllipse.center.x += offset.x;
    this->pupilEllipse.center.y += offset.y;
    for(std::vector<Point>::iterator it = this->glintCenters.begin(); it != this->glintCenters.end(); it++) {
        (*it) += offset;
    }
}

std::string GazeData::asCsvString() {
    std::stringstream csv;
    csv << this->side << "f, " << this->getTime() << ", " << this->isPupilFound() << "," << std::endl;
    csv << this->side << "p, " << this->getTime() << ", " << this->pupilCenter.x << ", " << this->pupilCenter.y << std::endl;
    csv << this->side << "s, " << this->getTime() << ", " << this->gazePosition.x << ", " << this->gazePosition.y << std::endl;
    csv << this->side << "m, " << this->getTime() << ", " << this->monitorGazePosition.x << ", " << this->monitorGazePosition.y << std::endl;
    return csv.str();
}

/**
 * \returns A string containing the following values: "d, timestamp, pupil found, pupil center x, pupil center y, scene gaze x, scene gaze y, monitor gaze x, monitor gaze y"
 * ('d' is the character d for "data" and the other values are numeric).
 */
std::string GazeData::asOneLineCsvString() {
    std::stringstream csv;
    csv << this->side << "d, " << this->getTime() << ", " << this->isPupilFound() << ", ";
    csv << this->pupilCenter.x << ", " << this->pupilCenter.y << ", ";
    csv << this->gazePosition.x << ", " << this->gazePosition.y << ", ";
    csv << this->monitorGazePosition.x << ", " << this->monitorGazePosition.y << std::endl;
    return csv.str();
}

/**
 * \brief Measures pupil ellipse diameter.
 */
void GazeData::measurePupilDiameter() {
    if (this->isPupilFound()) {
        //large axis of ellipse
        this->pupilDiameter = (int) this->getPupilEllipse().size.height;
    }
}

bool GazeData::operator ==(const GazeData &other) const {
    return this->pupilCenter == other.pupilCenter;
}

bool GazeData::operator !=(const GazeData &other) const {
    return !(*this == other);
}

// BinocularGazeData

BinocularGazeData::BinocularGazeData() {
}

void BinocularGazeData::setRightData(GazeData rightData) {
    rightData.setIsLeft(false);
    this->rightData = rightData;
}

GazeData &BinocularGazeData::getRightData() {
    return this->rightData;
}

void BinocularGazeData::setLeftData(GazeData leftData) {
    leftData.setIsLeft(true);
    this->leftData = leftData;
}

GazeData &BinocularGazeData::getLeftData() {
    return this->leftData;
}

GazeData BinocularGazeData::getMeanData() {
    if(!this->rightData.isPupilFound()) {
        return this->leftData;
    }
    if(!this->leftData.isPupilFound()) {
        return this->rightData;
    }
    GazeData retVal;
    retVal.setGazePosition((this->leftData.getGazePosition() + this->rightData.getGazePosition())/2);
    retVal.setGlintCenters(this->getLeftData().getGlintCenters());
    retVal.setMonitorGazePosition((this->leftData.getMonitorGazePosition() + this->rightData.getMonitorGazePosition())/2);
    retVal.setPupilCenter(this->getLeftData().getPupilCenter());
    retVal.setPupilFound(true);
    retVal.setTime(this->getLeftData().getTime());
    retVal.setUsePupilGlint(this->getLeftData().getUsePupilGlint());
    return retVal;
}

/**
 * \returns Eye data timestamp.
 */
double BinocularGazeData::getTime() const {
    return time;
}

/**
 * \brief Sets current timestamp.
 */
void BinocularGazeData::setTime(double time) {
    this->time = time;
    GazeData noData;
    if(this->leftData != noData) {
        this->leftData.setTime(time);
    }
    if(this->rightData != noData) {
        this->rightData.setTime(time);
    }
}

void BinocularGazeData::setUsePupilGlint(bool usePupilGlint) {
    this->getLeftData().setUsePupilGlint(usePupilGlint);
    this->getRightData().setUsePupilGlint(usePupilGlint);
}

const HeadGesture &BinocularGazeData::getGesture() {
    return this->gesture;
}

void BinocularGazeData::setGesture(HeadGesture gesture) {
    this->gesture = gesture;
}

const Point& BinocularGazeData::getGazePosition(bool useBinocularData) const {
    if(useBinocularData) {
        Point leftPosition = this->leftData.getGazePosition();
        Point rightPosition = this->rightData.getGazePosition();
        if(leftPosition != Point(-1, -1) && rightPosition != Point(-1, -1))
            return (leftPosition + rightPosition)/2;
        else if(leftPosition != Point(-1, -1))
            return leftPosition;
        else
            return rightPosition;
    }
    return this->rightData.getGazePosition();
}

std::string BinocularGazeData::asCsvString(bool printLeft) {
    std::stringstream csv;
    csv << this->rightData.asCsvString();
    if(printLeft)
        csv << this->leftData.asCsvString();
    return csv.str();
}

} /* namespace haytham */
