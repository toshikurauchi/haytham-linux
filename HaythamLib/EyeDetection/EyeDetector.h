/*
 * EyeDetector.h
 *
 *  Created on: 26/07/2012
 *      Author: Andrew Kurauchi
 */

#ifndef EYEDETECTOR_H_
#define EYEDETECTOR_H_

#include <opencv2/opencv.hpp>
#include <ctime>
#include <string>
#include <QMetaType>

#include "../ImageProcessing/ImageProcessing.h"
#include "../EyeDetection/EyeBlob.h"
#include "../EyeDetection/GazeData.h"
#include "../Common/Point.h"
#include "Common/METState.h"
#include "Starburst/Starburst.h"

namespace haytham {

class EyeDetector {
public:
    EyeDetector(std::string methodName);
    virtual GazeData findPupil(cv::Mat const &eyeImg, int threshold, int irisDiameter, double minPupilScale, double maxPupilScale, int glintThreshold=-1) = 0;
    std::string &getMethodName();
private:
    std::string methodName;
};

class ThresholdEyeDetector : public EyeDetector {
public:
    ThresholdEyeDetector();
    ~ThresholdEyeDetector();
    GazeData findPupil(cv::Mat const &eyeImg, int threshold, int irisDiameter, double minPupilScale, double maxPupilScale, int glintThreshold);
private:
    GazeData lastAcquiredData;
    ImageProcessing imageProcessor;

    GazeData detectPupil(cv::Mat &grayImg, int threshold, cv::Size fullsize,
                         cv::Rect roi, int irisDiameter, double minPupilScale,
                         double maxPupilScale, std::vector<cv::Rect> ignoreRegions);
    cv::Rect getPupilROI(const cv::Mat &eyeImg);
    void fillGaps(cv::Mat &grayImg, std::vector<cv::Rect> glintRegions);
    std::vector<cv::Rect> findGlintRegions(cv::Mat &grayImg, int threshold);
    GazeData detectPupilBlob(cv::Mat &grayImg, cv::Size fullsize, cv::Rect roi,
                             int irisDiameter, double minPupilScale, double maxPupilScale,
                             std::vector<cv::Rect> ignoreRegions);
    std::vector<cv::Point> filterPoints(const std::vector<cv::Point> &points, std::vector<cv::Rect> &ignoreRegions);
// Used for unit testing
friend class EyeDetectorMock;
};

class TemplateEyeDetector : public EyeDetector {
public:
    TemplateEyeDetector(METState *state);
    virtual GazeData findPupil(cv::Mat const &eyeImg, int threshold, int irisDiameter, double minPupilScale, double maxPupilScale, int glintThreshold);
private:
    Point maxProjection(cv::Mat &eyeImg, int rowSlices, int verticalStep, int pupilDiameter);
    cv::Mat movingAverage(cv::Mat a, int n);
    cv::Mat createCircleTemplate(int pupilDiameter, int maxLength);
    GazeData segmentPupil(cv::Mat &grayImg, Point pupilCenter, int pupilDiameter, int thresholdVicinity);
    METState *state;
};

class ColorEyeDetector : public EyeDetector {
public:
    ColorEyeDetector(METState *state);
    virtual GazeData findPupil(cv::Mat const &eyeImg, int threshold, int irisDiameter, double minPupilScale, double maxPupilScale, int glintThreshold);
private:
    METState *state;
};

class StarburstEyeDetector : public EyeDetector {
public:
    StarburstEyeDetector();
    virtual GazeData findPupil(cv::Mat const &eyeImg, int threshold, int irisDiameter, double minPupilScale, double maxPupilScale, int glintThreshold);
private:
    starburst::Starburst starburst;
};

} /* namespace haytham */

Q_DECLARE_METATYPE(haytham::EyeDetector*)
Q_DECLARE_METATYPE(haytham::ThresholdEyeDetector*)
Q_DECLARE_METATYPE(haytham::TemplateEyeDetector*)

#endif /* EYEDETECTOR_H_ */
