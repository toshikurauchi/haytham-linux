/*
 * BinocularROIDetector.cpp
 *
 *  Created on: 02/12/2013
 *      Author: Andrew Kurauchi
 */

#include "BinocularROIDetector.h"

namespace haytham {

BinocularROIDetector::BinocularROIDetector() :
    leftIsSet(false),
    rightIsSet(false)
{
}

bool BinocularROIDetector::leftReady() {
    return this->leftIsSet;
}

bool BinocularROIDetector::rightReady() {
    return this->rightIsSet;
}

void BinocularROIDetector::setLeftROI(cv::Rect leftROI) {
    this->leftROI = leftROI;
    this->leftIsSet = true;
}

cv::Rect &BinocularROIDetector::getLeftROI() {
    return this->leftROI;
}

void BinocularROIDetector::setRightROI(cv::Rect rightROI) {
    this->rightROI = rightROI;
    this->rightIsSet = true;
}

cv::Rect &BinocularROIDetector::getRightROI() {
    return this->rightROI;
}

cv::Mat BinocularROIDetector::extractLeftROIImage(const cv::Mat &fullImage) {
    cv::Rect roi = this->cropROI(this->leftROI, fullImage.size());
    return fullImage(roi);
}

cv::Mat BinocularROIDetector::extractRightROIImage(const cv::Mat &fullImage) {
    cv::Rect roi = this->cropROI(this->rightROI, fullImage.size());
    return fullImage(roi);
}

cv::Rect BinocularROIDetector::cropROI(cv::Rect &origROI, cv::Size imgSize) {
    int x0 = origROI.x;
    int x1 = origROI.x + origROI.width;
    int y0 = origROI.y;
    int y1 = origROI.y + origROI.height;

    x0 = std::max(std::min(imgSize.width, x0), 0);
    x1 = std::max(std::min(imgSize.width, x1), 0);
    y0 = std::max(std::min(imgSize.height, y0), 0);
    y1 = std::max(std::min(imgSize.height, y1), 0);

    return cv::Rect(x0, y0, x1-x0, y1-y0);
}

} // namespace haytham
