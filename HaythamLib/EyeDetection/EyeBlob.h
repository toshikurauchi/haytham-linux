/*
 * EyeBlob.h
 *
 *  Created on: 01/08/2012
 *      Author: Andrew Kurauchi
 */

#ifndef EYEBLOB_H_
#define EYEBLOB_H_

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <vector>

#include "Common/Point.h"
#include "Common/Blob.h"

namespace haytham {

class EyeBlob {
public:
	EyeBlob(cv::Mat eyeImage, int minWidth, int maxWidth, int minHeight,
			int maxHeight, double minFullness, int maxNumOfFilteredBlobs);
    void selectClosestBlobFromPoint(Point pointOfInterest);
    bool borderIntersectsROIBorder(cv::Rect transferROI, cv::Size fullImageSize);

	std::vector<Blob> const &getFilteredBlobs();
	Blob* getSelectedBlob() const;
private:
	std::vector<Blob> filteredBlobs;
	Blob *selectedBlob;
};

} /* namespace haytham */
#endif /* EYEBLOB_H_ */
