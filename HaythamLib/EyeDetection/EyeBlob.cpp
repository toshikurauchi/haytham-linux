/*
 * EyeBlob.cpp
 *
 *  Created on: 01/08/2012
 *      Author: Andrew Kurauchi
 */

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <vector>
#include <iostream>
#include <limits>

#include "EyeBlob.h"
#include "BlobDetector.h"
#include "ShapeChecker.h"

namespace haytham {

/**
 * \param eyeImage Target image to find eye blob
 * \param minWidth Minimum bounding box width for a blob to be considered
 * an eye blob candidate
 * \param maxWidth Maximum bounding box width for a blob to be considered
 * an eye blob candidate
 * \param minHeight Minimum bounding box height for a blob to be considered
 * an eye blob candidate
 * \param maxHeight Maximum bounding box height for a blob to be considered
 * an eye blob candidate
 * \param minFullness Minimum fullness for a blob to be considered an eye
 * blob candidate
 * \param maxNumOfFilteredBlobs Maximum number of eye blob candidates
 *
 * \brief Looks for circular blobs within the given conditions.
 *
 * Fullness is defined as follows:
 * \f[
 * 		\frac{\text{Blob area}}{\text{Blob bounding box area}}
 * \f]
 *
 * A blob will be considered circular if http://www.aforgenet.com/framework/docs/html/a4c8c190-d752-79bc-7fb6-76f1b3c3d4a7.htm
 TODO
 */
EyeBlob::EyeBlob(cv::Mat eyeImage, int minWidth, int maxWidth, int minHeight, int maxHeight,
		double minFullness, int maxNumOfFilteredBlobs) : selectedBlob(0) {
    BlobDetector detector;
    detector.findBlobs(eyeImage, this->filteredBlobs);
    for(std::vector<Blob>::iterator it = this->filteredBlobs.begin(); it != this->filteredBlobs.end();) {
        cv::Rect boundingBox = it->getBoundingBox();
        if(it->getFullness() <= minFullness ||
           boundingBox.height < minHeight ||
           boundingBox.height > maxHeight ||
           boundingBox.width < minWidth ||
           boundingBox.width > maxWidth) {
            it = this->filteredBlobs.erase(it);
        }
        else {
            it++;
        }
    }
}

/**
 * \param pointOfInterest Target point to have closest blob detected.
 *
 * \brief Selects and stores closest blob from pointOfInterest.
 */
void EyeBlob::selectClosestBlobFromPoint(Point pointOfInterest) {
    cv::Point2f pOI = pointOfInterest.asCVPoint2f();
	this->selectedBlob = 0;

	double minDistance = std::numeric_limits<double>::infinity();
	for(std::vector<Blob>::iterator blob = this->filteredBlobs.begin(); blob != this->filteredBlobs.end(); blob++) {
        float distance = cv::norm((pOI - blob->getCenterOfGravity()));
		if(distance < minDistance) {
			minDistance = distance;
			selectedBlob = &(*blob);
		}
	}
}

// TODO I didn't understand why we need this...
/**
 * \param transferROI Rectangle defining the current region of interest location and size in the current image.
 * \param fullImageSize Size of the full current image.
 *
 * \brief TODO
 *
 * \returns true if the blob border intersects region of interest border, false otherwise.
 */
bool EyeBlob::borderIntersectsROIBorder(cv::Rect transferROI, cv::Size fullImageSize) {
	std::vector<cv::Point> border = this->selectedBlob->getPoints();
	int fromBorders=3;
	for(std::vector<cv::Point>::iterator it = border.begin(); it != border.end(); it++) {
        if(it->x + transferROI.x < fromBorders || it->x + transferROI.x > fullImageSize.width - fromBorders ||
           it->y + transferROI.y < fromBorders || it->y + transferROI.y > fullImageSize.height - fromBorders) {
			return true;
		}
	}
	return false;
}

/**
 * \returns A vector containing the eye blobs detected in the image.
 *
 * Eye blobs are blobs that fit the requirements given to the constructor.
 */
std::vector<Blob> const &EyeBlob::getFilteredBlobs() {
	return this->filteredBlobs;
}

// TODO this is terrible! Think of a better solution that doesn't depend on the programmer remembering to call selectClosestBlobFromPoint before...
/**
 * \returns The selected blob (selectClosestBlobFromPoint must be called before calling this function).
 *
 * Selected blob is the closest blob from some point given as parameter to the selectClosestBlobFromPoint function, so it MUST be called before calling this function.
 */
Blob* EyeBlob::getSelectedBlob() const {
	return this->selectedBlob;
}

} /* namespace haytham */
