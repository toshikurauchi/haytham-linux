/*
 * ShapeChecker.cpp
 *
 *  Created on: 02/08/2012
 *      Author: Andrew Kurauchi
 */

#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <cmath>
#include <algorithm>

#include "ShapeChecker.h"
#include "EyeBlob.h"

namespace haytham {

/**
 * Based on SimpleShapeChecker.isCircle from AForge.NET
 *
 * \param edgePoints Shape's points to check
 *
 * \brief Check if the specified set of points form a circle shape.
 *
 * \returns true if the specified set of points form a circle shape or false otherwise.
 *
 * Circle shape must contain at least 8 points to be recognized. The method returns false always,
 * if the number of points in the specified shape is less than 8.
 */
bool ShapeChecker::isCircle(Blob &blob) {
	// Make sure we have at least 8 points for circle shape
	if (blob.size() < 8) {
		return false;
	}

	cv::Rect boundingBox = blob.getBoundingBox();
	cv::Point center = cv::Point(boundingBox.x + boundingBox.size().width/2,
								boundingBox.y + boundingBox.size().height/2);
	// BoundingBox considers the first row and column without a blob pixel
	double radius = (double)(boundingBox.height-1 + boundingBox.width-1)/4;

	// calculate mean distance between provided edge points and estimated circle’s edge
	float meanDistance = 0;
	std::vector<cv::Point> points = blob.getPoints();
	for(std::vector<cv::Point>::iterator it = points.begin(); it != points.end(); it++) {
		meanDistance += (float) abs(cv::norm(*it - center) - radius);
	}
	meanDistance /= points.size();

	float maxDistance = std::max(minAcceptableDistortion,
			((float) boundingBox.width + boundingBox.height)/2 * relativeDistortionLimit);

	return meanDistance <= maxDistance;
}

} /* namespace haytham */
