/*
 * GazeData.h
 *
 *  Created on: 25/10/2012
 *      Author: Andrew Kurauchi
 */

#ifndef EYEDATA_H
#define EYEDATA_H

#include <string>
#include <vector>

#include "../Common/Point.h"
#include "../HeadGesture/HeadGesture.h"

namespace haytham {

class GazeData {
public:
    GazeData();

    const std::vector<Point>& getGlintCenters() const;
    void setGlintCenters(const std::vector<Point> glintCenter);
    Point &getPupilCenter();
    void setPupilCenter(Point pupilCenter);
    const Point getPupilGlintVector() const;
    int getPupilDiameter() const;
    void setPupilDiameter(int pupilDiameter);
    cv::RotatedRect& getPupilEllipse();
    void setPupilEllipse(const cv::RotatedRect& pupilEllipse);
    bool isPupilFound() const;
    void setPupilFound(bool pupilFound);
    const std::string& getTag() const;
    void setTag(const std::string& tag);
    const bool getUsePupilGlint();
    void setUsePupilGlint(bool usePupilGlint);
    double getTime() const;
    void setTime(double time);
    void setIsLeft(bool left);
    bool getIsLeft();
    const Point& getGazePosition() const;
    void setGazePosition(const Point gazePosition);
    const Point& getMonitorGazePosition() const;
    void setMonitorGazePosition(const Point monitorGazePosition);
    void addOffset(Point offset);
    std::string asCsvString();
    std::string asOneLineCsvString();

    bool operator ==(const GazeData &other) const;
    bool operator !=(const GazeData &other) const;

private:
    void measurePupilDiameter();

    std::string tag;
    double time;
    std::string side;
    bool left;
    bool usePupilGlint;

    // PUPIL
    bool pupilFound;
    Point pupilCenter;
    cv::RotatedRect pupilEllipse;
    int pupilDiameter;

    // GLINT
    std::vector<Point> glintCenters;

    // GAZE ESTIMATION
    Point gazePosition;
    Point monitorGazePosition;
};

class BinocularGazeData {
public:
    BinocularGazeData();

    void setRightData(GazeData rightData);
    GazeData &getRightData();
    void setLeftData(GazeData leftData);
    GazeData &getLeftData();
    GazeData getMeanData();
    double getTime() const;
    void setTime(double time);
    void setUsePupilGlint(bool usePupilGlint);
    const HeadGesture &getGesture();
    void setGesture(HeadGesture gesture);
    const Point& getGazePosition(bool useBinocularData) const;
    std::string asCsvString(bool printLeft);
private:
    GazeData rightData;
    GazeData leftData;

    double time;

    // HEAD GESTURE
    HeadGesture gesture;
};

} /* namespace haytham */

#endif // EYEDATA_H
