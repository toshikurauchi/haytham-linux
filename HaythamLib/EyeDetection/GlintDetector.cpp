/*
 * GlintDetector.cpp
 *
 *  Created on: 08/10/2013
 *      Author: Andrew Kurauchi
 */

#include <algorithm>
#include <opencv2/imgproc.hpp>

#include "GlintDetector.h"
#include "ImageProcessing.h"
#include "ContourUtil.h"

namespace haytham {

GlintDetector::GlintDetector()
{
}

std::vector<Point> GlintDetector::detectGlint(cv::Mat eyeImg, int threshold, int nGlints, Point offset) {
    ImageProcessing processor;
    cv::Mat grayImg = processor.bgr2gray(eyeImg);
    cv::Mat binImg = processor.filterThreshold(grayImg, threshold, false);

    // Find maximum area contours
    std::vector<std::vector<cv::Point> > contours;
    cv::findContours(binImg, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
    ContourUtil sorter;
    sorter.sortDecreasingArea(contours);

    // Return the centers of the largest contours
    std::vector<Point> glints;
    for(int i = 0; i < std::min(nGlints, int(contours.size())); i++) {
        cv::Moments m = cv::moments(contours.at(i));
        if(m.m00 != 0) {
            glints.push_back(Point(m.m10/m.m00, m.m01/m.m00) + offset);
        }
    }
    return glints;
}

} // namespace haytham
