/*
 * ShapeChecker.h
 *
 *  Created on: 02/08/2012
 *      Author: Andrew Kurauchi
 */

#ifndef SHAPECHECKER_H_
#define SHAPECHECKER_H_

#include <vector>
#include <opencv2/opencv.hpp>

namespace haytham {

class Blob;

class ShapeChecker {
public:
	ShapeChecker(float minAcceptableDistortionValue, float relativeDistortionLimitValue) :
		minAcceptableDistortion(minAcceptableDistortionValue),
		relativeDistortionLimit(relativeDistortionLimitValue) {}
	bool isCircle(Blob &blob);
private:
	float minAcceptableDistortion;
	float relativeDistortionLimit;
};

} /* namespace haytham */
#endif /* SHAPECHECKER_H_ */
