#ifndef STARBURST_H
#define STARBURST_H

#include "ransac_ellipse.h"
#include "../../Common/Point.h"
#include "../GazeData.h"

/*
 * Starburst.h
 *
 * Based on the code from:
 * cvEyeTracker - Version 1.2.5
 * Part of the openEyes ToolKit -- http://hcvl.hci.iastate.edu/openEyes
 * Release Date:
 * Authors : Dongheng Li <dhli@iastate.edu>
 *           Derrick Parkhurst <derrick.parkhurst@hcvl.hci.iastate.edu>
 *           Jason Babcock <babcock@nyu.edu>
 *           David Winfield <dwinfiel@iastate.edu>
 * Copyright (c) 2004-2006
 * All Rights Reserved.
 *
 *  Created on: 17/01/2013
 *      Author: Andrew Kurauchi
 */

namespace starburst {

class Starburst {
public:
    Starburst();
    ~Starburst();

    // int edge_threshold = 20;		//threshold of pupil edge points detection
    // int rays = 18;				//number of rays to use to detect feature points
    // int min_feature_candidates = 10;	//minimum number of pupil feature candidates
    haytham::GazeData processImage(cv::Mat &eyeImg, int edge_threshold, int rays, int min_feature_candidates);
private:
    void reduceLineNoise(cv::Mat &inImage);
    void calculateAvgIntensityHori(cv::Mat &inImage);
    void recreateIntensityArrays(cv::Mat &refImage);
    void deleteArrays();

    RansacEllipse ransacEllipse;
    const double beta;           //hysteresis factor for noise reduction
    double *avgIntensityHori;    //horizontal average intensity
    double *intensityFactorHori; //horizontal intensity factor for noise reduction
    int curH;
    haytham::Point startPoint;
    int crWindowSize;		//corneal reflection search window size

    int lostFrameNum;
};

} // namespace starburst

#endif // STARBURST_H
