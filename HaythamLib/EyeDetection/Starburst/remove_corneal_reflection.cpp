/*
 *
 * cvEyeTracker is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * cvEyeTracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cvEyeTracker; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * cvEyeTracker - Version 1.2.5
 * Part of the openEyes ToolKit -- http://hcvl.hci.iastate.edu/openEyes
 * Release Date:
 * Authors : Dongheng Li <dhli@iastate.edu>
 *           Derrick Parkhurst <derrick.parkhurst@hcvl.hci.iastate.edu>
 *           Jason Babcock <babcock@nyu.edu>
 *           David Winfield <dwinfiel@iastate.edu>
 * Copyright (c) 2004-2006
 * All Rights Reserved.
 *
 * Modified by: Andrew Kurauchi
 *
 */

#include <opencv2/imgproc.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "remove_corneal_reflection.h"

namespace starburst {

void remove_corneal_reflection(cv::Mat *image, int sx, int sy, int window_size, int biggest_crr, int &crx, int& cry, int& crr)
{
    int crar = -1;	//corneal reflection approximate radius
    crx = cry = crar = -1;

    float angle_delta = 1*PI/180;
    int angle_num = (int)(2*PI/angle_delta);

    double *angle_array = (double*)malloc(angle_num*sizeof(double));
    double *sin_array = (double*)malloc(angle_num*sizeof(double));
    double *cos_array = (double*)malloc(angle_num*sizeof(double));
    for (int i = 0; i < angle_num; i++) {
        angle_array[i] = i*angle_delta;
        sin_array[i] = sin(angle_array[i]);
        cos_array[i] = cos(angle_array[i]);
    }

    locate_corneal_reflection(image, sx, sy, window_size, (int)(biggest_crr/2.5), crx, cry, crar);
    crr = fit_circle_radius_to_corneal_reflection(image, crx, cry, crar, (int)(biggest_crr/2.5),  sin_array, cos_array, angle_num);
    crr = (int)(2.5*crr);
    interpolate_corneal_reflection(image, crx, cry, crr, sin_array, cos_array, angle_num);

    free(angle_array);
    free(sin_array);
    free(cos_array);
}

void locate_corneal_reflection(cv::Mat *image, int sx, int sy, int window_size, int biggest_crar, int &crx, int &cry, int &crar) {
    if (window_size%2 == 0) {
        printf("Error! window_size should be odd!\n");
    }

    int r = (window_size-1)/2;
    int startx = MAX(sx-r, 0);
    int endx = MIN(sx+r, image->size().width-1);
    int starty = MAX(sy-r, 0);
    int endy = MIN(sy+r, image->size().height-1);
    cv::Mat roiImage = (*image)(cv::Rect(startx, starty, endx-startx+1, endy-starty+1));
    cv::Mat roiThresholdImage;
    roiImage.copyTo(roiThresholdImage);

    double min_value, max_value;
    cv::Point min_loc, max_loc; //location
    cv::minMaxLoc(*image, &min_value, &max_value, &min_loc, &max_loc);

    int threshold;
    std::vector<std::vector<cv::Point> > contours;
    double *scores = (double*)malloc(sizeof(double)*((int)max_value+1));
    memset(scores, 0, sizeof(double)*((int)max_value+1));
    int area, max_area, sum_area;
    for (threshold = (int)max_value; threshold >= 1; threshold--) {
        cv::threshold(roiImage, roiThresholdImage, threshold, 1, cv::THRESH_BINARY);
        cv::findContours(roiThresholdImage, contours, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);
        max_area = 0;
        sum_area = 0;
        std::vector<cv::Point> max_contour;
        for (std::vector<std::vector<cv::Point> >::iterator it = contours.begin(); it != contours.end(); it++) {
            area = it->size() + (int)(fabs(cv::contourArea(*it)));
            sum_area += area;
            if (area > max_area) {
                max_area = area;
                max_contour = *it;
            }
        }
        if (sum_area-max_area > 0) {
            scores[threshold-1] = max_area / (sum_area-max_area);
        }
        else
            continue;

        if (scores[threshold-1] - scores[threshold] < 0) {
            //found the corneal reflection
            crar = (int)sqrt(max_area / PI);
            int sum_x = 0;
            int sum_y = 0;
            for (std::vector<cv::Point>::iterator it = max_contour.begin(); it != max_contour.end(); it++) {
                sum_x += it->x;
                sum_y += it->y;
            }
            crx = sum_x/max_contour.size();
            cry = sum_y/max_contour.size();
            break;
        }
    }
    free(scores);

    if (crar > biggest_crar) {
        printf("(corneal) size wrong! crx:%d, cry:%d, crar:%d (should be less than %d)\n", crx, cry, crar, biggest_crar);
        cry = crx = -1;
        crar = -1;
    }

    if (crx != -1 && cry != -1) {
        printf("(corneal) startx:%d, starty:%d, crx:%d, cry:%d, crar:%d\n", startx, starty, crx, cry, crar);
        crx += startx;
        cry += starty;
    }
}

int fit_circle_radius_to_corneal_reflection(cv::Mat *image, int crx, int cry, int crar, int biggest_crar, double *sin_array, double *cos_array, int array_len) {
    if (crx == -1 || cry == -1 || crar == -1)
        return -1;

    double *ratio = (double*)malloc((biggest_crar-crar+1)*sizeof(double));
    int i, r, r_delta=1;
    int x, y, x2, y2;
    double sum, sum2;
    for (r = crar; r <= biggest_crar; r++) {
        sum = 0;
        sum2 = 0;
        for (i = 0; i < array_len; i++) {
            x = (int)(crx + (r+r_delta)*cos_array[i]);
            y = (int)(cry + (r+r_delta)*sin_array[i]);
            x2 = (int)(crx + (r-r_delta)*cos_array[i]);
            y2 = (int)(cry + (r+r_delta)*sin_array[i]);
            if ((x >= 0 && y >=0 && x < image->size().width && y < image->size().height) &&
                (x2 >= 0 && y2 >=0 && x2 < image->size().width && y2 < image->size().height)) {
                sum += *(image->data + y*image->size().width + x);
                sum2 += *(image->data + y2*image->size().width + x2);
            }
        }
        ratio[r-crar] = sum / sum2;
        if (r - crar >= 2) {
            if (ratio[r-crar-2] < ratio[r-crar-1] && ratio[r-crar] < ratio[r-crar-1]) {
                free(ratio);
                return r-1;
            }
        }
    }
  
    free(ratio);
    printf("ATTN! fit_circle_radius_to_corneal_reflection() do not change the radius\n");
    return crar;
}

void interpolate_corneal_reflection(cv::Mat *image, int crx, int cry, int crr, double *sin_array, double *cos_array,
int array_len) {
    if (crx == -1 || cry == -1 || crr == -1)
        return;

    if (crx-crr < 0 || crx+crr >= image->size().width || cry-crr < 0 || cry+crr >= image->size().height) {
        printf("Error! Corneal reflection is too near the image border\n");
        return;
    }

    int i, r, r2,  x, y;
    UINT8 *perimeter_pixel = (UINT8*)malloc(array_len*sizeof(int));
    int sum=0;
    double avg;
    for (i = 0; i < array_len; i++) {
        x = (int)(crx + crr*cos_array[i]);
        y = (int)(cry + crr*sin_array[i]);
        perimeter_pixel[i] = (UINT8)(*(image->data + y*image->size().width + x));
        sum += perimeter_pixel[i];
    }
    avg = sum*1.0/array_len;

    for (r = 1; r < crr; r++) {
        r2 = crr-r;
        for (i = 0; i < array_len; i++) {
            x = (int)(crx + r*cos_array[i]);
            y = (int)(cry + r*sin_array[i]);
            *(image->data + y*image->size().width + x) = (UINT8)((r2*1.0/crr)*avg + (r*1.0/crr)*perimeter_pixel[i]);
        }
    }
    free(perimeter_pixel);
}

} // namespace starburst
