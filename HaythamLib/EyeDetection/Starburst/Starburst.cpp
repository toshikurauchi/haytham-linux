/*
 *
 * cvEyeTracker is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * cvEyeTracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cvEyeTracker; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 * cvEyeTracker - Version 1.2.5
 * Part of the openEyes ToolKit -- http://hcvl.hci.iastate.edu/openEyes
 * Release Date:
 * Authors : Dongheng Li <dhli@iastate.edu>
 *           Derrick Parkhurst <derrick.parkhurst@hcvl.hci.iastate.edu>
 *           Jason Babcock <babcock@nyu.edu>
 *           David Winfield <dwinfiel@iastate.edu>
 * Copyright (c) 2004-2006
 * All Rights Reserved.
 *
 * Modified by: Andrew Kurauchi
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <linux/videodev.h>
#include <sys/ioctl.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>
#include <libraw1394/raw1394.h>
#include <dc1394/dc1394.h>
#include <opencv2/imgproc.hpp>

#include "remove_corneal_reflection.h"
#include "timing.h"
#include "svd.h"
#include "Starburst.h"

#ifdef _CH_
#pragma package <opencv>
#endif

#ifndef _EiC
#include <opencv2/opencv.hpp>
#include "opencv2/highgui.hpp"
#endif


#define UINT8 unsigned char

#ifndef PI
#define PI 3.141592653589
#endif

#define DEBUG 1

#define INFO(args...) if (DEBUG) printf(args)

#define CLIP(x,l,u) ((x)<(l)?((l)):((x)>(u)?(u):(x)))
#define ISIN(x,l,u) ((x)<(l)?((0)):((x)>(u)?(0):(1)))

#define CALIBRATIONPOINTS    9

FILE *logfile;
#define Log(args...) fprintf(logfile,args);

FILE *ellipse_log;

#define MIN_PUPIL_CONTOUR_POINTS  	500   
#define MAX_PUPIL_CONTOUR_POINTS  	10000    
#define PUPIL_SIZE_TOLERANCE 		1000	//range of allowed pupil diameters
#define MAX_CONTOUR_COUNT		20

namespace starburst {

Starburst::Starburst() : beta(0.2), avgIntensityHori(0), intensityFactorHori(0), curH(0), startPoint(-1, -1), crWindowSize(301), lostFrameNum(0) {
}

Starburst::~Starburst() {
    this->deleteArrays();
}


int view_cal_points = 1;
int do_map2scene = 0;

int number_calibration_points_set = 0;
int ok_calibrate = 0;

CvPoint  calipoints[CALIBRATIONPOINTS];       //conversion from eye to scene calibration points
CvPoint  scenecalipoints[CALIBRATIONPOINTS]; //captured (with mouse) calibration points
CvPoint  pucalipoints[CALIBRATIONPOINTS];   //captured eye points while looking at the calibration points in the scene
CvPoint  crcalipoints[CALIBRATIONPOINTS];    //captured corneal reflection points while looking at the calibration points in the scene
CvPoint  vectors[CALIBRATIONPOINTS];         //differences between the corneal reflection and pupil center

//scene coordinate interpolation variables
float a, b, c, d, e;                            //temporary storage of coefficients
float aa, bb, cc, dd, ee;                       //pupil X coefficients    
float ff, gg, hh, ii, jj;			//pupil Y coefficients 

float centx, centy;                             // translation to center pupil data after biquadratics
float cmx[4], cmy[4];                           // corner correctioncoefficients 
int inx, iny;                                   // translation to center pupil data before biquadratics

int White,Red,Green,Blue,Yellow;
int frame_number=0;

#define FRAMEW 640
#define FRAMEH 480

int monobytesperimage=FRAMEW*FRAMEH;
int yuv411bytesperimage=FRAMEW*FRAMEH*12/8;

//int cameramode[2]={MODE_640x480_MONO,MODE_640x480_YUV411};

double map_matrix[3][3];
int save_image = 0;
int image_no = 0;
int save_ellipse = 0;
int ellipse_no = 0;
char eye_file[30];
char scene_file[30];
char ellipse_file[40];

#define YUV2RGB(y, u, v, r, g, b)\
  r = y + ((v*1436) >> 10);\
  g = y - ((u*352 + v*731) >> 10);\
  b = y + ((u*1814) >> 10);\
  r = r < 0 ? 0 : r;\
  g = g < 0 ? 0 : g;\
  b = b < 0 ? 0 : b;\
  r = r > 255 ? 255 : r;\
  g = g > 255 ? 255 : g;\
  b = b > 255 ? 255 : b

#define FIX_UINT8(x) ( (x)<0 ? 0 : ((x)>255 ? 255:(x)) )


//---------- calibration  coefficient calculation ---------------//
// biquadratic equation fitter               
// x, y are coordinates of eye tracker point 
// X is x or y coordinate of screen point    
// computes a, b, c, d, and e in the biquadratic 
// X = a + b*(x-inx) + c*(y-iny) + d*(x-inx)*(x-inx) + e*(y-iny)*(y-iny) 
// where inx = x1, y1 = y1 to reduce the solution to a 4x4 matrix        

void dqfit( float x1, float y1, 
	    float x2, float y2, 
	    float x3, float y3, 
	    float x4, float y4, 
	    float x5, float y5,
	    float X1, float X2, float X3, float X4, float X5 )
{
 float den;
 float x22,x32,x42,x52;    // squared terms 
 float y22,y32,y42,y52;

 inx = (int)x1;            // record eye tracker centering constants 
 iny = (int)y1;
 a = X1;                    // first coefficient 
 X2 -= X1;  X3 -= X1;       // center screen points 
 X4 -= X1;  X5 -= X1;
 x2 -= x1;  x3 -= x1;       // center eye tracker points 
 x4 -= x1;  x5 -= x1;
 y2 -= y1;  y3 -= y1;  
 y4 -= y1;  y5 -= y1;
 x22 = x2*x2; x32 = x3*x3;   // squared terms of biquadratic 
 x42 = x4*x4; x52 = x5*x5;
 y22 = y2*y2; y32 = y3*y3;
 y42 = y4*y4; y52 = y5*y5;

 //Cramer's rule solution of 4x4 matrix */
 den = -x2*y3*x52*y42-x22*y3*x4*y52+x22*y5*x4*y32-y22*x42*y3*x5-
    x32*y22*x4*y5-x42*x2*y5*y32+x32*x2*y5*y42-y2*x52*x4*y32+
    x52*x2*y4*y32+y22*x52*y3*x4+y2*x42*x5*y32+x22*y3*x5*y42-
    x32*x2*y4*y52-x3*y22*x52*y4+x32*y22*x5*y4-x32*y2*x5*y42+
    x3*y22*x42*y5+x3*y2*x52*y42+x32*y2*x4*y52+x42*x2*y3*y52-
    x3*y2*x42*y52+x3*x22*y4*y52-x22*y4*x5*y32-x3*x22*y5*y42;

 b =  (-y32*y2*x52*X4-X2*y3*x52*y42-x22*y3*X4*y52+x22*y3*y42*X5+
    y32*y2*x42*X5-y22*x42*y3*X5+y22*y3*x52*X4+X2*x42*y3*y52+
    X3*y2*x52*y42-X3*y2*x42*y52-X2*x42*y5*y32+x32*y42*y5*X2+
    X2*x52*y4*y32-x32*y4*X2*y52-x32*y2*y42*X5+x32*y2*X4*y52+
    X4*x22*y5*y32-y42*x22*y5*X3-x22*y4*y32*X5+x22*y4*X3*y52+
    y22*x42*y5*X3+x32*y22*y4*X5-y22*x52*y4*X3-x32*y22*y5*X4)/den;

 c =  (-x32*x4*y22*X5+x32*x5*y22*X4-x32*y42*x5*X2+x32*X2*x4*y52+
    x32*x2*y42*X5-x32*x2*X4*y52-x3*y22*x52*X4+x3*y22*x42*X5+
    x3*x22*X4*y52-x3*X2*x42*y52+x3*X2*x52*y42-x3*x22*y42*X5-
    y22*x42*x5*X3+y22*x52*x4*X3+x22*y42*x5*X3-x22*x4*X3*y52-
    x2*y32*x42*X5+X2*x42*x5*y32+x2*X3*x42*y52+x2*y32*x52*X4+
    x22*x4*y32*X5-x22*X4*x5*y32-X2*x52*x4*y32-x2*X3*x52*y42)/den;

 d = -(-x4*y22*y3*X5+x4*y22*y5*X3-x4*y2*X3*y52+x4*y2*y32*X5-
    x4*y32*y5*X2+x4*y3*X2*y52-x3*y22*y5*X4+x3*y22*y4*X5+
    x3*y2*X4*y52-x3*y2*y42*X5+x3*y42*y5*X2-x3*y4*X2*y52-
    y22*y4*x5*X3+y22*X4*y3*x5-y2*X4*x5*y32+y2*y42*x5*X3+
    x2*y3*y42*X5-y42*y3*x5*X2+X4*x2*y5*y32+y4*X2*x5*y32-
    y42*x2*y5*X3-x2*y4*y32*X5+x2*y4*X3*y52-x2*y3*X4*y52)/den;

 e = -(-x3*y2*x52*X4+x22*y3*x4*X5+x22*y4*x5*X3-x3*x42*y5*X2-
    x42*x2*y3*X5+x42*x2*y5*X3+x42*y3*x5*X2-y2*x42*x5*X3+
    x32*x2*y4*X5-x22*y3*x5*X4+x32*y2*x5*X4-x22*y5*x4*X3+
    x2*y3*x52*X4-x52*x2*y4*X3-x52*y3*x4*X2-x32*y2*x4*X5+
    x3*x22*y5*X4+x3*y2*x42*X5+y2*x52*x4*X3-x32*x5*y4*X2-
    x32*x2*y5*X4+x3*x52*y4*X2+x32*x4*y5*X2-x3*x22*y4*X5)/den;
}


/*void Normalize_Line_Histogram(IplImage *in_image)
{
 unsigned char *s=(unsigned char *)in_image->imageData;
 int x,y;
 int linesum;
 double factor=0;
 int subsample=10;
 double hwidth=(100.0f*(double)width/(double)subsample);
/*
 char adjustment;
 for (y=0;y<height;y++) {
   linesum=0; 
   for (x=0;x<width;x+=subsample) {
     linesum+=*s;
     s+=subsample;
   }
   s-=width;
   adjustment=(char)(128-(double)(linesum)/(double)(width/subsample));
   for (x=0;x<width;x++) {
     *s=MIN(*s+adjustment,255);
     s++;
   }
 }
*/
 /*for (y=0;y<height;y++) {
   linesum=1; 
   for (x=0;x<width;x+=subsample) {
     linesum+=*s;
     s+=subsample;
   }
   s-=width;
   factor=hwidth/((double)linesum);
   for (x=0;x<width;x++) {
     *s=(unsigned char)(((double)*s)*factor);
     s++;
   }
 }
}
*/

void Starburst::calculateAvgIntensityHori(cv::Mat &inImage) {
    UINT8 *pixel = (UINT8*) inImage.data;
    int sum;
    int i, j;
    this->recreateIntensityArrays(inImage);
    for (j = 0; j < inImage.size().height; j++) {
        sum = 0;
        for (i = 0; i < inImage.size().width; i++) {
            sum += *pixel;
            pixel++;
        }
        this->avgIntensityHori[j] = (double)sum/inImage.size().width; // TODO transform in object attribute
    }
}

void Starburst::reduceLineNoise(cv::Mat &inImage) {
    UINT8 *pixel = (UINT8*) inImage.data;
    int i, j;
    double beta2 = 1 - this->beta;
    int adjustment;

    this->recreateIntensityArrays(inImage);
    this->calculateAvgIntensityHori(inImage);
    for (j = 0; j < inImage.size().height; j++) {
        this->intensityFactorHori[j] = this->avgIntensityHori[j]*this->beta + this->intensityFactorHori[j]*beta2;
        adjustment = (int)(this->intensityFactorHori[j] - this->avgIntensityHori[j]);
        for (i = 0; i < inImage.size().width; i++) {
            *pixel =  FIX_UINT8(*pixel+adjustment);
            pixel++;
        }
    }
}

void Starburst::recreateIntensityArrays(cv::Mat &refImage) {
    if(this->curH != refImage.size().height) {
        this->curH = refImage.size().height;
        this->deleteArrays();
        this->avgIntensityHori = new double[this->curH];
        this->intensityFactorHori = new double[this->curH];
    }
}

void Starburst::deleteArrays() {
    if(this->avgIntensityHori != 0) {
        delete[] this->avgIntensityHori;
    }
    if(this->intensityFactorHori != 0) {
        delete[] this->intensityFactorHori;
    }
}

haytham::GazeData Starburst::processImage(cv::Mat &eyeImg, int edge_threshold, int rays, int min_feature_candidates) {
    int *inliers_index;
    cv::Size ellipse_axis;

    // TODO these two lines don't seem to be really relevant
    cv::GaussianBlur(eyeImg, eyeImg, cv::Size(11, 11), 0);
    this->reduceLineNoise(eyeImg);

    // corneal reflection
    cv::Point corneal_reflection(0, 0); //coordinates of corneal reflection in tracker coordinate system
    int corneal_reflection_r = 0;       //the radius of corneal reflection
    remove_corneal_reflection(&eyeImg, (int) this->startPoint.x, (int) this->startPoint.y, this->crWindowSize,
                   (int) eyeImg.size().height/10, corneal_reflection.x, corneal_reflection.y, corneal_reflection_r);
    printf("corneal reflection: (%d, %d)\n", corneal_reflection.x, corneal_reflection.y);

    //starburst pupil contour detection
    this->ransacEllipse.starburst_pupil_contour_detection((UINT8*)eyeImg.data, this->startPoint, eyeImg.size().width, eyeImg.size().height,
                                edge_threshold, rays, min_feature_candidates);
  
    int inliers_num = 0;
    cv::Point pupil(0,0);              //coordinates of pupil in tracker coordinate system
    inliers_index = this->ransacEllipse.pupil_fitting_inliers((UINT8*)eyeImg.data, eyeImg.size().width, eyeImg.size().height, inliers_num);
    ellipse_axis.width = (int)2*this->ransacEllipse.pupil_param[0];
    ellipse_axis.height = (int)2*this->ransacEllipse.pupil_param[1];
    pupil.x = (int)this->ransacEllipse.pupil_param[2];
    pupil.y = (int)this->ransacEllipse.pupil_param[3];
    haytham::GazeData gazeData;

    printf("ellipse a:%lf; b:%lf, cx:%lf, cy:%lf, theta:%lf; inliers_num:%d\n\n",
           this->ransacEllipse.pupil_param[0], this->ransacEllipse.pupil_param[1],
           this->ransacEllipse.pupil_param[2], this->ransacEllipse.pupil_param[3],
           this->ransacEllipse.pupil_param[4], inliers_num);

    free(inliers_index);

    if (ellipse_axis.width > 0 && ellipse_axis.height > 0) {
        this->startPoint.x = pupil.x;
        this->startPoint.y = pupil.y;

        this->lostFrameNum = 0;
        // Set return value
        gazeData.setPupilCenter(haytham::Point(pupil));
        gazeData.setPupilDiameter((ellipse_axis.width + ellipse_axis.height)/2);
        gazeData.setPupilEllipse(cv::RotatedRect(pupil, ellipse_axis, (float)this->ransacEllipse.pupil_param[4]));
        gazeData.setPupilFound(true);
    } else {
        this->lostFrameNum++;
    }
    if (this->lostFrameNum > 5) {
        this->startPoint.x = eyeImg.size().width/2;
        this->startPoint.y = eyeImg.size().height/2;
    }

    printf("Time elapsed: %.3f\n", Time_Elapsed());
    return gazeData;
}
/*
int main( int argc, char** argv )
{
  char c;

  Open_IEEE1394();

  Open_GUI();

  Open_Logfile(argc,argv);

  Start_Timer();

  int i, j;
  double T[3][3], T1[3][3];
  for (j = 0; j < 3; j++) {
    for (i = 0; i < 3; i++) {
      T[j][i] = j*3+i+1;
    }
  }
  T[2][0] = T[2][1] = 0;
  printf("\nT: \n");
  for (j = 0; j < 3; j++) {
    for (i = 0; i < 3; i++) {
      printf("%6.2lf ", T[j][i]);
    }
    printf("\n");
  }
  affine_matrix_inverse(T, T1);
  printf("\nT1: \n");
  for (j = 0; j < 3; j++) {
    for (i = 0; i < 3; i++) {
      printf("%6.2lf ", T1[j][i]);
    }
    printf("\n");
  }
  

  
  
  while ((c=cvWaitKey(50))!='q') {
    if (c == 's') {
      sprintf(eye_file, "eye%05d.bmp", image_no);
      sprintf(scene_file, "scene%05d.bmp", image_no);
      image_no++;
      cvSaveImage(eye_file, eye_image);
      cvSaveImage(scene_file, scene_image);
      printf("thres: %d\n", pupil_edge_thres);
    } else if (c == 'c') {
      save_image = 1 - save_image;
      printf("save_image = %d\n", save_image);
    } else if (c == 'e') {
      save_ellipse = 1 - save_ellipse;
      printf("save_ellipse = %d\n", save_ellipse);
      if (save_ellipse == 1) {
        Open_Ellipse_Log();
      } else {
        fclose(ellipse_log);
      }
    }
    if (start_point.x == -1 && start_point.y == -1)
      Grab_Camera_Frames();
    else 
      process_image(); 
    if (frame_number%1==0) Update_Gui_Windows(); 
  }

  Close_Logfile();

  Close_GUI();

  Close_IEEE1394();

  return 0;
}

#ifdef _EiC
main(1,"cvEyeTracker.c");
#endif
*/
} // namespace starburst
