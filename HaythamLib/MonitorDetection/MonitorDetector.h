/*
 * MonitorDetector.h
 *
 *  Created on: 17/01/2013
 *      Author: Andrew Kurauchi
 */

#ifndef MONITORDETECTOR_H
#define MONITORDETECTOR_H

#include <opencv2/opencv.hpp>
#include <QMetaType>

#include "../ImageProcessing/ImageProcessing.h"
#include "../Common/Point.h"

namespace haytham {

class MonitorDetector {
public:
    MonitorDetector();
    MonitorDetector(std::string methodName);
    virtual bool detectScreen(const cv::Mat &sceneImage, int threshold, float screenMinSize);
    void drawScreen(const cv::Mat &sceneImage);
    std::vector<Point> &getScreenCorners();

    cv::Mat &getFilteredGrayImg();
    std::string &getMethodName();
protected:
    void sortScreenCorners();
    double calcMinArea(double screenMinSize, cv::Mat &img);

    ImageProcessing imageProcessor;
    std::vector<Point> screenCorners;
    cv::Mat filteredGrayImg;
private:
    std::string methodName;
};

class ContrastMonitorDetector : public MonitorDetector {
public:
    ContrastMonitorDetector();
    virtual bool detectScreen(const cv::Mat &sceneImage, int threshold, float screenMinSize);
private:
    cv::Mat applyCanny(const cv::Mat &sceneImage, int threshold);
    bool findCorners(cv::Mat cannyEdgesImg, float screenMinSize);
};

class LedMonitorDetector : public MonitorDetector {
public:
    LedMonitorDetector();
    virtual bool detectScreen(const cv::Mat &sceneImage, int threshold, float screenMinSize);
};

class ColorMonitorDetector : public MonitorDetector {
public:
    ColorMonitorDetector();
    virtual bool detectScreen(const cv::Mat &sceneImage, int threshold, float screenMinSize);
    bool findCorners(cv::Mat bwImg, float screenMinSize);
};

} // namespace haytham

Q_DECLARE_METATYPE(haytham::MonitorDetector*)
Q_DECLARE_METATYPE(haytham::ContrastMonitorDetector*)
Q_DECLARE_METATYPE(haytham::LedMonitorDetector*)

#endif // MONITORDETECTOR_H
