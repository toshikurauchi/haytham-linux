/*
 * Monitor.h
 *
 *  Created on: 21/01/2013
 *      Author: Andrew Kurauchi
 */

#ifndef MONITOR_H
#define MONITOR_H

#include <vector>

#include "../Common/Point.h"
#include "../Calibration/Calibration.h"

namespace haytham {

class Monitor {
public:
    Monitor();
    Monitor(int width, int height);
    Point map(Point scenePoint, std::vector<Point> screenCorners);
private:
    HomographyCalibration calibration;
    bool active;
    int width;
    int height;
};

} // namespace haytham

#endif // MONITOR_H
