/*
 * Monitor.cpp
 *
 *  Created on: 21/01/2013
 *      Author: Andrew Kurauchi
 */

#include "Monitor.h"
#include "Calibration.h"
#include "GazeData.h"
#include "EyeDetector.h"
#include "MonitorDetector.h"

namespace haytham {

Monitor::Monitor() : calibration(4), active(true), width(1), height(1) {
}

Monitor::Monitor(int width, int height) :
    calibration(4), active(true), width(width), height(height) {
}

Point Monitor::map(Point scenePoint, std::vector<Point> screenCorners) {
    if(this->active) {
        // Calibration
        this->calibration.startCalibrating();
        this->calibration.addPoint(screenCorners[0], Point(0, 0));
        this->calibration.addPoint(screenCorners[1], Point(this->width, 0));
        this->calibration.addPoint(screenCorners[2], Point(this->width, this->height));
        this->calibration.addPoint(screenCorners[3], Point(0, this->height));
        this->calibration.calibrate();
        // Mapping
        return this->calibration.map(scenePoint);
    }
    return Point(-1, -1);
}

} // namespace haytham
