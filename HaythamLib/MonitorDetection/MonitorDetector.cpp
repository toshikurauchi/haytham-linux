/*
 * MonitorDetector.cpp
 *
 * Some code has been based on squares.cpp OpenCV example.
 *
 *  Created on: 17/01/2013
 *      Author: Andrew Kurauchi
 */

#include <opencv2/opencv.hpp>
#include <vector>
#include <algorithm>
#include <QDebug>

#include "MonitorDetector.h"
#include "Exceptions.h"
#include "ContourUtil.h"

namespace haytham {

MonitorDetector::MonitorDetector() : methodName("N/A"), screenCorners(4) {
}

MonitorDetector::MonitorDetector(std::string methodName) : methodName(methodName), screenCorners(4) {
}

/**
 * TODO
 *
 * \param sceneImage BGR scene image
 */
bool MonitorDetector::detectScreen(const cv::Mat &sceneImage, int threshold, float screenMinSize) {
    throw MethodNotImplementedException();
    return false;
}

/**
 * TODO
 *
 * \param sceneImage BGR scene image
 */
void MonitorDetector::drawScreen(const cv::Mat &sceneImage) {
    this->imageProcessor.drawRectangle(sceneImage, this->screenCorners, 0, true, "Screen");
}

cv::Mat &MonitorDetector::getFilteredGrayImg() {
    return this->filteredGrayImg;
}

std::string &MonitorDetector::getMethodName() {
    return this->methodName;
}

void MonitorDetector::sortScreenCorners() {
    float meanX = 0;
    float meanY = 0;
    Point aux[4];

    for(int i = 0; i < 4; i++) {
        meanX += this->screenCorners[i].x;
        meanY += this->screenCorners[i].y;
    }
    meanX /= 4;
    meanY /= 4;

    // 1  2
    // 4  3
    for(int i = 0; i < 4; i++) {
        Point currentPoint = this->screenCorners[i];
        if(currentPoint.x < meanX && currentPoint.y < meanY) aux[0] = currentPoint;
        if(currentPoint.x > meanX && currentPoint.y < meanY) aux[1] = currentPoint;
        if(currentPoint.x > meanX && currentPoint.y > meanY) aux[2] = currentPoint;
        if(currentPoint.x < meanX && currentPoint.y > meanY) aux[3] = currentPoint;
    }
    for(int i = 0; i < 4; i++) {
        this->screenCorners[i] = aux[i];
    }
}

std::vector<Point> &MonitorDetector::getScreenCorners() {
    return this->screenCorners;
}

double MonitorDetector::calcMinArea(double screenMinSize, cv::Mat &img) {
    return screenMinSize * img.rows * screenMinSize * img.cols;
}

// ************* ContrastMonitorDetector ****************** //

ContrastMonitorDetector::ContrastMonitorDetector() : MonitorDetector("Contrast Monitor Detection") {
}

bool ContrastMonitorDetector::detectScreen(const cv::Mat &sceneImage, int threshold, float screenMinSize) {
    this->filteredGrayImg = this->applyCanny(sceneImage, threshold);
    return this->findCorners(this->filteredGrayImg, screenMinSize);
}

cv::Mat ContrastMonitorDetector::applyCanny(const cv::Mat &sceneImage, int threshold) {
    cv::Size size = cv::Size(sceneImage.cols / 2,
            sceneImage.rows / 2);
    cv::Mat pyrDownImage(size, sceneImage.depth(),
                         sceneImage.channels());
    cv::Mat grayImage = this->imageProcessor.bgr2gray(sceneImage);

    cv::pyrDown(grayImage, pyrDownImage);
    cv::pyrUp(pyrDownImage, grayImage);
    cv::Canny(grayImage, grayImage, 0, threshold, 5);
    cv::dilate(grayImage, grayImage, cv::Mat());

    return grayImage;
}

bool ContrastMonitorDetector::findCorners(cv::Mat cannyEdgesImg, float screenMinSize) {
    cv::Mat img;
    cannyEdgesImg.copyTo(img);
    bool rectangleFound = false;

    double minArea = this->calcMinArea(screenMinSize, img);

    std::vector<std::vector<cv::Point> > contours;
    // If this is slow take a look at startFindContours and findNextContour
    cv::findContours(img, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
    for(std::vector<std::vector<cv::Point> >::iterator it = contours.begin(); it != contours.end(); it++) {
        std::vector<cv::Point> polygon;
        //0.08 has been tested and works well with applyCanny function (comment from original code)
        cv::approxPolyDP(*it, polygon, cv::arcLength(cv::Mat(*it), true) * 0.08, true);

        if(polygon.size() == 4) {
            // We do this here so the area is only calculated once and only when polygon has 4 points
            double area = cv::contourArea(polygon);
            if(area > minArea && cv::isContourConvex(polygon)) {
                rectangleFound = true;
                minArea = area;
                int i = 0;
                for(std::vector<cv::Point>::iterator it1 = polygon.begin(); it1 != polygon.end(); it1++) {
                    this->screenCorners[i].x = it1->x;
                    this->screenCorners[i++].y = it1->y;
                }
            }
        }
    }
    if(rectangleFound)
        this->sortScreenCorners();

    return rectangleFound;
}

// ************* LedMonitorDetector ****************** //

LedMonitorDetector::LedMonitorDetector() : MonitorDetector("LED Monitor Detection") {
}

bool LedMonitorDetector::detectScreen(const cv::Mat &sceneImage, int threshold, float screenMinSize) {
    this->filteredGrayImg = this->imageProcessor.bgr2gray(sceneImage);
    cv::threshold(this->filteredGrayImg, this->filteredGrayImg, threshold, 255, cv::THRESH_BINARY);
    cv::Mat binImg;
    this->filteredGrayImg.copyTo(binImg);
    this->filteredGrayImg = this->imageProcessor.gray2bgr(this->filteredGrayImg);

    std::vector<std::vector<cv::Point> > contours;
    cv::findContours(binImg, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    // All 4 corners must be detected
    if(contours.size() < 4) {
        return false;
    }

    // Sort contours by decreasing area
    ContourUtil sorter;
    sorter.sortDecreasingArea(contours);

    // The screen corners are the mass centers of the 4 largest contours
    for(int i = 0; i < 4; i++) {
        cv::Moments moments = cv::moments(contours.at(i));
        this->screenCorners[i].x = moments.m10/moments.m00;
        this->screenCorners[i].y = moments.m01/moments.m00;
    }
    this->sortScreenCorners();

    // Corner candidates to calculate area
    std::vector<cv::Point> cornerCandidates;
    for(int i = 0; i < 4; i++) {
        cornerCandidates.push_back(cv::Point(this->screenCorners[i].x, this->screenCorners[i].y));
    }

    // Draw lines connecting the candidate points
    std::vector<std::vector<cv::Point> > candidatesVector;
    candidatesVector.push_back(cornerCandidates);
    cv::drawContours(this->filteredGrayImg, candidatesVector, -1, cv::Scalar(0, 255, 0));

    if(cv::contourArea(cornerCandidates) > this->calcMinArea(screenMinSize, binImg)) {
        return true;
    }
    return false;
}

// ************* ColorMonitorDetector ****************** //

ColorMonitorDetector::ColorMonitorDetector() : MonitorDetector("Color Monitor Detection") {
}

/**
 * \param threshold Value is used to determine hue region to be found
 */
bool ColorMonitorDetector::detectScreen(const cv::Mat &sceneImage, int threshold, float screenMinSize) {
    cv::Mat hsvImg = this->imageProcessor.bgr2hsv(sceneImage);
    std::vector<cv::Mat> hsv;
    cv::split(hsvImg, hsv);
    cv::Mat hueImg = hsv[0];
    cv::Mat bwImg;

    // TODO Remove magic number!
    cv::inRange(hueImg, threshold - 10, threshold + 10, bwImg);
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5));
    cv::morphologyEx(bwImg, bwImg, cv::MORPH_CLOSE, kernel);

    bwImg.copyTo(this->filteredGrayImg);
    this->filteredGrayImg = this->imageProcessor.gray2bgr(this->filteredGrayImg);

    return this->findCorners(bwImg, screenMinSize);
}

bool ColorMonitorDetector::findCorners(cv::Mat bwImg, float screenMinSize) {
    cv::Mat img;
    bwImg.copyTo(img);
    int rectanglesFound = 0;
    std::vector<Point> largestCorners(4);

    double largestArea = this->calcMinArea(screenMinSize, img);
    double secondLargestArea = largestArea;

    std::vector<std::vector<cv::Point> > contours;
    // If this is slow take a look at startFindContours and findNextContour
    cv::findContours(img, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
    for(std::vector<std::vector<cv::Point> >::iterator it = contours.begin(); it != contours.end(); it++) {
        std::vector<cv::Point> polygon;
        //0.08 has been tested and works well with applyCanny function (comment from original code)
        cv::approxPolyDP(*it, polygon, cv::arcLength(cv::Mat(*it), true) * 0.08, true);

        if(polygon.size() == 4) {
            // We do this here so the area is only calculated once and only when polygon has 4 points
            double area = cv::contourArea(polygon);
            if(cv::isContourConvex(polygon)) {
                rectanglesFound++;
                if(area > secondLargestArea) {
                    std::vector<std::vector<cv::Point> > contour;
                    contour.push_back(polygon);
                    cv::drawContours(this->filteredGrayImg, contour, 0, cv::Scalar(0, 0, 255));
                }
                if(area > largestArea) {
                    secondLargestArea = largestArea;
                    largestArea = area;
                    for(int i = 0; i < 4; i++) {
                        this->screenCorners[i].x = largestCorners[i].x;
                        this->screenCorners[i].y = largestCorners[i].y;
                        largestCorners[i].x = polygon[i].x;
                        largestCorners[i].y = polygon[i].y;
                    }
                }
                else if(area > secondLargestArea) {
                    secondLargestArea = area;
                    for(int i = 0; i < 4; i++) {
                        this->screenCorners[i].x = polygon[i].x;
                        this->screenCorners[i].y = polygon[i].y;
                    }
                }
            }
        }
    }
    if(rectanglesFound > 1)
        this->sortScreenCorners();

    return rectanglesFound > 1;
}

} // namespace haytham
