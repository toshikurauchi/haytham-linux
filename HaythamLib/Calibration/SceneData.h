/*
 * SceneData.h
 *
 *  Created on: 31/10/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_SCENEDATA_H
#define HAYTHAM_SCENEDATA_H

#include <opencv2/opencv.hpp>
#include <vector>

namespace haytham {

class SceneData
{
public:
    SceneData();
    void setCalibrationPoints(std::vector<cv::Point2f> calibrationPoints);
    std::vector<cv::Point2f> getCalibrationPoints();
private:
    std::vector<cv::Point2f> calibrationPoints;
};

} // namespace haytham

#endif // HAYTHAM_SCENEDATA_H
