/*
 * SceneData.cpp
 *
 *  Created on: 31/10/2013
 *      Author: Andrew Kurauchi
 */

#include "SceneData.h"

namespace haytham {

SceneData::SceneData()
{
}

void SceneData::setCalibrationPoints(std::vector<cv::Point2f> calibrationPoints) {
    this->calibrationPoints = calibrationPoints;
}

std::vector<cv::Point2f> SceneData::getCalibrationPoints() {
    return this->calibrationPoints;
}

} // namespace haytham
