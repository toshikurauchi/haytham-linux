/*
 * Calibration.cpp
 *
 *  Created on: 14/11/2012
 *      Author: Andrew Kurauchi
 */

#include <stdexcept>
#include <cstdio>
#include <string>
#include <vector>
#include <opencv2/calib3d/calib3d.hpp>
#include <limits>
#include <QDebug>
#include <cfloat>
#include <boost/lexical_cast.hpp>

#include "Calibration.h"
#include "CalibrationPointsDetection.h"
#include "ImageProcessing.h"
#include "IntersectLine3d.h"

#define N_RATIOS 6

namespace haytham {

/**
 * \brief Constructor.
 *
 * \param name The name of the calibration method. Provided by the subclasses.
 * \param neededPoints The number of points the calibration method needs. Provided by the subclasses.
 *
 */
Calibration::Calibration(std::string name, int neededPoints, bool requiresSceneData) :
    methodName(name),
    calibrating(false),
    calibrated(false),
    ready(false),
    currentPoint(0),
    neededPoints(neededPoints),
    requiresSceneData(requiresSceneData)
{
    int size[2] = {neededPoints,2};
    this->getOriginPoints().create(2, size, CV_32F);
    this->getDestinationPoints().create(2, size, CV_32F);
}

/**
 * \brief Starts calibration process.
 */
void Calibration::startCalibrating() {
    this->calibrating = true;
    this->calibrated = false;
    this->ready = false;
    this->currentPoint = 0;
}

/**
 * \brief Stops calibration process.
 */
void Calibration::stopCalibrating() {
    this->calibrated = false;
    this->calibrating = false;
    this->ready = false;
    this->currentPoint = 0;
}

/**
 * \returns True if calibration process is occuring, False otherwise.
 */
bool Calibration::isCalibrating() {
    return this->calibrating;
}

/**
 * \returns True if calibration process is concluded, False otherwise.
 */
bool Calibration::isCalibrated() {
    return this->calibrated;
}

/**
 * \returns True if all points have been added and it is ready to calculate the function, False otherwise.
 */
bool Calibration::isReady() {
    return this->ready;
}

/**
 * \returns The name of this calibration process.
 */
std::string &Calibration::getMethodName() {
    return this->methodName;
}

/**
 * \returns The number of remaining calibration points.
 */
int Calibration::getRemainingPoints() {
    return this->neededPoints - this->currentPoint;
}

/**
 * \returns The number of calibration points needed.
 */
int Calibration::getTotalPoints() {
    return this->neededPoints;
}

/**
 * \returns The number of points already added.
 */
int Calibration::getCurrentPoint() {
    return this->currentPoint;
}

/**
 * \returns If the calibration needs scene data to be calculated.
 */
bool Calibration::getRequiresSceneData() {
    return this->requiresSceneData;
}

/**
 * \returns The points in the origin image.
 */
cv::Mat &Calibration::getOriginPoints() {
    return this->originPoints;
}

/**
 * \returns The points in the destination image.
 */
cv::Mat &Calibration::getDestinationPoints() {
    return this->destinationPoints;
}

/**
 * \brief Adds a calibration point (composed of origin and destination points).
 *
 * \param origin The point in the original image
 * \param destination The corresponding point in the destination image
 *
 * \returns True if points were added, False otherwise.
 */
bool Calibration::addPoint(Point origin, Point destination, SceneData sceneData) {
    if(this->currentPoint >= this->neededPoints || !this->calibrating) return false;
    this->originPoints.at<float>(this->currentPoint, 0) = origin.x;
    this->originPoints.at<float>(this->currentPoint, 1) = origin.y;
    this->destinationPoints.at<float>(this->currentPoint, 0) = destination.x;
    this->destinationPoints.at<float>(this->currentPoint, 1) = destination.y;
    this->currentPoint++;
    if(this->currentPoint >= this->neededPoints) this->ready = true;
    return true;
}

bool Calibration::addPoint(Point origin, Point destination) {
    SceneData emptyData;
    return this->addPoint(origin, destination, emptyData);
}

/**
 * \brief Calculates the calibration function.
 *
 * The calculated function varies depending on the executeCalibration
 * function implemented by the subclasses of Calibration.
 */
void Calibration::calibrate() {
    this->executeCalibration();
    this->calibrating = false;
    this->calibrated = true;
}

/**
 * \brief Maps a point in the origin image to the destination image with the calculated
 * calibration function.
 *
 * \param point The point to be mapped to the destination image
 * \param correction The correction factor to be considered by the calibration method
 *
 * \throws runtime_error If not all needed points have been provided yet.
 *
 * Let \f$m\f$ be the mapped point calculated by the calibration function and \f$c\f$ the correction factor.
 * Then the mapped point returned will be \f$e - c\f$.
 *
 * \returns The mapped point considering the correction.
 *
 */
Point Calibration::map(Point point, const SceneData &sceneData, Point correction) {
    char needed[3];
    sprintf(needed, "%d", this->neededPoints);
    if(!this->isReady())
        throw std::runtime_error(std::string("Map function called before all ") + needed + " points have been added.");
    return this->executeMap(point, sceneData) - correction;
}

Point Calibration::map(Point point, Point correction) {
    SceneData emptyData;
    return this->map(point, emptyData, correction);
}

// ********************** Polynomial Calibration ****************************** //

PolynomialCalibration::PolynomialCalibration(std::string name, int neededPoints, int params) : Calibration(name, neededPoints), params(params)
{
}

/**
 * \brief Calculates parameters for polynomial calibration function using least-squares approach.
 *
 * 9 points must have been added in order for this function to work properly. The parameters  \f$ a, b, c, d, e, f\f$ are estimated for the following formula:
 * \f[
 * 		a + bx + cy + dxy + ex^2 + fy^2
 * \f]
 */
void PolynomialCalibration::executeCalibration() {
    cv::Mat eyeFeatures(this->getTotalPoints(), this->params, CV_32F);
    for(int i = 0; i < this->getTotalPoints(); i++) {
        float x, y;
        x = this->getOriginPoints().at<float>(i, 0);
        y = this->getOriginPoints().at<float>(i, 1);

        // Add row of eye features to matrix
        this->calculateEyeFeatures(Point(x, y)).copyTo(eyeFeatures.row(i));
    }
    cv::solve(eyeFeatures, this->getDestinationPoints(), this->polynomialCoeffs, cv::DECOMP_SVD);
}

/**
 * \brief Maps the point using the calibrated polynomial function.
 *
 * \param point The point in the original image to be mapped.
 *
 * \returns The mapped point.
 *
 * Let point=\f$(x,y)\f$ be the input point. Given the calibration has been executed, the output point will be:
 * \f[
 * 		a + bx + cy + dxy + ex^2 + fy^2
 * \f]
 */
Point PolynomialCalibration::executeMap(Point point, SceneData sceneData) {
    cv::Mat eyeFeatures = this->calculateEyeFeatures(point);

    Point mapped;
    mapped.x = this->polynomialCoeffs.col(0).t().dot(eyeFeatures);
    mapped.y = this->polynomialCoeffs.col(1).t().dot(eyeFeatures);

    return mapped;
}

// ********************** 9 Points Full Polynomial Calibration ****************************** //

PolynomialCalibration9PointsFull::PolynomialCalibration9PointsFull() : PolynomialCalibration("9 Points Full Polynomial", 9, 6)
{
}

/**
 * \returns A grid pattern to be used for calibration.
 */
cv::Size PolynomialCalibration9PointsFull::getPatternSize() {
    return cv::Size(3, 3);
}

cv::Mat PolynomialCalibration9PointsFull::calculateEyeFeatures(Point point) {
    cv::Mat eyeFeatures(1, 6, CV_32F);

    eyeFeatures.at<float>(0, 0) = 1;
    eyeFeatures.at<float>(0, 1) = point.x;
    eyeFeatures.at<float>(0, 2) = point.y;
    eyeFeatures.at<float>(0, 3) = point.x*point.y;
    eyeFeatures.at<float>(0, 4) = point.x*point.x;
    eyeFeatures.at<float>(0, 5) = point.y*point.y;

    return eyeFeatures;
}

// ********************** 9 Points Polynomial Calibration ****************************** //

PolynomialCalibration9Points::PolynomialCalibration9Points() : PolynomialCalibration("9 Points Polynomial", 9, 4)
{
}

/**
 * \returns A grid pattern to be used for calibration.
 */
cv::Size PolynomialCalibration9Points::getPatternSize() {
    return cv::Size(3, 3);
}

cv::Mat PolynomialCalibration9Points::calculateEyeFeatures(Point point) {
    cv::Mat eyeFeatures(1, 4, CV_32F);

    eyeFeatures.at<float>(0, 0) = 1;
    eyeFeatures.at<float>(0, 1) = point.x;
    eyeFeatures.at<float>(0, 2) = point.y;
    eyeFeatures.at<float>(0, 3) = point.x*point.y;

    return eyeFeatures;
}

// ********************** 4 Points Polynomial Calibration ****************************** //

PolynomialCalibration4Points::PolynomialCalibration4Points() : PolynomialCalibration("4 Points Polynomial", 4, 4)
{
}

/**
 * \returns A grid pattern to be used for calibration.
 */
cv::Size PolynomialCalibration4Points::getPatternSize() {
    return cv::Size(2, 2);
}

cv::Mat PolynomialCalibration4Points::calculateEyeFeatures(Point point) {
    cv::Mat eyeFeatures(1, 4, CV_32F);

    eyeFeatures.at<float>(0, 0) = 1;
    eyeFeatures.at<float>(0, 1) = point.x;
    eyeFeatures.at<float>(0, 2) = point.y;
    eyeFeatures.at<float>(0, 3) = point.x*point.y;

    return eyeFeatures;
}

// ********************** Cerolaza Polynomial Calibration 9 Points ****************************** //

CerolazaCalibration9Points::CerolazaCalibration9Points() : Calibration("Cerolaza 9 Points Calibration", 9)
{
}

/**
 * \returns A grid pattern to be used for calibration.
 */
cv::Size CerolazaCalibration9Points::getPatternSize() {
    return cv::Size(3, 3);
}

/**
 * \brief Calculates parameters for polynomial calibration function using least-squares approach.
 *
 * 9 points must have been added in order for this function to work properly. Let \f$POR_x\f$ and \f$POR_y\f$
 * be the \f$x\f$ and \f$y\f$ coordinates of the point of regard. The parameters  \f$ a, b, c, d, e, f, g, h\f$
 * are estimated for the following formula:
 * \f[
 * 		POR_x = a + bx + cx^3 + dy^2
 * \f]
 * \f[
 * 		POR_y = e + fx + gy + hx^2y
 * \f]
 */
void CerolazaCalibration9Points::executeCalibration() {
    cv::Mat eyeFeaturesForX(this->getTotalPoints(), 4, CV_32F);
    cv::Mat eyeFeaturesForY(this->getTotalPoints(), 4, CV_32F);
    for(int i = 0; i < this->getTotalPoints(); i++) {
        float x, y;
        x = this->getOriginPoints().at<float>(i, 0);
        y = this->getOriginPoints().at<float>(i, 1);

        // Add row of eye features to matrix
        this->calculateEyeFeaturesForX(Point(x, y)).copyTo(eyeFeaturesForX.row(i));
        this->calculateEyeFeaturesForY(Point(x, y)).copyTo(eyeFeaturesForY.row(i));
    }
    cv::solve(eyeFeaturesForX, this->getDestinationPoints().col(0), this->polynomialCoeffsForX, cv::DECOMP_SVD);
    cv::solve(eyeFeaturesForY, this->getDestinationPoints().col(1), this->polynomialCoeffsForY, cv::DECOMP_SVD);
}

/**
 * \brief Maps the point using the calibrated polynomial function.
 *
 * \param point The point in the original image to be mapped.
 *
 * \returns The mapped point.
 *
 * Let point=\f$(x,y)\f$ be the input point. Given the calibration has been executed, the output point will be:
 * \f[
 * 		a + bx + cy + dxy + ex^2 + fy^2
 * \f]
 */
Point CerolazaCalibration9Points::executeMap(Point point, SceneData sceneData) {
    cv::Mat eyeFeaturesForX = this->calculateEyeFeaturesForX(point);
    cv::Mat eyeFeaturesForY = this->calculateEyeFeaturesForY(point);

    Point mapped;
    mapped.x = this->polynomialCoeffsForX.t().dot(eyeFeaturesForX);
    mapped.y = this->polynomialCoeffsForY.t().dot(eyeFeaturesForY);

    return mapped;
}

cv::Mat CerolazaCalibration9Points::calculateEyeFeaturesForX(Point point) {
    cv::Mat eyeFeatures(1, 4, CV_32F);

    eyeFeatures.at<float>(0, 0) = 1;
    eyeFeatures.at<float>(0, 1) = point.x;
    eyeFeatures.at<float>(0, 2) = point.x*point.x*point.x;
    eyeFeatures.at<float>(0, 3) = point.y*point.y;

    return eyeFeatures;
}

cv::Mat CerolazaCalibration9Points::calculateEyeFeaturesForY(Point point) {
    cv::Mat eyeFeatures(1, 4, CV_32F);

    eyeFeatures.at<float>(0, 0) = 1;
    eyeFeatures.at<float>(0, 1) = point.x;
    eyeFeatures.at<float>(0, 2) = point.y;
    eyeFeatures.at<float>(0, 3) = point.x*point.x*point.y;

    return eyeFeatures;
}

// ********************** Homography Calibration ****************************** //

HomographyCalibration::HomographyCalibration(int neededPoints) : Calibration(boost::lexical_cast<std::string>(neededPoints) + " Points Homography", neededPoints) {
}

/**
 * \returns A grid pattern to be used for calibration.
 */
cv::Size HomographyCalibration::getPatternSize() {
    return cv::Size(2, 2);
}

/**
 * \brief Calculates the homography for the points in the original image to the destination image.
 */
void HomographyCalibration::executeCalibration() {
    std::vector<cv::Point2f> src;
    std::vector<cv::Point2f> dst;
    cv::Mat origin = this->getOriginPoints();
    cv::Mat destination = this->getDestinationPoints();
    for(int i = 0; i < this->getTotalPoints(); i++) {
        src.push_back(cv::Point2f(origin.at<float>(i, 0), origin.at<float>(i, 1)));
        dst.push_back(cv::Point2f(destination.at<float>(i, 0), destination.at<float>(i, 1)));
    }
    this->homography = cv::findHomography(src, dst);
}

/**
 * \brief Maps the point using the calculated homography.
 *
 * \param point The point in the original image to be mapped.
 *
 * \returns The mapped point.
 *
 * Let point=\f$(x,y)\f$ be the input point and \f$H\f$ the calculated homography. The output point \f$p_c\f$ will be:
 * \f[
 *      (x', y', z')^t = H(x, y, z)^t
 * \f]
 * \f[
 * 		p_c = (x'/z', y'/z')
 * \f]
 */
Point HomographyCalibration::executeMap(Point point, SceneData sceneData) {
    // Warning! Homography matrix contains doubles and not floats!
    cv::Mat src(3, 1, this->homography.type());
    cv::Mat dst;

    src.at<double>(0, 0) = point.x;
    src.at<double>(1, 0) = point.y;
    src.at<double>(2, 0) = 1;
    dst = this->homography*src;
    return Point(dst.at<double>(0, 0)/dst.at<double>(2, 0),
                 dst.at<double>(1, 0)/dst.at<double>(2, 0));
}

// ********************** Two Distances Calibration ****************************** //

TwoDistancesCalibration::TwoDistancesCalibration(CameraCalibration &cameraCalibration) :
    Calibration("2 Distances Calibration", 8, true),
    cameraCalibration(cameraCalibration),
    tvec1(3, 1, CV_64FC1),
    tvec2(3, 1, CV_64FC1),
    eyeC(3, 1, CV_64FC1),
    aspect(0) {
}

TwoDistancesCalibration::~TwoDistancesCalibration() {
    if(this->aspect) {
        delete(this->aspect);
    }
}

bool TwoDistancesCalibration::addPoint(Point origin, Point destination, SceneData sceneData) {
    if(this->currentPoint >= this->getTotalPoints() || !this->isCalibrating()) return false;
    this->currentPoint++;
    Calibration &current4PointsCalib = this->getCurrent4PointsCalib();
    if(current4PointsCalib.getTotalPoints() == current4PointsCalib.getRemainingPoints()) {
        std::vector<cv::Point2f> monitorPoints = sceneData.getCalibrationPoints();
        if(monitorPoints.size() < 4) {
            qDebug() << "Monitor points not detected";
        }
        this->imagePoints.push_back(monitorPoints);
    }

    // We are going to calibrate to a normalized region of 4 points (that define the monitor)
    // with the origin at the bottom left corner
    if(current4PointsCalib.getRemainingPoints() == 4) {
        destination = Point(0, 1);
    }
    else if(current4PointsCalib.getRemainingPoints() == 3) {
        destination = Point(1, 1);
    }
    else if(current4PointsCalib.getRemainingPoints() == 2) {
        destination = Point(0, 0);
    }
    else {
        destination = Point(1, 0);
    }

    current4PointsCalib.addPoint(origin, destination);
    if(this->calib4Points1.isReady() && !this->calib4Points1.isCalibrated()) {
        this->calib4Points1.calibrate();
        this->calib4Points2.startCalibrating();
    }
}

void TwoDistancesCalibration::startCalibrating() {
    Calibration::startCalibrating();
    this->calib4Points1.startCalibrating();
    this->calib4Points2.stopCalibrating();
}

void TwoDistancesCalibration::stopCalibrating() {
    Calibration::stopCalibrating();
    this->calib4Points1.stopCalibrating();
    this->calib4Points2.stopCalibrating();
}

bool TwoDistancesCalibration::isReady() {
    return this->calib4Points2.isReady();
}

int TwoDistancesCalibration::getCurrentPoint() {
    return this->getCurrent4PointsCalib().getCurrentPoint();
}

cv::Size TwoDistancesCalibration::getPatternSize() {
    return this->getCurrent4PointsCalib().getPatternSize();
}

void TwoDistancesCalibration::executeCalibration() {
    this->calib4Points2.calibrate();

    if(this->aspect) {
        delete(this->aspect);
    }
    this->aspect = AspectRatio::from(this->imagePoints);
    qDebug() << "Monitor aspect ratio: " << this->aspect->w << " " << this->aspect->h;

    // Create object points
    this->objectPoints.erase(this->objectPoints.begin(), this->objectPoints.end());
    this->objectPoints.push_back(cv::Point3f(0, aspect->h, 0));
    this->objectPoints.push_back(cv::Point3f(aspect->w, aspect->h, 0));
    this->objectPoints.push_back(cv::Point3f(0, 0, 0));
    this->objectPoints.push_back(cv::Point3f(aspect->w, 0, 0));

    // Calculate 3D position
    cv::Mat rvec1(3, 1, CV_64FC1);
    cv::Mat rvec2(3, 1, CV_64FC1);
    cv::solvePnP(this->objectPoints, this->imagePoints[0], this->cameraCalibration.getCameraMatrix(), this->cameraCalibration.getDistCoeffs(), rvec1, this->tvec1);
    cv::solvePnP(this->objectPoints, this->imagePoints[1], this->cameraCalibration.getCameraMatrix(), this->cameraCalibration.getDistCoeffs(), rvec2, this->tvec2);
    cv::Rodrigues(rvec1, this->rmat1);
    cv::Rodrigues(rvec2, this->rmat2);
    // Apparently the output of solvePnP is always CV_64FC1 even when CV_32FC1 Mat is passed
    //this->tvec1.convertTo(this->tvec1, CV_32FC1);
    //this->tvec2.convertTo(this->tvec2, CV_32FC1);
    //this->rmat1.convertTo(this->rmat1, CV_32FC1);
    //this->rmat2.convertTo(this->rmat2, CV_32FC1);

    // Calculate the mapped points for all pupil center positions inside the region in
    // which the pupil varied for both calibrations
    cv::Mat origin1 = this->calib4Points1.getOriginPoints();
    origin1.convertTo(origin1, CV_64FC1);
    cv::Mat origin2 = this->calib4Points2.getOriginPoints();
    origin2.convertTo(origin2, CV_64FC1);
    // Use the width to decide which one is the smaller monitor projection
    double dx1 = origin1.at<double>(0, 0) - origin1.at<double>(1, 0);
    double dy1 = origin1.at<double>(0, 1) - origin1.at<double>(1, 1);
    double dx2 = origin2.at<double>(0, 0) - origin2.at<double>(1, 0);
    double dy2 = origin2.at<double>(0, 1) - origin2.at<double>(1, 1);
    Point eyePositions[4];
    cv::Mat smallestOrigin;
    if(sqrt(dx1*dx1 + dy1*dy1) < sqrt(dx2*dx2 + dy2*dy2)) {
        smallestOrigin = origin1;
        this->largestMonitorId = 2;
    }
    else {
        smallestOrigin = origin2;
        this->largestMonitorId = 1;
    }
    for(int i = 0; i < 4; i++) {
        eyePositions[i] = Point(smallestOrigin.at<double>(i, 0), smallestOrigin.at<double>(i, 1));
    }
    // TODO Remove commented lines when the method starts working
    //cv::Mat linesA3d(4, 3, CV_32FC1);
    //cv::Mat linesB3d(4, 3, CV_32FC1);

    // Begin zPlane
    // Define z=0 plane in scene camera coordinate system
    float zPlaneData[3][3] = {{0, 0, 1},{0, 1, 0},{0, 0, 0}};
    cv::Mat zPlane(3, 3, CV_64FC1, &zPlaneData);
    IntersectLine3d intersectionFinder;
    this->eyeC.setTo(cv::Scalar(0));
    // End zPlane
    for(int i = 0; i < 4; i++) {
        // Each direction is mapped to different points for each plane
        Point mapped1 = this->calib4Points1.map(eyePositions[i]);
        Point mapped2 = this->calib4Points2.map(eyePositions[i]);
        cv::Mat mapped3d1(3, 1, CV_64FC1);
        cv::Mat mapped3d2(3, 1, CV_64FC1);
        // Mapped point in each plane in 3D
        mapped3d1.at<double>(0, 0) = mapped1.x*this->aspect->w;
        mapped3d1.at<double>(1, 0) = mapped1.y*this->aspect->h;
        mapped3d1.at<double>(2, 0) = 0;
        mapped3d2.at<double>(0, 0) = mapped2.x*this->aspect->w;
        mapped3d2.at<double>(1, 0) = mapped2.y*this->aspect->h;
        mapped3d2.at<double>(2, 0) = 0;
        // Transform to scene camera coordinate system
        mapped3d1 = this->rmat1*mapped3d1 + this->tvec1;
        mapped3d2 = this->rmat2*mapped3d2 + this->tvec2;

        // Begin zPlane
        // Intersect line with z=0 plane
        cv::Mat line(3, 2, CV_64FC1);
        mapped3d1.copyTo(line(cv::Rect(cv::Point(0,0), mapped3d1.size())));
        mapped3d2.copyTo(line(cv::Rect(cv::Point(1,0), mapped3d2.size())));
        cv::Mat intersection = intersectionFinder.intersectPlane(line, zPlane);
        this->eyeC.at<double>(0, 0) += intersection.at<double>(0, 0);
        this->eyeC.at<double>(1, 0) += intersection.at<double>(1, 0);
        this->eyeC.at<double>(2, 0) += intersection.at<double>(2, 0);
        // End zPlane

        /*linesA3d.at<float>(i, 0) = mapped3d1.at<float>(0, 0);
        linesA3d.at<float>(i, 1) = mapped3d1.at<float>(1, 0);
        linesA3d.at<float>(i, 2) = mapped3d1.at<float>(2, 0);
        linesB3d.at<float>(i, 0) = mapped3d2.at<float>(0, 0);
        linesB3d.at<float>(i, 1) = mapped3d2.at<float>(1, 0);
        linesB3d.at<float>(i, 2) = mapped3d2.at<float>(2, 0);*/
    }

    // Begin zPlane
    // The estimation for the eye center of projection is the mean of the 4 intersection points
    this->eyeC /= 4;
    // End zPlane

    //IntersectLine3d intersectionFinder;
    //this->eyeC = intersectionFinder.intersectLines(linesA3d, linesB3d);
    qDebug() << "Eye projection center: " << this->eyeC.at<float>(0, 0) << " " << this->eyeC.at<float>(1, 0) << " " << this->eyeC.at<float>(2, 0);
}

Point TwoDistancesCalibration::executeMap(Point point, SceneData sceneData) {
    // Map to first plane
    Point mapped1 = this->calib4Points1.map(point);
    // Define point in plane in 3D
    cv::Mat mapped3d1(3, 1, CV_64FC1);
    mapped3d1.at<double>(0, 0) = mapped1.x*this->aspect->w;
    mapped3d1.at<double>(1, 0) = mapped1.y*this->aspect->h;
    mapped3d1.at<double>(2, 0) = 0;
    // Transform to camera coordinate system
    mapped3d1 = this->rmat1*mapped3d1 + this->tvec1;

    // Begin without eyeC
    // Map to second plane
    Point mapped2 = this->calib4Points2.map(point);
    // Define point in plane in 3D
    cv::Mat mapped3d2(3, 1, CV_64FC1);
    mapped3d2.at<double>(0, 0) = mapped2.x*this->aspect->w;
    mapped3d2.at<double>(1, 0) = mapped2.y*this->aspect->h;
    mapped3d2.at<double>(2, 0) = 0;
    // Transform to camera coordinate system
    mapped3d2 = this->rmat2*mapped3d2 + this->tvec2;
    // End without eyeC

    // Define gaze direction
    cv::Mat gazeDir(3, 2, CV_64FC1);
    // Begin old
    /*gazeDir.at<float>(0, 0) = this->eyeC.at<float>(0, 0);
    gazeDir.at<float>(1, 0) = this->eyeC.at<float>(1, 0);
    gazeDir.at<float>(2, 0) = this->eyeC.at<float>(2, 0);
    gazeDir.at<float>(0, 1) = mapped3d1.at<float>(0, 0);
    gazeDir.at<float>(1, 1) = mapped3d1.at<float>(1, 0);
    gazeDir.at<float>(2, 1) = mapped3d1.at<float>(2, 0);*/
    // End old

    // Begin without eyeC
    gazeDir.at<double>(0, 0) = mapped3d1.at<double>(0, 0);
    gazeDir.at<double>(1, 0) = mapped3d1.at<double>(1, 0);
    gazeDir.at<double>(2, 0) = mapped3d1.at<double>(2, 0);
    gazeDir.at<double>(0, 1) = mapped3d2.at<double>(0, 0);
    gazeDir.at<double>(1, 1) = mapped3d2.at<double>(1, 0);
    gazeDir.at<double>(2, 1) = mapped3d2.at<double>(2, 0);
    // End without eyeC

    // Calculate current plane in camera coordinate system
    cv::Mat plane(3, 3, CV_64FC1);
    std::vector<cv::Point2f> corners = sceneData.getCalibrationPoints();
    if(corners.size() < 4) {
        corners = this->imagePoints[0];
    }
    cv::Mat rvec(3, 1, CV_64FC1);
    cv::Mat tvec(3, 1, CV_64FC1);
    cv::Mat rmat;
    cv::solvePnP(this->objectPoints, corners, this->cameraCalibration.getCameraMatrix(), this->cameraCalibration.getDistCoeffs(), rvec, tvec);
    cv::Rodrigues(rvec, rmat);
    for(int i = 0; i < 3; i++) {
        cv::Mat corner(3, 1, CV_64FC1);
        corner.at<double>(0, 0) = this->objectPoints[i].x;
        corner.at<double>(1, 0) = this->objectPoints[i].y;
        corner.at<double>(2, 0) = this->objectPoints[i].z;
        // Transform to camera coordinate system
        corner = rmat*corner + tvec;
        plane.at<double>(0, i) = corner.at<double>(0, 0);
        plane.at<double>(1, i) = corner.at<double>(1, 0);
        plane.at<double>(2, i) = corner.at<double>(2, 0);
    }

    // Intersect gaze direction and current plane
    IntersectLine3d intersectionFinder;
    cv::Mat gaze3d = intersectionFinder.intersectPlane(gazeDir, plane);

    // Project to camera plane
    // TODO There is a OpenCV function to project a 3D point that considers distortion coefficients as well
    cv::Mat cameraMatrix;
    this->cameraCalibration.getCameraMatrix().convertTo(cameraMatrix, CV_64FC1);
    cv::Mat gaze2d = cameraMatrix*gaze3d;
    double x = gaze2d.at<double>(0, 0);
    double y = gaze2d.at<double>(1, 0);
    double w = gaze2d.at<double>(2, 0);

    return Point(x/w, y/w);
}

Calibration &TwoDistancesCalibration::getCurrent4PointsCalib() {
    if(this->calib4Points1.isCalibrating()) {
        return this->calib4Points1;
    }
    return this->calib4Points2;
}

TwoDistancesCalibration::AspectRatio *TwoDistancesCalibration::AspectRatio::from(std::vector<std::vector<cv::Point2f> > observations) {
    // For each monitor points calculate the closest aspect ratio
    std::vector<float> ratios;
    for(std::vector<std::vector<cv::Point2f> >::iterator it = observations.begin(); it != observations.end(); it++) {
        float w1 = sqrt((it->at(1).x - it->at(0).x)*(it->at(1).x - it->at(0).x) + abs(it->at(1).y - it->at(0).y)*abs(it->at(1).y - it->at(0).y));
        float w2 = sqrt((it->at(3).x - it->at(2).x)*(it->at(3).x - it->at(2).x) + abs(it->at(3).y - it->at(2).y)*abs(it->at(3).y - it->at(2).y));
        float h1 = sqrt((it->at(2).x - it->at(0).x)*(it->at(2).x - it->at(0).x) + abs(it->at(2).y - it->at(0).y)*abs(it->at(2).y - it->at(0).y));
        float h2 = sqrt((it->at(3).x - it->at(1).x)*(it->at(3).x - it->at(1).x) + abs(it->at(3).y - it->at(1).y)*abs(it->at(3).y - it->at(1).y));
        ratios.push_back((w1+w2)/(h1+h2));
    }

    // Source: http://en.wikipedia.org/wiki/Display_resolution
    //float realRatios[N_RATIOS][2] = {{4,3},{5,3},{5,4},{16,9},{16,10},{17, 10}};
    float realRatios[N_RATIOS][2] = {{40,30},{50,30},{50,40},{160,90},{160,100},{170, 100}};
    //float realRatios[N_RATIOS][2] = {{34,27},{34,27},{34,27},{34,27},{34,27},{34,27}};
    float bestDist = FLT_MAX;

    int w, h;
    for(int i = 0; i < N_RATIOS; i++) {
        float distSum = 0;
        for(std::vector<float>::iterator it = ratios.begin(); it != ratios.end(); it++) {
            float dist = (*it) - float(realRatios[i][0])/realRatios[i][1];
            distSum += dist*dist;
        }
        float meanDist = distSum/ratios.size();
        if(meanDist < bestDist) {
            w = realRatios[i][0];
            h = realRatios[i][1];
            bestDist = meanDist;
        }
    }

    return new AspectRatio(w, h);
}

} /* namespace haytham */
