/*
 * Calibration.h
 *
 *  Created on: 14/11/2012
 *      Author: Andrew Kurauchi
 */

#ifndef CALIBRATION_H
#define CALIBRATION_H

#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

#include "../Common/Point.h"
#include "Camera/CameraCalibration.h"
#include "SceneData.h"

namespace haytham {

/**
 * An example of application for the calibration is the following:
 * \image html calibration.jpg "Example of calibration application"
 *
 * In this example we have the eye coordinates \f$(eye_x, eye_y)\f$ in eye image coordinates.
 * We want to find \f$(scene_x, scene_y)\f$ in scene image coordinates. A calibration can
 * be done in order to find a function that maps points in the eye image to points in the scene image.
 */
class Calibration
{
public:
    explicit Calibration(std::string name, int neededPoints, bool requiresSceneData = false);
    void calibrate();
    Point map(Point point, const SceneData &sceneData, Point correction = Point());
    Point map(Point point, Point correction = Point());
    virtual bool addPoint(Point origin, Point destination, SceneData sceneData);
    virtual bool addPoint(Point origin, Point destination);
    virtual void startCalibrating();
    virtual void stopCalibrating();
    bool isCalibrating();
    bool isCalibrated();
    virtual bool isReady();
    std::string &getMethodName();
    int getRemainingPoints();
    int getTotalPoints();
    virtual int getCurrentPoint();
    virtual cv::Size getPatternSize() = 0;
    bool getRequiresSceneData();

    cv::Mat &getOriginPoints();
    cv::Mat &getDestinationPoints();

    virtual void executeCalibration() = 0;
    virtual Point executeMap(Point point, SceneData sceneData) = 0;

protected:
    int currentPoint;
private:
    std::string methodName;
    cv::Mat originPoints;
    cv::Mat destinationPoints;
    bool calibrating;
    bool calibrated;
    bool ready;
    int neededPoints;
    bool requiresSceneData;
};

class PolynomialCalibration : public Calibration
{
public:
    PolynomialCalibration(std::string name, int neededPoints, int params);
    virtual cv::Size getPatternSize() = 0;
    virtual void executeCalibration();
    virtual Point executeMap(Point point, SceneData sceneData);
    virtual cv::Mat calculateEyeFeatures(Point point) = 0;
private:
    cv::Mat polynomialCoeffs;
    int params;
};

class PolynomialCalibration9PointsFull : public PolynomialCalibration
{
public:
    PolynomialCalibration9PointsFull();
    virtual cv::Size getPatternSize();
    virtual cv::Mat calculateEyeFeatures(Point point);
};

class PolynomialCalibration9Points : public PolynomialCalibration
{
public:
    PolynomialCalibration9Points();
    virtual cv::Size getPatternSize();
    virtual cv::Mat calculateEyeFeatures(Point point);
};

class PolynomialCalibration4Points : public PolynomialCalibration
{
public:
    PolynomialCalibration4Points();
    virtual cv::Size getPatternSize();
    virtual cv::Mat calculateEyeFeatures(Point point);
};

class CerolazaCalibration9Points : public Calibration
{
public:
    CerolazaCalibration9Points();
    virtual cv::Size getPatternSize();
    virtual void executeCalibration();
    virtual Point executeMap(Point point, SceneData sceneData);
    virtual cv::Mat calculateEyeFeaturesForX(Point point);
    virtual cv::Mat calculateEyeFeaturesForY(Point point);
private:
    cv::Mat polynomialCoeffsForX;
    cv::Mat polynomialCoeffsForY;
};

class HomographyCalibration : public Calibration
{
public:
    HomographyCalibration(int neededPoints);
    virtual cv::Size getPatternSize();
    virtual void executeCalibration();
    virtual Point executeMap(Point point, SceneData sceneData);
private:
    cv::Mat homography;
};

class TwoDistancesCalibration : public Calibration
{
public:
    TwoDistancesCalibration(CameraCalibration &cameraCalibration);
    ~TwoDistancesCalibration();
    virtual bool addPoint(Point origin, Point destination, SceneData sceneData);
    virtual void startCalibrating();
    virtual void stopCalibrating();
    virtual bool isReady();
    virtual int getCurrentPoint();
    virtual cv::Size getPatternSize();
    virtual void executeCalibration();
    virtual Point executeMap(Point point, SceneData sceneData);
private:
    Calibration &getCurrent4PointsCalib();

    // Aux struct that estimates the monitor aspect ratio from a set of observations
    struct AspectRatio {
        int w, h;
        static AspectRatio* from(std::vector<std::vector<cv::Point2f> > observations);
    private:
        AspectRatio(int w, int h) {
            this->w = w;
            this->h = h;
        }
    };

    CameraCalibration &cameraCalibration;
    PolynomialCalibration4Points calib4Points1;
    PolynomialCalibration4Points calib4Points2;
    Point epipole;
    // 3D monitor pose estimation
    std::vector<std::vector<cv::Point2f> > imagePoints;
    std::vector<cv::Point3f> objectPoints;
    int largestMonitorId;
    cv::Mat rmat1;
    cv::Mat tvec1;
    cv::Mat rmat2;
    cv::Mat tvec2;
    cv::Mat eyeC;
    AspectRatio *aspect;
};

} /* namespace haytham */

#endif // CALIBRATION_H
