/*
 * CalibrationPointsDetection.cpp
 *
 *  Created on: 10/04/2013
 *      Author: Andrew Kurauchi
 */

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>
#include <algorithm>
#include <climits>
#include <QDebug>

#include "CalibrationPointsDetection.h"
#include "ImageProcessing.h"
#include "Point.h"
#include "ShapeChecker.h"
#include "Blob.h"

// TODO There are some hard coded constants -> remove
// TODO Refactor this whole class!

namespace haytham {

CalibrationPointsDetection::CalibrationPointsDetection()
{
}

bool byArea(Blob b1, Blob b2) {
    return b1.getArea() < b2.getArea();
}

bool compY(cv::Point2f p1, cv::Point2f p2) {
    return p1.y < p2.y;
}

class GridSorter {
public:
    GridSorter(cv::Size patterSize) : rows(patterSize.height), cols(patterSize.width) {
        this->grid = new Blob*[this->rows];
        for(int i = 0; i < this->rows; i++) {
            this->grid[i] = new Blob[this->cols];
        }
    }

    ~GridSorter() {
        for(int i = 0; i < this->rows; i++) {
            delete[] this->grid[i];
        }
        delete[] this->grid;
    }

    std::vector<Blob> sorted(std::vector<Blob> points) {
        std::vector<Blob> sortedPoints;
        if(points.empty()) {
            return sortedPoints;
        }
        std::vector<cv::Point2f> corners;
        for(std::vector<Blob>::iterator it = points.begin(); it != points.end() && corners.size() < 4; it++) {
            double maxDist = 0;
            cv::Point2f farthest = it->getCenterOfGravity();
            for(std::vector<Blob>::iterator it2 = points.begin(); it2 != points.end(); it2++) {
                double dist = cv::norm(it2->getCenterOfGravity() - it->getCenterOfGravity());
                if(dist > maxDist) {
                    maxDist = dist;
                    farthest = it2->getCenterOfGravity();
                }
            }
            if(std::find(corners.begin(), corners.end(), farthest) == corners.end()) {
                corners.push_back(farthest);
            }
        }
        if(corners.size() < 4) return std::vector<Blob>();

        std::vector<cv::Point2f> upper;
        // c1 c2
        // c3 c4
        std::vector<cv::Point2f>::iterator minY = std::min_element(corners.begin(), corners.end(), compY);
        upper.push_back(*minY);
        corners.erase(minY);
        minY = std::min_element(corners.begin(), corners.end(), compY);
        upper.push_back(*minY);
        corners.erase(minY);

        cv::Point2f c1;
        cv::Point2f c2;
        cv::Point2f c3;
        cv::Point2f c4;
        if(upper[0].x < upper[1].x) {
            c1 = upper[0];
            c2 = upper[1];
        }
        else {
            c1 = upper[1];
            c2 = upper[0];
        }
        if(corners[0].x < corners[1].x) {
            c3 = corners[0];
            c4 = corners[1];
        }
        else {
            c3 = corners[1];
            c4 = corners[0];
        }

        cv::Point xDir, yDir;
        xDir = c2 - c1 + c4 - c3;
        xDir.x /= 2;
        xDir.y /= 2;
        yDir = c3 - c1 + c4 - c2;
        yDir.x /= 2;
        yDir.y /= 2;

        for(std::vector<Blob>::iterator it = points.begin(); it != points.end(); it++) {
            int row = -1;
            int col = -1;
            double dist = INT_MAX;
            for(int i = 0; i < this->rows; i++) {
                for(int j = 0; j < this->cols; j++) {
                    float rowProp = float(i)/(this->rows-1);
                    float colProp = float(j)/(this->cols-1);
                    cv::Point2f current(c1.x + xDir.x*colProp + yDir.x*rowProp, c1.y + xDir.y*colProp + yDir.y*rowProp);
                    double currDist = cv::norm(current - it->getCenterOfGravity());
                    if(currDist < dist) {
                        dist = currDist;
                        row = i;
                        col = j;
                    }
                }
            }
            grid[row][col] = *it;
        }

        for(int i = 0; i < this->rows; i++) {
            for(int j = 0; j < this->cols; j++) {
                if(grid[i][j].empty()) return std::vector<Blob>();
                sortedPoints.push_back(grid[i][j]);
            }
        }
        return sortedPoints;
    }
private:
    int rows;
    int cols;

    Blob **grid;
};

std::vector<Blob> CalibrationPointsDetection::findPoints(cv::Mat image, cv::Size patternSize, int threshold, const cv::Mat &dest, Calibration *calibration) {
    int points = patternSize.width * patternSize.height;

    ImageProcessing processor;
    cv::Mat grayImg = processor.bgr2gray(image);
    GaussianBlur(grayImg, grayImg, cv::Size(15, 15), 8, 8);

    std::vector<std::vector<cv::Point> > contours;
    std::vector<Blob> filtered;

    cv::threshold(grayImg, grayImg, threshold, 255, cv::THRESH_BINARY);
    cv::findContours(grayImg, contours, cv::RETR_LIST, cv::CHAIN_APPROX_TC89_L1);

    ShapeChecker shapeChecker(0.5, 0.15);
    for(std::vector<std::vector<cv::Point> >::iterator it = contours.begin(); it != contours.end(); it++) {
        Blob blob(*it);
        if(shapeChecker.isCircle(blob) && blob.getArea() < 1000) {
            filtered.push_back(blob);
        }
    }

    std::sort(filtered.begin(), filtered.end(), byArea);
    int minDiff = INT_MAX;
    int gridFirst = 0;
    for(int i = 0; i <= int(filtered.size()) - points; i++) {
        Blob b1 = filtered[i];
        Blob b2 = filtered[i + points - 1];
        if(b2.getArea() - b1.getArea() < minDiff) {
            minDiff = b2.getArea() - b1.getArea();
            gridFirst = i;
        }
    }
    std::vector<std::vector<cv::Point> > detected;
    std::vector<Blob> retVal;
    if(gridFirst + points <= filtered.size()) {
        GridSorter sorter(patternSize);
        retVal = sorter.sorted(std::vector<Blob>(filtered.begin() + gridFirst, filtered.begin() + gridFirst + points));
        for(std::vector<Blob>::iterator it = filtered.begin() + gridFirst; it != filtered.begin() + gridFirst + points; it++) {
            detected.push_back(it->getPoints());
        }
        if(!dest.empty() && !retVal.empty() && calibration != 0) {
            cv::drawContours(dest, detected, -1, cv::Scalar(1), -1);
            processor.drawPoint(dest, Point(retVal[calibration->getCurrentPoint()].getCenterOfGravity()), 20);
        }
    }
    return retVal;
}

std::vector<Blob> CalibrationPointsDetection::findPoints(cv::Mat image, cv::Size patternSize, int threshold, Calibration *calibration) {
    cv::Mat empty;
    return this->findPoints(image, patternSize, threshold, empty, calibration);
}

} // namespace haytham
