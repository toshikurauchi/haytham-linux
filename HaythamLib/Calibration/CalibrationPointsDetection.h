/*
 * CalibrationPointsDetection.h
 *
 *  Created on: 10/04/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_CALIBRATIONPOINTSDETECTION_H
#define HAYTHAM_CALIBRATIONPOINTSDETECTION_H

#include <opencv2/opencv.hpp>

#include "Calibration.h"
#include "Common/Blob.h"

namespace haytham {

class CalibrationPointsDetection
{
public:
    CalibrationPointsDetection();   

    std::vector<Blob> findPoints(cv::Mat image, cv::Size patternSize, int threshold, const cv::Mat &dest, Calibration *calibration = 0);
    std::vector<Blob> findPoints(cv::Mat image, cv::Size patternSize, int threshold, Calibration *calibration = 0);
};

} // namespace haytham

#endif // HAYTHAM_CALIBRATIONPOINTSDETECTION_H
