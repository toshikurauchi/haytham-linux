#ifndef HAYTHAM_SCENEVISUALIZATION_H
#define HAYTHAM_SCENEVISUALIZATION_H

#include <opencv2/highgui.hpp>
#include <string>

#include "Common/Point.h"
#include "Common/CircularBuffer.h"
#include "Common/GaussianMat.h"

namespace haytham {

class SceneVisualization
{
public:
    SceneVisualization(std::string name);
    virtual cv::Mat& process(cv::Mat &sceneImage, Point gaze) = 0;
    std::string getName();
private:
    std::string name;
};

class SceneVisualizationPOR : public SceneVisualization {
public:
    SceneVisualizationPOR();
    virtual cv::Mat& process(cv::Mat &sceneImage, Point gaze);
};

class SceneVisualizationGaussian : public SceneVisualization {
public:
    SceneVisualizationGaussian();
    SceneVisualizationGaussian(std::string name);
    virtual cv::Mat& process(cv::Mat &sceneImage, Point gaze);
protected:
    GaussianMat gaussianMat;

    virtual cv::Mat computeWeights(cv::Mat img, Point gaze);
    virtual cv::Mat getFilteredImg(cv::Mat &img);
private:
    cv::Mat grayImg;
};

class SceneVisualizationGaussianCumulative : public SceneVisualizationGaussian {
public:
    SceneVisualizationGaussianCumulative();
protected:
    virtual cv::Mat computeWeights(cv::Mat img, Point gaze);
private:
    CircularBuffer<cv::Mat> weightsBuf;
    float coefs[10];
};

class SceneVisualizationPath : public SceneVisualization {
public:
    SceneVisualizationPath();
    virtual cv::Mat& process(cv::Mat &sceneImage, Point gaze);
private:
    CircularBuffer<Point> history;
};

} // namespace haytham

#endif // HAYTHAM_SCENEVISUALIZATION_H
