/*
 * ImageProcessing.h
 *
 *  Created on: 30/07/2012
 *      Author: Andrew Kurauchi
 */

#ifndef IMAGEPROCESSING_H_
#define IMAGEPROCESSING_H_

#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

#include "../Common/Point.h"

namespace haytham {

class ImageProcessing {
public:
	ImageProcessing();
	cv::Mat filterThreshold(cv::Mat &grayImg, int threshold, bool inverse);
	cv::Mat filterPupilAdaptiveThreshold(cv::Mat &grayImg, int maxValue, bool inverse, int constant);
	cv::Mat filterGlintAdaptiveThreshold(cv::Mat &grayImg, int maxValue, bool inverse);
    cv::Mat filterAdaptiveThreshold(cv::Mat &grayImg, int maxValue, bool inverse, int constant, int adaptiveMethod, int blockSize);
	cv::Mat filterSobel(cv::Mat &grayImg);
    bool drawIrisCircle(cv::Mat &eyeImg, int centerX, int centerY, int diameter, double minPupilScale, double maxPupilScale);
    void drawPupil(cv::Mat &eyeImg, const cv::RotatedRect &pupilEllipse);
    void drawGlint(cv::Mat &img, Point point, int radius = 10);
    void drawTarget(cv::Mat &img, Point target);
    void drawLine(cv::Mat &img, Point p1, Point p2, cv::Scalar color, int thickness = 1);
    bool drawCross(cv::Mat &img, int intersectX, int intersectY, cv::Scalar color, int length = -1, int innerLength = 0);
    void drawEllipse(cv::Mat &img, const cv::RotatedRect &ellipse, cv::Scalar color, int thickness = 1);
    void drawRectangle(cv::Mat &img, Point corner1, Point corner2, cv::Scalar color);
    void drawRectangle(cv::Mat &img, cv::Rect rectangle, cv::Scalar color);
    void drawRectangle(const cv::Mat &img, Point center, cv::Size2f size);
    void drawRectangle(const cv::Mat &img, std::vector<Point> corners, int expand, bool fill, std::string text);
    void drawRectangle(const cv::Mat &img, Point corners[4], int expand, bool fill, std::string text);
    void drawCircle(cv::Mat &img, Point center, int radius, cv::Scalar color, int thickness = 1);
    void drawPoint(const cv::Mat &img, Point point, int radius = 5);
    float findMedian(std::vector<float> &numbers);
    cv::RotatedRect ellipseLeastSquareFitting(std::vector<cv::Point> points);
    cv::Rect createROIForSize(cv::Size size, int x, int y, int width, int height);
    cv::Mat bgr2gray(const cv::Mat &image);
    cv::Mat gray2bgr(const cv::Mat &image);
    cv::Mat bgr2hsv(const cv::Mat &image);
    cv::Rect calcROI(Point center, double radius, cv::Size imgSize);
private:
    void drawRedCircle(const cv::Mat &img, int centerX, int centerY, int diameter, int thickness=1);
};

} /* namespace haytham */
#endif /* IMAGEPROCESSING_H_ */
