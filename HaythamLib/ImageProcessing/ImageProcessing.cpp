/*
 * ImageProcessing.cpp
 *
 *  Created on: 30/07/2012
 *      Author: Andrew Kurauchi
 */

#include <opencv2/opencv.hpp>
#include <vector>
#include <algorithm>
#include <cmath>

#include "ImageProcessing.h"
#include "Colors.h"

namespace haytham {

ImageProcessing::ImageProcessing() {
	// TODO Auto-generated constructor stub

}

/**
 * \param grayImg A grayscale image to be processed
 * \param threshold Threshold value
 * \param inverse Uses binary inverse threshold if true and binary otherwise (see details below)
 *
 * \returns A binary image processed with the given threshold parameters
 *
 * \brief Transforms a grayscale image to a binary image according to the given threshold
 *
 * Binary threshold processes the image using the following formula:
 * \f[
 * 		\mbox{processedImage}(x,y) = \left\{
 * 			\begin{array}{ll}
 * 				255 & \mbox{if grayImg}(x,y) > \mbox{threshold} \\
 * 				0 & \mbox{otherwise}
 * 			\end{array}
 * 		\right\}
 * \f]
 * Binary inverse threshold uses this formula:
 * \f[
 * 		\mbox{processedImage}(x,y) = \left\{
 * 			\begin{array}{ll}
 * 				0 & \mbox{if grayImg}(x,y) > \mbox{threshold} \\
 * 				255 & \mbox{otherwise}
 * 			\end{array}
 * 		\right\}
 * \f]
 */
cv::Mat ImageProcessing::filterThreshold(cv::Mat &grayImg, int threshold, bool inverse) {
	cv::Mat processedImg = grayImg.clone();
	int thresholdType;
	if(inverse) {
        thresholdType = cv::THRESH_BINARY_INV;
	}
	else {
        thresholdType = cv::THRESH_BINARY;
	}
	cv::threshold(grayImg, processedImg, threshold, 255, thresholdType);
	return processedImg;
}

/**
 * \param grayImg A grayscale image to be processed
 * \param maxValue Non-zero value assigned to the pixels for which the condition is satisfied. See
 * the details below.
 * \param inverse Uses binary inverse threshold if true and binary otherwise (see details below)
 * \param constant Constant subtracted from the mean or weighted mean (see the details below). If
 * set to -1 uses default value.
 *
 * \returns A binary image processed with the given threshold parameters
 *
 * \brief Transforms a grayscale image to a binary image according to the given threshold
 *
 * Binary threshold processes the image using the following formula:
 * \f[
 * 		\mbox{processedImage}(x,y) = \left\{
 * 			\begin{array}{ll}
 * 				\mbox{maxValue} & \mbox{if grayImg}(x,y) > \mbox{T}(x,y) \\
 * 				0 & \mbox{otherwise}
 * 			\end{array}
 * 		\right\}
 * \f]
 * Binary inverse threshold uses this formula:
 * \f[
 * 		\mbox{processedImage}(x,y) = \left\{
 * 			\begin{array}{ll}
 * 				0 & \mbox{if grayImg}(x,y) > \mbox{T}(x,y) \\
 * 				\mbox{maxValue} & \mbox{otherwise}
 * 			\end{array}
 * 		\right\}
 * \f]
 *
 * Where \f$\mbox{T}(x,y)\f$ is a threshold calculated for each pixel.
 */
cv::Mat ImageProcessing::filterPupilAdaptiveThreshold(cv::Mat &grayImg, int maxValue, bool inverse, int constant) {
	// TODO get the following values from METState
	// Original code:
	//		constant = METState.Current.PAdaptive_Constant;
	// 		adaptiveMethod = METState.Current.PAdaptive_type;
	//		blockSize = METState.Current.PAdaptive_blockSize;
	if(constant == -1) constant = 10;
    int adaptiveMethod = cv::ADAPTIVE_THRESH_GAUSSIAN_C;
	int blockSize = 3;

	return this->filterAdaptiveThreshold(grayImg, maxValue, inverse, constant, adaptiveMethod, blockSize);
}

/**
 * \param grayImg A grayscale image to be processed
 * \param maxValue Non-zero value assigned to the pixels for which the condition is satisfied. See
 * the details below.
 * \param inverse Uses binary inverse threshold if true and binary otherwise (see details below)
 *
 * \returns A binary image processed with the given threshold parameters
 *
 * \brief Transforms a grayscale image to a binary image according to the given threshold
 *
 * Binary threshold processes the image using the following formula:
 * \f[
 * 		\mbox{processedImage}(x,y) = \left\{
 * 			\begin{array}{ll}
 * 				\mbox{maxValue} & \mbox{if grayImg}(x,y) > \mbox{T}(x,y) \\
 * 				0 & \mbox{otherwise}
 * 			\end{array}
 * 		\right\}
 * \f]
 * Binary inverse threshold uses this formula:
 * \f[
 * 		\mbox{processedImage}(x,y) = \left\{
 * 			\begin{array}{ll}
 * 				0 & \mbox{if grayImg}(x,y) > \mbox{T}(x,y) \\
 * 				\mbox{maxValue} & \mbox{otherwise}
 * 			\end{array}
 * 		\right\}
 * \f]
 *
 * Where \f$\mbox{T}(x,y)\f$ is a threshold calculated for each pixel.
 */
cv::Mat ImageProcessing::filterGlintAdaptiveThreshold(cv::Mat &grayImg, int maxValue, bool inverse) {
	// TODO get the following values from METState
	// Original code:
	// 		adaptiveMethod = METState.Current.GAdaptive_type;
	//		blockSize = METState.Current.GAdaptive_blockSize;
	int constant = 10;
    int adaptiveMethod = cv::ADAPTIVE_THRESH_GAUSSIAN_C;
	int blockSize = 3;

	return this->filterAdaptiveThreshold(grayImg, maxValue, inverse, constant, adaptiveMethod, blockSize);
}

cv::Mat ImageProcessing::filterAdaptiveThreshold(cv::Mat &grayImg, int maxValue, bool inverse, int constant,
		int adaptiveMethod, int blockSize) {
	cv::Mat processedImg = grayImg.clone();

	int thresholdType;
	if(inverse) {
        thresholdType = cv::THRESH_BINARY_INV;
	}
	else {
        thresholdType = cv::THRESH_BINARY;
	}
	cv::adaptiveThreshold(grayImg, processedImg, maxValue,
			adaptiveMethod, thresholdType, blockSize, constant);
	return processedImg;
}

/**
 * \param grayImg Target image to be processed
 *
 * \returns Processed image
 *
 * \brief Applies a sobel operator to the target image.
 */
cv::Mat ImageProcessing::filterSobel(cv::Mat &grayImg) {
	cv::Mat processedImg = grayImg.clone();
	cv::Sobel(grayImg, processedImg, -1, 1, 1, 1);
	return processedImg;
}

/**
 * \param eyeImg Eye image where the circles are going to be drawn
 * \param centerX Component x of the circles' center
 * \param centerY Component y of the circles' center
 * \param diameter Diameter of the iris circle center
 *
 * \returns true if circles were drawn, false otherwise
 *
 * \brief Draws iris circles into eyeImg
 *
 * Draws iris circle and two other circles with the following diameters:
 * \f$\mbox{minPupilScale} \times \mbox{diameter}\f$ and \f$\mbox{maxPupilScale} \times \mbox{diameter}\f$,
 * where \f$\mbox{minPupilScale}\f$ and \f$\mbox{maxPupilScale}\f$ are defined in the UI
 */
bool ImageProcessing::drawIrisCircle(cv::Mat &eyeImg, int centerX, int centerY, int diameter, double minPupilScale, double maxPupilScale) {
	if(centerX <= 0 || centerX >= eyeImg.cols || centerY <= 0 || centerY >= eyeImg.rows) {
		return false;
	}

    cv::Scalar color = color::white;
    cv::Point center(centerX, centerY);
    cv::Size axes((int) diameter/2, (int) diameter/2);
    cv::Size axesMax((int) diameter*maxPupilScale/2, (int) diameter*maxPupilScale/2);
    cv::circle(eyeImg, center, diameter*minPupilScale/2, color, 2);
    cv::ellipse(eyeImg, center, axes, 0, 45, 195, color, 2);
    cv::ellipse(eyeImg, center, axes, 0, 225, 375, color, 2);
    cv::ellipse(eyeImg, center, axesMax, 0, -30, 90, color, 2);
    cv::ellipse(eyeImg, center, axesMax, 0, 150, 270, color, 2);

	return true;
}

void ImageProcessing::drawRedCircle(const cv::Mat &img, int centerX, int centerY, int diameter, int thickness) {
    cv::Mat dest; // This had to be done because img is a const ref (and we need it to be const)
    img.copyTo(dest);
    cv::circle(dest, cvPoint(centerX, centerY), diameter/2, color::red, thickness);
    dest.copyTo(img);
}

void ImageProcessing::drawPupil(cv::Mat &eyeImg, const cv::RotatedRect &pupilEllipse) {
    this->drawEllipse(eyeImg, pupilEllipse, color::blue, -1);
    int crossLength = std::min(eyeImg.size().width, eyeImg.size().height)/5;
    this->drawCross(eyeImg, pupilEllipse.center.x, pupilEllipse.center.y, color::white, crossLength, crossLength/5);
}

void ImageProcessing::drawGlint(cv::Mat &img, Point point, int radius) {
    cv::circle(img, point.asCVPoint(), radius, color::yellowgreen, -1);
    int crossLength = std::min(img.size().width, img.size().height)/15;
    this->drawCross(img, point.x, point.y, color::white, crossLength, crossLength/5);
}

void ImageProcessing::drawTarget(cv::Mat &img, Point target) {
    int length = std::min(img.size().width, img.size().height)/5;
    int innerLength = length/5;
    this->drawCross(img, target.x, target.y, color::orange, length, innerLength);
    this->drawCircle(img, target, innerLength + (length - innerLength)/4, color::orange, 2);
    this->drawCircle(img, target, innerLength + (length - innerLength)*3/4, color::orange, 2);
}

/**
 * \param img Pointer to the destiny image
 * \param p1 Line starting point
 * \param p2 Line ending point
 * \param color Line color
 *
 * \brief Draws a line from point p1 to point p2
 */
void ImageProcessing::drawLine(cv::Mat &img, Point p1, Point p2, cv::Scalar color, int thickness) {
    cv::line(img, p1.asCVPoint(), p2.asCVPoint(), color, thickness);
}

/**
 * \param img Destination image
 * \param intersectX Intersection point x coordinate
 * \param intersectY Intersection point y coordinate
 * \param color Cross color
 *
 * \returns true if cross has been drawn, false otherwise
 *
 * \brief Draws a cross with lines intersecting in \f$(\mbox{intersectX}, \mbox{intersectY})\f$
 */
bool ImageProcessing::drawCross(cv::Mat &img, int intersectX, int intersectY, cv::Scalar color, int length, int innerLength) {
    if(intersectX <= 0 || intersectX >= img.cols -1 || intersectY <= 0 || intersectY >= img.rows - 1) {
		return false;
    }

    int iniX, endX, iniY, endY;
    if(length <= 0) {
        iniX = 0;
        endX = img.cols - 1;
        iniY = 0;
        endY = img.rows - 1;
    }
    else {
        iniX = std::max(intersectX - length, 0);
        endX = std::min(intersectX + length, img.cols - 1);
        iniY = std::max(intersectY - length, 0);
        endY = std::min(intersectY + length, img.rows - 1);
    }
    int inLeftX = std::max(intersectX - innerLength, 0);
    int inRightX = std::min(intersectX + innerLength, img.cols - 1);
    int inUpY = std::max(intersectY - innerLength, 0);
    int inBotY = std::min(intersectY + innerLength, img.rows - 1);
    cv::line(img, cv::Point(intersectX, iniY), cv::Point(intersectX, inUpY), color, 2);
    cv::line(img, cv::Point(intersectX, inBotY), cv::Point(intersectX, endY), color, 2);
    cv::line(img, cv::Point(iniX, intersectY), cv::Point(inLeftX, intersectY), color, 2);
    cv::line(img, cv::Point(inRightX, intersectY), cv::Point(endX, intersectY), color, 2);
	return true;
}

/**
 * \param img Destination image
 * \param ellipse Ellipse to be drawn to image
 * \param color Ellipse color
 *
 * \brief Draws an ellipse
 */
void ImageProcessing::drawEllipse(cv::Mat &img, const cv::RotatedRect &ellipse, cv::Scalar color, int thickness) {
    cv::ellipse(img, ellipse, color, thickness);
}

/**
 * \param img Destination image
 * \param corner1 First corner of the rectangle
 * \param corner2 Second corner of the rectangle
 * \param color Rectangle color
 *
 * \brief Draws a rectangle that goes from corner1 to corner2
 */
void ImageProcessing::drawRectangle(cv::Mat &img, Point corner1, Point corner2, cv::Scalar color) {
    cv::rectangle(img, corner1.asCVPoint(), corner2.asCVPoint(), color);
}

/**
 * \param img Destination image
 * \param rectangle Rectangle to be drawn
 * \param color Rectangle color
 *
 * \brief Draws a rectangle that goes from corner1 to corner2
 */
void ImageProcessing::drawRectangle(cv::Mat &img, cv::Rect rectangle, cv::Scalar color) {
    cv::rectangle(img, rectangle, color);
}

/**
 * \param img Destination image
 * \param center Center of the rectangle
 * \param size Size of the rectangle
 *
 * \brief Draws a rectangle
 */
void ImageProcessing::drawRectangle(const cv::Mat &img, Point center, cv::Size2f size) {
    cv::Mat rectMat;
    img.copyTo(rectMat);
    rectMat.setTo(0);
    cv::Rect rect;
    rect.width = size.width;
    rect.height = size.height;
    rect.x = center.x - size.width/2;
    rect.y = center.y - size.height/2;
    cv::rectangle(rectMat, rect, cv::Scalar(0, 0, 255), CV_FILLED);
    cv::addWeighted(img, 1.0, rectMat, 1.0, 0.0, img);
}

/**
 * \param img Destination image
 * \param corners Rectangle 4 corners
 * \param expand Offset to be added to the rectangle
 * \param fill Fills the rectangle if true, and doesn't fill it otherwise
 * \param text Text to be drawn in the rectangle
 *
 * \brief Draws a rectangle
 */
void ImageProcessing::drawRectangle(const cv::Mat &img, std::vector<Point> corners, int expand, bool fill, std::string text) {
    if(corners.size() == 4) {
        Point cornerArray[4];
        for(int i = 0; i < 4; i++) {
            cornerArray[i] = corners[i];
        }
        this->drawRectangle(img, cornerArray, expand, fill, text);
    }
}

/**
 * \param img Destination image
 * \param corners Rectangle 4 corners
 * \param expand Offset to be added to the rectangle
 * \param fill Fills the rectangle if true, and doesn't fill it otherwise
 * \param text Text to be drawn in the rectangle
 *
 * \brief Draws a rectangle
 */
void ImageProcessing::drawRectangle(const cv::Mat &img, Point corners[4], int expand, bool fill, std::string text) {
    Point expandedCorners[4];
    for(int i = 0; i < 4; i++) {
        expandedCorners[i].x = corners[i].x;
        expandedCorners[i].y = corners[i].y;
    }
    if(expand != 0) {
        expandedCorners[0].x = expandedCorners[0].x - expand;
        expandedCorners[1].x = expandedCorners[1].x + expand;
        expandedCorners[2].x = expandedCorners[2].x + expand;
        expandedCorners[3].x = expandedCorners[3].x - expand;

        expandedCorners[0].y = expandedCorners[0].y - expand;
        expandedCorners[1].y = expandedCorners[0].y - expand;
        expandedCorners[2].y = expandedCorners[0].y + expand;
        expandedCorners[3].y = expandedCorners[0].y + expand;
    }

    Point center;
    cv::Point polyPoints[1][4];
    for(int i = 0; i < 4; i++) {
        polyPoints[0][i] = expandedCorners[i].asCVPoint();
        center.x = center.x + expandedCorners[i].x;
        center.y = center.y + expandedCorners[i].y;
    }
    center.x = center.x/4;
    center.y = center.y/4;

    const cv::Point *poly[1] = {polyPoints[0]};
    int nPoints[] = {4};
    cv::Mat rectImg;
    img.copyTo(rectImg);
    rectImg.setTo(0);
    if(fill) {
        cv::fillPoly(rectImg, poly, nPoints, 1, cv::Scalar(0, 0, 255));
    }
    else {
        cv::fillPoly(rectImg, poly, nPoints, 1, cv::Scalar(50, 50, 50));
    }
    cv::addWeighted(img, 1.0, rectImg, 1.0, 0.0, img);

    // Workaround because putText doesn't accept const Mat&
    cv::Mat copyImg;
    img.copyTo(copyImg);
    cv::putText(copyImg, text, center.asCVPoint(), CV_FONT_HERSHEY_PLAIN, 2, cv::Scalar(0, 255, 255));
    copyImg.copyTo(img);
}

void ImageProcessing::drawCircle(cv::Mat &img, Point center, int radius, cv::Scalar color, int thickness) {
    cv::circle(img, center.asCVPoint(), radius, color, thickness);
}

void ImageProcessing::drawPoint(const cv::Mat &img, Point point, int radius) {
    this->drawRedCircle(img, point.x, point.y, radius, -1);
}

/**
 * \param numbers Vector of numbers
 *
 * \returns Vector median value
 *
 * \brief Finds the mean value of the vector
 *
 * In case of even number of elements the median value is the mean of the two middle values
 */
float ImageProcessing::findMedian(std::vector<float> &numbers) {
	double median = 0;
    float middleValue = 0;
	double half = 0;

	if(numbers.empty()) {
		return 0;
	}

	std::sort(numbers.begin(), numbers.end());
	half = (numbers.size() - 1.0) / 2;

	if (numbers.size() % 2 == 1) {
		median = numbers.at(std::ceil(half));
	}
	else {
		middleValue = std::floor(half);
		median = (double)(numbers.at(middleValue) + numbers.at(middleValue + 1)) / 2;
	}

	return median;
}

/**
 * \param points Data to fit ellipse
 *
 * \returns The best fitting ellipse according to the least square method
 *
 * \brief Fits an ellipse to the data
 */
cv::RotatedRect ImageProcessing::ellipseLeastSquareFitting(std::vector<cv::Point> points) {
	return cv::fitEllipse(points);
}

/**
 * \param image Image for which the ROI Rect will be created
 * \param x ROI x value (if x < 0, 0 will be used)
 * \param y ROI y value (if y < 0, 0 will be used)
 * \param width ROI width (will be cropped if it doesn't fit the image)
 * \param height ROI height (will be cropped if it doesn't fit the image)
 *
 * \returns ROI Rect with the given params or a cropped one if dimensions are beyond image boundaries
 *
 * \brief Creates a ROI Rect with the given params
 *
 * If any param exceeds the image boundaries the Rect will be cropped to fit it.
 */
cv::Rect ImageProcessing::createROIForSize(cv::Size size, int x, int y, int width, int height) {
	if(x < 0) {
		width += x;
		x = 0;
	}
	if(y < 0) {
		height += y;
		y = 0;
	}
    width = std::min(width, size.width - x);
    height = std::min(height, size.height - y);
    if(width <= 0 || height <= 0) {
        width = size.width;
        height = size.height;
        x = 0;
        y = 0;
    }

	return cv::Rect(x, y, width, height);
}

/**
 * \param BGR image
 *
 * \returns The grayscale image
 *
 * \brief Converts BGR image to grayscale image
 */
cv::Mat ImageProcessing::bgr2gray(const cv::Mat &image) {
    cv::Mat grayImg;

    image.copyTo(grayImg);
    if(image.channels() < 3) return grayImg;
    cv::cvtColor(image, grayImg, cv::COLOR_BGR2GRAY);
    return grayImg;
}

/**
 * \param Grayscale image
 *
 * \returns The BGR image
 *
 * \brief Converts grayscale image to BGR image
 */
cv::Mat ImageProcessing::gray2bgr(const cv::Mat &image) {
    cv::Mat bgrImg;

    image.copyTo(bgrImg);
    if(image.channels() == 3) return bgrImg;
    cv::cvtColor(image, bgrImg, cv::COLOR_GRAY2BGR);
    return bgrImg;
}

/**
 * \param BGR image
 *
 * \returns The HSV image
 *
 * \brief Converts BGR image to HSV image
 */
cv::Mat ImageProcessing::bgr2hsv(const cv::Mat &image) {
    cv::Mat hsvImg;
    cv::cvtColor(image, hsvImg, cv::COLOR_BGR2HSV);
    return hsvImg;
}

/**
 * \param center Point in the center of the ROI
 * \param radius ROI radius
 * \param imgSize Full image size
 *
 * \returns The ROI Rect adjusted to the image limits
 *
 * \brief Calculates a ROI Rect that fits the image limits
 */
cv::Rect ImageProcessing::calcROI(Point center, double radius, cv::Size imgSize) {
    int x = int(center.x - radius);
    int y = int(center.y - radius);
    int w = int(2*radius);
    int h = int(2*radius);
    return createROIForSize(imgSize, x, y, w, h);
}

} /* namespace haytham */
