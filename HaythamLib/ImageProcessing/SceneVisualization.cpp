#include <QDebug>

#include "SceneVisualization.h"
#include "ImageProcessing.h"
#include "Colors.h"

namespace haytham {

SceneVisualization::SceneVisualization(std::string name) : name(name)
{
}

std::string SceneVisualization::getName() {
    return this->name;
}

/************* Scene visualization with gaze target *****************/

SceneVisualizationPOR::SceneVisualizationPOR() : SceneVisualization("Draw point of regard")
{
}

cv::Mat& SceneVisualizationPOR::process(cv::Mat &sceneImage, Point gaze) {
    ImageProcessing processor;
    processor.drawTarget(sceneImage, gaze);
    return sceneImage;
}

/************* Scene visualization with gaussian blur *****************/

SceneVisualizationGaussian::SceneVisualizationGaussian() : SceneVisualization("Peripheral vision blur")
{
}

SceneVisualizationGaussian::SceneVisualizationGaussian(std::string name) : SceneVisualization(name)
{
}

cv::Mat& SceneVisualizationGaussian::process(cv::Mat &sceneImage, Point gaze) {
    cv::Mat weights = this->computeWeights(sceneImage, gaze);
    cv::Mat filteredImg = this->getFilteredImg(sceneImage);

    cv::Mat sceneImgF = cv::Mat(sceneImage.size(), CV_32FC3);
    sceneImage.convertTo(sceneImgF, CV_32F, 1/255.0);
    cv::multiply(sceneImgF, weights, sceneImgF);
    cv::subtract(1, weights, weights);
    cv::multiply(filteredImg, weights, filteredImg);
    cv::add(sceneImgF, filteredImg, sceneImgF);
    sceneImgF.convertTo(sceneImage, sceneImage.type(), 255);
    return sceneImage;
}

cv::Mat SceneVisualizationGaussian::computeWeights(cv::Mat img, Point gaze) {
    int w = img.size().width;
    int h = img.size().height;

    cv::Mat weights = this->gaussianMat.getMat(w, h, gaze.x, gaze.y);

    cv::multiply(weights, 0.7, weights);
    cv::add(weights, 0.3, weights);

    return weights;
}

cv::Mat SceneVisualizationGaussian::getFilteredImg(cv::Mat &img) {
    int w = img.size().width;
    int h = img.size().height;
    if(this->grayImg.empty() || this->grayImg.size().width != w || this->grayImg.size().height != h) {
        this->grayImg = cv::Mat(h, w, CV_32FC3, cv::Scalar(0.5f,0.5f,0.5f));
    }
    return this->grayImg;
}

/************* Scene visualization with cumulative gaussian blur (that considers past gaze points) *****************/

SceneVisualizationGaussianCumulative::SceneVisualizationGaussianCumulative() : SceneVisualizationGaussian("Cumulative peripheral vision blur"), weightsBuf(10) {
    this->coefs[9] = 0.03305785;
    this->coefs[8] = 0.07438017;
    this->coefs[7] = 0.1322314;
    this->coefs[6] = 0.20661157;
    this->coefs[5] = 0.29752066;
    this->coefs[4] = 0.40495868;
    this->coefs[3] = 0.52892562;
    this->coefs[2] = 0.66942149;
    this->coefs[1] = 0.82644628;
    this->coefs[0] = 1.;
}

cv::Mat SceneVisualizationGaussianCumulative::computeWeights(cv::Mat img, Point gaze) {
    int w = img.size().width;
    int h = img.size().height;

    cv::Mat newWeight = this->gaussianMat.getMat(w, h, gaze.x, gaze.y);
    this->weightsBuf.add(newWeight);

    cv::Mat weights = cv::Mat(newWeight.size(), newWeight.type());

    CircularBuffer<cv::Mat>::CircularBufferIterator it = this->weightsBuf.iterator();

    for(int i = 0; it.hasNext(); it = it.next()) {
        cv::Mat w = it.val();
        weights += w/this->weightsBuf.size()*this->coefs[i];
        i++;
    }

    double maxVal, minVal;
    cv::minMaxLoc(weights, &minVal, &maxVal);
    weights /= maxVal;

    cv::multiply(weights, 0.9, weights);
    cv::add(weights, 0.1, weights);

    return weights;
}

/************* Scene visualization with path *****************/

SceneVisualizationPath::SceneVisualizationPath() : SceneVisualization("Draw path (NO fixation detection)"), history(30)
{
}

cv::Mat& SceneVisualizationPath::process(cv::Mat &sceneImage, Point gaze) {
    ImageProcessing processor;
    this->history.add(gaze);
    CircularBuffer<Point>::CircularBufferIterator it = this->history.iterator();
    for(; it.next().hasNext(); it = it.next()) {
        Point cur = it.val();
        Point next = it.next().val();
        processor.drawLine(sceneImage, cur, next, color::red, 2);
    }

    return sceneImage;
}

} // namespace haytham
