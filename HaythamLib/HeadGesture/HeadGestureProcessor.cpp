/*
 * HeadGestureProcessor.cpp
 *
 *  Created on: 18/03/2013
 *      Author: Andrew Kurauchi
 */

#include "HeadGestureProcessor.h"

namespace haytham {

HeadGestureProcessor::HeadGestureProcessor() : lastUpdate(-1000), maximumVelocity(12), minimumVelocity(2) {
}

HeadGesture HeadGestureProcessor::process(BinocularGazeData data) {
    HeadGesture gesture;

    if(this->timer.now() - this->lastUpdate <= 500) {
        if(data.getRightData() != GazeData()) {
            Point direction = data.getRightData().getPupilCenter() - this->lastData.getRightData().getPupilCenter();
            gesture = this->gestureFromDirection(direction);
        }
    }
    this->lastUpdate = this->timer.now();
    this->lastData = data;

    return gesture;
}

HeadGesture HeadGestureProcessor::detect(std::vector<BinocularGazeData> data) {
    HeadGesture gesture;
    int horizontal = 0;
    int vertical = 0;

    int n = data.size();
    if(n <= 0) return gesture;
    for(int i = 0; i < n; i++) {
        horizontal += data[i].getGesture().horizontal;
        vertical += data[i].getGesture().vertical;
    }
    int horizontalAbs = abs(horizontal);
    int verticalAbs = abs(vertical);
    if(horizontalAbs > 3*n/4) gesture.horizontal = horizontal/horizontalAbs;
    if(verticalAbs > 3*n/4) gesture.vertical = vertical/verticalAbs;

    return gesture;
}

HeadGesture HeadGestureProcessor::gestureFromDirection(Point direction) {
    HeadGesture gesture;
    double segmentVelocity = direction.norm();

    if(segmentVelocity >= this->minimumVelocity && segmentVelocity <= this->maximumVelocity) {
        // direction won't be (0,0) here, so we don't have to worry about that
        float absX = abs(direction.x);
        float absY = abs(direction.y);

        if(absX >= absY) {
            gesture.horizontal = (direction.x > 0 ? HeadGesture::RIGHT : HeadGesture::LEFT);
            if(absY/absX >= 0.3) gesture.vertical = (direction.y > 0 ? HeadGesture::UP : HeadGesture::DOWN);
        }
        else {
            gesture.vertical = (direction.y > 0 ? HeadGesture::UP : HeadGesture::DOWN);
            if(absX/absY >= 0.3) gesture.horizontal = (direction.x > 0 ? HeadGesture::RIGHT : HeadGesture::LEFT);
        }
    }
    if(segmentVelocity >= 2*(this->maximumVelocity-this->minimumVelocity)/3) {
        gesture.horizontal *= 3;
        gesture.vertical *= 3;
    }
    else if(segmentVelocity >= (this->maximumVelocity-this->minimumVelocity)/3) {
        gesture.horizontal *= 2;
        gesture.vertical *= 2;
    }

    return gesture;
}

} // namespace haytham
