/*
 * HeadGesture.cpp
 *
 *  Created on: 18/03/2013
 *      Author: Andrew Kurauchi
 */

#include <sstream>

#include "HeadGesture.h"

namespace haytham {

const int HeadGesture::UP = 1;
const int HeadGesture::DOWN = -1;
const int HeadGesture::LEFT = -1;
const int HeadGesture::RIGHT = 1;
const int HeadGesture::NEUTRAL = 0;

HeadGesture::HeadGesture() : vertical(NEUTRAL), horizontal(NEUTRAL) {
}

bool HeadGesture::occurred() {
    return this->vertical != NEUTRAL || this->horizontal != NEUTRAL;
}

std::string HeadGesture::asCsvString() {
    std::stringstream csv;
    csv << "g, " << this->timestamp << ", " << this->horizontal << ", " << this->vertical << std::endl;
    return csv.str();
}

bool HeadGesture::operator ==(HeadGesture other) {
    return this->horizontal == other.horizontal && this->vertical == other.vertical;
}

bool HeadGesture::operator !=(HeadGesture other) {
    return !(*this == other);
}

} // namespace haytham
