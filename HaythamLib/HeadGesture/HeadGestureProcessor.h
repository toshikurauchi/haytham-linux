/*
 * HeadGestureProcessor.h
 *
 *  Created on: 18/03/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HEADGESTUREPROCESSOR_H
#define HEADGESTUREPROCESSOR_H

#include <vector>

#include "HeadGesture.h"
#include "Common/Timer.h"
#include "EyeDetection/GazeData.h"
#include "Common/Point.h"

namespace haytham {

class HeadGestureProcessor
{
public:
    HeadGestureProcessor();
    HeadGesture process(BinocularGazeData data);
    HeadGesture detect(std::vector<BinocularGazeData> data);
private:
    Timer timer;
    double lastUpdate;
    BinocularGazeData lastData;
    int maximumVelocity;
    int minimumVelocity;

    HeadGesture gestureFromDirection(Point direction);
};

} // namespace haytham

#endif // HEADGESTUREPROCESSOR_H
