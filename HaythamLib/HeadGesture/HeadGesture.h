/*
 * HeadGesture.h
 *
 *  Created on: 18/03/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_HEADGESTURE_H
#define HAYTHAM_HEADGESTURE_H

#include <string>

namespace haytham {

struct HeadGesture
{
    HeadGesture();
    bool occurred();

    int vertical;
    int horizontal;
    double timestamp;

    static const int UP;
    static const int DOWN;
    static const int LEFT;
    static const int RIGHT;
    static const int NEUTRAL;

    std::string asCsvString();

    bool operator==(HeadGesture other);
    bool operator!=(HeadGesture other);
};

} // namespace haytham

#endif // HAYTHAM_HEADGESTURE_H
