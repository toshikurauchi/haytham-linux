/*
 * VRPNGazeServer.h
 *
 *  Created on: 06/03/2013
 *      Author: Andrew Kurauchi
 */

#ifndef VRPNGAZESERVER_H
#define VRPNGAZESERVER_H

#include "VRPNGazeTracker.h"
#include "Common/Point.h"

namespace haytham {

class GazeSharer
{
public:
    GazeSharer(std::string deviceName = "haytham");
    virtual ~GazeSharer();

    // please call this on each frame of your app
    virtual void shareGazeData(BinocularGazeData binocularData);
protected:
    std::string _deviceName;
    vrpn_GazeTracker* pupilTracker;
    vrpn_GazeTracker* eyeFeatureTracker;
    vrpn_GazeTracker* sceneGazeTracker;
    vrpn_GazeTracker* monitorGazeTracker;
    static vrpn_Connection* _connection;
    static int _refCount;

    // utils
    void _sharePoint(vrpn_GazeTracker* tracker, Point point, bool pupilFound);
};

} // namespace haytham

#endif // VRPNGAZESERVER_H
