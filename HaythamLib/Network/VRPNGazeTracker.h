/*
 * VRPNServer.h
 *
 *  Created on: 06/03/2013
 *      Author: Andrew Kurauchi
 */

#ifndef VRPNSERVER_H
#define VRPNSERVER_H

#include <string>
#include <ctime>
#include <vrpn_Tracker.h>

#include "Common/Point.h"
#include "EyeDetection/GazeData.h"

namespace haytham {

class vrpn_GazeTracker : public vrpn_Tracker_Server {
public:
    vrpn_GazeTracker(vrpn_Connection* conn, const char* trackerName);
    virtual ~vrpn_GazeTracker();

    virtual void update(Point point, bool pupilFound);
    virtual void mainloop();

private:
    Point point;
    bool pupilFound;
    timeval _timestamp;
    std::string _trackerName;
};

} // namespace haytham

#endif // VRPNSERVER_H
