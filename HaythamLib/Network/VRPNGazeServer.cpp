/*
 * VRPNGazeServer.cpp
 *
 *  Created on: 06/03/2013
 *      Author: Andrew Kurauchi
 */

#include "VRPNGazeServer.h"

namespace haytham {

// to which port should we connect???
#define VRPN_DEFAULT_PORT 3893

vrpn_Connection* GazeSharer::_connection = 0;
int GazeSharer::_refCount = 0;

GazeSharer::GazeSharer(std::string deviceName) : _deviceName(deviceName) {
    if(!_connection)
        _connection = vrpn_create_server_connection(VRPN_DEFAULT_PORT); //new vrpn_Connection_IP(VRPN_DEFAULT_PORT);
    this->pupilTracker = new vrpn_GazeTracker(_connection, "haytham.pupil");
    this->eyeFeatureTracker = new vrpn_GazeTracker(_connection, "haytham.eye_feature");
    this->sceneGazeTracker = new vrpn_GazeTracker(_connection, "haytham.scene_gaze");
    this->monitorGazeTracker = new vrpn_GazeTracker(_connection, "haytham.monitor_gaze");
    _refCount++;
}

GazeSharer::~GazeSharer() {
    delete this->pupilTracker;
    delete this->eyeFeatureTracker;
    delete this->sceneGazeTracker;
    delete this->monitorGazeTracker;

    if(0 == --_refCount) {
        delete _connection;
        _connection = 0;
    }
}

void GazeSharer::shareGazeData(BinocularGazeData binocularData) {
    GazeData data = binocularData.getMeanData();
    this->_sharePoint(this->pupilTracker, data.getPupilCenter(), data.isPupilFound());
    if(data.getUsePupilGlint()) {
        this->_sharePoint(this->eyeFeatureTracker, data.getPupilGlintVector(), data.isPupilFound());
    }
    else {
        this->_sharePoint(this->eyeFeatureTracker, data.getPupilCenter(), data.isPupilFound());
    }
    this->_sharePoint(this->sceneGazeTracker, data.getGazePosition(), data.isPupilFound());
    this->_sharePoint(this->monitorGazeTracker, data.getMonitorGazePosition(), data.isPupilFound());

    _connection->mainloop();
}

void GazeSharer::_sharePoint(vrpn_GazeTracker *tracker, Point point, bool pupilFound) {
    tracker->update(point, pupilFound);
    tracker->mainloop();
}

} // namespace haytham
