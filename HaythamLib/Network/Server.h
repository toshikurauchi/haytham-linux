/*
 * Server.h
 *
 *  Created on: 05/11/2012
 *      Author: Andrew Kurauchi
 */

#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QStringList>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QNetworkSession>
#include <vector>

#include "EyeDetection/GazeData.h"
#include "HeadGesture/HeadGesture.h"
#include "VRPNGazeServer.h"
#include "Common/Point.h"

namespace haytham {

class METState;

class Server : public QTcpServer
{
    Q_OBJECT

public:
    Server(METState *state, QObject *parent = 0);
    const QStringList& getSentMessages();
    bool listen();
    void sendGazeData(BinocularGazeData &data);
    void sendHeadGesture(HeadGesture &gesture);
signals:
    void sendMessage(QString message);
private slots:
    void readyRead();
    void disconnected();
protected:
    void incomingConnection(int);

private:
    QStringList sentMessages;
    METState *state;
    std::vector<QTcpSocket *> gazeDataStreamClients;
    GazeSharer vrpnSharer;
    std::vector<Point> calibrationEyePoints;
    std::vector<Point> calibrationScenePoints;

    void sendAndStoreMessage(QString message);
    // Recording
    void recordData(QString header, QTcpSocket *client);
    void stopRecordingData();
    // Streaming
    void streamData(QString header, QTcpSocket *client);
    void stopStreamingData(QTcpSocket *client);
    // Monitor
    void bindMonitor(QString header);
    void unbindMonitor();
    // Calibration
    void addCalibrationPoint(QString header);
    void clearCalibrationPoints();

    // Commands
    static const char RECORD;
    static const char STOP_RECORDING;
    static const char STREAM;
    static const char STOP_STREAM;
    static const char BIND_MONITOR;
    static const char UNBIND_MONITOR;
    static const char CALIBRATION_POINT;
    static const char CLEAR_CALIBRATION_POINTS;

    // Arguments
    static const char EYE_DATA;
    static const char EYE_VIDEO;
    static const char SCENE_VIDEO;
};

} /* namespace haytham */

#endif // SERVER_H
