/*
 * Server.cpp
 *
 *  Created on: 05/11/2012
 *      Author: Andrew Kurauchi
 *
 * Original code from:
 *      http://stackoverflow.com/questions/9714913/qtcpsocket-does-not-send-data-sometimes
 */

#include <QtNetwork>
#include <sstream>

#include "Server.h"
#include "METState.h"
#include "Monitor.h"
#include "Calibration.h"

namespace haytham {

// TODO Documentation!
// Commands
const char Server::RECORD = 'R';
const char Server::STOP_RECORDING = 'r';
const char Server::STREAM = 'S';
const char Server::STOP_STREAM = 's';
const char Server::BIND_MONITOR = 'M';
const char Server::UNBIND_MONITOR = 'm';
const char Server::CALIBRATION_POINT = 'C';
const char Server::CLEAR_CALIBRATION_POINTS = 'c';

// Arguments
const char Server::EYE_DATA = 'D';
const char Server::EYE_VIDEO = 'V';
const char Server::SCENE_VIDEO = 'S';

#define SERVER_DEFAULT_PORT 3894

Server::Server(METState *state, QObject *parent) : QTcpServer(parent), state(state) {
}

bool Server::listen() {
    QString ipAddress;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // use the first non-localhost IPv4 address
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
            ipAddressesList.at(i).toIPv4Address()) {
            ipAddress = ipAddressesList.at(i).toString();
            break;
        }
    }
    // if we did not find one, use IPv4 localhost
    if (ipAddress.isEmpty())
        ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
    bool success = QTcpServer::listen(QHostAddress(ipAddress), SERVER_DEFAULT_PORT);
    if(success) {
        sendAndStoreMessage(tr("Server running on:\n    IP: %1\n    port: %2\n").arg(this->serverAddress().toString()).arg(this->serverPort()));
    }
    else {
        sendAndStoreMessage(tr("Could not start server\n"));
    }
    return success;
}

void Server::sendGazeData(BinocularGazeData &data) {
    for(std::vector<QTcpSocket*>::iterator it = this->gazeDataStreamClients.begin(); it != this->gazeDataStreamClients.end(); it++) {
        if((*it)->state() == QAbstractSocket::ConnectedState) {
            for(int left = 0; left <= this->state->getUseBinocular(); left++) {
                if(left)
                    (*it)->write(data.getLeftData().asOneLineCsvString().c_str());
                else
                    (*it)->write(data.getRightData().asOneLineCsvString().c_str());
            }

            (*it)->waitForBytesWritten();
        }
    }
    this->vrpnSharer.shareGazeData(data);
}

void Server::sendHeadGesture(HeadGesture &gesture) {
    for(std::vector<QTcpSocket*>::iterator it = this->gazeDataStreamClients.begin(); it != this->gazeDataStreamClients.end(); it++) {
        if((*it)->state() == QAbstractSocket::ConnectedState) {
            (*it)->write(gesture.asCsvString().c_str());
            (*it)->waitForBytesWritten();
        }
    }
}

const QStringList& Server::getSentMessages() {
    return this->sentMessages;
}

void Server::incomingConnection(int socketfd) {
    // TODO Maybe we have to create the socket in another thread
    // http://harmattan-dev.nokia.com/docs/library/html/qt4/qtcpserver.html#incomingConnection
    QTcpSocket *client = new QTcpSocket(this);
    client->setSocketDescriptor(socketfd);

    connect(client, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(client, SIGNAL(disconnected()), this, SLOT(disconnected()));
}

void Server::disconnected() {
    QTcpSocket *client = (QTcpSocket*) sender();

    qDebug() << " INFO : " << QDateTime::currentDateTime()
            << " : CLIENT DISCONNECTED " << client->peerAddress().toString();
}

void Server::readyRead() {
    QTcpSocket *client = (QTcpSocket*) sender();

    if (client->canReadLine()) {
        QString line = QString::fromUtf8(client->readLine()).trimmed();
        if(line.startsWith(RECORD)) {
            this->recordData(line.right(line.size()-1), client);
        }
        else if(line.startsWith(STOP_RECORDING)) {
            this->stopRecordingData();
        }
        else if(line.startsWith(STREAM)) {
            this->streamData(line.right(line.size()-1), client);
        }
        else if(line.startsWith(STOP_STREAM)) {
            this->stopStreamingData(client);
        }
        else if(line.startsWith(BIND_MONITOR)) {
            this->bindMonitor(line.right(line.size()-1));
        }
        else if(line.startsWith(UNBIND_MONITOR)) {
            this->unbindMonitor();
        }
        else if(line.startsWith(CALIBRATION_POINT)) {
            this->addCalibrationPoint(line.right(line.size()-1));
        }
        else if(line.startsWith(CLEAR_CALIBRATION_POINTS)) {
            this->clearCalibrationPoints();
        }
    }
}

void Server::sendAndStoreMessage(QString message) {
    emit(sendMessage(message));
    this->sentMessages << message;
}

void Server::recordData(QString header, QTcpSocket *client) {
    int dataId = 1;
    QString gazeDataFilename, eyeVideoFilename, sceneVideoFilename;
    while (client->canReadLine()) {
        if(header.at(dataId) == QChar(EYE_DATA)) {
            gazeDataFilename = QString::fromUtf8(client->readLine()).trimmed();
            sendAndStoreMessage(tr("Eye data filename received: ").append(gazeDataFilename));
        }
        else if(header.at(dataId) == QChar(EYE_VIDEO)) {
            eyeVideoFilename = QString::fromUtf8(client->readLine()).trimmed();
            sendAndStoreMessage(tr("Eye Video filename received: ").append(eyeVideoFilename));
        }
        else if(header.at(dataId) == QChar(SCENE_VIDEO)) {
            sceneVideoFilename = QString::fromUtf8(client->readLine()).trimmed();
            sendAndStoreMessage(tr("Scene Video filename received: ").append(sceneVideoFilename));
        }
        dataId++;
    }
    this->state->getRecordManager().startRecording(eyeVideoFilename, sceneVideoFilename, gazeDataFilename);
    sendAndStoreMessage(tr("Started recording"));
}

void Server::stopRecordingData() {
    this->state->getRecordManager().stopRecording();
    sendAndStoreMessage(tr("Stopped recording"));
}

void Server::streamData(QString header, QTcpSocket *client) {
    for(int i = 0; i < header.size(); i++) {
        if(header.at(i) == QChar(EYE_DATA)) {
            this->gazeDataStreamClients.push_back(client);
            sendAndStoreMessage(tr("Streaming eye data"));
        }
    }
    sendAndStoreMessage(tr("Started streaming"));
}

void Server::stopStreamingData(QTcpSocket *client) {
    std::remove(this->gazeDataStreamClients.begin(), this->gazeDataStreamClients.end(), client);
    sendAndStoreMessage(tr("Stopped streaming"));
}

void Server::bindMonitor(QString header) {
    int width, height;
    QStringList dimensionStrings = header.split(' ').filter(QRegExp("\\d+"));
    width = dimensionStrings.at(0).toInt();
    height = dimensionStrings.at(1).toInt();
    if(width != 0 && height != 0)
        this->state->setActiveMonitor(Monitor(width, height));
}

void Server::unbindMonitor() {
    this->state->setActiveMonitor(Monitor());
}

void Server::addCalibrationPoint(QString header) {
    QStringList values = header.split(" ");
    float numVals[4];
    int count = 0;
    if(values.count() > 4) {
        for(int i = 0; i < values.count() && count < 4; i++) {
            if(values.at(i).length() > 0) {
                numVals[count++] = values.at(i).toFloat();
            }
        }
    }
    if(count == 4) {
        this->calibrationEyePoints.push_back(Point(numVals[0], numVals[1]));
        this->calibrationScenePoints.push_back(Point(numVals[2], numVals[3]));
        std::stringstream message;
        message << "Calibration point added: eye(" << numVals[0] << "," << numVals[1] << ") scene(" << numVals[2] << "," << numVals[3] << ")";
        sendAndStoreMessage(tr(message.str().c_str()));
    }
    else {
        sendAndStoreMessage(tr("Could not add calibration point"));
    }
    for(int left = 0; left <= this->state->getUseBinocular(); left++) {
        Calibration *calibration;
        if(left) {
            calibration = this->state->getLeftEyeToSceneMapping();
        }
        else {
            calibration = this->state->getRightEyeToSceneMapping();
        }
        if(this->calibrationEyePoints.size() == calibration->getTotalPoints()) {
            calibration->startCalibrating();
            for(int i = 0; i < this->calibrationEyePoints.size(); i++) {
                calibration->addPoint(this->calibrationEyePoints.at(i), this->calibrationScenePoints.at(i));
            }
            this->calibrationEyePoints.erase(this->calibrationEyePoints.begin(), this->calibrationEyePoints.end());
            this->calibrationScenePoints.erase(this->calibrationScenePoints.begin(), this->calibrationScenePoints.end());
            calibration->calibrate();
            sendAndStoreMessage(tr("Calibrated"));
        }
    }
}

void Server::clearCalibrationPoints() {
    this->calibrationEyePoints.erase(this->calibrationEyePoints.begin(), this->calibrationEyePoints.end());
    this->calibrationScenePoints.erase(this->calibrationScenePoints.begin(), this->calibrationScenePoints.end());
}

} /* namespace haytham */
