/*
 * VRPNServer.cpp
 *
 *  Created on: 06/03/2013
 *      Author: Andrew Kurauchi
 */

#include <iostream>

#include "VRPNGazeTracker.h"

namespace haytham {

vrpn_GazeTracker::vrpn_GazeTracker(vrpn_Connection *conn, const char *trackerName) : vrpn_Tracker_Server(trackerName, conn, 1), _trackerName(trackerName)
{
    std::cerr << "[vrpn] initializing '" << trackerName << "'..." << std::endl;
    _timestamp.tv_sec = _timestamp.tv_usec = 0;
}

vrpn_GazeTracker::~vrpn_GazeTracker()
{
}

void vrpn_GazeTracker::update(Point point, bool pupilFound)
{
    this->point = point;
    this->pupilFound = pupilFound;
    vrpn_gettimeofday(&_timestamp, NULL);
}

void vrpn_GazeTracker::mainloop()
{
    vrpn_float64 pos[3] = { this->point.x, this->point.y, 0 };
    vrpn_float64 quat[4] = { this->pupilFound, 0.0, 0.0, 1.0 };

    if(report_pose(0, _timestamp, pos, quat) != 0)
        std::cerr << "[vrpn] vrpn_GazeTracker: an error occured in report_pose()" << std::endl;

    server_mainloop();
}

} // namespace haytham
