#include <boost/python.hpp>

#include "PyExtractor.h"

namespace haytham {

void* PyExtractor::extract_tuple(PyObject* obj) {
    void *retVal = PyTuple_Check(obj) ? obj : 0;
    return retVal;
}

} // namespace haytham
