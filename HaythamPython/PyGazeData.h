/*
 * PyGazeData.h
 *
 *  Created on: 17/12/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_PYGAZEDATA_H
#define HAYTHAM_PYGAZEDATA_H

namespace haytham {

struct PyGazeData {
    static void registerPyGazeData();
};

} // namespace haytham

#endif // HAYTHAM_PYGAZEDATA_H
