/*
 * PySceneData.h
 *
 *  Created on: 20/12/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_PYSCENEDATA_H
#define HAYTHAM_PYSCENEDATA_H

namespace haytham {

struct PySceneData
{
    static void registerPySceneData();
};

} // namespace haytham

#endif // HAYTHAM_PYSCENEDATA_H
