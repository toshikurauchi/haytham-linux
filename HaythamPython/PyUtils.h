/*
 * PyUtils.h
 *
 *  Created on: 18/12/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_PYUTILS_H
#define HAYTHAM_PYUTILS_H

#include <vector>

namespace haytham {

struct PyUtils
{
    static void registerUtils();
};

} // namespace haytham

#endif // HAYTHAM_PYUTILS_H
