/*
 * PyCalibrationPointsDetection.cpp
 *
 *  Created on: 17/01/2014
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>

#include "PyCalibrationPointsDetection.h"
#include "Calibration/CalibrationPointsDetection.h"

namespace haytham {
using namespace boost::python;

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findPointsWithDest,    CalibrationPointsDetection::findPoints, 4, 5)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findPointsWithoutDest, CalibrationPointsDetection::findPoints, 3, 3) // Qt Creator says there is an error here, but it's ok

void PyCalibrationPointsDetection::registerPyCalibrationPointsDetection() {
    class_<CalibrationPointsDetection>("CalibrationPointsDetection")
            .def("findPoints", static_cast<std::vector<Blob>(CalibrationPointsDetection::*)
                 (cv::Mat, cv::Size, int, const cv::Mat&, Calibration*)>(&CalibrationPointsDetection::findPoints),
                 findPointsWithDest())
            .def("findPoints", static_cast<std::vector<Blob>(CalibrationPointsDetection::*)
                 (cv::Mat, cv::Size, int, Calibration*)>(&CalibrationPointsDetection::findPoints),
                 findPointsWithoutDest());
}

} // namespace haytham
