/*
 * PyMonitorDetector.cpp
 *
 *  Created on: 19/12/2013
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>

#include "PyMonitorDetector.h"
#include "MonitorDetection/MonitorDetector.h"

namespace haytham {
using namespace boost::python;

struct MonitorDetectorWrap : MonitorDetector, wrapper<MonitorDetector> {
    bool detectScreen(const cv::Mat &sceneImage, int threshold, float screenMinSize) {
        return this->get_override("detectScreen")(sceneImage, threshold, screenMinSize);
    }
};

void PyMonitorDetector::registerPyMonitorDetector() {
    class_<MonitorDetectorWrap, boost::noncopyable>("MonitorDetector", no_init)
            .def("detectScreen", &MonitorDetector::detectScreen)
            .def("drawScreen", &MonitorDetector::drawScreen)
            .def("filteredGrayImg", &MonitorDetector::getFilteredGrayImg, return_value_policy<return_by_value>())
            .def("methodName", &MonitorDetector::getMethodName, return_value_policy<return_by_value>())
            .def("screenCorners", &MonitorDetector::getScreenCorners, return_value_policy<return_by_value>());
    class_<ContrastMonitorDetector, bases<MonitorDetector> >("ContrastMonitorDetector");
    class_<LedMonitorDetector, bases<MonitorDetector> >("LedMonitorDetector");
}

} // namespace haytham
