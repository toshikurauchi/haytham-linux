/*
 * PyPoint.cpp
 *
 *  Created on: 13/12/2013
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>

#include "PyPoint.h"
#include "PyExtractor.h"

namespace haytham {
using namespace boost::python;

void pointSetItem(object self, object key, object val) {
    Point& point = extract<Point&>(self);
    int i = extract<int>(key);
    double v = extract<double>(val);
    point[i] = v;
}

void PyPoint::registerPyPoint() {
    class_<Point>("Point")
            .def(init<double, double>())
            .def(self_ns::str(self_ns::self))
            .def("norm", &Point::norm)
            .def_readwrite("x", &Point::x)
            .def_readwrite("y", &Point::y)
            .def(self + self)
            .def(self - self)
            .def(self += self)
            .def(self -= self)
            .def(self == self)
            .def(self != self)
            .def(self_ns::str(self_ns::self))
            .def("__getitem__", &Point::operator[], boost::python::arg("index"), return_value_policy<return_by_value>())
            .def("__setitem__", &pointSetItem, boost::python::arg("index"));

    boost::python::converter::registry::push_back(
                &PyExtractor::extract_tuple,
                &construct_point,
                boost::python::type_id<Point>());
}

void PyPoint::construct_point(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data) {
    void *storage = ((boost::python::converter::rvalue_from_python_storage<Point>*) data)->storage.bytes;
    Point point;

    if(obj_ptr && obj_ptr != Py_None) {
        if(!!PyComplex_CheckExact(obj_ptr)) {
            Py_complex c = PyComplex_AsCComplex(obj_ptr);
            point.x = c.real;
            point.y = c.imag;
        }
        else {
            PyArg_ParseTuple(obj_ptr, "dd", &point.x, &point.y);
        }
    }

    new (storage) Point(point.x, point.y);
    data->convertible = storage;
}

} // namespace haytham
