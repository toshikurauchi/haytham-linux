/*
 * PyCalibrationPointsDetection.h
 *
 *  Created on: 17/01/2014
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_PYCALIBRATIONPOINTSDETECTION_H
#define HAYTHAM_PYCALIBRATIONPOINTSDETECTION_H

namespace haytham {

struct PyCalibrationPointsDetection
{
    static void registerPyCalibrationPointsDetection();
};

} // namespace haytham

#endif // HAYTHAM_PYCALIBRATIONPOINTSDETECTION_H
