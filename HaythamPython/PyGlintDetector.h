/*
 * PyGlintDetector.h
 *
 *  Created on: 17/01/2014
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_PYGLINTDETECTOR_H
#define HAYTHAM_PYGLINTDETECTOR_H

namespace haytham {

struct PyGlintDetector
{
    static void registerPyGlintDetector();
};

} // namespace haytham

#endif // HAYTHAM_PYGLINTDETECTOR_H
