#ifndef HAYTHAM_PYEXTRACTOR_H
#define HAYTHAM_PYEXTRACTOR_H

namespace haytham {

struct PyExtractor
{
    static void* extract_tuple(PyObject* obj);
};

} // namespace haytham

#endif // HAYTHAM_PYEXTRACTOR_H
