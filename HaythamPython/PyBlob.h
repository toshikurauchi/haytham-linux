/*
 * PyBlob.h
 *
 *  Created on: 17/01/2014
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_PYBLOB_H
#define HAYTHAM_PYBLOB_H

namespace haytham {

struct PyBlob
{
    static void registerPyBlob();
};

} // namespace haytham

#endif // HAYTHAM_PYBLOB_H
