/*
 * PyPoint.h
 *
 *  Created on: 13/12/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_PYPOINT_H
#define HAYTHAM_PYPOINT_H

#include "Common/Point.h"

namespace haytham {

struct PyPoint
{
    static void registerPyPoint();
    static void construct_point(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data);
};

} // namespace haytham

#endif // HAYTHAM_PYPOINT_H
