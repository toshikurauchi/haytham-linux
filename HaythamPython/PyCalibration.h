/*
 * PyCalibration.h
 *
 *  Created on: 20/12/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_PYCALIBRATION_H
#define HAYTHAM_PYCALIBRATION_H

namespace haytham {

struct PyCalibration
{
    static void registerPyCalibration();
};

} // namespace haytham

#endif // HAYTHAM_PYCALIBRATION_H
