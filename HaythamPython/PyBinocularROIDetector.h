/*
 * PyBinocularROIDetector.h
 *
 *  Created on: 20/01/2014
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_PYBINOCULARROIDETECTOR_H
#define HAYTHAM_PYBINOCULARROIDETECTOR_H

namespace haytham {

struct PyBinocularROIDetector
{
    static void registerPyBinocularROIDetector();
};

} // namespace haytham

#endif // HAYTHAM_PYBINOCULARROIDETECTOR_H
