/*
 * Haytham.cpp
 *
 *  Created on: 13/12/2013
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>
#include <opencv2/opencv.hpp>

#include "PyPoint.h"
#include "PyOpenCV.h"
#include "PyEyeDetector.h"
#include "PyMonitorDetector.h"
#include "PyCameraCalibration.h"
#include "PyCalibration.h"
#include "PyGazeData.h"
#include "PySceneData.h"
#include "PyBlob.h"
#include "PyCalibrationPointsDetection.h"
#include "PyGlintDetector.h"
#include "PyBinocularROIDetector.h"
#include "PyUtils.h"

namespace haytham {

BOOST_PYTHON_MODULE(haytham)
{
    PyPoint::registerPyPoint();
    PyOpenCV::registerOpenCV();
    PyEyeDetector::registerPyEyeDetector();
    PyMonitorDetector::registerPyMonitorDetector();
    PyCameraCalibration::registerPyCameraCalibration();
    PyCalibration::registerPyCalibration();
    PyGazeData::registerPyGazeData();
    PySceneData::registerPySceneData();
    PyBlob::registerPyBlob();
    PyCalibrationPointsDetection::registerPyCalibrationPointsDetection();
    PyGlintDetector::registerPyGlintDetector();
    PyBinocularROIDetector::registerPyBinocularROIDetector();

    // Leave this in the end (depends on previous wrappers)
    PyUtils::registerUtils();
}

}
