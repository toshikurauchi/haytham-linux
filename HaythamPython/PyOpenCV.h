/*
 * PyOpenCV.h
 *
 *  Created on: 13/12/2013
 *      Author: Andrew Kurauchi
 *
 * Source: http://stackoverflow.com/questions/19136944/call-c-opencv-functions-from-python-send-a-cv-mat-to-c-dll-which-is-usi
 */

#ifndef BOOSTOPENCV_H
#define BOOSTOPENCV_H

#include <boost/python.hpp>
#include <opencv2/opencv.hpp>

namespace haytham {

struct PyOpenCV {
    static void registerOpenCV();
    static void* extract_pyarray(PyObject* obj);
    static void construct_mat(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data);
    static void construct_rect(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data);
    static void construct_rotated_rect(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data);
    static void construct_size(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data);
    static void construct_point2f(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data);
    static void construct_point(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data);
};

struct OpenCVConverters {
    static PyObject* convert(cv::Mat const &mat);
    static PyObject* convert(cv::Rect const &rect);
    static PyObject* convert(cv::RotatedRect const &rect);
    static PyObject* convert(cv::Size const &size);
    static PyObject* convert(cv::Point2f const &point);
    static PyObject* convert(cv::Point const &point);
};

}

#endif // BOOSTOPENCV_H
