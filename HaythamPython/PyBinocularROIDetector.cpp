/*
 * PyBinocularROIDetector.cpp
 *
 *  Created on: 20/01/2014
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>

#include "PyBinocularROIDetector.h"
#include "EyeDetection/BinocularROIDetector.h"

namespace haytham {
using namespace boost::python;

class BinocularROIDetectorWrapper : public BinocularROIDetector {
public:
    virtual cv::Mat extractLeftROIImage(const cv::Mat &fullImage) {
        // We had to do this because Mat->Numpy-Array conversion ignores ROIs.
        return BinocularROIDetector::extractLeftROIImage(fullImage).clone();
    }

    virtual cv::Mat extractRightROIImage(const cv::Mat &fullImage) {
        // We had to do this because Mat->Numpy-Array conversion ignores ROIs.
        return BinocularROIDetector::extractRightROIImage(fullImage).clone();
    }
};

void PyBinocularROIDetector::registerPyBinocularROIDetector() {
    class_<BinocularROIDetector>("SuperBinocularROIDetector", no_init)
            .def("leftReady", &BinocularROIDetector::leftReady)
            .def("rightReady", &BinocularROIDetector::rightReady)
            .add_property("leftROI", make_function(&BinocularROIDetector::getLeftROI, return_value_policy<return_by_value>()), &BinocularROIDetector::setLeftROI)
            .add_property("rightROI", make_function(&BinocularROIDetector::getRightROI, return_value_policy<return_by_value>()), &BinocularROIDetector::setRightROI);
    class_<BinocularROIDetectorWrapper, bases<BinocularROIDetector> >("BinocularROIDetector")
            .def("extractLeftROIImage", &BinocularROIDetectorWrapper::extractLeftROIImage)
            .def("extractRightROIImage", &BinocularROIDetectorWrapper::extractRightROIImage);
}

} // namespace haytham
