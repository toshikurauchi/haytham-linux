/*
 * PyCameraCalibration.h
 *
 *  Created on: 20/12/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_PYCAMERACALIBRATION_H
#define HAYTHAM_PYCAMERACALIBRATION_H

namespace haytham {

struct PyCameraCalibration
{
    static void registerPyCameraCalibration();
};

} // namespace haytham

#endif // HAYTHAM_PYCAMERACALIBRATION_H
