/*
 * PyOpenCV.cpp
 *
 *  Created on: 13/12/2013
 *      Author: Andrew Kurauchi
 *
 * Source: http://stackoverflow.com/questions/19136944/call-c-opencv-functions-from-python-send-a-cv-mat-to-c-dll-which-is-usi
 */

#include "PyOpenCV.h"
#include "PythonToOCV.h"
#include "numpy/ndarrayobject.h"
#include "PyExtractor.h"

namespace haytham {

void PyOpenCV::registerOpenCV() {
    import_array1();
    boost::python::converter::registry::push_back(
                &extract_pyarray,
                &construct_mat,
                boost::python::type_id<cv::Mat>());
    boost::python::converter::registry::push_back(
                &PyExtractor::extract_tuple,
                &construct_rect,
                boost::python::type_id<cv::Rect>());
    boost::python::converter::registry::push_back(
                &PyExtractor::extract_tuple,
                &construct_rotated_rect,
                boost::python::type_id<cv::RotatedRect>());
    boost::python::converter::registry::push_back(
                &PyExtractor::extract_tuple,
                &construct_size,
                boost::python::type_id<cv::Size>());
    boost::python::converter::registry::push_back(
                &PyExtractor::extract_tuple,
                &construct_point2f,
                boost::python::type_id<cv::Point2f>());
    boost::python::converter::registry::push_back(
                &PyExtractor::extract_tuple,
                &construct_point,
                boost::python::type_id<cv::Point>());
    boost::python::to_python_converter<cv::Mat, OpenCVConverters>();
    boost::python::to_python_converter<cv::Rect, OpenCVConverters>();
    boost::python::to_python_converter<cv::RotatedRect, OpenCVConverters>();
    boost::python::to_python_converter<cv::Size, OpenCVConverters>();
    boost::python::to_python_converter<cv::Point2f, OpenCVConverters>();
    boost::python::to_python_converter<cv::Point, OpenCVConverters>();
}

void* PyOpenCV::extract_pyarray(PyObject* obj) {
    void *retVal = PyObject_TypeCheck(obj, &PyArray_Type ) ? obj : 0;
    return retVal;
}

void PyOpenCV::construct_mat(PyObject *obj_ptr, boost::python::converter::rvalue_from_python_stage1_data *data) {
    void *storage = ((boost::python::converter::rvalue_from_python_storage<cv::Mat>*) data)->storage.bytes;
    cv::Mat image;
    ArgInfo info("Mat", true);
    pyopencv_to(obj_ptr, image, info);
    new (storage) cv::Mat(image);
    data->convertible = storage;
}

void PyOpenCV::construct_rect(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data) {
    void *storage = ((boost::python::converter::rvalue_from_python_storage<cv::Rect>*) data)->storage.bytes;
    cv::Rect rect;
    pyopencv_to(obj_ptr, rect, "Rect");
    new (storage) cv::Rect(rect.x, rect.y, rect.width, rect.height);
    data->convertible = storage;
}

void PyOpenCV::construct_rotated_rect(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data) {
    void *storage = ((boost::python::converter::rvalue_from_python_storage<cv::RotatedRect>*) data)->storage.bytes;
    cv::RotatedRect rect;
    pyopencv_to(obj_ptr, rect, "RotatedRect");
    new (storage) cv::RotatedRect(rect.center, rect.size, rect.angle);
    data->convertible = storage;
}

void PyOpenCV::construct_size(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data) {
    void *storage = ((boost::python::converter::rvalue_from_python_storage<cv::Size>*) data)->storage.bytes;
    cv::Size size;
    pyopencv_to(obj_ptr, size, "Size");
    new (storage) cv::Size(size.width, size.height);
    data->convertible = storage;
}

void PyOpenCV::construct_point2f(PyObject *obj_ptr, boost::python::converter::rvalue_from_python_stage1_data *data) {
    void *storage = ((boost::python::converter::rvalue_from_python_storage<cv::Point2f>*) data)->storage.bytes;
    cv::Point2f point;
    pyopencv_to(obj_ptr, point, "Point2f");
    new (storage) cv::Point2f(point.x, point.y);
    data->convertible = storage;
}

void PyOpenCV::construct_point(PyObject *obj_ptr, boost::python::converter::rvalue_from_python_stage1_data *data) {
    void *storage = ((boost::python::converter::rvalue_from_python_storage<cv::Point>*) data)->storage.bytes;
    cv::Point point;
    pyopencv_to(obj_ptr, point, "Point");
    new (storage) cv::Point(int(point.x), int(point.y));
    data->convertible = storage;
}

PyObject* OpenCVConverters::convert(cv::Mat const &mat) {
    return pyopencv_from(mat);
}

PyObject* OpenCVConverters::convert(cv::Rect const &rect) {
    return pyopencv_from(rect);
}

PyObject* OpenCVConverters::convert(cv::RotatedRect const &rect) {
    return pyopencv_from(rect);
}

PyObject* OpenCVConverters::convert(cv::Size const &size) {
    return pyopencv_from(size);
}

PyObject* OpenCVConverters::convert(const cv::Point2f &point) {
    return pyopencv_from(point);
}

PyObject* OpenCVConverters::convert(const cv::Point &point) {
    return pyopencv_from(point);
}

}
