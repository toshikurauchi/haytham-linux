/*
 * PyGazeData.cpp
 *
 *  Created on: 17/12/2013
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>

#include "PyGazeData.h"
#include "EyeDetection/GazeData.h"

namespace haytham {
using namespace boost::python;

void PyGazeData::registerPyGazeData() {
    class_<GazeData>("GazeData")
            .def("csvString", &GazeData::asCsvString)
            .def("oneLineCsvString", &GazeData::asOneLineCsvString)
            .add_property("gaze", make_function(&GazeData::getGazePosition, return_internal_reference<1>()), &GazeData::setGazePosition)
            //.def("gesture", &GazeData::getGesture, return_internal_reference<1>())
            .add_property("glintCenters", make_function(&GazeData::getGlintCenters, return_value_policy<return_by_value>()), &GazeData::setGlintCenters)
            .add_property("monitorGaze", make_function(&GazeData::getMonitorGazePosition, return_internal_reference<1>()), &GazeData::setMonitorGazePosition)
            .add_property("pupilCenter", make_function(&GazeData::getPupilCenter, return_internal_reference<1>()), &GazeData::setPupilCenter)
            .add_property("pupilDiameter", &GazeData::getPupilDiameter, &GazeData::setPupilDiameter)
            .add_property("pupilEllipse", make_function(&GazeData::getPupilEllipse, return_value_policy<return_by_value>()), &GazeData::setPupilEllipse)
            .def("pupilGlintVector", &GazeData::getPupilGlintVector)
            .add_property("tag", make_function(&GazeData::getTag, return_value_policy<return_by_value>()), &GazeData::setTag)
            .add_property("time", &GazeData::getTime, &GazeData::setTime)
            .def("addOffset", &GazeData::addOffset)
            .add_property("pupilFound", &GazeData::isPupilFound, &GazeData::setPupilFound);
}

} // namespace haytham
