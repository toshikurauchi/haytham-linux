/*
 * PyEyeDetector.h
 *
 *  Created on: 17/12/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_PYEYEDETECTOR_H
#define HAYTHAM_PYEYEDETECTOR_H

namespace haytham {

struct PyEyeDetector {
    static void registerPyEyeDetector();
};

} // namespace haytham

#endif // HAYTHAM_PYEYEDETECTOR_H
