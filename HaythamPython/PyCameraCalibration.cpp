/*
 * PyCameraCalibration.cpp
 *
 *  Created on: 20/12/2013
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>

#include "PyCameraCalibration.h"
#include "Camera/CameraCalibration.h"

namespace haytham {
using namespace boost::python;

void PyCameraCalibration::registerPyCameraCalibration() {
    class_<CameraCalibration>("CameraCalibration")
            .def(init<optional<cv::Size, float> >())
            .def("isCalibrating", &CameraCalibration::isCalibrating)
            .def("startCalibrating", &CameraCalibration::startCalibrating)
            .def("addCalibrationImage", &CameraCalibration::addCalibrationImage)
            .def("loadCalibration", &CameraCalibration::loadCalibration)
            .def("undistort", &CameraCalibration::undistort)
            .def("findCorners", &CameraCalibration::findCorners)
            .def("shortFilename", &CameraCalibration::getShortFilename)
            .def("patternSize", &CameraCalibration::getPatternSize)
            .def("remainingFrames", &CameraCalibration::getRemainingFrames)
            .def("cameraMatrix", &CameraCalibration::getCameraMatrix, return_value_policy<return_by_value>())
            .def("distCoeffs", &CameraCalibration::getDistCoeffs, return_value_policy<return_by_value>());
}

} // namespace haytham
