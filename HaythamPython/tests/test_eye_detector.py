from nose.tools import assert_equals, assert_not_equal, assert_true
import nose.config
import numpy as np
import cv2
import sys

sys.path.append("../../")
import HaythamPython.haytham as haytham

class TestEyeDetector:
    def setUp(self):
        import gc
        for obj in gc.get_objects():
            if isinstance(obj, nose.config.Config):
                break
        self.verbose = obj.verbosity > 1

    def test_threshold_detector_name(self):
        ted = haytham.ThresholdEyeDetector()
        assert_equals(ted.methodName(), 'Threshold Eye Detection')

    def test_threshold_detector(self):
        ted = haytham.ThresholdEyeDetector()
        img = cv2.imread('eye.jpg')
        gd = ted.findPupil(img, 75, 225, 0.1, 0.9)

        assert_equals(len(gd.glintCenters), 0)
        assert_equals(int(gd.pupilCenter.x), 336)
        assert_equals(int(gd.pupilCenter.y), 232)
        assert_equals(gd.pupilDiameter, 69)
        assert_not_equal(gd.time, 0)
        assert_true(gd.pupilFound)

    def test_threshold_detector_default_one_argument(self):
        ted = haytham.ThresholdEyeDetector()
        img = cv2.imread('eye.jpg')
        gd = ted.findPupil(img, 75, 225, 0.1)

        assert_equals(len(gd.glintCenters), 0)
        assert_equals(int(gd.pupilCenter.x), 336)
        assert_equals(int(gd.pupilCenter.y), 232)
        assert_equals(gd.pupilDiameter, 69)
        assert_not_equal(gd.time, 0)
        assert_true(gd.pupilFound)

    def test_threshold_detector_default_arguments(self):
        ted = haytham.ThresholdEyeDetector()
        img = cv2.imread('eye.jpg')
        gd = ted.findPupil(img, 75, 225)

        assert_equals(len(gd.glintCenters), 0)
        assert_equals(int(gd.pupilCenter.x), 336)
        assert_equals(int(gd.pupilCenter.y), 232)
        assert_equals(gd.pupilDiameter, 69)
        assert_not_equal(gd.time, 0)
        assert_true(gd.pupilFound)

        center = (int(gd.pupilCenter.x), int(gd.pupilCenter.y))
        cv2.ellipse(img, gd.pupilEllipse, (0, 0, 255), 2)
        if self.verbose:
            windowName = "Threshold Eye Detection"
            cv2.imshow(windowName, img)
            cv2.waitKey()
            cv2.destroyWindow(windowName)

    def test_starburst_detector_name(self):
        sed = haytham.StarburstEyeDetector()
        assert_equals(sed.methodName(), 'Starburst Eye Detection')
