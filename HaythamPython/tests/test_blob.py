from nose.tools import assert_equals, assert_not_equal, assert_true, assert_false
import nose.config
import numpy as np
import cv2
import sys

sys.path.append("../../")
import HaythamPython.haytham as haytham

class TestBlob:
    def setUp(self):
        import gc
        for obj in gc.get_objects():
            if isinstance(obj, nose.config.Config):
                break
        self.verbose = obj.verbosity > 1

    def test_empty_blob(self):
        blob = haytham.Blob()
        assert_true(blob.empty())

    def test_blob_with_points(self):
        points = [(0,0), (0,10), (10,10), (10,0)]
        blob = haytham.Blob(points)
        assert_equals(len(points), blob.size())
        assert_equals((0, 0, 11, 11), blob.boundingBox())
        for i in range(len(points)):
            assert_equals(points[i], blob.points()[i])
            assert_true(points[i] in blob.hull())
        assert_equals(len(points), len(blob.hull()))
        assert_equals(0.8264462809917356, blob.fullness())
        assert_equals((5, 5), blob.centerOfGravity())
        assert_equals(100, blob.area())
        assert_false(blob.empty())
