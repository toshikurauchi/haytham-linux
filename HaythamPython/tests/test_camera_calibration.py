from nose.tools import assert_equals, assert_not_equal, assert_true, assert_is_not_none
import nose.config
import numpy as np
import cv2
import sys

sys.path.append("../../")
import HaythamPython.haytham as haytham

class TestCameraCalibration:
    def setUp(self):
        import gc
        for obj in gc.get_objects():
            if isinstance(obj, nose.config.Config):
                break
        self.verbose = obj.verbosity > 1

    def test_constructors(self):
        assert_is_not_none(haytham.CameraCalibration())
        calib1Param = haytham.CameraCalibration((5, 3))
        calib2Params = haytham.CameraCalibration((5, 3), 0.05)
        assert_is_not_none(calib1Param)
        assert_equals((5, 3), calib1Param.patternSize())
        assert_is_not_none(calib2Params)
        assert_equals((5, 3), calib2Params.patternSize())

    def test_load_calibration_file(self):
        cc = haytham.CameraCalibration()
        assert_true(cc.loadCalibration('calibration.xml'))
        # TODO the funcion calls below are causing segmentation fault
        # print cc.cameraMatrix()
        # print cc.distCoeffs()

    # TODO Test calibration itself (we are currently only testing calibration file loading)
