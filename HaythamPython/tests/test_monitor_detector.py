from nose.tools import assert_equals, assert_not_equal, assert_true
import nose.config
import cv2
import sys

sys.path.append("../../")
import HaythamPython.haytham as haytham

class TestMonitorDetector:
    def setUp(self):
        import gc
        for obj in gc.get_objects():
            if isinstance(obj, nose.config.Config):
                break
        self.verbose = obj.verbosity > 1

    def test_threshold_detector_name(self):
        cmd = haytham.ContrastMonitorDetector()
        assert_equals(cmd.methodName(), 'Contrast Monitor Detection')
        lmd = haytham.LedMonitorDetector()
        assert_equals(lmd.methodName(), 'LED Monitor Detection')

    def test_contrast_detector(self):
        lmd = haytham.LedMonitorDetector()
        img = cv2.imread('led_monitor.jpg')
        assert_true(lmd.detectScreen(img, 100, 0.3))
        grayImg = lmd.filteredGrayImg()
        lmd.drawScreen(grayImg)
        if self.verbose:
            windowName = "LED Detected Monitor"
            cv2.imshow(windowName, grayImg)
            cv2.waitKey()
            cv2.destroyWindow(windowName)
        corners = lmd.screenCorners()
        expected = [(122, 80), (542, 87), (520, 403), (130, 402)]
        cur = 0
        for corner in corners:
            assert_equals(int(corner.x), expected[cur][0])
            assert_equals(int(corner.y), expected[cur][1])
            cur += 1
