from nose.tools import assert_equals, assert_not_equal, assert_true, assert_false, raises
import nose.config
import numpy as np
import cv2
import sys

sys.path.append("../../")
import HaythamPython.haytham as haytham

class TestBinocularROIDetector:
    def setUp(self):
        import gc
        for obj in gc.get_objects():
            if isinstance(obj, nose.config.Config):
                break
        self.verbose = obj.verbosity > 1

    @raises(RuntimeError)
    def test_cant_construct_super_binocular_roi_detector(self):
        haytham.SuperBinocularROIDetector()

    def test_binocular_roi_detector_not_ready_when_created(self):
        brd = haytham.BinocularROIDetector()
        assert_false(brd.leftReady())
        assert_false(brd.rightReady())

    def test_binocular_roi_detector_ready_after_right_roi_is_set(self):
        brd = haytham.BinocularROIDetector()
        right_roi = (130, 165, 230, 255)
        brd.rightROI = right_roi
        assert_false(brd.leftReady())
        assert_true(brd.rightReady())
        assert_equals(right_roi, brd.rightROI)

    def test_binocular_roi_detector_ready_after_left_roi_is_set(self):
        brd = haytham.BinocularROIDetector()
        left_roi = (510, 140, 250, 155)
        brd.leftROI = left_roi
        assert_true(brd.leftReady())
        assert_false(brd.rightReady())
        assert_equals(left_roi, brd.leftROI)

    def test_binocular_roi_detector_extract_roi(self):
        brd = haytham.BinocularROIDetector()
        left_roi = (510, 140, 250, 155)
        right_roi = (130, 165, 230, 255)
        brd.leftROI = left_roi
        brd.rightROI = right_roi
        assert_true(brd.leftReady())
        assert_true(brd.rightReady())
        assert_equals(left_roi, brd.leftROI)
        assert_equals(right_roi, brd.rightROI)

        img = cv2.imread("eyes.jpg")
        if self.verbose:
            origWindowName = "Binocular ROI Detector: Original"
            leftWindowName = "Binocular ROI Detector: Left"
            rightWindowName = "Binocular ROI Detector: Right"
            cv2.imshow(origWindowName, img)
            cv2.imshow(leftWindowName, brd.extractLeftROIImage(img))
            cv2.imshow(rightWindowName, brd.extractRightROIImage(img))
            cv2.waitKey()
            cv2.destroyWindow(origWindowName)
            cv2.destroyWindow(leftWindowName)
            cv2.destroyWindow(rightWindowName)

    def test_binocular_roi_detector_extract_larger_roi_doesnt_break(self):
        brd = haytham.BinocularROIDetector()
        left_roi = (-10, -10, 700, 700)
        right_roi = (-10, -10, 700, 700)
        brd.leftROI = left_roi
        brd.rightROI = right_roi
        assert_true(brd.leftReady())
        assert_true(brd.rightReady())
        assert_equals(left_roi, brd.leftROI)
        assert_equals(right_roi, brd.rightROI)

        img = cv2.imread("eye.jpg")
        if self.verbose:
            origWindowName = "Binocular ROI Detector: Original"
            leftWindowName = "Binocular ROI Detector: Left (should be full image)"
            rightWindowName = "Binocular ROI Detector: Right (should be full image)"
            cv2.imshow(origWindowName, img)
            cv2.imshow(leftWindowName, brd.extractLeftROIImage(img))
            cv2.imshow(rightWindowName, brd.extractRightROIImage(img))
            cv2.waitKey()
            cv2.destroyWindow(origWindowName)
            cv2.destroyWindow(leftWindowName)
            cv2.destroyWindow(rightWindowName)
