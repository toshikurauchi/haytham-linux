from nose.tools import assert_equals, assert_not_equal, assert_true, assert_false
import nose.config
import numpy as np
import cv2
import sys

sys.path.append("../../")
import HaythamPython.haytham as haytham

class TestCalibrationPointsDetection:
    def setUp(self):
        import gc
        for obj in gc.get_objects():
            if isinstance(obj, nose.config.Config):
                break
        self.verbose = obj.verbosity > 1

    def test_detect_points(self):
        cpd = haytham.CalibrationPointsDetection()
        img = cv2.imread("calib_points.jpg")
        points = cpd.findPoints(img, (3, 3), 150)
        for point in points:
            center = point.centerOfGravity()
            cv2.circle(img, (int(center[0]), int(center[1])), 8, (0, 255, 0), -1)
        if self.verbose:
            windowName = "Calibration Points Detection"
            cv2.imshow(windowName, img)
            cv2.waitKey()
            cv2.destroyWindow(windowName)
        assert_equals(9, len(points))

    def test_detect_points_with_dest_img(self):
        cpd = haytham.CalibrationPointsDetection()
        img = cv2.imread("calib_points.jpg")
        points = cpd.findPoints(img, (3, 3), 150, img)
        if self.verbose:
            windowName = "Calibration Points Detection Passing Destination Image"
            cv2.imshow(windowName, img)
            cv2.waitKey()
            cv2.destroyWindow(windowName)
        assert_equals(9, len(points))

    def test_detect_points_with_dest_img_and_calibration(self):
        cpd = haytham.CalibrationPointsDetection()
        calib = haytham.PolynomialCalibration9Points()
        img = cv2.imread("calib_points.jpg")
        points = cpd.findPoints(img, (3, 3), 150, img, calib)
        if self.verbose:
            windowName = "Calibration Points Detection Passing Destination Image and Calibration"
            cv2.imshow(windowName, img)
            cv2.waitKey()
            cv2.destroyWindow(windowName)
        assert_equals(9, len(points))
