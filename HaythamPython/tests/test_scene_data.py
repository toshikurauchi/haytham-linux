from nose.tools import assert_equals, assert_true, assert_false, assert_is_not_none
import nose.config
import numpy as np
import cv2
import sys

sys.path.append("../../")
import HaythamPython.haytham as haytham

class TestSceneData:
    def setUp(self):
        import gc
        for obj in gc.get_objects():
            if isinstance(obj, nose.config.Config):
                break
        self.verbose = obj.verbosity > 1

    def test_scene_data(self):
        sd = haytham.SceneData()
        assert_is_not_none(sd)
        points = [(1, 2), (3, 4), (5, 6), (7, 8)]
        sd.calibrationPoints = points
        assert_equals(points, sd.calibrationPoints)

