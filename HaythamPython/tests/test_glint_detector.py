from nose.tools import assert_equals, assert_not_equal, assert_true, assert_false
import nose.config
import numpy as np
import cv2
import sys

sys.path.append("../../")
import HaythamPython.haytham as haytham

class TestGlintDetector:
    def setUp(self):
        import gc
        for obj in gc.get_objects():
            if isinstance(obj, nose.config.Config):
                break
        self.verbose = obj.verbosity > 1

    def test_detect_glint(self):
        eyeImg = cv2.imread("eye.jpg")
        gd = haytham.GlintDetector()
        glints = gd.detectGlint(eyeImg, 200, 2, haytham.Point(0, 0))
        assert_equals(2, len(glints))
