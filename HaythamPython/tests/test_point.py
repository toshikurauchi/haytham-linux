from nose.tools import assert_equals, raises
import nose.config
import numpy as np
import cv2
import sys

sys.path.append("../../")
import HaythamPython.haytham as haytham

class TestPoint:
    def setUp(self):
        import gc
        for obj in gc.get_objects():
            if isinstance(obj, nose.config.Config):
                break
        self.verbose = obj.verbosity > 1

    def test_point_default_constructor(self):
        p = haytham.Point()
        assert_equals(p.x, 0)
        assert_equals(p.y, 0)

    def test_point_constructor_with_args(self):
        p = haytham.Point(1,2)
        assert_equals(p.x, 1)
        assert_equals(p.y, 2)

    @raises(RuntimeError)
    def test_point_cant_get_item_after_1(self):
        p = haytham.Point()
        p[2]

    @raises(RuntimeError)
    def test_point_cant_set_item_after_1(self):
        p = haytham.Point()
        p[2] = 10

    def test_point_get_item(self):
        p = haytham.Point()
        p[0] = 1
        p[1] = 2
        assert_equals(1, p[0])
        assert_equals(2, p[1])
        assert_equals(1, p.x)
        assert_equals(2, p.y)

    def test_point_operations(self):
        p = haytham.Point(1,2)
        q = haytham.Point(3,4)

        assert p != q
        assert_equals(p, p)
        assert_equals(q, q)
        assert_equals(p+q, haytham.Point(4,6))
        assert_equals(p-q, haytham.Point(-2, -2))
        assert_equals(q.norm(), 5)
        assert_equals(str(p), '(1,2)')

        p += q
        assert_equals(p, haytham.Point(4,6))
        p -= q
        assert_equals(p, haytham.Point(1,2))

    def test_point_constructed_as_tuple(self):
        p = haytham.Point(1,2) + (2,3)
        assert_equals(p, haytham.Point(3,5))

    def test_point_str(self):
        p = haytham.Point(1,2)
        assert_equals(str(p), "(1,2)");
