from nose.tools import assert_equals, assert_not_equal, assert_true, assert_false
import nose.config
import numpy as np
import cv2
import sys

sys.path.append("../../")
import HaythamPython.haytham as haytham

class TestCalibration:
    def setUp(self):
        import gc
        for obj in gc.get_objects():
            if isinstance(obj, nose.config.Config):
                break
        self.verbose = obj.verbosity > 1

    def test_calibration_names(self):
        pc9p = haytham.PolynomialCalibration9Points()
        pc9pf = haytham.PolynomialCalibration9PointsFull()
        pc4p = haytham.PolynomialCalibration4Points()
        hc = haytham.HomographyCalibration(4)
        cc = haytham.CameraCalibration()
        cc.loadCalibration('calibration.xml')
        tdc = haytham.TwoDistancesCalibration(cc)
        cc9p = haytham.CerolazaCalibration9Points()

        assert_equals(pc9p.methodName(), '9 Points Polynomial')
        assert_equals(pc9pf.methodName(), '9 Points Full Polynomial')
        assert_equals(pc4p.methodName(), '4 Points Polynomial')
        assert_equals(hc.methodName(), '4 Points Homography')
        assert_equals(tdc.methodName(), '2 Distances Calibration')
        assert_equals(cc9p.methodName(), 'Cerolaza 9 Points Calibration')

    def test_PolynomialCalibration9PointsFull_shouldnt_calibrate_with_less_than_9_points(self):
        pc9pf = haytham.PolynomialCalibration9PointsFull()
        pc9pf.startCalibrating()
        pc9pf.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9pf.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9pf.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9pf.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9pf.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9pf.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9pf.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9pf.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        assert_false(pc9pf.isReady())

    def test_PolynomialCalibration9PointsFull_calibrate(self):
        # Function used: f(x,y) = (1+2x+3y+4xy+5x^2+6y^2, 6+5x+4y+3xy+2x^2+y^2)
        pc9pf = haytham.PolynomialCalibration9PointsFull()
        pc9pf.startCalibrating()
        pc9pf.addPoint(haytham.Point(1,1), haytham.Point(21,21))
        pc9pf.addPoint(haytham.Point(1,2), haytham.Point(46,31))
        pc9pf.addPoint(haytham.Point(2,1), haytham.Point(42,35))
        pc9pf.addPoint(haytham.Point(3,4), haytham.Point(208,107))
        pc9pf.addPoint(haytham.Point(4,3), haytham.Point(200,115))
        pc9pf.addPoint(haytham.Point(2,5), haytham.Point(230,99))
        pc9pf.addPoint(haytham.Point(1,4), haytham.Point(132,57))
        pc9pf.addPoint(haytham.Point(4,1), haytham.Point(114,75))
        pc9pf.addPoint(haytham.Point(3,3), haytham.Point(151,87))
        assert_true(pc9pf.isReady())
        assert_false(pc9pf.isCalibrated())
        pc9pf.calibrate()
        assert_true(pc9pf.isCalibrated())

    def test_PolynomialCalibration9PointsFull_calibrate_and_map(self):
        # Function used: f(x,y) = (1+2x+3y+4xy+5x^2+6y^2, 6+5x+4y+3xy+2x^2+y^2)
        pc9pf = haytham.PolynomialCalibration9PointsFull()
        pc9pf.startCalibrating()
        pc9pf.addPoint(haytham.Point(1,1), haytham.Point(21,21))
        pc9pf.addPoint(haytham.Point(1,2), haytham.Point(46,31))
        pc9pf.addPoint(haytham.Point(2,1), haytham.Point(42,35))
        pc9pf.addPoint(haytham.Point(3,4), haytham.Point(208,107))
        pc9pf.addPoint(haytham.Point(4,3), haytham.Point(200,115))
        pc9pf.addPoint(haytham.Point(2,5), haytham.Point(230,99))
        pc9pf.addPoint(haytham.Point(1,4), haytham.Point(132,57))
        pc9pf.addPoint(haytham.Point(4,1), haytham.Point(114,75))
        pc9pf.addPoint(haytham.Point(3,3), haytham.Point(151,87))
        assert_true(pc9pf.isReady())
        assert_false(pc9pf.isCalibrated())
        pc9pf.calibrate()
        assert_true(pc9pf.isCalibrated())
        mapped = pc9pf.map(haytham.Point(5,5), haytham.Point(0,0))
        assert_equals(401, int(mapped.x))
        assert_equals(201, int(mapped.y))

    def test_PolynomialCalibration9PointsFull_calibrate_and_add_correction(self):
        # Function used: f(x,y) = (1+2x+3y+4xy+5x^2+6y^2, 6+5x+4y+3xy+2x^2+y^2)
        pc9pf = haytham.PolynomialCalibration9PointsFull()
        pc9pf.startCalibrating()
        pc9pf.addPoint(haytham.Point(1,1), haytham.Point(21,21))
        pc9pf.addPoint(haytham.Point(1,2), haytham.Point(46,31))
        pc9pf.addPoint(haytham.Point(2,1), haytham.Point(42,35))
        pc9pf.addPoint(haytham.Point(3,4), haytham.Point(208,107))
        pc9pf.addPoint(haytham.Point(4,3), haytham.Point(200,115))
        pc9pf.addPoint(haytham.Point(2,5), haytham.Point(230,99))
        pc9pf.addPoint(haytham.Point(1,4), haytham.Point(132,57))
        pc9pf.addPoint(haytham.Point(4,1), haytham.Point(114,75))
        pc9pf.addPoint(haytham.Point(3,3), haytham.Point(151,87))
        assert_true(pc9pf.isReady())
        assert_false(pc9pf.isCalibrated())
        pc9pf.calibrate()
        assert_true(pc9pf.isCalibrated())
        mapped = pc9pf.map(haytham.Point(5,5), haytham.Point(1,2))
        assert_equals(400, int(mapped.x))
        assert_equals(199, int(mapped.y))

    def test_PolynomialCalibration9Points_shouldnt_calibrate_with_less_than_9_points(self):
        pc9p = haytham.PolynomialCalibration9Points()
        pc9p.startCalibrating()
        pc9p.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9p.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9p.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9p.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9p.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9p.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9p.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        pc9p.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        assert_false(pc9p.isReady())

    def test_PolynomialCalibration9Points_calibrate(self):
        # Function used: f(x,y) = (1+2x+3y+4xy+5x^2+6y^2, 6+5x+4y+3xy+2x^2+y^2)
        pc9p = haytham.PolynomialCalibration9Points()
        pc9p.startCalibrating()
        pc9p.addPoint(haytham.Point(1,1), haytham.Point(21,21))
        pc9p.addPoint(haytham.Point(1,2), haytham.Point(46,31))
        pc9p.addPoint(haytham.Point(2,1), haytham.Point(42,35))
        pc9p.addPoint(haytham.Point(3,4), haytham.Point(208,107))
        pc9p.addPoint(haytham.Point(4,3), haytham.Point(200,115))
        pc9p.addPoint(haytham.Point(2,5), haytham.Point(230,99))
        pc9p.addPoint(haytham.Point(1,4), haytham.Point(132,57))
        pc9p.addPoint(haytham.Point(4,1), haytham.Point(114,75))
        pc9p.addPoint(haytham.Point(3,3), haytham.Point(151,87))
        assert_true(pc9p.isReady())
        assert_false(pc9p.isCalibrated())
        pc9p.calibrate()
        assert_true(pc9p.isCalibrated())

    def test_PolynomialCalibration9Points_calibrate_and_map(self):
        # Function used: f(x,y) = (1+2x+3y+4xy+5x^2+6y^2, 6+5x+4y+3xy+2x^2+y^2)
        pc9p = haytham.PolynomialCalibration9Points()
        pc9p.startCalibrating()
        pc9p.addPoint(haytham.Point(1,1), haytham.Point(21,21))
        pc9p.addPoint(haytham.Point(1,2), haytham.Point(46,31))
        pc9p.addPoint(haytham.Point(2,1), haytham.Point(42,35))
        pc9p.addPoint(haytham.Point(3,4), haytham.Point(208,107))
        pc9p.addPoint(haytham.Point(4,3), haytham.Point(200,115))
        pc9p.addPoint(haytham.Point(2,5), haytham.Point(230,99))
        pc9p.addPoint(haytham.Point(1,4), haytham.Point(132,57))
        pc9p.addPoint(haytham.Point(4,1), haytham.Point(114,75))
        pc9p.addPoint(haytham.Point(3,3), haytham.Point(151,87))
        assert_true(pc9p.isReady())
        assert_false(pc9p.isCalibrated())
        pc9p.calibrate()
        assert_true(pc9p.isCalibrated())
        mapped = pc9p.map(haytham.Point(5,5))
        assert_equals(324, int(mapped.x))
        assert_equals(179, int(mapped.y))

    def test_PolynomialCalibration9Points_calibrate_and_add_correction(self):
        # Function used: f(x,y) = (1+2x+3y+4xy+5x^2+6y^2, 6+5x+4y+3xy+2x^2+y^2)
        pc9p = haytham.PolynomialCalibration9Points()
        pc9p.startCalibrating()
        pc9p.addPoint(haytham.Point(1,1), haytham.Point(21,21))
        pc9p.addPoint(haytham.Point(1,2), haytham.Point(46,31))
        pc9p.addPoint(haytham.Point(2,1), haytham.Point(42,35))
        pc9p.addPoint(haytham.Point(3,4), haytham.Point(208,107))
        pc9p.addPoint(haytham.Point(4,3), haytham.Point(200,115))
        pc9p.addPoint(haytham.Point(2,5), haytham.Point(230,99))
        pc9p.addPoint(haytham.Point(1,4), haytham.Point(132,57))
        pc9p.addPoint(haytham.Point(4,1), haytham.Point(114,75))
        pc9p.addPoint(haytham.Point(3,3), haytham.Point(151,87))
        assert_true(pc9p.isReady())
        assert_false(pc9p.isCalibrated())
        pc9p.calibrate()
        assert_true(pc9p.isCalibrated())
        mapped = pc9p.map(haytham.Point(5,5), haytham.Point(1,2))
        assert_equals(323, int(mapped.x))
        assert_equals(177, int(mapped.y))

    def test_HomographyCalibration_shouldnt_calibrate_with_less_than_4_points(self):
        hc = haytham.HomographyCalibration(4)
        hc.startCalibrating()
        hc.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        hc.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        hc.addPoint(haytham.Point(1,1), haytham.Point(1,1))
        assert_false(hc.isReady())

    def test_HomographyCalibration_calibrate(self):
        hc = haytham.HomographyCalibration(4)
        hc.startCalibrating()
        hc.addPoint(haytham.Point(1,1), haytham.Point(11,11))
        hc.addPoint(haytham.Point(1,3), haytham.Point(11,13))
        hc.addPoint(haytham.Point(3,3), haytham.Point(13,13))
        hc.addPoint(haytham.Point(3,1), haytham.Point(13,11))
        assert_true(hc.isReady())
        assert_false(hc.isCalibrated())
        hc.calibrate()
        assert_true(hc.isCalibrated())
        mapped = hc.map(haytham.Point(2,3), haytham.Point(0,0))
        assert_equals(12, int(mapped.x))
        assert_equals(13, int(mapped.y))

    # TODO Missing two distances calibration tests

    def test_TwoDistancesCalibration_shouldnt_calibrate_with_less_than_8_points(self):
        cc = haytham.CameraCalibration()
        cc.loadCalibration('calibration.xml')
        tdc = haytham.TwoDistancesCalibration(cc)
        tdc.startCalibrating()
        #hc.addPoint(haytham.Point(1,1), haytham.Point(11,11), ) # TODO We need SceneData
