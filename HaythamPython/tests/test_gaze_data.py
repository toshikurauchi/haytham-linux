from nose.tools import assert_equals, assert_false, assert_true
import nose.config
import numpy as np
import cv2
import sys

sys.path.append("../../")
import HaythamPython.haytham as haytham

class TestGazeData:
    def setUp(self):
        import gc
        for obj in gc.get_objects():
            if isinstance(obj, nose.config.Config):
                break
        self.verbose = obj.verbosity > 1

    def test_gaze_data_default_constructor(self):
        gd = haytham.GazeData()
        #gd.csvString()
        #gd.oneLineCsvString()
        assert_equals(gd.gaze, haytham.Point(-1, -1))
        #print gd.gesture()
        assert_equals(len(gd.glintCenters), 0)
        assert_equals(gd.monitorGaze, haytham.Point(-1, -1))
        assert_equals(gd.pupilCenter, haytham.Point(-1, -1))
        assert_equals(gd.pupilDiameter, 0)
        assert_equals(gd.pupilEllipse, ((0, 0), (0, 0), 0))
        assert_equals(gd.tag, '')
        assert_false(gd.pupilFound)

    def test_gaze_data_properties(self):
        gd = haytham.GazeData()
        gd.gaze = haytham.Point(10, 20)
        assert_equals(gd.gaze, haytham.Point(10, 20))
        glintCenters = [haytham.Point(1, 2), haytham.Point(3, 4)]
        gd.glintCenters = glintCenters
        assert_equals(len(gd.glintCenters), 2)
        for glint in glintCenters:
            assert_true(glint in gd.glintCenters)
        gd.monitorGaze = haytham.Point(14, 15)
        assert_equals(gd.monitorGaze, haytham.Point(14, 15))
        gd.pupilCenter = haytham.Point(23, 24)
        assert_equals(gd.pupilCenter, haytham.Point(23, 24))
        assert_equals(gd.pupilGlintVector(), haytham.Point(21,21))
        gd.pupilDiameter = 12
        assert_equals(gd.pupilDiameter, 12)
        gd.pupilEllipse = ((10, 11), (12, 13), 14)
        assert_equals(gd.pupilEllipse, ((10, 11), (12, 13), 14))
        gd.tag = 'test'
        assert_equals(gd.tag, 'test')
        gd.pupilFound = True
        assert_true(gd.pupilFound)

    def test_gaze_data_add_offset(self):
        gd = haytham.GazeData()
        gd.gaze = haytham.Point(10, 20)
        glintCenters = [haytham.Point(1, 2), haytham.Point(3, 4)]
        gd.glintCenters = glintCenters
        gd.monitorGaze = haytham.Point(14, 15)
        gd.pupilCenter = haytham.Point(23, 24)
        gd.pupilDiameter = 12
        gd.pupilEllipse = ((10, 11), (12, 13), 14)
        gd.pupilFound = True

        # Add offset
        off = haytham.Point(10, 11)
        gd.addOffset(off)

        print(gd.gaze)
        assert_equals(gd.gaze, haytham.Point(10, 20))
        assert_equals(len(gd.glintCenters), 2)
        for glint in glintCenters:
            assert_true((glint+off) in gd.glintCenters)
        assert_equals(gd.monitorGaze, haytham.Point(14, 15))
        assert_equals(gd.pupilCenter, haytham.Point(33, 35))
        assert_equals(gd.pupilGlintVector(), haytham.Point(21,21))
        assert_equals(gd.pupilDiameter, 12)
        assert_equals(gd.pupilEllipse, ((20, 22), (12, 13), 14))
        assert_true(gd.pupilFound)
