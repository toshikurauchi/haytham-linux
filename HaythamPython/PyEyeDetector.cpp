/*
 * PyEyeDetector.cpp
 *
 *  Created on: 17/12/2013
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>

#include "PyEyeDetector.h"
#include "EyeDetection/EyeDetector.h"

namespace haytham {
using namespace boost::python;

struct EyeDetectorWrap : EyeDetector, wrapper<EyeDetector> {
    GazeData findPupil(cv::Mat const &eyeImg, int threshold, int irisDiameter, double minPupilScale=0.1, double maxPupilScale=0.9, int glintThreshold=-1) {
        return this->get_override("findPupil")(eyeImg, threshold, irisDiameter, minPupilScale, maxPupilScale, glintThreshold);
    }
};

void PyEyeDetector::registerPyEyeDetector() {
    class_<EyeDetectorWrap, boost::noncopyable>("EyeDetector", no_init)
            .def("findPupil", pure_virtual(&EyeDetector::findPupil), (boost::python::arg("minPupilScale")=0.1, boost::python::arg("maxPupilScale")=0.9, boost::python::arg("glintThreshold")=-1))
            .def("methodName", &EyeDetector::getMethodName, return_value_policy<return_by_value>());
    class_<ThresholdEyeDetector, bases<EyeDetector> >("ThresholdEyeDetector");
    class_<StarburstEyeDetector, bases<EyeDetector> >("StarburstEyeDetector");
}

} // namespace haytham
