/*
 * PyUtils.cpp
 *
 *  Created on: 18/12/2013
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <opencv2/opencv.hpp>
#include <vector>

#include "PyUtils.h"
#include "Common/Point.h"
#include "Common/Blob.h"

namespace haytham {
using namespace boost::python;

// Source (attachment): http://boost.2283326.n4.nabble.com/boost-python-writing-converters-for-list-to-std-vector-lt-T-gt-td4574497.html
template<typename T>
struct Vector_from_python_list
{
    Vector_from_python_list() {
        boost::python::converter::registry::push_back(
                    &Vector_from_python_list<T>::convertible,
                    &Vector_from_python_list<T>::construct,
                    type_id<std::vector<T> >());
    }

    // Determine if obj_ptr can be converted in a std::vector<T>
    static void* convertible(PyObject* obj_ptr) {
        if (!PyList_Check(obj_ptr)) {
            return 0;
        }
        return obj_ptr;
    }

    // Convert obj_ptr into a std::vector<T>
    static void construct(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data) {
        list l(handle<>(borrowed(obj_ptr)));

        // Grab pointer to memory into which to construct the new std::vector<T>
        void* storage = ((boost::python::converter::rvalue_from_python_storage<std::vector<T> >*)data)->storage.bytes;

        // in-place construct the new std::vector<T> using the character data
        // extraced from the python object
        std::vector<T>& v = *(new (storage) std::vector<T>());

        // populate vector with list content
        int le = len(l);
        v.resize(le);
        for(int i = 0; i != le; i++){
            v[i] = extract<T>(l[i]);
        }

        // Stash the memory chunk pointer for later use by boost.python
        data->convertible = storage;
    }
};

template<typename T>
struct Vector_to_python_list
{
    static PyObject* convert(std::vector<T> const& v) {
        list l;
        typename std::vector<T>::const_iterator p;
        for(p = v.begin(); p != v.end(); p++){
            l.append(object(*p));
        }
        return boost::python::incref(l.ptr());
    }
};

void PyUtils::registerUtils() {
    Vector_from_python_list<Point>();
    to_python_converter<std::vector<Point>, Vector_to_python_list<Point> >();
    Vector_from_python_list<cv::Point2f>();
    to_python_converter<std::vector<cv::Point2f>, Vector_to_python_list<cv::Point2f> >();
    Vector_from_python_list<cv::Point>();
    to_python_converter<std::vector<cv::Point>, Vector_to_python_list<cv::Point> >();
    Vector_from_python_list<Blob>();
    to_python_converter<std::vector<Blob>, Vector_to_python_list<Blob> >();
}

} // namespace haytham
