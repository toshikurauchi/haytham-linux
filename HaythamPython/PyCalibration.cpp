/*
 * PyCalibration.cpp
 *
 *  Created on: 20/12/2013
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>

#include "PyCalibration.h"
#include "Calibration/Calibration.h"

namespace haytham {
using namespace boost::python;

struct CalibrationWrap : Calibration, wrapper<Calibration> {
    bool addPoint(Point origin, Point destination, SceneData sceneData) {
        return this->get_override("addPoint")(origin, destination, sceneData);
    }

    bool addPoint(Point origin, Point destination) {
        return this->get_override("addPoint")(origin, destination);
    }

    void startCalibrating() {
        this->get_override("startCalibrating")();
    }

    void stopCalibrating() {
        this->get_override("stopCalibrating")();
    }

    bool isReady() {
        return this->get_override("isReady")();
    }

    int getCurrentPoint() {
        return this->get_override("getCurrentPoint")();
    }

    cv::Size getPatternSize() {
        return this->get_override("getPatternSize")();
    }

    void executeCalibration() {
        this->get_override("executeCalibration")();
    }

    Point executeMap(Point point, SceneData &sceneData) {
        return this->get_override("executeMap")(point, sceneData);
    }
};

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mapOverloadsWithData, Calibration::map, 2, 3)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(mapOverloadsWithoutData, Calibration::map, 1, 2) // QtCreator indicates an error here, but it's ok :)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(addPointOverloads, Calibration::addPoint, 2, 3)

void PyCalibration::registerPyCalibration() {
    class_<CalibrationWrap, boost::noncopyable>("Calibration", no_init)
            .def("executeCalibration", pure_virtual(&Calibration::executeCalibration))
            .def("executeMap", pure_virtual(&Calibration::executeMap))
            .def("calibrate", &Calibration::calibrate)
            .def("map", static_cast< Point(Calibration::*)(Point, const SceneData&, Point)>(&Calibration::map), mapOverloadsWithData())
            .def("map", static_cast< Point(Calibration::*)(Point, Point)>(&Calibration::map), mapOverloadsWithoutData())
            .def("addPoint", static_cast< bool(Calibration::*)(Point, Point, SceneData)>(&Calibration::addPoint), addPointOverloads())
            .def("startCalibrating", &Calibration::startCalibrating)
            .def("stopCalibrating", &Calibration::stopCalibrating)
            .def("isCalibrating", &Calibration::isCalibrating)
            .def("isCalibrated", &Calibration::isCalibrated)
            .def("isReady", &Calibration::isReady)
            .def("methodName", &Calibration::getMethodName, return_value_policy<return_by_value>())
            .def("remainingPoints", &Calibration::getRemainingPoints)
            .def("totalPoints", &Calibration::getTotalPoints)
            .def("currentPoint", &Calibration::getCurrentPoint)
            .def("patternSize", pure_virtual(&Calibration::getPatternSize))
            .def("requiresSceneData", &Calibration::getRequiresSceneData)
            .def("originPoints", &Calibration::getOriginPoints, return_value_policy<return_by_value>())
            .def("destinationPoints", &Calibration::getDestinationPoints, return_value_policy<return_by_value>());
    class_<PolynomialCalibration9PointsFull, bases<Calibration> >("PolynomialCalibration9PointsFull");
    class_<PolynomialCalibration9Points, bases<Calibration> >("PolynomialCalibration9Points");
    class_<PolynomialCalibration4Points, bases<Calibration> >("PolynomialCalibration4Points");
    class_<HomographyCalibration, bases<Calibration> >("HomographyCalibration", init<int>());
    class_<TwoDistancesCalibration, bases<Calibration> >("TwoDistancesCalibration", init<CameraCalibration&>()[with_custodian_and_ward<1,2>()]);
    class_<CerolazaCalibration9Points, bases<Calibration> >("CerolazaCalibration9Points");
}

} // namespace haytham
