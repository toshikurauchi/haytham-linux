/*
 * PySceneData.cpp
 *
 *  Created on: 20/12/2013
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>

#include "PySceneData.h"
#include "Calibration/SceneData.h"

namespace haytham {
using namespace boost::python;

void PySceneData::registerPySceneData() {
    class_<SceneData>("SceneData")
            .add_property("calibrationPoints", &SceneData::getCalibrationPoints, &SceneData::setCalibrationPoints);
}

} // namespace haytham
