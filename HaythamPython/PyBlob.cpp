/*
 * PyBlob.cpp
 *
 *  Created on: 17/01/2014
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>

#include "PyBlob.h"
#include "Common/Blob.h"

namespace haytham {
using namespace boost::python;

void PyBlob::registerPyBlob() {
    class_<Blob>("Blob")
            .def(init<std::vector<cv::Point> >())
            .def("size", &Blob::size)
            .def("boundingBox", &Blob::getBoundingBox, return_value_policy<copy_const_reference>())
            .def("points", &Blob::getPoints, return_value_policy<copy_const_reference>())
            .def("hull", &Blob::getHull, return_value_policy<copy_const_reference>())
            .def("fullness", &Blob::getFullness)
            .def("centerOfGravity", &Blob::getCenterOfGravity)
            .def("area", &Blob::getArea)
            .def("empty", &Blob::empty);
}

} // namespace haytham
