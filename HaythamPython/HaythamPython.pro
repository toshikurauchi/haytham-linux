TEMPLATE = lib

CONFIG += plugin
QT += network

PY3 = TRUE

# Including HaythamLib

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../HaythamLib/release/ -lHaythamLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../HaythamLib/debug/ -lHaythamLib
else:unix:!symbian: LIBS += -L$$OUT_PWD/../HaythamLib/ -lHaythamLib

INCLUDEPATH += $$PWD/../HaythamLib
DEPENDPATH += $$PWD/../HaythamLib

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../HaythamLib/release/HaythamLib.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../HaythamLib/debug/HaythamLib.lib
else:unix:!symbian: PRE_TARGETDEPS += $$OUT_PWD/../HaythamLib/libHaythamLib.a

INCLUDEPATH += /usr/include/boost \
               /usr/local/include/opencv2 \
               /opt/local/include
isEmpty(PY3) {
    INCLUDEPATH += /usr/include/python2.7
} else {
    INCLUDEPATH += /usr/include/python3.3
}

DEFINES += BOOST_PYTHON_DYNAMIC_LIB

isEmpty(PY3) {
LIBS += -L/usr/lib/python2.7 \
        -lpython2.7 \
} else {
LIBS += -L/usr/lib/python3.3 \
        -lpython3.3m \
}
LIBS += -L/usr/local/lib \
        `pkg-config --static --cflags --libs opencv` \
        -lpthread \
        -lvrpn
unix:!macx {
isEmpty(PY3) {
    LIBS += -lboost_python-py27 \
} else {
    LIBS += -lboost_python3 \
}
LIBS += -lboost_regex \
        -lboost_chrono \
        -lboost_system
}
macx {
LIBS += -L/opt/local/lib
isEmpty(PY3) {
    LIBS += -lboost_python-py27-mt \
} else {
    LIBS += -lboost_python-py32-mt \
}
LIBS += -lboost_regex-mt \
        -lboost_chrono-mt \
        -lboost_system-mt
}

# To copy tests to build directory (must add build step 'make install')
nose_tests.path = $$OUT_PWD
nose_tests.files = tests __init__.py
INSTALLS += nose_tests

SOURCES += *.cpp
HEADERS += *.h

# Python wants the library name to be haytham.so when we import haytham.
QMAKE_PRE_LINK = rm -f haytham.so
QMAKE_POST_LINK = ln -s libhaytham.so haytham.so
QMAKE_DISTCLEAN += haytham.so

TARGET = haytham
