/*
 * PythonToOCV.h
 *
 *  Created on: 13/12/2013
 *      Author: Andrew Kurauchi
 *
 * Source: http://stackoverflow.com/questions/19136944/call-c-opencv-functions-from-python-send-a-cv-mat-to-c-dll-which-is-usi
 */

#ifndef PYTHONTOOCV_H
#define PYTHONTOOCV_H

#include <iostream>
#include <Python.h>
#include <boost/python.hpp>
#include "numpy/ndarrayobject.h"
#include "opencv2/core/core.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/core/mat.hpp"

/**
 * \brief Import Numpy array. Necessary to avoid PyArray_Check() to crash
 */
void doImport( );

int failmsg( const char *fmt, ... );

static size_t REFCOUNT_OFFSET = ( size_t )&((( PyObject* )0)->ob_refcnt ) +
( 0x12345678 != *( const size_t* )"\x78\x56\x34\x12\0\0\0\0\0" )*sizeof( int );

static inline PyObject* pyObjectFromRefcount( const int* refcount )
{
    return ( PyObject* )(( size_t )refcount - REFCOUNT_OFFSET );
}

static inline int* refcountFromPyObject( const PyObject* obj )
{
    return ( int* )(( size_t )obj + REFCOUNT_OFFSET );
}

class NumpyAllocator : public cv::MatAllocator
{
public:
    NumpyAllocator( ) { stdAllocator = cv::Mat::getStdAllocator(); }
    ~NumpyAllocator( ) { }

    cv::UMatData* allocate(PyObject* o, int dims, const int* sizes, int type, size_t* step) const;
    cv::UMatData* allocate(int dims0, const int* sizes, int type, void* data, size_t* step, int flags, cv::UMatUsageFlags usageFlags) const;
    bool allocate(cv::UMatData* u, int accessFlags, cv::UMatUsageFlags usageFlags) const;
    void deallocate(cv::UMatData* u) const;

    const MatAllocator* stdAllocator;
};

struct ArgInfo
{
    const char * name;
    bool outputarg;
    // more fields may be added if necessary

    ArgInfo(const char * name_, bool outputarg_)
        : name(name_)
        , outputarg(outputarg_) {}

    // to match with older pyopencv_to function signature
    operator const char *() const { return name; }
};

/**
 * \brief Convert a numpy array to a cv::Mat. This is used to import images
 * from Python.
 * This function is extracted from opencv/modules/python/src2/cv2.cpp
 * in OpenCV 2.4
 */
bool pyopencv_to(PyObject* o, cv::Mat& m, const ArgInfo info);

/**
 * \brief Convert a cv::Mat to a numpy array.
 * This function is extracted from opencv/modules/python/src2/cv2.cpp
 * in OpenCV 2.4
 */
PyObject* pyopencv_from(const cv::Mat& m);

/**
 * \brief Convert a Python tuple to a cv::Rect.
 * This function is extracted from opencv/modules/python/src2/cv2.cpp
 * in OpenCV 2.4
 */
bool pyopencv_to(PyObject *obj, cv::Rect& dst, const char *name);

/**
 * \brief Convert a cv::Rect to a Python tuple.
 * This function is extracted from opencv/modules/python/src2/cv2.cpp
 * in OpenCV 2.4
 */
PyObject* pyopencv_from(const cv::Rect& src);

/**
 * \brief Convert a Python tuple to a cv::RotatedRect.
 * This function is extracted from opencv/modules/python/src2/cv2.cpp
 * in OpenCV 2.4
 */
bool pyopencv_to(PyObject *obj, cv::RotatedRect& dst, const char *name);

/**
 * \brief Convert a cv::RotatedRect to a Python tuple.
 * This function is extracted from opencv/modules/python/src2/cv2.cpp
 * in OpenCV 2.4
 */
PyObject* pyopencv_from(const cv::RotatedRect& src);

/**
 * \brief Convert a Python tuple to a cv::Size.
 * This function is extracted from opencv/modules/python/src2/cv2.cpp
 * in OpenCV 2.4
 */
bool pyopencv_to(PyObject* obj, cv::Size& sz, const char* name);

/**
 * \brief Convert a cv::Size to a Python tuple.
 * This function is extracted from opencv/modules/python/src2/cv2.cpp
 * in OpenCV 2.4
 */
PyObject* pyopencv_from(const cv::Size& sz);

/**
 * \brief Convert a Python tuple to a cv::Point2f.
 * This function is extracted from opencv/modules/python/src2/cv2.cpp
 * in OpenCV 2.4
 */
bool pyopencv_to(PyObject* obj, cv::Point2f& p, const char* name);

/**
 * \brief Convert a cv::Point2f to a Python tuple.
 * This function is extracted from opencv/modules/python/src2/cv2.cpp
 * in OpenCV 2.4
 */
PyObject* pyopencv_from(const cv::Point2f& p);

/**
 * \brief Convert a Python tuple to a cv::Point.
 * This function is extracted from opencv/modules/python/src2/cv2.cpp
 * in OpenCV 2.4
 */
bool pyopencv_to(PyObject* obj, cv::Point& p, const char* name);

/**
 * \brief Convert a cv::Point to a Python tuple.
 * This function is extracted from opencv/modules/python/src2/cv2.cpp
 * in OpenCV 2.4
 */
PyObject* pyopencv_from(const cv::Point& p);

#endif // PYTHONTOOCV_H
