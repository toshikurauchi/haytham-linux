/*
 * PyMonitorDetector.h
 *
 *  Created on: 19/12/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_PYMONITORDETECTOR_H
#define HAYTHAM_PYMONITORDETECTOR_H

namespace haytham {

struct PyMonitorDetector
{
    static void registerPyMonitorDetector();
};

} // namespace haytham

#endif // HAYTHAM_PYMONITORDETECTOR_H
