/*
 * PyGlintDetector.cpp
 *
 *  Created on: 17/01/2014
 *      Author: Andrew Kurauchi
 */

#include <boost/python.hpp>

#include "PyGlintDetector.h"
#include "EyeDetection/GlintDetector.h"

namespace haytham {
using namespace boost::python;

void PyGlintDetector::registerPyGlintDetector() {
    class_<GlintDetector>("GlintDetector")
        .def("detectGlint", &GlintDetector::detectGlint);
}

} // namespace haytham
