/*
 * SceneMain.h
 *
 *  Created on: 17/01/2013
 *      Author: Andrew Kurauchi
 */

#ifndef SCENEMAIN_H
#define SCENEMAIN_H

#include "Common/METState.h"
#include "MonitorDetection/MonitorDetector.h"
#include "ImageProcessing/ImageProcessing.h"

namespace haytham {

class SceneMain {
public:
    SceneMain(METState *state);

    void mainFunction(GazeData &data);
private:
    METState *state;
    ImageProcessing imageProcessing;
};

} // namespace haytham

#endif // SCENEMAIN_H
