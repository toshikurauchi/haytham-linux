#ifndef HAYTHAM_NETWORKTHREAD_H
#define HAYTHAM_NETWORKTHREAD_H

// Qt header files
#include <QThread>
#include <QtGui>
#include <QObject>

#include "EyeDetection/GazeData.h"
#include "HeadGesture/HeadGesture.h"
#include "Network/Server.h"
#include "Common/METState.h"

namespace haytham {

class NetworkThread : public QThread
{
    Q_OBJECT
public:
    NetworkThread(METState *state, QObject *parent);
    void stopNetworkThread();
    int getAvgFPS();
    Server *getServerPointer();
protected:
    void run();
private:
    void updateFPS(int);
    QTime t;
    QMutex stoppedMutex;
    QMutex dataMutex;
    QMutex gestureMutex;
    int captureTime;
    int avgFPS;
    QQueue<int> fps;
    int sampleNo;
    int fpsSum;
    volatile bool stopped;
    Server server;
    bool hasData;
    BinocularGazeData data;
    bool hasGesture;
    HeadGesture gesture;
private slots:
    void updateData(BinocularGazeData data);
    void updateGesture(HeadGesture gesture);
};

} // namespace haytham

#endif // HAYTHAM_NETWORKTHREAD_H
