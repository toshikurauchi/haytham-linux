/*
 * DataTab.h
 *
 *  Created on: 18/10/2012
 *      Author: Andrew Kurauchi
 */

#ifndef DATATAB_H
#define DATATAB_H

#include <QObject>
#include <QString>
#include <QPushButton>
#include <QCheckBox>

#include "Common/METState.h"

namespace haytham {

class DataTab : public QObject
{
    Q_OBJECT
public:
    explicit DataTab(METState *currentState, QObject *parent);
    void setStartStopButton(QPushButton *startStopButton);
    void setRecordEyeVideoCheckbox(QCheckBox *recordEyeVideoCheckbox);
    void setRecordSceneVideoCheckbox(QCheckBox *recordSceneVideoCheckbox);
    void setRecordGazeDataCheckbox(QCheckBox *recordGazeDataCheckbox);
    bool startRecording();
    void stopRecording();

public slots:
    void startStopRecording();

private:
    METState *state;
    QPushButton *startStopButton;
    QCheckBox *recordEyeVideoCheckbox;
    QCheckBox *recordSceneVideoCheckbox;
    QCheckBox *recordGazeDataCheckbox;
};

} /* namespace haytham */

#endif // DATATAB_H
