/*
 * EyeTab.cpp
 *
 *  Created on: 20/08/2012
 *      Author: Andrew Kurauchi
 */

#include <QMessageBox>

#include "EyeTab.h"
#include "Common/METState.h"
#include "Common/DefaultConfiguration.h"

namespace haytham {

/**
 * \param currentState Current METState (tipically the one returned by METState::getCurrent())
 */
EyeTab::EyeTab(METState *currentState, QObject *parent) : QObject(parent), state(currentState) {
    this->detectors.push_back(new ThresholdEyeDetector);
    this->detectors.push_back(new TemplateEyeDetector(currentState));
    this->detectors.push_back(new ColorEyeDetector(currentState));
    this->detectors.push_back(new StarburstEyeDetector);
}

EyeTab::~EyeTab() {
    for(std::vector<EyeDetector*>::iterator it = this->detectors.begin(); it != this->detectors.end(); it++) {
        delete(*it);
    }
}

void EyeTab::updateIrisSize(int newValue) {
	this->state->irisDiameter = newValue;
}

void EyeTab::setBinocularWidgets(QCheckBox *use_binocular, QPushButton *set_left_eye_roi, QPushButton *set_right_eye_roi) {
    this->set_left_eye_roi = set_left_eye_roi;
    this->set_right_eye_roi = set_right_eye_roi;

    use_binocular->setChecked(this->state->getUseBinocular());
    if(!this->state->getUseBinocular()) {
        this->set_left_eye_roi->setEnabled(false);
    }
    connect(use_binocular, SIGNAL(clicked(bool)), this, SLOT(updateUseBinocular(bool)));

    connect(set_left_eye_roi, SIGNAL(clicked()), this, SLOT(setLeftEyeROI()));
    connect(set_right_eye_roi, SIGNAL(clicked()), this, SLOT(setRightEyeROI()));
}

void EyeTab::setIrisSizeSlider(QSlider *iris_size) {
    iris_size->setRange(10, 500);
    iris_size->setValue(IRIS_SIZE);
    connect(iris_size, SIGNAL(valueChanged(int)), this, SLOT(updateIrisSize(int)));
    this->updateIrisSize(iris_size->value());
}

void EyeTab::setShowIrisSizeCheck(QCheckBox *show_iris_size) {
    connect(show_iris_size, SIGNAL(clicked(bool)), this, SLOT(updateShowIrisSize(bool)));
}

void EyeTab::setShowPupilCheck(QCheckBox *show_pupil) {
    connect(show_pupil, SIGNAL(clicked(bool)), this, SLOT(updateShowPupil(bool)));
    show_pupil->setChecked(SHOW_PUPIL);
    this->updateShowPupil(show_pupil->isChecked());
}

void EyeTab::setPupilThresholdSlider(QSlider *pupil_threshold) {
    pupil_threshold->setRange(0, 300);
    pupil_threshold->setValue(PUPIL_THRESHOLD);
    connect(pupil_threshold, SIGNAL(valueChanged(int)), this, SLOT(updatePupilThreshold(int)));
    this->updatePupilThreshold(pupil_threshold->value());
}

void EyeTab::setFillGapsCheck(QCheckBox *fill_gaps) {
    connect(fill_gaps, SIGNAL(clicked(bool)), this, SLOT(updateFillGaps(bool)));
    this->updateFillGaps(fill_gaps->isChecked());
}

void EyeTab::setRemoveGlintCheck(QCheckBox *remove_glint) {
    connect(remove_glint, SIGNAL(clicked(bool)), this, SLOT(updateRemoveGlint(bool)));
    this->updateRemoveGlint(remove_glint->isChecked());
}

void EyeTab::setShowGlintCheck(QCheckBox *show_glint) {
    connect(show_glint, SIGNAL(clicked(bool)), this, SLOT(updateShowGlint(bool)));
    show_glint->setChecked(SHOW_GLINT);
    this->updateShowGlint(show_glint->isChecked());
}

void EyeTab::setGlintThresholdSlider(QSlider *glint_threshold) {
    glint_threshold->setRange(0, 300);
    glint_threshold->setValue(GLINT_THRESHOLD);
    connect(glint_threshold, SIGNAL(valueChanged(int)), this, SLOT(updateGlintThreshold(int)));
    this->updateGlintThreshold(glint_threshold->value());
}

void EyeTab::setNumberOfGlintsCombo(QComboBox *numberOfGlintsCombo) {
    connect(numberOfGlintsCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateNumberOfGlints(int)));
    int initialIdx = 1;
    numberOfGlintsCombo->setCurrentIndex(initialIdx);
    updateNumberOfGlints(initialIdx);
}

void EyeTab::setEyeDetectorCombo(QComboBox *eyeDetectorCombo) {
    for(std::vector<EyeDetector*>::iterator it = this->detectors.begin(); it != this->detectors.end(); it++) {
        eyeDetectorCombo->addItem(QString((*it)->getMethodName().c_str()), QVariant::fromValue(*it));
    }

    updateEyeDetector(0);
    connect(eyeDetectorCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateEyeDetector(int)));
}

void EyeTab::updateUseBinocular(bool newValue) {
    this->state->useBinocular = newValue;
    // If use binocular enable left eye button, else disable it.
    this->set_left_eye_roi->setEnabled(newValue);
}

void EyeTab::setLeftEyeROI() {
    this->state->settingLeftEyeROI = true;
    this->state->settingRightEyeROI = false;
}

void EyeTab::setRightEyeROI() {
    this->state->settingLeftEyeROI = false;
    this->state->settingRightEyeROI = true;
}

/**
 * \param newValue New value set on checkbox.
 *
 * \brief Function called when showIrisSize checkbox was clicked in the GUI.
 */
void EyeTab::updateShowIrisSize(bool newValue) {
    this->state->showIris = newValue;
}

/**
 * \param newValue New value set on checkbox.
 *
 * \brief Function called when showPupil checkbox was clicked in the GUI.
 */
void EyeTab::updateShowPupil(bool newValue) {
	this->state->showPupil = newValue;
}

/**
 * \param newValue New value set on slider.
 *
 * \brief Function called when pupilThreshold slider value has changed.
 */
void EyeTab::updatePupilThreshold(int newValue) {
    this->state->pupilThreshold = newValue;
}

/**
 * \param newValue New value set on checkbox.
 *
 * \brief Function called when fillGaps checkbox was clicked in the GUI.
 */
void EyeTab::updateFillGaps(bool newValue) {
	this->state->dilateErode = newValue;
}

/**
 * \param newValue New value set on checkbox.
 *
 * \brief Function called when removeGlint checkbox was clicked in the GUI.
 */
void EyeTab::updateRemoveGlint(bool newValue) {
    this->state->removeGlint = newValue;
}

/**
 * \param newValue New value set on checkbox.
 *
 * \brief Function called when showGlint checkbox was clicked in the GUI.
 */
void EyeTab::updateShowGlint(bool newValue) {
    this->state->showGlint = newValue;
}

/**
 * \param newValue New value set on slider.
 *
 * \brief Function called when glintThreshold slider value has changed.
 */
void EyeTab::updateGlintThreshold(int newValue) {
    this->state->glintThreshold = newValue;
}

/**
 * \param index New index selected.
 *
 * \brief Function called when number_of_glints combobox has its value changed.
 */
void EyeTab::updateNumberOfGlints(int index) {
    this->state->numberOfGlints = index + 1;
}

void EyeTab::updateEyeDetector(int index) {
    this->state->detector = this->detectors.at(index);
}

} /* namespace haytham */
