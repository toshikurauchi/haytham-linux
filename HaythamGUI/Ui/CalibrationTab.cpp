/*
 * CalibrationTab.cpp
 *
 *  Created on: 14/11/2012
 *      Author: Andrew Kurauchi
 */

#include <map>
#include <string>
#include <QVariant>
#include <QDebug>

#include "CalibrationTab.h"
#include "Calibration/Calibration.h"
#include "Common/DefaultConfiguration.h"

namespace haytham {

/**
 * \brief Constructor.
 */
CalibrationTab::CalibrationTab(METState *state, QObject *parent) :
    QObject(parent)
{
    this->state = state;
}

void CalibrationTab::setMethodsCombo(QComboBox *methodsCombo) {
    this->methodsCombo = methodsCombo;
    std::map<std::string, std::pair<Calibration*, Calibration*>* > methods = this->state->getCalibrationMethods();
    for(std::map<std::string, std::pair<Calibration*, Calibration*>* >::iterator it = methods.begin(); it != methods.end(); it++) {
        methodsCombo->addItem(QString(it->first.c_str()), QVariant::fromValue((void*)it->second));
    }
    connect(this->methodsCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(methodChanged(int)));
    this->methodsCombo->setCurrentIndex(4);
}

void CalibrationTab::setStartButton(QPushButton *startButton, SceneLabelProcessor *sceneLabelProcessor) {
    this->startButton = startButton;
    this->sceneLabelProcessor = sceneLabelProcessor;
    connect(this->startButton, SIGNAL(clicked()), this, SLOT(startCalibrating()));
}

void CalibrationTab::setSmoothGazeCheck(QCheckBox *smoothGazeCheck) {
    this->smoothGazeCheck = smoothGazeCheck;
    connect(this->smoothGazeCheck, SIGNAL(clicked(bool)), this, SLOT(smoothGazeChanged(bool)));
    this->smoothGazeCheck->setChecked(this->state->getSmoothGaze());
}

void CalibrationTab::setRemainingCalibrationPointsLabel(CalibrationPointsLabel *remainingCalibrationPointsLabel) {
    this->remainingCalibrationPointsLabel = remainingCalibrationPointsLabel;
}

void CalibrationTab::setCalibPointDetectThreshSlider(QSlider *calibPointDetectThreshSlider) {
    this->calibPointDetectThreshSlider = calibPointDetectThreshSlider;
    connect(this->calibPointDetectThreshSlider, SIGNAL(sliderMoved(int)), this, SLOT(updateCalibPointThresh(int)));
    this->calibPointDetectThreshSlider->setMinimum(MIN_CALIB_POINTS_THRESH);
    this->calibPointDetectThreshSlider->setMaximum(MAX_CALIB_POINTS_THRESH);
    this->calibPointDetectThreshSlider->setValue(this->state->getCalibPointDetectThresh());
}

void CalibrationTab::setPupilCenterRadio(QRadioButton *pupilCenterRadio) {
    connect(pupilCenterRadio, SIGNAL(toggled(bool)), this, SLOT(updatePupilCenterRadio(bool)));
}

void CalibrationTab::setPupilGlintRadio(QRadioButton *pupilGlintRadio) {
    connect(pupilGlintRadio, SIGNAL(toggled(bool)), this, SLOT(updatePupilGlintRadio(bool)));
    pupilGlintRadio->setChecked(true);
}

void CalibrationTab::methodChanged(int index) {
    std::pair<Calibration*, Calibration*> *selectedMethod = (std::pair<Calibration*, Calibration*>*) qvariant_cast<void*>(this->methodsCombo->itemData(index));
    this->state->setEyeToSceneMapping(selectedMethod);
}

void CalibrationTab::startCalibrating() {
    if(QString::compare(this->startButton->text(), QString("Start Calibrating"), Qt::CaseInsensitive) == 0) {
        Calibration *selectedMethod;
        for(int left = 0; left <= this->state->getUseBinocular(); left++) {
            if(left)
                selectedMethod = this->state->getLeftEyeToSceneMapping();
            else
                selectedMethod = this->state->getRightEyeToSceneMapping();
            selectedMethod->startCalibrating();
        }
        this->startButton->setText("Stop Calibrating");
        this->remainingCalibrationPointsLabel->showRemainingPoints(selectedMethod->getRemainingPoints());
    }
    else {
        this->state->getLeftEyeToSceneMapping()->stopCalibrating();
        this->state->getRightEyeToSceneMapping()->stopCalibrating();
        this->startButton->setText("Start Calibrating");
        this->remainingCalibrationPointsLabel->setText("");
        this->sceneLabelProcessor->resetCalibrationPoints();
    }
}

void CalibrationTab::smoothGazeChanged(bool value) {
    this->state->setSmoothGaze(value);
}

void CalibrationTab::showGazeChanged(bool value) {
    this->state->setShowGaze(value);
}

void CalibrationTab::updateCalibPointThresh(int value) {
    this->state->setCalibPointDetectThresh(value);
}

void CalibrationTab::updatePupilCenterRadio(bool checked) {
    if(checked) {
        this->state->setUsePupilGlint(false);
    }
}

void CalibrationTab::updatePupilGlintRadio(bool checked) {
    if(checked) {
        this->state->setUsePupilGlint(true);
    }
}

} /* namespace haytham */
