/*
 * SceneTab.h
 *
 *  Created on: 17/01/2013
 *      Author: Andrew Kurauchi
 */

#ifndef SCENETAB_H
#define SCENETAB_H

#include <QObject>
#include <QSlider>
#include <QCheckBox>
#include <QGroupBox>
#include <QComboBox>
#include <QPushButton>
#include <QLabel>
#include <vector>

#include "Common/METState.h"

namespace haytham {

class SceneTab : public QObject
{
    Q_OBJECT
public:
    explicit SceneTab(METState *state, QObject *parent);
    void setThresholdSlider(QSlider *thresholdSlider);
    void setScreenMinSizeSlider(QSlider *screenMinSizeSlider);
    void setShowScreenCheckbox(QCheckBox *showScreenCheckbox);
    void setShowFilteredSceneCheckbox(QCheckBox *showFilteredSceneCheckbox);
    void setVisualizationGroup(QGroupBox *visualizationGroup);
    void setScreenDetectionMethodCombo(QComboBox *screenDetectionMethodCombo);
    void setCalibrateCameraButton(QPushButton *calibrateCameraButton);
    void setAddCalibrationFrameButton(QPushButton *addCalibrationFrameButton);
    void setLoadCalibrationButton(QPushButton *loadCalibrationButton);
    void setCalibrationFileLabel(QLabel *calibrationFileLabel);
    void setUndistortCheckbox(QCheckBox *undistortCheckbox);
    void addVisualizationPosition(int pos);
    void removeVisualizationPosition(int pos);
public slots:
    void updateSceneThreshold(int threshold);
    void updateScreenMinSize(int screenMinSize);
    void screenMinSliderPressed();
    void screenMinSliderReleased();
    void updateShowScreen(bool showScreen);
    void updateShowFilteredScene(bool showFilteredScene);
    void updateScreenDetectionMethod(int index);
    void updateUndistort(bool undistort);
    void calibrateCamera();
    void addCalibrationFrame();
    void loadCalibrationFile();
private:
    METState *state;
    QSlider *thresholdSlider;
    QSlider *screenMinSizeSlider;
    QCheckBox *showScreenCheckbox;
    QCheckBox *showFilteredSceneCheckbox;
    QComboBox *screenDetectionMethodCombo;
    QPushButton *calibrateCameraButton;
    QPushButton *loadCalibrationButton;
    QPushButton *addCalibrationFrameButton;
    QLabel *calibrationFileLabel;
    QCheckBox *undistortCheckbox;
    std::vector<int> visualizationPositions;
    std::vector<MonitorDetector*> monitorDetectors;
};

class VisualizationCheckHandler : public QObject
{
    Q_OBJECT
public:
    VisualizationCheckHandler(int id, SceneTab* tab);
public slots:
    void visualizationCheck(bool checked);
private:
    int id;
    SceneTab *tab;
};

} // namespace haytham

#endif // SCENETAB_H
