/*
 * CameraTab.h
 *
 *  Created on: 25/07/2012
 *      Author: Andrew Kurauchi
 */

#ifndef CAMERATAB_H_
#define CAMERATAB_H_

#include <QObject>
#include <QComboBox>
#include <QPushButton>
#include <string>
#include <map>

#include "ui_mainwindow.h"
#include "Camera/CameraController.h"
#include "Common/VideoCategory.h"
#include "VideoProcessingThread.h"
#include "Controller.h"
#include "ImageBuffer.h"

namespace haytham {

class CameraWidget : public QObject {
    Q_OBJECT
public:
    CameraWidget(QPushButton *startStopButton, QPushButton *settingsButton, QComboBox *cameraComboBox, ImageBuffer &buffer, VideoCategory category, QObject *parent = 0);
    ~CameraWidget();
    CameraInfo getSelectedCamera();
    QPushButton *getSettingsButton();
    VideoCategory getCategory();
    bool isConnected();
    bool connectToCamera();
    bool disconnectCamera();
    bool shouldConnectToCamera();
private:
    QPushButton *startStopButton;
    QPushButton *settingsButton;
    QComboBox *cameraComboBox;
    bool isCameraConnected;
    Controller *controller;
    ImageBuffer &buffer;
    VideoCategory category;
};

class CameraComponent : public QObject {
    Q_OBJECT
public:
    CameraComponent(std::string name);
    ~CameraComponent();
    void setCameraWidget(CameraWidget *camera);
public slots:
    void openSettings();
private:
    std::string componentName;
    bool useLED;
    CameraWidget *camera;
};

class CameraTab : QObject {
    Q_OBJECT
public:
    CameraTab(METState *state, QWidget *window, QObject *parent = 0);
    ~CameraTab();
    CameraComponent &getEyeComponent();
    CameraComponent &getSceneComponent();
    void setWidgets(QPushButton *start_stop_eye_camera, QPushButton *start_stop_scene_camera, QPushButton *start_both_cameras, QPushButton *settings_eye_camera, QPushButton *settings_scene_camera, QComboBox *eye_camera, QComboBox *scene_camera, VideoProcessingThread &processingThread);
public slots:
    void connectOrDisconnectCamera(QObject *cameraObject);
    void startStopBothCameras();
private:
    METState *state;
    QWidget *window;
    QSignalMapper cameraSignalMapper;
    CameraComponent eyeComponent;
    CameraComponent sceneComponent;
    QPushButton *start_both_cameras;
    std::map<haytham::VideoCategory, CameraWidget*> cameraWidgets;
};

} /* namespace haytham */
#endif /* CAMERATAB_H_ */
