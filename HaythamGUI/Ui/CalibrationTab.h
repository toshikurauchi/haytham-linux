/*
 * CalibrationTab.h
 *
 *  Created on: 14/11/2012
 *      Author: Andrew Kurauchi
 */

#ifndef CALIBRATIONTAB_H
#define CALIBRATIONTAB_H

#include <QObject>
#include <QComboBox>
#include <QPushButton>
#include <QCheckBox>
#include <QLabel>
#include <QRadioButton>

#include "Common/METState.h"
#include "CalibrationPointsLabel.h"
#include "ImageLabel.h"
#include "EventListener.h"

namespace haytham {

class CalibrationTab : public QObject
{
    Q_OBJECT
public:
    explicit CalibrationTab(METState *state, QObject *parent = 0);
    void setMethodsCombo(QComboBox *methodsCombo);
    void setStartButton(QPushButton *startButton, SceneLabelProcessor *sceneLabelProcessor);
    void setSmoothGazeCheck(QCheckBox *smoothGazeCheck);
    void setRemainingCalibrationPointsLabel(CalibrationPointsLabel *remainingCalibrationPointsLabel);
    void setCalibPointDetectThreshSlider(QSlider *calibPointDetectThreshSlider);
    void setPupilCenterRadio(QRadioButton *pupilCenterRadio);
    void setPupilGlintRadio(QRadioButton *pupilGlintRadio);
public slots:
    void methodChanged(int index);
    void startCalibrating();
    void smoothGazeChanged(bool value);
    void showGazeChanged(bool value);
    void updateCalibPointThresh(int value);
    void updatePupilCenterRadio(bool checked);
    void updatePupilGlintRadio(bool checked);
private:
    METState *state;
    QComboBox *methodsCombo;
    QPushButton *startButton;
    QCheckBox *smoothGazeCheck;
    QCheckBox *showGazeCheck;
    SceneLabelProcessor *sceneLabelProcessor;
    CalibrationPointsLabel *remainingCalibrationPointsLabel;
    QSlider *calibPointDetectThreshSlider;
};

} /* namespace haytham */

#endif // CALIBRATIONTAB_H
