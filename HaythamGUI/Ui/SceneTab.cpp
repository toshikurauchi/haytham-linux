/*
 * SceneTab.cpp
 *
 *  Created on: 17/01/2013
 *      Author: Andrew Kurauchi
 */

#include <QVBoxLayout>
#include <algorithm>
#include <QDebug>
#include <QVariant>
#include <QFileDialog>
#include <sstream>

#include "SceneTab.h"
#include "Common/DefaultConfiguration.h"
#include "ImageProcessing/SceneVisualization.h"
#include "MonitorDetection/MonitorDetector.h"

namespace haytham {

SceneTab::SceneTab(METState *state, QObject *parent) : QObject(parent), state(state) {
    // Add MonitorDetectors here
    this->monitorDetectors.push_back(new ContrastMonitorDetector());
    this->monitorDetectors.push_back(new LedMonitorDetector());
    this->monitorDetectors.push_back(new ColorMonitorDetector());
}

void SceneTab::setThresholdSlider(QSlider *thresholdSlider) {
    this->thresholdSlider = thresholdSlider;
    this->thresholdSlider->setRange(0, 255); // Old value: 0-500
    this->thresholdSlider->setValue(SCENE_THRESHOLD);

    connect(this->thresholdSlider, SIGNAL(valueChanged(int)), this, SLOT(updateSceneThreshold(int)));
    updateSceneThreshold(this->thresholdSlider->value());
}

void SceneTab::setScreenMinSizeSlider(QSlider *screenMinSizeSlider) {
    this->screenMinSizeSlider = screenMinSizeSlider;
    this->screenMinSizeSlider->setRange(5, 100);
    this->screenMinSizeSlider->setValue(SCREEN_MIN_SIZE);

    connect(this->screenMinSizeSlider, SIGNAL(sliderMoved(int)), this, SLOT(updateScreenMinSize(int)));
    connect(this->screenMinSizeSlider, SIGNAL(sliderPressed()), this, SLOT(screenMinSliderPressed()));
    connect(this->screenMinSizeSlider, SIGNAL(sliderReleased()), this, SLOT(screenMinSliderReleased()));
    updateScreenMinSize(this->screenMinSizeSlider->value());
}

void SceneTab::setShowScreenCheckbox(QCheckBox *showScreenCheckbox) {
    this->showScreenCheckbox = showScreenCheckbox;
    this->showScreenCheckbox->setChecked(SHOW_SCREEN);

    connect(this->showScreenCheckbox, SIGNAL(clicked(bool)), this, SLOT(updateShowScreen(bool)));
    updateShowScreen(this->showScreenCheckbox->isChecked());
}

void SceneTab::setShowFilteredSceneCheckbox(QCheckBox *showFilteredSceneCheckbox) {
    this->showFilteredSceneCheckbox = showFilteredSceneCheckbox;
    this->showFilteredSceneCheckbox->setChecked(SHOW_FILTERED_SCENE_IMAGE);

    connect(this->showFilteredSceneCheckbox, SIGNAL(clicked(bool)), this, SLOT(updateShowFilteredScene(bool)));
    updateShowFilteredScene(this->showFilteredSceneCheckbox->isChecked());
}

void SceneTab::setVisualizationGroup(QGroupBox *visualizationGroup) {
    QVBoxLayout *vbox = new QVBoxLayout;
    std::vector<SceneVisualization*> visualizations = this->state->getSceneVisualizations();
    for(int i = 0; i < visualizations.size(); i++) {
        SceneVisualization *visualization = visualizations.at(i);
        QCheckBox *check = new QCheckBox(visualization->getName().c_str());
        connect(check, SIGNAL(clicked(bool)), new VisualizationCheckHandler(i, this), SLOT(visualizationCheck(bool)));
        check->show();
        vbox->addWidget(check);
    }
    visualizationGroup->setLayout(vbox);
}

void SceneTab::setScreenDetectionMethodCombo(QComboBox *screenDetectionMethodCombo) {
    this->screenDetectionMethodCombo = screenDetectionMethodCombo;

    for(std::vector<MonitorDetector*>::iterator it = this->monitorDetectors.begin(); it != this->monitorDetectors.end(); it++) {
        this->screenDetectionMethodCombo->addItem(QString((*it)->getMethodName().c_str()), QVariant::fromValue(*it));
    }

    this->updateScreenDetectionMethod(0);

    connect(this->screenDetectionMethodCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateScreenDetectionMethod(int)));
    screenDetectionMethodCombo->setCurrentIndex(0);
}

void SceneTab::setCalibrateCameraButton(QPushButton *calibrateCameraButton) {
    this->calibrateCameraButton = calibrateCameraButton;
    connect(this->calibrateCameraButton, SIGNAL(clicked()), this, SLOT(calibrateCamera()));
}

void SceneTab::setLoadCalibrationButton(QPushButton *loadCalibrationButton) {
    this->loadCalibrationButton = loadCalibrationButton;
    connect(this->loadCalibrationButton, SIGNAL(clicked()), this, SLOT(loadCalibrationFile()));
}

void SceneTab::setAddCalibrationFrameButton(QPushButton *addCalibrationFrameButton) {
    this->addCalibrationFrameButton = addCalibrationFrameButton;
    addCalibrationFrameButton->setEnabled(false);
    connect(this->addCalibrationFrameButton, SIGNAL(clicked()), this, SLOT(addCalibrationFrame()));
}

void SceneTab::setCalibrationFileLabel(QLabel *calibrationFileLabel) {
    this->calibrationFileLabel = calibrationFileLabel;
}

void SceneTab::setUndistortCheckbox(QCheckBox *undistortCheckbox) {
    this->undistortCheckbox = undistortCheckbox;
    this->updateUndistort(false);
    connect(this->undistortCheckbox, SIGNAL(clicked(bool)), this, SLOT(updateUndistort(bool)));
}

void SceneTab::addVisualizationPosition(int pos) {
    if(std::find(this->visualizationPositions.begin(), this->visualizationPositions.end(), pos) == this->visualizationPositions.end()) {
        this->visualizationPositions.push_back(pos);
        std::sort(this->visualizationPositions.begin(), this->visualizationPositions.end());
        this->state->setSelectedVisualizations(this->visualizationPositions);
    }
}

void SceneTab::removeVisualizationPosition(int pos) {
    this->visualizationPositions.erase(std::remove(this->visualizationPositions.begin(), this->visualizationPositions.end(), pos), this->visualizationPositions.end());
    this->state->setSelectedVisualizations(this->visualizationPositions);
}

void SceneTab::updateSceneThreshold(int threshold) {
    this->state->sceneThreshold = threshold;
}

void SceneTab::updateScreenMinSize(int screenMinSize) {
    this->state->screenMinSize = (float) screenMinSize/100.0;
}

void SceneTab::screenMinSliderPressed() {
    this->state->showScreenSize = true;
}

void SceneTab::screenMinSliderReleased() {
    this->state->showScreenSize = false;
}

void SceneTab::updateShowScreen(bool showScreen) {
    this->state->showScreen = showScreen;
}

void SceneTab::updateShowFilteredScene(bool showFilteredScene) {
    this->state->showFilteredSceneImg = showFilteredScene;
}

void SceneTab::updateScreenDetectionMethod(int index) {
    this->state->setMonitorDetector(qvariant_cast<MonitorDetector*>(this->screenDetectionMethodCombo->itemData(index)));
}

void SceneTab::updateUndistort(bool undistort) {
    this->state->undistortScene = undistort;
}

void SceneTab::calibrateCamera() {
    QString defaultTitle = "calibration.xml";
    QString calibrationFilename = QFileDialog::getSaveFileName(this->calibrateCameraButton, tr("Save calibration file"), defaultTitle, tr("XML files (*.xml)"));
    if(calibrationFilename.size() == 0) return;
    this->state->getCameraCalibration().startCalibrating(calibrationFilename.toStdString());
    this->addCalibrationFrameButton->setEnabled(true);

    std::stringstream feedbackTextStream;
    feedbackTextStream << this->state->getCameraCalibration().getRemainingFrames() << " frames remaining";
    this->calibrationFileLabel->setText(feedbackTextStream.str().c_str());
}

void SceneTab::addCalibrationFrame() {
    CameraCalibration &cameraCalibration = this->state->getCameraCalibration();
    if(cameraCalibration.isCalibrating()) {
        int remaining = cameraCalibration.addCalibrationImage(this->state->getSceneImageOriginal());
        std::stringstream feedbackTextStream;
        if(remaining > 0) {
            feedbackTextStream << remaining << " frames remaining";
        }
        else {
            feedbackTextStream << "Using: " << cameraCalibration.getShortFilename();
            this->addCalibrationFrameButton->setEnabled(false);
        }
        this->calibrationFileLabel->setText(feedbackTextStream.str().c_str());
    }
}

void SceneTab::loadCalibrationFile() {
    CameraCalibration &cameraCalibration = this->state->getCameraCalibration();

    QString defaultTitle = "calibration.xml";
    QString calibrationFilename = QFileDialog::getOpenFileName(this->calibrateCameraButton, tr("Save calibration file"), defaultTitle, tr("XML files (*.xml)"));
    if(calibrationFilename.size() == 0) return;
    cameraCalibration.loadCalibration(calibrationFilename.toStdString());

    std::stringstream feedbackTextStream;
    feedbackTextStream << "Using: " << cameraCalibration.getShortFilename();
    this->calibrationFileLabel->setText(feedbackTextStream.str().c_str());
}

/********* VisualizationCheckHandler ***********/

VisualizationCheckHandler::VisualizationCheckHandler(int id, SceneTab *tab) : id(id), tab(tab) {
}

void VisualizationCheckHandler::visualizationCheck(bool checked) {
    if(checked) {
        this->tab->addVisualizationPosition(this->id);
    }
    else {
        this->tab->removeVisualizationPosition(this->id);
    }
}

} // namespace haytham
