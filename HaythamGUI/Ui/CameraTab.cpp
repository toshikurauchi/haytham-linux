/*
 * CameraTab.cpp
 *
 *  Created on: 25/07/2012
 *      Author: Andrew Kurauchi
 */

#include <QComboBox>
#include <QMessageBox>
#include <vector>
#include <QApplication>
#include <QVariant>

#include "CameraTab.h"
#include "mainwindow.h"
#include "camerasettingsdialog.h"
#include "Camera/CameraController.h"

namespace haytham {

CameraTab::CameraTab(METState *state, QWidget *window, QObject *parent) : QObject(parent), state(state), window(window), cameraSignalMapper(this), eyeComponent("Eye Camera"), sceneComponent("Scene Camera") {
}

CameraTab::~CameraTab() {
    for(std::map<haytham::VideoCategory, CameraWidget*>::iterator it = this->cameraWidgets.begin(); it != this->cameraWidgets.end(); it++) {
        if(it->second->isConnected()) {
            it->second->disconnectCamera();
            delete it->second;
        }
    }
}

CameraComponent &CameraTab::getEyeComponent() {
    return this->eyeComponent;
}

CameraComponent &CameraTab::getSceneComponent() {
    return this->sceneComponent;
}

void CameraTab::setWidgets(QPushButton *start_stop_eye_camera, QPushButton *start_stop_scene_camera, QPushButton *start_both_cameras, QPushButton *settings_eye_camera, QPushButton *settings_scene_camera, QComboBox *eye_camera, QComboBox *scene_camera, VideoProcessingThread &processingThread) {
    this->start_both_cameras = start_both_cameras;

    // Camera widgets
    CameraWidget *eyeCameraWidget = new CameraWidget(start_stop_eye_camera,
                                                     settings_eye_camera,
                                                     eye_camera,
                                                     processingThread.getEyeBuffer(),
                                                     EYE);
    CameraWidget *sceneCameraWidget = new CameraWidget(start_stop_scene_camera,
                                                       settings_scene_camera,
                                                       scene_camera,
                                                       processingThread.getSceneBuffer(),
                                                       SCENE);
    this->cameraWidgets.insert(std::pair<haytham::VideoCategory, CameraWidget*>(EYE, eyeCameraWidget));
    this->cameraWidgets.insert(std::pair<haytham::VideoCategory, CameraWidget*>(SCENE, sceneCameraWidget));
    this->eyeComponent.setCameraWidget(eyeCameraWidget);
    this->sceneComponent.setCameraWidget(sceneCameraWidget);

    // Settings Eye Camera
    connect(settings_eye_camera, SIGNAL(clicked()), &this->getEyeComponent(), SLOT(openSettings()));
    settings_eye_camera->setEnabled(false);
    // Settings Scene Camera
    connect(settings_scene_camera, SIGNAL(clicked()), &this->getSceneComponent(), SLOT(openSettings()));
    settings_scene_camera->setEnabled(false);

    this->cameraSignalMapper.setMapping(start_stop_eye_camera, eyeCameraWidget);
    connect(start_stop_eye_camera, SIGNAL(clicked()), &this->cameraSignalMapper, SLOT(map()));
    this->cameraSignalMapper.setMapping(start_stop_scene_camera, this->cameraWidgets.find(SCENE)->second);
    connect(start_stop_scene_camera, SIGNAL(clicked()), &this->cameraSignalMapper, SLOT(map()));
    connect(&this->cameraSignalMapper, SIGNAL(mapped(QObject*)), this, SLOT(connectOrDisconnectCamera(QObject*)));

    CameraController *cameraController = CameraController::newController();
    if(cameraController != 0) {
        std::vector<CameraInfo> cameraInfoList = cameraController->getCameraInfoList();
        delete(cameraController);
        for(std::vector<CameraInfo>::iterator it = cameraInfoList.begin(); it != cameraInfoList.end(); it++) {
            eye_camera->addItem(QString(it->getName().c_str()), QVariant::fromValue(*it));
            scene_camera->addItem(QString(it->getName().c_str()), QVariant::fromValue(*it));
        }
    }

    connect(start_both_cameras, SIGNAL(clicked()), this, SLOT(startStopBothCameras()));
}

void CameraTab::connectOrDisconnectCamera(QObject *cameraObject) {
    CameraWidget *camera = static_cast<CameraWidget*>(cameraObject);
    if(camera->shouldConnectToCamera()) {
        if(camera->connectToCamera()) {
            this->state->setProcessingVideo(camera->getCategory());
            camera->getSettingsButton()->setEnabled(true);
            this->start_both_cameras->setText(QString("Stop Both"));
        }
        // Display error dialog if camera connection is unsuccessful
        else
        {
            QMessageBox::warning(this->window,"ERROR:","Could not connect to camera.");
        }
    }
    else {
        if(camera->isConnected()) {
            camera->disconnectCamera();
            camera->getSettingsButton()->setEnabled(false);
            this->state->setStoppedProcessingVideo(camera->getCategory());

            // Change start both cameras button text if both cameras are disconnected
            bool shouldChange = true;
            for(std::map<haytham::VideoCategory, CameraWidget*>::iterator it = this->cameraWidgets.begin(); it != this->cameraWidgets.end(); it++) {
                if(it->second->isConnected()) {
                    shouldChange = false;
                }
            }
            if(shouldChange) this->start_both_cameras->setText(QString("Start Both"));
        }
        // Display error dialog
        else
            QMessageBox::warning(this->window,"ERROR:","Camera already disconnected.");
    }
}

void CameraTab::startStopBothCameras() {
    if(QString::compare(this->start_both_cameras->text(), QString("Start Both"), Qt::CaseInsensitive) == 0) {
        for(std::map<haytham::VideoCategory, CameraWidget*>::iterator it = this->cameraWidgets.begin(); it != this->cameraWidgets.end(); it++) {
            if(!it->second->isConnected() && it->second->connectToCamera()) {
                it->second->getSettingsButton()->setEnabled(true);
                this->state->setProcessingVideo(it->second->getCategory());
            }
        }
        this->start_both_cameras->setText(QString("Stop Both"));
    }
    else {
        for(std::map<haytham::VideoCategory, CameraWidget*>::iterator it = this->cameraWidgets.begin(); it != this->cameraWidgets.end(); it++) {
            if(it->second->isConnected()) {
                it->second->disconnectCamera();
                it->second->getSettingsButton()->setEnabled(false);
                this->state->setStoppedProcessingVideo(it->second->getCategory());
            }
        }
        this->start_both_cameras->setText(QString("Start Both"));
    }
}

////////////////////////////

CameraComponent::CameraComponent(std::string name) : componentName(name), useLED(false) {
}

CameraComponent::~CameraComponent() {
}

void CameraComponent::setCameraWidget(CameraWidget *camera) {
    this->camera = camera;
}

void CameraComponent::openSettings() {
    int cameraId = this->camera->getSelectedCamera().getId();
    CameraSettingsDialog *settingsDialog = new CameraSettingsDialog(QApplication::activeWindow(), this->useLED);
    settingsDialog->setWindowTitle(QString((this->componentName + " Settings").c_str()));
    // TODO Refactor!
    if(settingsDialog->exec()) {
        if(settingsDialog->getUseLED() != this->useLED) {
            CameraController *cameraController = CameraController::newController();
            if(cameraController != 0) {
                if(settingsDialog->getUseLED()) {
                    cameraController->turnLEDOn(cameraId);
                }
                else {
                    cameraController->turnLEDOff(cameraId);
                }
                delete(cameraController);
            }
            this->useLED = settingsDialog->getUseLED();
        }
    }
    delete settingsDialog;
}


// ************************** CameraWidget **************************************
CameraWidget::CameraWidget(QPushButton *startStopButton, QPushButton *settingsButton, QComboBox *cameraComboBox, ImageBuffer &buffer, VideoCategory category, QObject *parent) :
    QObject(parent), startStopButton(startStopButton), settingsButton(settingsButton), cameraComboBox(cameraComboBox), isCameraConnected(false), buffer(buffer), category(category) {
    this->controller = new Controller();
}

CameraWidget::~CameraWidget() {
    delete this->controller;
}

CameraInfo CameraWidget::getSelectedCamera() {
    return qvariant_cast<CameraInfo>(this->cameraComboBox->itemData(this->cameraComboBox->currentIndex()));
}

QPushButton *CameraWidget::getSettingsButton() {
    return this->settingsButton;
}

VideoCategory CameraWidget::getCategory() {
    return this->category;
}

bool CameraWidget::isConnected() {
    return isCameraConnected;
}

bool CameraWidget::connectToCamera() {
    CameraInfo selectedCamera = this->getSelectedCamera();

    // Store device number in local variable
    // Connect to camera
    this->isCameraConnected = this->controller->connectToCamera(selectedCamera.getId(),
                                                          this->buffer,
                                                          QThread::NormalPriority);

    if(this->isCameraConnected) {
        this->startStopButton->setText(QString("Stop"));
    }

    return this->isCameraConnected;
}

bool CameraWidget::disconnectCamera() {
    if(this->controller->captureThread->isCameraConnected()) {
        this->controller->disconnectCamera();
        // Reset flag
        this->isCameraConnected=false;
        if(!this->isCameraConnected) {
            this->startStopButton->setText(QString("Start"));
        }
        return true;
    }
    return false;
}

bool CameraWidget::shouldConnectToCamera() {
    return QString::compare(this->startStopButton->text(), QString("Start"), Qt::CaseInsensitive) == 0;
}


} /* namespace haytham */
