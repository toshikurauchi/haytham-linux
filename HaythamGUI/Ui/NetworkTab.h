/*
 * NetworkTab.h
 *
 *  Created on: 05/11/2012
 *      Author: Andrew Kurauchi
 */

#ifndef NETWORKTAB_H
#define NETWORKTAB_H

#include <QObject>
#include <QPlainTextEdit>

#include "Network/Server.h"
#include "Common/METState.h"

namespace haytham {

class NetworkTab : public QObject
{
    Q_OBJECT

public:
    explicit NetworkTab(METState *state, Server *server, QObject *parent = 0);
    void setNetworkMessagesEdit(QPlainTextEdit *messages);

private slots:
    void writeMessage(QString message);

private:
    METState *state;
    QPlainTextEdit *messages;
    Server *server;
};

} /* namespace haytham */

#endif // NETWORKTAB_H
