/*
 * EyeTab.h
 *
 *  Created on: 20/08/2012
 *      Author: Andrew Kurauchi
 */

#ifndef EYETAB_H_
#define EYETAB_H_

#include <QObject>
#include <QSlider>
#include <QCheckBox>
#include <QComboBox>
#include <vector>
#include <QPushButton>

#include "Common/METState.h"
#include "EyeDetection/EyeDetector.h"

class MainWindow;

namespace haytham {

class EyeTab : public QObject
{
	Q_OBJECT

public:
    EyeTab(METState *currentState, QObject *parent = 0);
    ~EyeTab();

    void setBinocularWidgets(QCheckBox *use_binocular, QPushButton *set_left_eye_roi, QPushButton *set_right_eye_roi);
    void setIrisSizeSlider(QSlider *iris_size);
    void setShowIrisSizeCheck(QCheckBox *show_iris_size);
    void setShowPupilCheck(QCheckBox *show_pupil);
    void setPupilThresholdSlider(QSlider *pupil_threshold);
    void setFillGapsCheck(QCheckBox *fill_gaps);
    void setRemoveGlintCheck(QCheckBox *remove_glint);
    void setShowGlintCheck(QCheckBox *show_glint);
    void setGlintThresholdSlider(QSlider *glint_threshold);
    void setNumberOfGlintsCombo(QComboBox *numberOfGlintsCombo);
    void setEyeDetectorCombo(QComboBox *eyeDetectorCombo);
public slots:
    // Binocular
    void updateUseBinocular(bool newValue);
    void setLeftEyeROI();
    void setRightEyeROI();
    // Iris
    void updateIrisSize(int newValue);
    void updateShowIrisSize(bool newValue);
	//Pupil
	void updateShowPupil(bool newValue);
    void updatePupilThreshold(int newValue);
	// Eye
	void updateFillGaps(bool newValue);
	// Glint
	void updateRemoveGlint(bool newValue);
    void updateShowGlint(bool newValue);
	void updateGlintThreshold(int newValue);
    void updateNumberOfGlints(int index);
    // Detector
    void updateEyeDetector(int index);
private:
    METState *state;
    std::vector<EyeDetector*> detectors;
    QPushButton *set_left_eye_roi;
    QPushButton *set_right_eye_roi;
};

} /* namespace haytham */
#endif /* EYETAB_H_ */
