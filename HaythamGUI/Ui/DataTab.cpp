/*
 * DataTab.cpp
 *
 *  Created on: 18/10/2012
 *      Author: Andrew Kurauchi
 */

#include <QDebug>
#include <QFileDialog>

#include "DataTab.h"

namespace haytham {

DataTab::DataTab(METState *currentState, QObject *parent) : QObject(parent), state(currentState) {
}

void DataTab::setStartStopButton(QPushButton *startStopButton) {
    this->startStopButton = startStopButton;
    connect(this->startStopButton, SIGNAL(clicked()), this, SLOT(startStopRecording()));
}

void DataTab::setRecordEyeVideoCheckbox(QCheckBox *recordEyeVideoCheckbox) {
    this->recordEyeVideoCheckbox = recordEyeVideoCheckbox;
}

void DataTab::setRecordSceneVideoCheckbox(QCheckBox *recordSceneVideoCheckbox) {
    this->recordSceneVideoCheckbox = recordSceneVideoCheckbox;
}

void DataTab::setRecordGazeDataCheckbox(QCheckBox *recordGazeDataCheckbox) {
    this->recordGazeDataCheckbox = recordGazeDataCheckbox;
}

bool DataTab::startRecording() {
    bool recordEyeVideo, recordGazeData, recordSceneVideo;
    recordEyeVideo = this->recordEyeVideoCheckbox->isChecked();
    recordGazeData = this->recordGazeDataCheckbox->isChecked();
    recordSceneVideo = this->recordSceneVideoCheckbox->isChecked();
    QString eyeVideoFilename, gazeDataFilename, sceneVideoFilename;
    // The same variable is used for all default filenames in order to consider the filename chosen by the user for the next ones
    QString defaultTitle = "untitled_eye.avi";

    if(recordEyeVideo) {
        eyeVideoFilename = QFileDialog::getSaveFileName(this->startStopButton, tr("Save eye video"), defaultTitle, tr("AVI files (*.avi)"));
        defaultTitle = eyeVideoFilename;
        if(eyeVideoFilename.size() == 0) return false;
    }

    defaultTitle.replace(QString("eye"), QString("scene"), Qt::CaseInsensitive);
    if(recordSceneVideo) {
        sceneVideoFilename = QFileDialog::getSaveFileName(this->startStopButton, tr("Save scene video"), defaultTitle, tr("AVI files (*.avi)"));
        defaultTitle = sceneVideoFilename;
        if(sceneVideoFilename.size() == 0) return false;
    }

    defaultTitle.replace(QString("_scene"), QString(""), Qt::CaseInsensitive)
                .replace(QString("scene"), QString(""), Qt::CaseInsensitive)
                .replace(QString("avi"), QString("csv"), Qt::CaseInsensitive);
    if(recordGazeData) {
        gazeDataFilename = QFileDialog::getSaveFileName(this->startStopButton, tr("Save eye data"), defaultTitle, tr("CSV files (*.csv)"));
        if(gazeDataFilename.size() == 0) return false;
    }

    return this->state->getRecordManager().startRecording(eyeVideoFilename, sceneVideoFilename, gazeDataFilename);
}

void DataTab::stopRecording() {
    this->state->getRecordManager().stopRecording();
}

void DataTab::startStopRecording() {
    if(QString::compare(this->startStopButton->text(), QString("Start recording"), Qt::CaseInsensitive) == 0) {
        if(startRecording()) {
            this->startStopButton->setText(QString("Stop recording"));
        }
    }
    else {
        stopRecording();
        this->startStopButton->setText(QString("Start recording"));
    }
}

} /* namespace haytham */
