/*
 * NetworkTab.cpp
 *
 *  Created on: 05/11/2012
 *      Author: Andrew Kurauchi
 */

#include <QStringList>

#include "NetworkTab.h"

namespace haytham {

NetworkTab::NetworkTab(METState *state, Server *server, QObject *parent) : QObject(parent), state(state), server(server)
{
}

void NetworkTab::setNetworkMessagesEdit(QPlainTextEdit *messages) {
    this->messages = messages;
    writeMessage(this->server->getSentMessages().join("\n"));
    connect(this->server, SIGNAL(sendMessage(QString)), this, SLOT(writeMessage(QString)));
}

void NetworkTab::writeMessage(QString message) {
    this->messages->appendPlainText(message + "\n");
}

} /* namespace haytham */
