/*
 * camerasettingsdialog.cpp
 *
 *  Created on: 01/10/2012
 *      Author: Andrew Kurauchi
 */

#include "camerasettingsdialog.h"
#include "ui_camerasettingsdialog.h"

CameraSettingsDialog::CameraSettingsDialog(QWidget *parent, bool useLED) :
    QDialog(parent),
    ui(new Ui::CameraSettingsDialog)
{
    ui->setupUi(this);
    this->ui->useLED->setChecked(useLED);
}

CameraSettingsDialog::~CameraSettingsDialog()
{
    delete ui;
}

bool CameraSettingsDialog::getUseLED() {
    return this->ui->useLED->isChecked();
}
