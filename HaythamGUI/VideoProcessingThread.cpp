/*
 * VideoProcessingThread.cpp
 *
 *  Created on: 23/08/2012
 *      Author: Andrew Kurauchi
 *
 * Based on Qt-opencv-multithreaded's ProcessingThread.h: http://code.google.com/p/qt-opencv-multithreaded/source/browse/trunk/src/ProcessingThread.cpp
 */

#include <vector>
#include <algorithm>

#include "VideoProcessingThread.h"
#include "ImageProcessing/ImageProcessing.h"
#include "MatToQImage.h"
#include "Common/METState.h"
#include "EyeDetection/GazeData.h"
#include "Common/VideoCategory.h"
#include "IO/VideoRecorder.h"
#include "IO/DataRecorder.h"
#include "Calibration/Calibration.h"
#include "HeadGesture/HeadGesture.h"
#include "Calibration/CalibrationPointsDetection.h"
#include "Common/Blob.h"
#include "EyeDetection/GlintDetector.h"

namespace haytham {

VideoProcessingThread::VideoProcessingThread() :
    QThread(),
    state(METState::getCurrent()),
    eyeBuffer(1, true),
    sceneBuffer(1, true),
    stopped(false),
    fpsSum(0),
    sampleNo(0),
    avgFPS(0),
    sceneMain(METState::getCurrent()),
    dataManager(METState::getCurrent())
{
    fps.clear();
}

VideoProcessingThread::~VideoProcessingThread() {
}

void VideoProcessingThread::stopProcessingThread() {
    stoppedMutex.lock();
    stopped=true;
    stoppedMutex.unlock();
    qDebug() << "Average processing FPS: " << this->getAvgFPS();
}

/**
 * \returns The average frames per second (FPS).
 */
int VideoProcessingThread::getAvgFPS()
{
    return avgFPS;
} // getAvgFPS()

void VideoProcessingThread::run() {
    while(1) {
        /////////////////////////////////
        // Stop thread if stopped=TRUE //
        /////////////////////////////////
        stoppedMutex.lock();
        if (stopped)
        {
            stopped=false;
            stoppedMutex.unlock();
            break;
        }
        stoppedMutex.unlock();
        /////////////////////////////////

        // Save processing time
        processingTime=t.elapsed();
        // Start timer (used to calculate processing rate)
        t.start();

        // Process frames
        if(this->state->isProcessingEye()) {
            cv::Mat eyeImage = this->eyeBuffer.getFrame();
            QImage eyeFrame = MatToQImage(this->processEyeFrame(eyeImage));
            emit newFrame(Frame(EYE, eyeFrame));
        }
        if(this->state->isProcessingScene()) {
            cv::Mat sceneImage = this->sceneBuffer.getFrame();
            sceneImage = this->processSceneFrame(sceneImage);
            std::vector<SceneVisualization*> sceneVisualizations = this->state->getSelectedSceneVisualizations();
            for(std::vector<SceneVisualization*>::iterator it = sceneVisualizations.begin(); it != sceneVisualizations.end(); it++) {
                sceneImage = (*it)->process(sceneImage, this->state->getLatestGazeData().getGazePosition(this->state->getUseBinocular()));
            }
            QImage sceneFrame = MatToQImage(sceneImage);
            emit newFrame(Frame(SCENE, sceneFrame));
        }
        else {
            for(int left = 0; left < 2; left++) {
                Calibration *calibration;
                Point calibrationCorrection;
                if(left) {
                    calibration = this->state->getLeftEyeToSceneMapping();
                    calibrationCorrection = Point(this->state->getLeftCalibrationCorrectionX(), this->state->getLeftCalibrationCorrectionY());
                }
                else {
                    calibration = this->state->getRightEyeToSceneMapping();
                    calibrationCorrection = Point(this->state->getRightCalibrationCorrectionX(), this->state->getRightCalibrationCorrectionY());
                }
                if(calibration->isCalibrated() && !calibration->getRequiresSceneData()) {
                    Point eyeFeature;
                    if(this->state->getSmoothGaze()) {
                        eyeFeature = this->state->getEyeFeatureMedian(5, left);
                    }
                    else {
                        eyeFeature = this->state->getEyeFeature(left);
                    }
                    SceneData sceneData;
                    Point mapped = calibration->map(eyeFeature, sceneData, calibrationCorrection);
                    if(left) {
                        this->state->getLatestGazeData().getLeftData().setGazePosition(mapped);
                    }
                    else {
                        this->state->getLatestGazeData().getRightData().setGazePosition(mapped);
                    }
                }
            }
        }

        HeadGesture gesture = this->headGestureProcessor.detect(this->state->getLatestGazeData(5));
        gesture.timestamp = this->state->timer.now();
        if(gesture.occurred()) {
            if(this->dataManager.handleHeadGesture(gesture)) {
                emit newHeadGesture(gesture);
            }
        }

        BinocularGazeData data = this->state->getLatestGazeData();
        data.setUsePupilGlint(this->state->getUsePupilGlint());
        this->dataManager.handleGazeData(data);
        emit newGazeData(data);

        // Update statistics
        updateFPS(processingTime);
    }
    qDebug() << "Stopping processing thread...";
}

ImageBuffer &VideoProcessingThread::getEyeBuffer() {
    return this->eyeBuffer;
}

ImageBuffer &VideoProcessingThread::getSceneBuffer() {
    return this->sceneBuffer;
}

void VideoProcessingThread::updateFPS(int timeElapsed) {
    // Add instantaneous FPS value to queue
    if(timeElapsed>0)
    {
        fps.enqueue((int)1000/timeElapsed);
        // Increment sample number
        sampleNo++;
    }
    if(fps.size()>32)
        fps.dequeue();
    if((fps.size()==32)&&(sampleNo==32))
    {
        // Empty queue and store sum
        while(!fps.empty())
            fpsSum+=fps.dequeue();
        // Calculate average FPS
        avgFPS=fpsSum/32;
        // Reset sum
        fpsSum=0;
        // Reset sample number
        sampleNo=0;
    }
}

cv::Rect calcROI(Point center, double radius, cv::Size imgSize) {
    int beforeX = std::min(radius, center.x);
    int beforeY = std::min(radius, center.y);
    int afterX = std::min(radius, imgSize.width - center.x);
    int afterY = std::min(radius, imgSize.height - center.y);
    return cv::Rect(center.x - beforeX, center.y - beforeY, beforeX + afterX - 1, beforeY + afterY - 1);
}

cv::Mat &VideoProcessingThread::processEyeFrame(cv::Mat &eyeFrame) {
    this->state->eyeImageOrginal = eyeFrame;
    eyeFrame.copyTo(this->state->eyeImageForShow);

    BinocularGazeData data;
    for(int left = 0; left <= this->state->getUseBinocular(); left++) {
        if(left && this->state->getBinocularROIDetector().leftReady()) {
            cv::Mat roiImage = this->state->getBinocularROIDetector().extractLeftROIImage(this->state->getEyeImageOriginal());
            cv::Rect &roi = this->state->getBinocularROIDetector().getLeftROI();
            data.setLeftData(this->processMonocularEyeFrame(roiImage, Point(roi.x, roi.y)));
        }
        else if(!left && this->state->getBinocularROIDetector().rightReady()) {
            cv::Mat roiImage = this->state->getBinocularROIDetector().extractRightROIImage(this->state->getEyeImageOriginal());
            cv::Rect &roi = this->state->getBinocularROIDetector().getRightROI();
            data.setRightData(this->processMonocularEyeFrame(roiImage, Point(roi.x, roi.y)));
        }
        else if(!left && !this->state->getUseBinocular()) {
            data.setRightData(this->processMonocularEyeFrame(this->state->getEyeImageOriginal()));
        }
        else {
            // TODO Calculate ROI!
        }
    }

    data.setTime(this->state->getTimer().now());

    VideoRecorder &eyeRecorder = this->state->getEyeVideoRecorder();
    if(eyeRecorder.isRecording()) {
        eyeRecorder.addFrame(this->state->eyeImageOrginal);
    }

    HeadGesture gesture = this->headGestureProcessor.process(data);
    data.setGesture(gesture);

    this->state->addGazeData(data);

    //if(this->state->getBinocularROIDetector().rightReady())
    //    this->state->eyeImageForShow = this->state->getBinocularROIDetector().getRightROIImage(this->state->getEyeImageForShow());
    return this->state->eyeImageForShow;
}

cv::Mat &VideoProcessingThread::processSceneFrame(cv::Mat &sceneFrame) {
    // TODO Move this to sceneMain
    if(this->state->getUndistortScene()) {
        sceneFrame = this->state->getCameraCalibration().undistort(sceneFrame);
    }
    this->state->sceneImageOrginal = sceneFrame;
    sceneFrame.copyTo(this->state->sceneImageForShow);

    bool pointsDetected = false; // To detect calibration points only once
    for(int left = 0; left < 2; left++) {
        Calibration *calibration;
        int calibrationCorrectionX, calibrationCorrectionY;
        if(left) {
            calibration = this->state->getLeftEyeToSceneMapping();
            calibrationCorrectionX = this->state->getLeftCalibrationCorrectionX();
            calibrationCorrectionY = this->state->getLeftCalibrationCorrectionY();
        }
        else {
            calibration = this->state->getRightEyeToSceneMapping();
            calibrationCorrectionX = this->state->getRightCalibrationCorrectionX();
            calibrationCorrectionY = this->state->getRightCalibrationCorrectionY();
        }
        if(calibration->isCalibrated()) {
            Point eyeFeature;
            if(this->state->getSmoothGaze()) {
                eyeFeature = this->state->getEyeFeatureMedian(5, left);
            }
            else {
                eyeFeature = this->state->getEyeFeature(left);
            }
            SceneData sceneData;
            if(calibration->getRequiresSceneData()) {
                CalibrationPointsDetection detector;
                std::vector<Blob> calibPoints = detector.findPoints(this->state->getSceneImageOriginal(),
                                                                    calibration->getPatternSize(),
                                                                    this->state->getCalibPointDetectThresh());
                std::vector<cv::Point2f> calibrationPoints;
                for(std::vector<Blob>::iterator it = calibPoints.begin(); it != calibPoints.end(); it++) {
                    calibrationPoints.push_back(it->getCenterOfGravity());
                }
                sceneData.setCalibrationPoints(calibrationPoints);
            }
            Point mapped = calibration->map(eyeFeature, sceneData, Point(calibrationCorrectionX, calibrationCorrectionY));
            if(left) {
                this->state->getLatestGazeData().getLeftData().setGazePosition(mapped);
            }
            else {
                this->state->getLatestGazeData().getRightData().setGazePosition(mapped);
            }

        }
        else if(calibration->isCalibrating() && !pointsDetected) {
            CalibrationPointsDetection pointsDetector;
            std::vector<Blob> points = pointsDetector.findPoints(this->state->sceneImageOrginal, calibration->getPatternSize(), this->state->getCalibPointDetectThresh(), this->state->sceneImageForShow, calibration);
            if(!points.empty()) {
                this->state->setCurrentSceneCalibrationPoint(Point(points[calibration->getCurrentPoint()].getCenterOfGravity()));
            }
            else {
                this->state->setCurrentSceneCalibrationPoint(Point(-1, -1));
            }
            pointsDetected = true;
        }
    }
    this->sceneMain.mainFunction(this->state->getLatestGazeData().getLeftData());
    this->sceneMain.mainFunction(this->state->getLatestGazeData().getRightData());

    VideoRecorder &sceneRecorder = this->state->getSceneVideoRecorder();
    if(sceneRecorder.isRecording()) {
        sceneRecorder.addFrame(this->state->sceneImageOrginal);
    }

    return this->state->sceneImageForShow;
}

GazeData VideoProcessingThread::processMonocularEyeFrame(cv::Mat &eyeFrame, Point offset) {
    ImageProcessing processor;
    GazeData data = this->state->getEyeDetector()->findPupil(eyeFrame, this->state->getPupilThreshold(), this->state->getIrisDiameter(), this->state->getMinPupilScale(), this->state->getMaxPupilScale(), this->state->getGlintThreshold());
    if(data.isPupilFound()) {
        GlintDetector glintDetector;
        float corneaRadius = this->state->getMaxPupilScale()*this->state->getIrisDiameter()/2;
        cv::Rect roi = calcROI(data.getPupilCenter(), corneaRadius, eyeFrame.size());
        if(roi.width > 0 && roi.height > 0) {
            std::vector<Point> glintCenters = glintDetector.detectGlint(eyeFrame(roi), this->state->getGlintThreshold(), this->state->getNumberOfGlints(), Point(roi.x, roi.y));
            data.setGlintCenters(glintCenters);
            data.addOffset(offset);
            if(this->state->getShowPupil()) {
                processor.drawPupil(this->state->getEyeImageForShow(),
                                    data.getPupilEllipse());
            }
            if(this->state->getShowGlint()) {
                for(std::vector<Point>::const_iterator it = data.getGlintCenters().begin(); it != data.getGlintCenters().end(); it++) {
                    processor.drawGlint(this->state->getEyeImageForShow(), *it);
                }
            }
        }
    }

    if(this->state->getShowIris()) {
        processor.drawIrisCircle(this->state->getEyeImageForShow(),
                                 data.getPupilCenter().x,
                                 data.getPupilCenter().y,
                                 this->state->getIrisDiameter(),
                                 this->state->getMinPupilScale(),
                                 this->state->getMaxPupilScale());
    }

    return data;
}

} /* namespace haytham */
