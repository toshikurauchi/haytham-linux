/*
 * VideoProcessingThread.h
 *
 *  Created on: 23/08/2012
 *      Author: Andrew Kurauchi
 *
 * Based on Qt-opencv-multithreaded's ProcessingThread.h: http://code.google.com/p/qt-opencv-multithreaded/source/browse/trunk/src/ProcessingThread.cpp
 */

#ifndef VIDEOPROCESSINGTHREAD_H_
#define VIDEOPROCESSINGTHREAD_H_

#include <QThread>
#include <QtGui>
#include <opencv2/opencv.hpp>

#include "EyeDetection/EyeDetector.h"
#include "Common/METState.h"
#include "SceneMain.h"
#include "ImageBuffer.h"
#include "Common/VideoCategory.h"
#include "IO/DataManager.h"
#include "HeadGesture/HeadGestureProcessor.h"

namespace haytham {

class VideoProcessingThread : public QThread {
	Q_OBJECT

public:
    VideoProcessingThread();
    ~VideoProcessingThread();
    void stopProcessingThread();
    int getAvgFPS();
    ImageBuffer &getEyeBuffer();
    ImageBuffer &getSceneBuffer();
protected:
    METState *state;

	void run();
private:
    ImageBuffer eyeBuffer;
    ImageBuffer sceneBuffer;
    volatile bool stopped;
    QTime t;
    int processingTime;
    QQueue<int> fps;
    int fpsSum;
    int sampleNo;
    int avgFPS;
    QMutex stoppedMutex;
    QMutex updateMembersMutex;
    Size frameSize;
    Point framePoint;

    SceneMain sceneMain;
    DataManager dataManager;
    HeadGestureProcessor headGestureProcessor;

    void updateFPS(int);
    cv::Mat &processEyeFrame(cv::Mat &eyeFrame);
    cv::Mat &processSceneFrame(cv::Mat &sceneFrame);
    GazeData processMonocularEyeFrame(cv::Mat &eyeFrame, Point offset = Point(0, 0));

signals:
    void newFrame(const Frame &frame);
    void newGazeData(BinocularGazeData gazeData);
    void newHeadGesture(HeadGesture headGesture);
};

} /* namespace haytham */
#endif /* VIDEOPROCESSINGTHREAD_H_ */
