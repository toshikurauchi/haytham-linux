/*
 * camerasettingsdialog.h
 *
 *  Created on: 01/10/2012
 *      Author: Andrew Kurauchi
 */

#ifndef CAMERASETTINGSDIALOG_H
#define CAMERASETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
class CameraSettingsDialog;
}

class CameraSettingsDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit CameraSettingsDialog(QWidget *parent = 0, bool useLED = false);
    ~CameraSettingsDialog();
    bool getUseLED();
    
private:
    Ui::CameraSettingsDialog *ui;
};

#endif // CAMERASETTINGSDIALOG_H
