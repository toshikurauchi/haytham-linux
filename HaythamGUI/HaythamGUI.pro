# -------------------------------------------------
# Project created by QtCreator 2012-07-18T15:58:29
# -------------------------------------------------

include(./Ui/ui.pri)

CONFIG += qt thread
QT += core \
    gui \
    widgets \
    network
TARGET = ../Haytham-Linux
TEMPLATE = app

# Including HaythamLib

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../HaythamLib/release/ -lHaythamLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../HaythamLib/debug/ -lHaythamLib
else:unix:!symbian: LIBS += -L$$OUT_PWD/../HaythamLib/ -lHaythamLib

INCLUDEPATH += $$PWD/../HaythamLib
DEPENDPATH += $$PWD/../HaythamLib

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../HaythamLib/release/HaythamLib.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../HaythamLib/debug/HaythamLib.lib
else:unix:!symbian: PRE_TARGETDEPS += $$OUT_PWD/../HaythamLib/libHaythamLib.a

DEPENDPATH += .
INCLUDEPATH += /usr/local/include/opencv2 \
    /opt/local/include
LIBS += -L/usr/local/lib \
    `pkg-config --static --cflags --libs opencv` \
    -lpthread \
    -lvrpn
unix:!macx {
LIBS += -lboost_regex \
    -lboost_chrono \
    -lboost_system
}
macx {
CONFIG += objective_c
LIBS += -L/opt/local/lib
LIBS += -lboost_regex-mt \
    -lboost_chrono-mt \
    -lboost_system-mt
}
SOURCES += *.cpp
HEADERS += *.h
FORMS += mainwindow.ui \
    camerasettingsdialog.ui
OTHER_FILES +=

# TODO Found this at http://stackoverflow.com/questions/17127762/cvmat-to-qimage-and-back test it later
## add open CV
#unix {
#    CONFIG += link_pkgconfig
#    PKGCONFIG += opencv
#}
