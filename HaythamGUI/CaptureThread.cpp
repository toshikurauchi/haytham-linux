/************************************************************************/
/* qt-opencv-multithreaded:                                             */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* CaptureThread.cpp                                                    */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2012 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#include "CaptureThread.h"
#include "ImageBuffer.h"
#include "Camera/CameraController.h"

// Qt header files
#include <QDebug>

/**
 * \param imageBuffer Pointer to a buffer where acquired images will be stored.
 */
CaptureThread::CaptureThread(ImageBuffer &imageBuffer) : QThread(),
                             imageBuffer(imageBuffer)
{
    // Initialize variables
    stopped=false;
    sampleNo=0;
    fpsSum=0;
    avgFPS=0;
    fps.clear();
} // CaptureThread constructor

/**
 * \brief Method executed on thread. Keeps capturing frames from
 * camera and pushing them into the buffer.
 */
void CaptureThread::run()
{
    while(1)
    {
        /////////////////////////////////
        // Stop thread if stopped=TRUE //
        /////////////////////////////////
        stoppedMutex.lock();
        if (stopped)
        {
            stopped=false;
            stoppedMutex.unlock();
            break;
        }
        stoppedMutex.unlock();
        /////////////////////////////////
        /////////////////////////////////
        // Save capture time
        captureTime=t.elapsed();
        // Start timer (used to calculate capture rate)
        t.start();
        // Capture and add frame to buffer
        cap>>grabbedFrame;
        imageBuffer.addFrame(grabbedFrame);
        // Update statistics
        updateFPS(captureTime);
    }
    qDebug() << "Stopping capture thread...";
} // run()

/**
 * \param deviceNumber Camera device id to be used.
 *
 * \brief Opens a connection with the given camera.
 *
 * \returns true if connection was successful. false otherwise.
 */
bool CaptureThread::connectToCamera(int deviceNumber)
{
    // Open camera and return result
    cap.open(deviceNumber);
    return cap.isOpened();
} // connectToCamera()

/**
 * \brief Disconnects from current camera (if any).
 */
void CaptureThread::disconnectCamera()
{
    // Check if camera is connected
    if(cap.isOpened())
    {
        // Disconnect camera
        cap.release();
        qDebug() << "Camera successfully disconnected.";
        qDebug() << "Average capture FPS: " << this->getAvgFPS();
        this->imageBuffer.clearBuffer();
    }
} // disconnectCamera()

void CaptureThread::updateFPS(int timeElapsed)
{
    // Add instantaneous FPS value to queue
    if(timeElapsed>0)
    {
        fps.enqueue((int)1000/timeElapsed);
        // Increment sample number
        sampleNo++;
    }
    // Maximum size of queue is DEFAULT_32
    if(fps.size()>32)
        fps.dequeue();
    // Update FPS value every DEFAULT_32 samples
    if((fps.size()==32)&&(sampleNo==32))
    {
        // Empty queue and store sum
        while(!fps.empty())
            fpsSum+=fps.dequeue();
        // Calculate average FPS
        avgFPS=fpsSum/32;
        // Reset sum
        fpsSum=0;
        // Reset sample number
        sampleNo=0;
    }
} // updateFPS()

/**
 * \brief Stops capturing frames from camera.
 */
void CaptureThread::stopCaptureThread()
{
    stoppedMutex.lock();
    stopped=true;
    stoppedMutex.unlock();
} // stopCaptureThread()

/**
 * \returns The average frames per second (FPS).
 */
int CaptureThread::getAvgFPS()
{
    return avgFPS;
} // getAvgFPS()

/**
 * \returns true if the current camera is connected, false otherwise.
 */
bool CaptureThread::isCameraConnected()
{
    if(cap.isOpened())
        return true;
    else
        return false;
} // isCameraConnected()

/**
 * \returns The camera image width.
 */
int CaptureThread::getInputSourceWidth()
{
    return cap.get(cv::CAP_PROP_FRAME_WIDTH);
} // getInputSourceWidth()

/**
 * \returns The camera image height.
 */
int CaptureThread::getInputSourceHeight()
{
    return cap.get(cv::CAP_PROP_FRAME_HEIGHT);
} // getInputSourceHeight()

/**
 * \returns The image buffer.
 */
ImageBuffer &CaptureThread::getImageBuffer()
{
    return this->imageBuffer;
} // getInputSourceHeight()
