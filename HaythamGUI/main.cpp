#include <QApplication>
#include <QMetaType>

#include "mainwindow.h"
#include "Common/VideoCategory.h"
#include "HeadGesture/HeadGesture.h"
#include "EyeDetection/GazeData.h"

using namespace haytham;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    qRegisterMetaType<Frame>("Frame");
    qRegisterMetaType<HeadGesture>("HeadGesture");
    qRegisterMetaType<BinocularGazeData>("BinocularGazeData");

    MainWindow w;
    w.setWindowTitle("Haytham");
    w.show();
    
    return a.exec();
}
