/*
 * EventListener.h
 *
 *  Created on: 03/12/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_EVENTLISTENER_H
#define HAYTHAM_EVENTLISTENER_H

#include <QMouseEvent>
#include <QImage>

#include "Common/METState.h"
#include "ImageLabel.h"

namespace haytham {

class MousePressListener
{
public:
    virtual void onMousePress(QMouseEvent *event) = 0;
};

class KeyPressListener {
public:
    virtual void onKeyPress(QKeyEvent *event) = 0;
};

class ImageSetListener {
public:
    virtual QImage onImageSet(QImage &image) = 0;
};

class SceneLabelProcessor : public MousePressListener, public KeyPressListener, public ImageSetListener {
public:
    SceneLabelProcessor(METState *state, ImageLabel *label);
    virtual void onMousePress(QMouseEvent *event);
    virtual void onKeyPress(QKeyEvent *event);
    virtual QImage onImageSet(QImage &image);
    void setCalibrationButton(QPushButton *calibrationButton);
    void setRemainingCalibrationPointsLabel(CalibrationPointsLabel *remainingCalibrationPointsLabel);
    void resetCalibrationPoints();
private:
    METState *state;
    ImageLabel *label;
    std::vector<Point> clickedPoints;
    QPushButton *calibrationButton;
    CalibrationPointsLabel *remainingCalibrationPointsLabel;

    void addCalibrationPoint(Point point);
};

class EyeLabelProcessor : public MousePressListener, public ImageSetListener {
public:
    EyeLabelProcessor(METState *state, ImageLabel *label);
    virtual void onMousePress(QMouseEvent *event);
    virtual QImage onImageSet(QImage &image);
private:
    METState *state;
    ImageLabel *label;
    Point initialPosition;
    bool firstPointSelected;

    void drawROI(cv::Mat &img, cv::Rect &roi, cv::Scalar color);
};

} // namespace haytham

#endif // HAYTHAM_EVENTLISTENER_H
