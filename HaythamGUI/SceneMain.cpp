/*
 * SceneMain.cpp
 *
 *  Created on: 17/01/2013
 *      Author: Andrew Kurauchi
 */

#include <opencv2/opencv.hpp>
#include "SceneMain.h"
#include "MonitorDetection/Monitor.h"
#include "Common/Point.h"
#include "Calibration/Calibration.h"

namespace haytham {

SceneMain::SceneMain(METState *state) : state(state) {
}

void SceneMain::mainFunction(GazeData &data) {
    CameraCalibration &cameraCalibration = this->state->getCameraCalibration();
    if(cameraCalibration.isCalibrating()) {
        std::vector<cv::Point2f> calibCorners = cameraCalibration.findCorners(this->state->getSceneImageOriginal());
        cv::drawChessboardCorners(this->state->getSceneImageForShow(), cameraCalibration.getPatternSize(), calibCorners, !calibCorners.empty());
    }

    MonitorDetector *monitorDetector = this->state->getMonitorDetector();
    bool screenFound = monitorDetector->detectScreen(this->state->getSceneImageOriginal(), this->state->getSceneThreshold(), this->state->getScreenMinSize());
    if(this->state->getShowFilteredSceneImg()) {
        cv::Mat sceneImage = this->state->getSceneImageForShow();
        cv::add(sceneImage, this->imageProcessing.gray2bgr(monitorDetector->getFilteredGrayImg()), sceneImage);
        this->imageProcessing.gray2bgr(monitorDetector->getFilteredGrayImg()).copyTo(this->state->getSceneImageForShow());
    }
    if(this->state->getShowScreen()) {
        if(this->state->getShowScreenSize()) {
            Point center(this->state->getSceneImageForShow().cols / 2, this->state->getSceneImageForShow().rows / 2);
            cv::Size2f size;
            float screenMinSize = this->state->getScreenMinSize();
            size.width = this->state->getSceneImageForShow().cols * screenMinSize;
            size.height = this->state->getSceneImageForShow().rows * screenMinSize;
            this->imageProcessing.drawRectangle(this->state->getSceneImageForShow(), center, size);
        }
        if(screenFound) {
            monitorDetector->drawScreen(this->state->getSceneImageForShow());
        }
    }
    if(screenFound && data.getGazePosition() != Point(-1,-1)) {
        Point monitorPoint = this->state->getActiveMonitor().map(data.getGazePosition(), monitorDetector->getScreenCorners());
        data.setMonitorGazePosition(monitorPoint);
    }
}

} // namespace haytham
