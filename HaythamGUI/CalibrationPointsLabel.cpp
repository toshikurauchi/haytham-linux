/*
 * CalibrationPointsLabel.cpp
 *
 *  Created on: 01/04/2013
 *      Author: Andrew Kurauchi
 */

#include <sstream>

#include "CalibrationPointsLabel.h"

namespace haytham {

CalibrationPointsLabel::CalibrationPointsLabel(QWidget *parent) : QLabel(parent)
{}

void CalibrationPointsLabel::showRemainingPoints(int points) {
    std::stringstream remainingPointsStream;
    if(points <= 0)
        remainingPointsStream << "Calibrated";
    else
        remainingPointsStream << points << " points remaining";
    this->setText(remainingPointsStream.str().c_str());
}

} // namespace haytham
