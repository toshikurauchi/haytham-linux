/*
 * EventListener.cpp
 *
 *  Created on: 03/12/2013
 *      Author: Andrew Kurauchi
 */


#include "EventListener.h"
#include "Calibration/CalibrationPointsDetection.h"
#include "MatToQImage.h"
#include "Calibration/Calibration.h"
#include "Calibration/SceneData.h"

namespace haytham {

SceneLabelProcessor::SceneLabelProcessor(METState *state, ImageLabel *label) : state(state), label(label) {
}

void SceneLabelProcessor::onMousePress(QMouseEvent *event) {
    Calibration *leftEyeToSceneMapping = this->state->getLeftEyeToSceneMapping();
    Calibration *rightEyeToSceneMapping = this->state->getRightEyeToSceneMapping();
    Point absoluteClickPos = Point(event->pos());
    Point imageClickPos = this->label->toImageCoordinate(absoluteClickPos);

    BinocularGazeData data = this->state->getLatestGazeData();
    Point leftEyeFeature, rightEyeFeature;
    if(this->state->getUsePupilGlint()) {
        leftEyeFeature = data.getLeftData().getPupilGlintVector();
        rightEyeFeature = data.getRightData().getPupilGlintVector();
    }
    else {
        leftEyeFeature = data.getLeftData().getPupilCenter();
        rightEyeFeature = data.getRightData().getPupilCenter();
    }

    if(event->button() == Qt::LeftButton) {
        this->addCalibrationPoint(imageClickPos);
    }
    else if(event->button() == Qt::RightButton) {
        SceneData sceneData;
        if(leftEyeToSceneMapping->getRequiresSceneData() && (leftEyeToSceneMapping->isCalibrated() || rightEyeToSceneMapping->isCalibrated())) {
            CalibrationPointsDetection detector;
            std::vector<Blob> calibPoints = detector.findPoints(this->state->getSceneImageOriginal(),
                                                                leftEyeToSceneMapping->getPatternSize(),
                                                                this->state->getCalibPointDetectThresh());
            std::vector<cv::Point2f> calibrationPoints;
            for(std::vector<Blob>::iterator it = calibPoints.begin(); it != calibPoints.end(); it++) {
                calibrationPoints.push_back(it->getCenterOfGravity());
            }
            sceneData.setCalibrationPoints(calibrationPoints);
        }
        if(leftEyeToSceneMapping->isCalibrated()) {
            Point correction = leftEyeToSceneMapping->map(leftEyeFeature, sceneData, imageClickPos);
            this->state->setLeftCalibrationCorrectionX(correction.x);
            this->state->setLeftCalibrationCorrectionY(correction.y);
        }
        if(rightEyeToSceneMapping->isCalibrated()) {
            Point correction = rightEyeToSceneMapping->map(rightEyeFeature, sceneData, imageClickPos);
            this->state->setRightCalibrationCorrectionX(correction.x);
            this->state->setRightCalibrationCorrectionY(correction.y);
        }
    }
}

void SceneLabelProcessor::onKeyPress(QKeyEvent *event) {
    Point calibrationPoint = this->state->getCurrentSceneCalibrationPoint();
    if((event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) && calibrationPoint != Point(-1, -1)) {
        this->addCalibrationPoint(calibrationPoint);
    }
}

QImage SceneLabelProcessor::onImageSet(QImage &image) {
    ImageProcessing processor;
    cv::Mat mat = QImageToMat(image);
    for(std::vector<Point>::iterator it = this->clickedPoints.begin(); it != this->clickedPoints.end(); it++) {
        processor.drawPoint(mat, *it);
    }
    return MatToQImage(mat);
}

void SceneLabelProcessor::setCalibrationButton(QPushButton *calibrationButton) {
    this->calibrationButton = calibrationButton;
}

void SceneLabelProcessor::setRemainingCalibrationPointsLabel(CalibrationPointsLabel *remainingCalibrationPointsLabel) {
    this->remainingCalibrationPointsLabel = remainingCalibrationPointsLabel;
}

void SceneLabelProcessor::resetCalibrationPoints() {
    this->clickedPoints.clear();
}

void SceneLabelProcessor::addCalibrationPoint(Point point) {
    BinocularGazeData binocularData = this->state->getLatestGazeData();
    Point eyeFeature;
    Calibration *eyeToSceneMapping;
    GazeData data;

    for(int left = 0; left <= this->state->getUseBinocular(); left++) {
        if(left) {
            eyeToSceneMapping = this->state->getLeftEyeToSceneMapping();
            data = binocularData.getLeftData();
        }
        else {
            eyeToSceneMapping = this->state->getRightEyeToSceneMapping();
            data = binocularData.getRightData();
        }
        if(this->state->getUsePupilGlint()) {
            eyeFeature = data.getPupilGlintVector();
        }
        else {
            eyeFeature = data.getPupilCenter();
        }

        if(eyeToSceneMapping->isCalibrating() && data.isPupilFound()) {
            SceneData sceneData;
            if(eyeToSceneMapping->getRequiresSceneData()) {
                CalibrationPointsDetection detector;
                std::vector<Blob> calibPoints = detector.findPoints(this->state->getSceneImageOriginal(),
                                                                    eyeToSceneMapping->getPatternSize(),
                                                                    this->state->getCalibPointDetectThresh());
                std::vector<cv::Point2f> calibrationPoints;
                for(std::vector<Blob>::iterator it = calibPoints.begin(); it != calibPoints.end(); it++) {
                    calibrationPoints.push_back(it->getCenterOfGravity());
                }
                sceneData.setCalibrationPoints(calibrationPoints);
            }
            eyeToSceneMapping->addPoint(eyeFeature, point, sceneData);
            this->clickedPoints.push_back(this->label->toAbsoluteImageCoordinate(point));
            if(eyeToSceneMapping->isReady()) {
                eyeToSceneMapping->calibrate();
                // This needs some refactoring... it's not cool...
                this->calibrationButton->setText("Start Calibrating");
                this->resetCalibrationPoints();
            }
            // This too
            this->remainingCalibrationPointsLabel->showRemainingPoints(eyeToSceneMapping->getRemainingPoints());
        }
    }
}

// EyeLabelProcessor

EyeLabelProcessor::EyeLabelProcessor(METState *state, ImageLabel *label) : state(state), label(label), firstPointSelected(false) {
}

void EyeLabelProcessor::onMousePress(QMouseEvent *event) {
    if(!this->state->getSettingLeftEyeROI() && !this->state->getSettingRightEyeROI()) {
        return;
    }
    if(event->button() == Qt::LeftButton) {
        Point absoluteClickPos = Point(event->pos());

        if(this->firstPointSelected) {
            Point initialPos = this->label->toImageCoordinate(this->initialPosition);
            Point imageClickPos = this->label->toImageCoordinate(absoluteClickPos);
            cv::Rect roi = cv::Rect(initialPos.asCVPoint(), imageClickPos.asCVPoint());
            if(this->state->getSettingLeftEyeROI()) {
                this->state->getBinocularROIDetector().setLeftROI(roi);
                this->state->setSettingLeftEyeROI(false);
            }
            else {
                this->state->getBinocularROIDetector().setRightROI(roi);
                this->state->setSettingRightEyeROI(false);
            }
            this->firstPointSelected = false;
        }
        else {
            this->initialPosition = absoluteClickPos;
            this->firstPointSelected = true;
        }
    }
}

QImage EyeLabelProcessor::onImageSet(QImage &image) {
    ImageProcessing processor;
    cv::Mat mat = QImageToMat(image);

    if(this->state->getBinocularROIDetector().leftReady()) {
        this->drawROI(mat, this->state->getBinocularROIDetector().getLeftROI(), cv::Scalar(100, 0, 255));
    }
    if(this->state->getBinocularROIDetector().rightReady()) {
        this->drawROI(mat, this->state->getBinocularROIDetector().getRightROI(), cv::Scalar(255, 0, 100));
    }
    if(this->firstPointSelected) {
        Point currentMouse = this->label->mapFromGlobal(QCursor::pos());
        currentMouse.x = std::max(std::min(this->label->size().width()-1, int(currentMouse.x)), 0);
        currentMouse.y = std::max(std::min(this->label->size().height()-1, int(currentMouse.y)), 0);
        processor.drawRectangle(mat, this->initialPosition, currentMouse, cv::Scalar(0, 200, 100));
    }
    return MatToQImage(mat);
}

void EyeLabelProcessor::drawROI(cv::Mat &img, cv::Rect &roi, cv::Scalar color) {
    ImageProcessing processor;
    Point p1 = this->label->toAbsoluteImageCoordinate(Point(roi.x, roi.y));
    Point p2 = this->label->toAbsoluteImageCoordinate(Point(roi.x + roi.width, roi.y + roi.height));
    processor.drawRectangle(img, p1, p2, color);
}

} // namespace haytham
