#include "NetworkThread.h"

namespace haytham {

NetworkThread::NetworkThread(METState *state, QObject *parent) :
    QThread(parent), server(state, parent), hasData(false), hasGesture(false)
{
    this->server.listen();
    // Initialize variables
    stopped=false;
    sampleNo=0;
    fpsSum=0;
    avgFPS=0;
    fps.clear();
}

/**
 * \brief Method executed on thread. Keeps sending gaze data.
 */
void NetworkThread::run()
{
    while(1)
    {
        /////////////////////////////////
        // Stop thread if stopped=TRUE //
        /////////////////////////////////
        stoppedMutex.lock();
        if (stopped)
        {
            stopped=false;
            stoppedMutex.unlock();
            break;
        }
        stoppedMutex.unlock();
        /////////////////////////////////
        /////////////////////////////////
        // Save capture time
        captureTime=t.elapsed();
        // Start timer (used to calculate capture rate)
        t.start();
        dataMutex.lock();
        if(this->hasData) {
            this->server.sendGazeData(this->data);
            this->hasData = false;
        }
        dataMutex.unlock();
        gestureMutex.lock();
        if(this->hasGesture) {
            this->server.sendHeadGesture(this->gesture);
            this->hasGesture = false;
        }
        gestureMutex.unlock();
        // Update statistics
        updateFPS(captureTime);
    }
    qDebug() << "Stopping network thread...";
    qDebug() << "Average network FPS: " << this->getAvgFPS();
}

void NetworkThread::updateFPS(int timeElapsed)
{
    // Add instantaneous FPS value to queue
    if(timeElapsed>0)
    {
        fps.enqueue((int)1000/timeElapsed);
        // Increment sample number
        sampleNo++;
    }
    // Maximum size of queue is DEFAULT_32
    if(fps.size()>32)
        fps.dequeue();
    // Update FPS value every DEFAULT_32 samples
    if((fps.size()==32)&&(sampleNo==32))
    {
        // Empty queue and store sum
        while(!fps.empty())
            fpsSum+=fps.dequeue();
        // Calculate average FPS
        avgFPS=fpsSum/32;
        // Reset sum
        fpsSum=0;
        // Reset sample number
        sampleNo=0;
    }
}

/**
 * \brief Stops sending gaze data.
 */
void NetworkThread::stopNetworkThread()
{
    stoppedMutex.lock();
    stopped=true;
    stoppedMutex.unlock();
}

/**
 * \returns The average frames per second (FPS).
 */
int NetworkThread::getAvgFPS()
{
    return avgFPS;
}

/**
 * \returns A pointer to the server object.
 */
Server *NetworkThread::getServerPointer() {
    return &(this->server);
}

/**
 * \brief Updates gaze data to be sent by the server.
 */
void NetworkThread::updateData(BinocularGazeData data) {
    dataMutex.lock();
    this->data = data;
    this->hasData = true;
    dataMutex.unlock();
}

/**
 * \brief Updates head gesture to be sent by the server.
 */
void NetworkThread::updateGesture(HeadGesture gesture) {
    gestureMutex.lock();
    this->gesture = gesture;
    this->hasGesture = true;
    gestureMutex.unlock();
}

} // namespace haytham
