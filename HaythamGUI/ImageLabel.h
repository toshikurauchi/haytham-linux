/*
 * ImageLabel.h
 *
 *  Created on: 14/11/2012
 *      Author: Andrew Kurauchi
 */

#ifndef SCENEIMAGELABEL_H
#define SCENEIMAGELABEL_H

#include <QLabel>
#include <QPushButton>
#include <vector>

#include "Common/METState.h"
#include "Common/Point.h"
#include "CalibrationPointsLabel.h"

namespace haytham {

class MousePressListener;
class KeyPressListener;
class ImageSetListener;

class ImageLabel : public QLabel
{
    Q_OBJECT
public:
    explicit ImageLabel(QWidget *parent = 0);
    void mousePressEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void setImage(QImage &image);
    void setMousePressListener(MousePressListener *mousePressListener);
    void setKeyPressListener(KeyPressListener *keyPressListener);
    void setImageSetListener(ImageSetListener *imageSetListener);
    Point toImageCoordinate(Point point);
    Point toAbsoluteImageCoordinate(Point point);
private:
    QSizeF imageSize;
    QSizeF labelSize;
    MousePressListener *mousePressListener;
    KeyPressListener *keyPressListener;
    ImageSetListener *imageSetListener;
};

} /* namespace haytham */

#endif // SCENEIMAGELABEL_H
