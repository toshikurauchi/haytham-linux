/*
 * ImageLabel.cpp
 *
 *  Created on: 14/11/2012
 *      Author: Andrew Kurauchi
 */

#include <QDebug>
#include <QMouseEvent>

#include "ImageLabel.h"
#include "EventListener.h"
#include "Calibration/Calibration.h"
#include "EyeDetection/EyeDetector.h"
#include "MatToQImage.h"
#include "Calibration/CalibrationPointsDetection.h"
#include "Calibration/SceneData.h"

namespace haytham {

ImageLabel::ImageLabel(QWidget *parent) :
    QLabel(parent), mousePressListener(0), keyPressListener(0), imageSetListener(0)
{
}

void ImageLabel::mousePressEvent(QMouseEvent *event) {
    if(this->mousePressListener != 0) {
        this->mousePressListener->onMousePress(event);
    }
}

void ImageLabel::keyPressEvent(QKeyEvent *event) {
    if(this->keyPressListener != 0) {
        this->keyPressListener->onKeyPress(event);
    }
}

void ImageLabel::setImage(QImage &image) {
    this->imageSize = image.size();
    if(this->size() != image.size()) {
        image = image.scaled(this->size(), Qt::KeepAspectRatio);
    }
    this->labelSize = image.size();
    if(this->imageSetListener != 0) {
        image = this->imageSetListener->onImageSet(image);
    }
    this->setPixmap(QPixmap::fromImage(image));
}

void ImageLabel::setMousePressListener(MousePressListener *mousePressListener) {
    this->mousePressListener = mousePressListener;
}

void ImageLabel::setKeyPressListener(KeyPressListener *keyPressListener) {
    this->keyPressListener = keyPressListener;
}

void ImageLabel::setImageSetListener(ImageSetListener *imageSetListener) {
    this->imageSetListener = imageSetListener;
}

Point ImageLabel::toImageCoordinate(Point point) {
    float xProportion, yProportion, newX, newY;

    xProportion = point.x / this->labelSize.width();
    yProportion = point.y / this->labelSize.height();

    newX = xProportion * this->imageSize.width();
    newY = yProportion * this->imageSize.height();

    return Point(newX, newY);
}

Point ImageLabel::toAbsoluteImageCoordinate(Point point) {
    float xProportion, yProportion, newX, newY;

    xProportion = point.x / this->imageSize.width();
    yProportion = point.y / this->imageSize.height();

    newX = xProportion * this->labelSize.width();
    newY = yProportion * this->labelSize.height();

    return Point(newX, newY);
}

} /* namespace haytham */
