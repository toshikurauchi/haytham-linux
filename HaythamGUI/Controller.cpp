/************************************************************************/
/* qt-opencv-multithreaded:                                             */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* Controller.cpp                                                       */
/*                                                                      */
/* Author: Nick D'Ademo <nickdademo@gmail.com>                          */
/* Modified by: Andrew Kurauchi <kurauchi@ime.usp.br>                   */
/*                                                                      */
/* Copyright (c) 2012 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

// Qt header files
#include <QtGui>

#include "Controller.h"
#include "ImageBuffer.h"

Controller::Controller()
{
} // Controller constructor

Controller::~Controller()
{
} // Controller destructor

/**
 * \param deviceNumber Camera device id to be used
 * \param imageBufferSize Size of the buffer used to store captured frames
 * \param dropFrame If true and the buffer is full current frame will be dropped.
 * Otherwise it will wait until a position in the buffer is free to add the new frame
 * \param capThreadPrio QThread::Priority value for capture thread priority
 * \param procThreadPrio QThread::Priority value for processing thread priority
 *
 * \brief Tries to connect to camera and, if connection is successful, starts running capture
 * and processing threads.
 */
bool Controller::connectToCamera(int deviceNumber, ImageBuffer &imageBuffer, int capThreadPrio)
{
    bool isOpened=false;
    this->captureThread = new CaptureThread(imageBuffer);
    // Attempt to connect to camera
    if((isOpened=captureThread->connectToCamera(deviceNumber)))
    {
        // Start capturing frames from camera
        this->captureThread->start((QThread::Priority)capThreadPrio);
    }
    else
    {
        delete captureThread;
    }
    // Return camera open result
    return isOpened;
} // connectToCamera()

/**
 * \brief Stops running threads (if any), clears image buffer and disconnects
 * camera.
 */
void Controller::disconnectCamera()
{
    if(captureThread->isRunning())
        stopCaptureThread();
    captureThread->disconnectCamera();
    delete captureThread;
} // disconnectCamera()

/**
 * \brief Stops running the capture thread.
 */
void Controller::stopCaptureThread()
{
    qDebug() << "About to stop capture thread...";
    captureThread->stopCaptureThread();
    // Take one frame off a FULL queue to allow the capture thread to finish
    if(captureThread->getImageBuffer().isFull())
        Mat temp = captureThread->getImageBuffer().getFrame();
    captureThread->wait();
    qDebug() << "Capture thread successfully stopped.";
} // stopCaptureThread()
