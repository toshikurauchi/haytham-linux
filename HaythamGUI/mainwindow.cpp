#include <QtGui>
#include <QString>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "CameraTab.h"
#include "EyeTab.h"
#include "VideoProcessingThread.h"
#include "Controller.h"
#include "EventListener.h"
#include "Calibration/Calibration.h"
#include "Common/DefaultConfiguration.h"

using namespace haytham;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    state(METState::getCurrent()),
    networkThread(METState::getCurrent(), this),
    cameraTab(METState::getCurrent(), this),
    eyeTab(METState::getCurrent()),
    dataTab(METState::getCurrent(), this),
    calibrationTab(METState::getCurrent(), this),
    networkTab(METState::getCurrent(), this->networkThread.getServerPointer(), this),
    sceneTab(METState::getCurrent(), this)
{
    this->ui->setupUi(this);

    // Frame labels
    this->frameLabels.insert(std::pair<haytham::VideoCategory, ImageLabel*>(EYE, ui->eye_image));
    this->frameLabels.insert(std::pair<haytham::VideoCategory, ImageLabel*>(SCENE ,ui->scene_image));

    state->addCalibrationMethod(new PolynomialCalibration9PointsFull, new PolynomialCalibration9PointsFull);
    state->addCalibrationMethod(new PolynomialCalibration9Points, new PolynomialCalibration9Points);
    state->addCalibrationMethod(new PolynomialCalibration4Points, new PolynomialCalibration4Points);
    state->addCalibrationMethod(new HomographyCalibration(4), new HomographyCalibration(4));
    state->addCalibrationMethod(new TwoDistancesCalibration(state->getCameraCalibration()), new TwoDistancesCalibration(state->getCameraCalibration()));

    this->setUpEyeTab();
    this->setUpCameraTab();
    this->setUpDataTab();
    this->setUpNetworkTab();
    this->setUpCalibrationTab();
    this->setUpSceneTab();

    this->processingThread.start(QThread::HighestPriority);
    this->networkThread.start(QThread::LowestPriority);
    connect(&this->processingThread, SIGNAL(newFrame(Frame)), this, SLOT(updateFrame(Frame)));
    connect(&this->processingThread, SIGNAL(newGazeData(BinocularGazeData)), &this->networkThread, SLOT(updateData(BinocularGazeData)));
    connect(&this->processingThread, SIGNAL(newHeadGesture(HeadGesture)), &this->networkThread, SLOT(updateGesture(HeadGesture)));
}

MainWindow::~MainWindow()
{
    cameraTab.~CameraTab();

    qDebug() << "About to stop processing thread...";
    this->processingThread.stopProcessingThread();
    qDebug() << "Processing thread successfully stopped.";
    qDebug() << "About to stop network thread...";
    this->networkThread.stopNetworkThread();
    qDebug() << "Network thread successfully stopped.";

    delete this->ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    if(event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) {
        this->ui->scene_image->keyPressEvent(event);
    }
}

void MainWindow::setUpEyeTab() {
    EyeLabelProcessor *eyeLabelProcessor = new EyeLabelProcessor(this->state, ui->eye_image);
    ui->eye_image->setMousePressListener(eyeLabelProcessor);
    ui->eye_image->setImageSetListener(eyeLabelProcessor);

    // Binocular
    eyeTab.setBinocularWidgets(ui->binocular, ui->set_left_eye_roi, ui->set_right_eye_roi);

    // Iris
    eyeTab.setIrisSizeSlider(ui->iris_size);
    eyeTab.setShowIrisSizeCheck(ui->show_iris_size);

    // Pupil
    eyeTab.setShowPupilCheck(ui->show_pupil);
    eyeTab.setPupilThresholdSlider(ui->pupil_threshold);

    // Gaps
    eyeTab.setFillGapsCheck(ui->fill_gaps);

    // Glint
    eyeTab.setRemoveGlintCheck(ui->remove_glint);
    eyeTab.setShowGlintCheck(ui->show_glint);
    eyeTab.setGlintThresholdSlider(ui->glint_threshold);
    eyeTab.setNumberOfGlintsCombo(ui->number_of_glints);

    // Eye Detectors
    eyeTab.setEyeDetectorCombo(ui->eye_detector);
}

void MainWindow::setUpCameraTab() {
    cameraTab.setWidgets(ui->start_stop_eye_camera, ui->start_stop_scene_camera, ui->start_both_cameras, ui->settings_eye_camera, ui->settings_scene_camera, ui->eye_camera, ui->scene_camera, this->processingThread);
    ui->show_images->setChecked(true);
    connect(ui->show_images, SIGNAL(clicked(bool)), this, SLOT(showImagesCheckClicked(bool)));
}

void MainWindow::setUpDataTab() {
    this->dataTab.setStartStopButton(ui->start_stop_recording);
    this->dataTab.setRecordGazeDataCheckbox(ui->record_eye_data);
    this->dataTab.setRecordEyeVideoCheckbox(ui->record_eye_video);
    this->dataTab.setRecordSceneVideoCheckbox(ui->record_scene_video);
}

void MainWindow::setUpNetworkTab() {
    this->networkTab.setNetworkMessagesEdit(this->ui->network_messages);
}

void MainWindow::setUpCalibrationTab() {
    SceneLabelProcessor *sceneLabelProcessor = new SceneLabelProcessor(this->state, ui->scene_image);
    sceneLabelProcessor->setCalibrationButton(ui->start_calibrating);
    sceneLabelProcessor->setRemainingCalibrationPointsLabel(ui->remaining_calibration_points);
    ui->scene_image->setMousePressListener(sceneLabelProcessor);
    ui->scene_image->setKeyPressListener(sceneLabelProcessor);
    ui->scene_image->setImageSetListener(sceneLabelProcessor);

    this->calibrationTab.setMethodsCombo(this->ui->calibration_method);
    this->calibrationTab.setStartButton(this->ui->start_calibrating, sceneLabelProcessor);
    this->calibrationTab.setSmoothGazeCheck(this->ui->gaze_smoothing);
    this->calibrationTab.setRemainingCalibrationPointsLabel(this->ui->remaining_calibration_points);
    this->calibrationTab.setCalibPointDetectThreshSlider(this->ui->calibPointDetectThresh);
    this->calibrationTab.setPupilCenterRadio(this->ui->pupil_center);
    this->calibrationTab.setPupilGlintRadio(this->ui->pupil_glint_vector);
}

void MainWindow::setUpSceneTab() {
    this->sceneTab.setThresholdSlider(ui->scene_threshold_slider);
    this->sceneTab.setScreenMinSizeSlider(ui->screen_min_size);
    this->sceneTab.setShowScreenCheckbox(ui->show_screen);
    this->sceneTab.setShowFilteredSceneCheckbox(ui->show_filtered_image);
    this->sceneTab.setVisualizationGroup(ui->visualization_group);
    this->sceneTab.setScreenDetectionMethodCombo(ui->screen_detectio_method);
    this->sceneTab.setCalibrateCameraButton(ui->calibrate_camera);
    this->sceneTab.setUndistortCheckbox(ui->undistort);
    this->sceneTab.setLoadCalibrationButton(ui->load_calibration);
    this->sceneTab.setAddCalibrationFrameButton(ui->add_calibration_frame);
    this->sceneTab.setCalibrationFileLabel(ui->calibration_file);
}

void MainWindow::updateFrame(const Frame &frame) {
    ImageLabel *frameLabel = frameLabels.find(frame.getCategory())->second;
    if(frameLabel != 0) {
        QImage image = frame.getFrame();
        frameLabel->setImage(image);
    }
}

void MainWindow::showImagesCheckClicked(bool checked) {
    if(checked) {
        connect(&this->processingThread, SIGNAL(newFrame(Frame)), this, SLOT(updateFrame(Frame)));
    }
    else {
        disconnect(&this->processingThread, SIGNAL(newFrame(Frame)), this, SLOT(updateFrame(Frame)));
    }
}
