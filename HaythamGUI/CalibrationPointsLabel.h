/*
 * CalibrationPointsLabel.h
 *
 *  Created on: 01/04/2013
 *      Author: Andrew Kurauchi
 */

#ifndef HAYTHAM_CALIBRATIONPOINTSLABEL_H
#define HAYTHAM_CALIBRATIONPOINTSLABEL_H

#include <QLabel>

namespace haytham {

class CalibrationPointsLabel : public QLabel
{
    Q_OBJECT
public:
    explicit CalibrationPointsLabel(QWidget *parent = 0);
    void showRemainingPoints(int points);
};

} // namespace haytham

#endif // HAYTHAM_CALIBRATIONPOINTSLABEL_H
