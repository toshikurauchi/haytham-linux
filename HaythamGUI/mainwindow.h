#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <map>
#include <QObject>

#include "Common/METState.h"
#include "CameraTab.h"
#include "EyeTab.h"
#include "DataTab.h"
#include "CalibrationTab.h"
#include "NetworkTab.h"
#include "SceneTab.h"
#include "VideoProcessingThread.h"
#include "Common/VideoCategory.h"
#include "CaptureThread.h"
#include "NetworkThread.h"
#include "ImageLabel.h"
#include "Camera/CameraController.h"

namespace Ui {
class MainWindow;
}

using namespace haytham;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void keyPressEvent(QKeyEvent *event);
private:
    Ui::MainWindow *ui;
    METState *state;
    haytham::VideoProcessingThread processingThread;
    haytham::NetworkThread networkThread;
    haytham::CameraTab cameraTab;
    haytham::EyeTab eyeTab;
    haytham::DataTab dataTab;
    haytham::CalibrationTab calibrationTab;
    haytham::NetworkTab networkTab;
    haytham::SceneTab sceneTab;
    std::map<haytham::VideoCategory, ImageLabel*> frameLabels;
    bool isCameraConnected;

    void setUpEyeTab();
    void setUpCameraTab();
    void setUpDataTab();
    void setUpNetworkTab();
    void setUpCalibrationTab();
    void setUpSceneTab();

private slots:
    void updateFrame(const Frame &frame);
    void showImagesCheckClicked(bool checked);
};

#endif // MAINWINDOW_H
