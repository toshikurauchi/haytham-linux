import HaythamEyeTrackerSync as et
import time

e = et.EyeTracker()

e.createEyeTracker()
e.connectEyeTracker()

if e.calibrateEyeTracker() == 1:
	e.startEyeTracker()

	for i in range(100):
		time.sleep(1.0/30.0)
		e.run()
		retVal, data = e.getEyeTrackerData()
		if retVal:
			for eye in data:
				print eye.x, eye.y, eye.pupilDiam, eye.tStamp
	
	e.stopEyeTracker()
