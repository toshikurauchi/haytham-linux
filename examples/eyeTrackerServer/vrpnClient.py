import vrpn

features = ["pupil", "eye_feature", "scene_gaze", "monitor_gaze"]

debug_print = False

class VrpnClient(object):
	def __init__(self, remoteAddr, port = 3893):
		self.trackers = []
		
		pos = remoteAddr.find('@')
		if pos != -1:
			device_name = remoteAddr[:pos]
			remote_ip = remoteAddr[pos+1:]
		else:
			device_name = "haytham"
			remote_ip = remoteAddr
		remote_ip += ":" + str(port)
		print "[vrpn] conecting to '" + remote_ip + "'..."
		
		count = 0
		for feature in features:
			address = device_name + "." + feature + "@" + remote_ip
			print "[vrpn] searching for '" + address + "'..."
			tracker = vrpn.receiver.Tracker(address)
			tracker.register_change_handler(count, self.handle_tracker, "position")
			self.trackers.append(tracker)
			count += 1
			print "[vrpn] OK!"
			
		self.feature_handlers = [None, None, None, None]
		
	def set_pupil_handler(self, pupil_handler):
		self.feature_handlers[0] = pupil_handler
	
	def set_eye_feature_handler(self, eye_feature_handler):
		self.feature_handlers[1] = eye_feature_handler
		
	def set_scene_handler(self, scene_handler):
		self.feature_handlers[2] = scene_handler
		
	def set_monitor_handler(self, monitor_handler):
		self.feature_handlers[3] = monitor_handler
			
	def handle_tracker(self, featureId, t):
		if self.feature_handlers[featureId] is None:
			if debug_print:
				print features[featureId], t["position"]
		else:
			data = list(t["position"])
			data.append((float(t["time"].strftime('%s.%f'))*1e6)%1e10)
			self.feature_handlers[featureId](data)
		
	def update(self):
		for i in range(len(self.trackers)):
			tracker = self.trackers[i]
			handler = self.feature_handlers[i]
			if debug_print or handler is not None:
				tracker.mainloop()

if __name__ == "__main__":
	client = VrpnClient("haytham@localhost")
	while 1:
		client.update()
