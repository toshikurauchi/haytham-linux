#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import pyglet
from PyQt4 import QtCore
from PyQt4 import QtNetwork
from vrpnClient import VrpnClient
import numpy as np
import threading
import time
import math
import sys
from stimulus import CalibrationPoint, Circle
sys.path.append('../')
from mouseUbuntu import Screen
from hostFinder import detect_host

HOST = detect_host()  # Haytham server host
PORT = 3894           # Haytham server port

SCREEN_ID = 0         # Screen to be used
CALIB_BORDER = 50     # Number of pixels between screen border and calibration points
CALIB_SHAPE = (3, 3)  # Shape of calibration points distribution
N_CALIB_POINTS = CALIB_SHAPE[0]*CALIB_SHAPE[1] # Number of calibration points

LANG = 'pt-BR'

if LANG == 'pt-BR':
	WAIT_LABEL_TEXT = u"Por favor aguarde as instruções do experimentador"
else:
	WAIT_LABEL_TEXT = "Please wait for the instructions from the experimenter"

class DataClient(threading.Thread):
	def __init__(self, host = HOST):
		threading.Thread.__init__(self)
		self.client = VrpnClient("haytham@" + host)
		self.latest_scene_data = None
		self.client.set_eye_feature_handler(self.handle_eye_feature)
		self.client.set_scene_handler(self.handle_scene)
		self.quit = False
		self.reset_buffer()
		
	def handle_eye_feature(self, data):
		if not math.isnan(data[0]) and not math.isnan(data[1]):
			# Increase a position in buffers
			if len(self.latest_eye_x) == self.eye_count:
				self.latest_eye_x.append(0)
				self.latest_eye_y.append(0)
			self.latest_eye_x[self.eye_count] = data[0]
			self.latest_eye_y[self.eye_count] = data[1]
			self.eye_count = (self.eye_count + 1)%self.buffer_sz
		
	def handle_scene(self, data):
		self.latest_scene_data = data
		
	def filtered_feature(self):
		if len(self.latest_eye_x) < self.buffer_sz:
			return [float('NaN'), float('NaN')]
		return [np.median(self.latest_eye_x), np.median(self.latest_eye_y)]
		
	def reset_buffer(self):
		self.buffer_sz = 5
		self.latest_eye_x = []
		self.latest_eye_y = []
		self.eye_count = 0
		
	def update(self):
		self.client.update()

	def run(self):
		while not self.quit:
			self.update()
			time.sleep(1.0/60)

def clear_calibration_points(host=HOST, port=PORT):
	tcpSocket = QtNetwork.QTcpSocket()
	tcpSocket.connectToHost(QtNetwork.QHostAddress(host), port)
	if tcpSocket.waitForConnected(-1):
		tcpSocket.write(unicode('c\n'))
		tcpSocket.waitForBytesWritten()
		tcpSocket.disconnectFromHost()

def send_calibration_point(eye_point, scene_point, host=HOST, port=PORT):
	tcpSocket = QtNetwork.QTcpSocket()
	tcpSocket.connectToHost(QtNetwork.QHostAddress(host), port)
	if tcpSocket.waitForConnected(-1):
		tcpSocket.write(unicode('C ' + str(eye_point[0]) + ' ' + str(eye_point[1]) + ' ' + \
								str(scene_point[0]) + ' ' + str(scene_point[1]) + '\n'))
		tcpSocket.waitForBytesWritten()
		print "Eye Point: " + str(eye_point) + " Scene Point: " + str(scene_point) + " added"
		tcpSocket.disconnectFromHost()

class Calibration(object):
	def __init__(self, host=HOST, port=PORT):
		self.host = host
		self.port = port
		self.cur_calib_confirm = 0
		self.show_gaze = True

	# Define callback for calibration validation points
	def next_calib_confirm(self):
		self.cur_calib_confirm += 1
		if self.cur_calib_confirm == len(self.calibPositions):
			self.validated = True

	def calibrate(self):
		# Init pyglet window
		screen = pyglet.window.get_platform().get_default_display().get_screens()[SCREEN_ID]
		self.width  = screen.width
		self.height = screen.height
		self.window = pyglet.window.Window(fullscreen=True, screen=screen)
		self.window.on_draw = self.on_draw
		self.window.on_key_press = self.on_key_press
		self.window.on_close = self.on_close
		
		# Init wait label
		self.wait_label = pyglet.text.Label(WAIT_LABEL_TEXT,
											font_name='Arial',
											font_size=36,
											x=self.window.width/2, y=self.window.height/2,
											anchor_x='center', anchor_y='center')
		
		# Init calibration positions
		self.calibPositions = self.calibPositionsForShape(CALIB_SHAPE)

		# Init calibration positions
		self.calibPoint = 0
		self.calibPositions = self.calibPositions
		self.calibConfirm = []
		self.calibPoints = []
		for position in self.calibPositions:
			self.calibConfirm.append(CalibrationPoint(position, self.next_calib_confirm, 30, 50))
			self.calibPoints.append(CalibrationPoint(position, lambda : None))

		# Init self.client
		self.client = DataClient(self.host)
		
		# Set update function
		self.validated = False
		pyglet.clock.schedule_interval(self.update, 1/60.0)
		
		# Clear previous calibration points from server buffer
		clear_calibration_points()
		
		# Start app
		pyglet.app.run()
		
		if self.validated:
			return 1
		return -1
		
	def calibPositionsForShape(self, shape):
		minX = CALIB_BORDER
		minY = CALIB_BORDER
		maxX = self.width - CALIB_BORDER
		maxY = self.height - CALIB_BORDER
		hStep = (maxX - minX)/(shape[0] - 1)
		vStep = (maxY - minY)/(shape[1] - 1)
		positions = []
		for j in range(shape[1]):
			for i in range(shape[0]):
				positions.append((minX + i*hStep, maxY - j*vStep))
		return positions

	def update(self, dt):
		self.client.update()
		self.data = self.client.latest_scene_data
		
	def on_key_press(self, symbol, modifiers):
		confirm_keys = [pyglet.window.key.ENTER, pyglet.window.key.SPACE]
		if symbol == pyglet.window.key.ESCAPE:
			self.window.close()
		# Add calibration point
		elif symbol in confirm_keys:
			# Calibration was validated
			if self.validated:
				self.window.close()
			
			# Go to next calibration point
			if self.calibPoint < N_CALIB_POINTS:
				feature = self.client.filtered_feature()
				if not math.isnan(feature[0]) and not math.isnan(feature[1]):
					calibPosition = self.calibPositions[self.calibPoint]
					calibPosition = (calibPosition[0], self.height - calibPosition[1])
					send_calibration_point(feature, 
										   calibPosition,
										   self.host, self.port)
					self.client.reset_buffer()
					self.calibPoint += 1
		elif symbol == pyglet.window.key.C:
			self.calibPoint = 0
			self.cur_calib_confirm = 0
			self.validated = False
			clear_calibration_points()
		elif symbol == pyglet.window.key.H:
			self.show_gaze = not self.show_gaze
					
	def on_close(self):
		self.window.close()
					
	def on_draw(self):
		self.window.clear()

		# If still calibrating, draw calibration target
		if self.calibPoint < N_CALIB_POINTS:
			self.calibPoints[self.calibPoint].draw()
		# Else draw current gaze position
		else:
			if not math.isnan(self.data[0]) and not math.isnan(self.data[1]):
				gaze = (int(self.data[0]), self.height - int(self.data[1]))
				if self.show_gaze:
					Circle(gaze, 10).draw()
				# If validating calibration
				if self.cur_calib_confirm < len(self.calibConfirm):
					self.calibConfirm[self.cur_calib_confirm].draw()
					self.calibConfirm[self.cur_calib_confirm].gaze_collision(gaze)
				else:
					self.wait_label.draw()

if __name__ == "__main__":
	if Calibration().calibrate() == 1:
		print 'System Calibrated'
	else:
		print 'System Not Calibrated'

