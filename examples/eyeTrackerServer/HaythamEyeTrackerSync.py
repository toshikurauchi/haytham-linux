'''
Class to read eye tracking data from Haytham eye tracker
'''

from multiprocessing import Array, Value
from collections import deque
import ctypes as c
import time
import math
import os
import sys
import csv
import platform
from haythamCalibrateToMonitor import DataClient, Calibration
t0 = time.time()
# !!!!! FOR MOUSE EMULATION !!!!!
canReadMouse = False
osName = platform.system()
if osName == "Windows":
    try: 
        import win32api
        canReadMouse = True
    except:
        pass
elif osName == "Linux":
    try: 
        from Xlib import display
        canReadMouse = True
        root         = display.Display().screen().root
    except:
        pass

MAX_SAMPLE_SIZE = 60

class Eye(c.Structure):
    _fields_ = [("x", c.c_double), ("y", c.c_double), ("pupilDiam", c.c_double), ("tStamp", c.c_double)]
    
class BothEyes(c.Structure):    
    _fields_ = [("left", Eye), ("right", Eye)]

class EyeTracker():
	def __init__(self, mouseEmu = False, binocular = False, fileName = "", scaleX = 1.0, scaleY = 1.0, host='localhost'):
		print "create eye tracker"
		self.mouseEmu  = mouseEmu
		self.binocular = binocular
		self.fileName  = fileName
		self.scaleX	= scaleX
		self.scaleY	= scaleY
		self.host = host
		self.client = None
		self.last_data = None
		self.last_tStamp = 0
		if fileName != "":
			print "Read eye data from file: ", fileName 
		elif mouseEmu:
			print "mouse emulation mode"
		if binocular:
			print "return binocular data"
			
	def createEyeTracker(self):
		print "Create eye tracking data"
		self.eyeData = deque(maxlen = MAX_SAMPLE_SIZE) # shared, can be used from multiple processes
		self.eyeCount = Value('i', 0)
		self.runningT = Value('i', 0)

	def connectEyeTracker(self):
		if self.fileName != "":
			print "Opening eye data file: ", self.fileName
			try:
				f = open(self.fileName)
			except:
				print "Eye data file does not exists"
				sys.exit(1)
			self.file = csv.reader(f, delimiter=',')	 
			if self.file == None:
				print "Unable to open eye data file"
				sys.exit(1)
			return 1
		elif self.mouseEmu:
			print "Mouse emulation mode activated"
			return 1
		elif self.fileName == "":
			print "Connect to eye tracker"
			# Connection is done at calibration success
			return 1
		
	def calibrateEyeTracker(self):
		if self.fileName == "" and not self.mouseEmu: 
			print "Calibrate eye tracker" 
			if Calibration().calibrate() == 1:
				self.client = DataClient(self.host)
				self.client.start()
				return 1
			else:
				# TODO What to do in this case?
				return -1
		else:
			return 1
		
	def loadCalibration(self, calibFileName):
		print "Load calibration from", calibFileName
		return -1
	
	def saveCalibration(self, calibFileName):
		print "Save calibration to", calibFileName
		return -1

	def startEyeTracker(self):
		print "start reading eye data"
		self.runningT.value = 1
		return 1
		
	def getEyeTrackerData(self):
		retCode = 1
		if len(self.eyeData) == 0:
			self.eyeData.append(self.last_data)
		retList = list(self.eyeData)
		self.clearEyeTrackerData()
		return [retCode, retList]

	def stopEyeTracker(self):
		print "stop reading from eye tracker"
		self.runningT.value = 0
		self.client.quit = True
		return 1
	
	def getMousePos(self):
		x, y = -1, -1
		if canReadMouse:
			if osName == "Linux":
				pointer = root.query_pointer()
				data = pointer._data
				x, y = data["root_x"], data["root_y"]
			elif osName == "Windows":
				x, y = win32api.GetCursorPos()
		return x, y
		
	def clearEyeTrackerData(self):
		self.eyeData.clear()
		return 1

	def run(self):
		retCode = -1
		if (self.runningT.value == 1):
			leftX = leftY = rightX = rightY = tStamp = 0
			leftPupil = rightPupil = 0.0
			# !!!!!! MOUSE EMULATION !!!!!!!
			if (self.mouseEmu):
				leftX, leftY = self.getMousePos()
				rightX = leftX
				rightY = leftY
				tStamp = time.time() * 1000000 # simula microsegundos, igual que o rastreador
				tStamp = c.c_double(math.floor(tStamp/100))
				leftPupil = 15.0
				rightPupil = 15.0
				retCode = 1 # simula a leitura feita com sucesso
				
			elif self.fileName != "": # Read eye data from a file
				try:
					line	   =  self.file.next()
				except:
					print "Reached end of eye data file"
					retCode = -1
				else:
					tStamp	 =  long(line[0])
					leftX	  =  int(float(line[1]))
					leftY	  =  int(float(line[2]))
					leftPupil  = float(line[3]) 
					rightX	 =  int(float(line[4]))
					rightY	 =  int(float(line[5]))
					rightPupil = float(line[6])
					retCode	= 1
			else: # Read from eye tracker
				data = self.client.latest_scene_data
				gaze = None
				if data is not None and not math.isnan(data[0]) and not math.isnan(data[1]):
					tStamp = data[3]
					gaze = (data[0], data[1])
					retCode = 1
				# ------------------------------
				if tStamp == self.last_tStamp:
					retCode = -1
				else:
					self.last_tStamp = tStamp
				if (retCode == 1):
					# calcula point of regard como sendo o ponto medio dos dois olhos
					leftX  = gaze[0]
					leftY  = gaze[1]
					rightX = gaze[0]
					rightY = gaze[1]
					leftPupil  = 15 # TODO do we need it?
					rightPupil = 15 # TODO do we need it?
					tStamp = c.c_double(tStamp)

			if (retCode == 1):
				# Leu dados do olhar, agora devolve-os monocular ou binocular
				if self.binocular:
					tmpData = BothEyes(Eye(leftX*self.scaleX, leftY*self.scaleY, leftPupil, tStamp), Eye(rightX*self.scaleX, rightY*self.scaleY, rightPupil, tStamp))
				else: 
					x = (leftX + rightX) / 2 * self.scaleX
					y = (leftY + rightY) / 2 * self.scaleY
					meanPupil = (leftPupil + rightPupil)/2
					tmpData = Eye(x, y, meanPupil, tStamp)

				self.last_data = tmpData
				self.eyeData.append(tmpData)
				self.eyeCount.value += 1
		# Se em modo simulacao, espera um pouquinho
		if self.mouseEmu or self.fileName != "":
			time.sleep(1.0/400.0)
		return retCode
