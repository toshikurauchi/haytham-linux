#!/usr/bin/env python
 
"""
Server process that reads eye data from the SMI computer and stores in a circular queue.
This implementation works only for one client.
Uses collections.deque to mainain a circular list
"""
 
# ----------------- Importing modules -----------------
import socket
import sys
import os
import pickle
import time
import argparse
from collections import deque
import platform

canMoveMouse = False
osName = platform.system()
if osName == "Windows":
    try: 
        import win32api
        canMoveMouse = True
    except:
        pass
elif osName == "Linux":
    try: 
        from Xlib import X, display
        canMoveMouse = True
        disp = display.Display() 
        root = disp.screen().root
    except:
        pass

sys.path.append("../EyeTracker")
import EyeTrackerSync as et

def moveMouseWithGaze():
    global e
    [r, eyeD] = e.getEyeTrackerData()
    if r == 1:
        l = len(eyeD) - 1
        if args.binocular:
            x = (eyeD[l].left.x + eyeD[l].right.x)/2
            y = (eyeD[l].left.y + eyeD[l].right.y)/2
        else:
            x, y = eyeD[l].x, eyeD[l].y
        if osName == "Linux":
            root.warp_pointer(x, y)
            disp.sync()
        elif osName == "Windows":
            win32api.SetCursorPos( (int(x), int(y)) )

def processMsg(data, address):
    """Receives and process a message from the client and sends the return data via socket.
    Return value: 0 = success, 1 = error"""
    global e
    global sock
    global running
    global protocol
    global count
    
    if data == "calibrate":
        r = e.calibrateEyeTracker()
        if (r == 1):
            sock.sendto("success", address)
        else:
            sock.sendto("error_%i"%r, address)
            
    elif data == "saveCalibration":
        print "------------- Client request to save current calibration, requesting the file name ------------"
        sock.setblocking(1)
        sock.sendto("sendCalibFile", address)
        try:
            print "Waiting for file name..."
            calibFileName, addr = sock.recvfrom(size)
            print "File name received: ", calibFileName
        except:
            print "Exception occurred: ", sys.exc_info()[1]
        if not args.mouseEmu:
            r = e.saveCalibration(calibFileName)
        else:
            r = 1
        if r == 1:
            print "Calibration file saved in server with success"
            sock.sendto("success", addr)
        else:
            print "Could not save calibration file to server, SMI return code:", r
            sock.sendto("error_%i"%r, addr)
        sock.setblocking(False)

    elif data == "loadCalibration":
        print "------------- Client request to load previously saved calibration, requesting the file name ------------"
        sock.setblocking(1)
        sock.sendto("sendCalibFile", address)
        try:
            print "Waiting for file name..."
            calibFileName, addr = sock.recvfrom(size)
            print "File name received: ", calibFileName
        except:
            print "Exception occurred: ", sys.exc_info()[1]
        if not args.mouseEmu:
            r = e.loadCalibration(calibFileName)
        else:
            r = 1
        if r == 1:
            print "Calibration file loaded with success"
            sock.sendto("success", addr)
        else:
            print "Could not load calibration file from server, SMI return code:", r
            sock.sendto("error_%i"%r, addr)
        sock.setblocking(False)

    elif data == "connect":
        r = e.connectEyeTracker()
        if (r == 1):
            sock.sendto(pickle.dumps("success", protocol), address)
        else:
            sock.sendto(pickle.dumps("error_%i"%r, protocol), address)

    elif data == "start":
        r = e.startEyeTracker()
        if (r == 1):
            sock.sendto(pickle.dumps("success", protocol), address)
            count = 0
        else:
            sock.sendto(pickle.dumps("error_%i"%r, protocol), address)

    elif data == "clear":
        r = e.clearEyeTrackerData()
        if (r == 1):
            sock.sendto(pickle.dumps("success", protocol), address)
            count = 0
        else:
            sock.sendto(pickle.dumps("error_%i"%r, protocol), address)

    elif data == "get_data":
        #t0 = time.time()
        [r, eyeD] = e.getEyeTrackerData()
        #t1 = time.time()
        #print "running time to get data: ", (t1-t0)
        if (r == 1):
            sock.sendto(pickle.dumps(eyeD, protocol), address)
            count += len(eyeD)
        else:
            sock.sendto(pickle.dumps("no_data", protocol), address)
#            print "no new data to send"

    elif data == "stop":
        r = e.stopEyeTracker()
        if (r == 1):
            sock.sendto(pickle.dumps("success", protocol), address)
            print "Last client received ", count, "samples"
        else:
            sock.sendto(pickle.dumps("error_%i"%r, protocol), address)
            
    elif data == "exit":
        running = 0

if __name__ == '__main__':
    # Define server properties
    host = ''
    port = 9003
    size = 65536
    count = 0
    protocol = 1 # binary
    # Configure server socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((host, port))
    sock.setblocking(False)
    print "Server listening to client on port %i"%port 
     
    # Parse command line arguments
    parser = argparse.ArgumentParser(description = "Server process for reading data from eye tracker")
    parser.add_argument("--mouseEmu" , default = 0 , help = "1 -> emulate the eye tracker with the mouse (default is 0)")  # Emulate eye tracker with mouse
    parser.add_argument("--binocular", default = 1 , help = "1 -> return binocular eye data (default") # Return data from both eyes
    parser.add_argument("--eyeFile"  , default = "", help = "if set, file previously recorded from where to read eye data")
    # Adjust the width and height
    parser.add_argument("--scaleX"   , default = 1.0, help = "float to scale x position of the eye (default is 1)")
    parser.add_argument("--scaleY"   , default = 1.0, help = "float to scale y position of the eye (default is 1)")    
    # Move the cursor pointer?
    parser.add_argument("--moveMouse", default = 0  , help = "1 -> update mouse position with the latest eye sample (default is 0)")
    
    args = parser.parse_args()
    print "mouseEmu  = ", args.mouseEmu
    print "binocular = ", args.binocular
    print "eyeFile   = ", args.eyeFile
    
    if args.moveMouse and not canMoveMouse:
        print "Error: can not move the mouse pointer with the gaze data, it seems that Xlib is not installed"
        print "You can install it with \"sudo apt-get install python-xlib\", aborting..."
        sys.exit(1)
    moveMouse = args.moveMouse
     
    e = et.EyeTracker( args.mouseEmu, args.binocular, args.eyeFile, float(args.scaleX), float(args.scaleY) )
            
    e.createEyeTracker()
    e.connectEyeTracker()
    
    # wait for connections
    # terminate with 
    running = 1
    while running:
        try:
            data, address = sock.recvfrom(size)
#            print "datagram from", address
            processMsg(data, address)
        except:
            e.run()
            if moveMouse:
                moveMouseWithGaze()
                
#            print "Erro com o socket"
#        finally:
#    e.stopEyeTracker()
            
    sock.close()
