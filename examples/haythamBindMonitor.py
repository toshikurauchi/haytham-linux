from PyQt4 import QtCore
from PyQt4 import QtNetwork

HOST = '192.168.30.62'    # Haytham server host
PORT = 49513             # Haytham server port

def bind_monitor(host=HOST, port=PORT):
	tcpSocket = QtNetwork.QTcpSocket()
	tcpSocket.connectToHost(QtNetwork.QHostAddress(host), port)
	if tcpSocket.waitForConnected(-1):
		tcpSocket.write(unicode('M 1680 1050\n'))
		tcpSocket.waitForBytesWritten()
		print "Monitor bound"
		tcpSocket.disconnectFromHost()

def unbind_monitor(host=HOST, port=PORT):
	tcpSocket = QtNetwork.QTcpSocket()
	tcpSocket.connectToHost(QtNetwork.QHostAddress(host), port)
	if tcpSocket.waitForConnected(-1):
		tcpSocket.write(unicode('m\n'))
		tcpSocket.waitForBytesWritten()
		print "Monitor unbound"
		tcpSocket.disconnectFromHost()

def __main__():
	bind_monitor()
