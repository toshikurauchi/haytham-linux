#!/usr/bin/python
import pygtk
pygtk.require('2.0')
import gtk
from haythamBindMonitor import bind_monitor
from haythamBindMonitor import unbind_monitor
from PyQt4 import QtNetwork

HOST = '192.168.30.62'    # Haytham server host
PORT = 44357             # Haytham server port

SIZE = 150
quit = False

def destroy(widget, data=None):
	global quit
	quit = True

def get_position(position):
	return int(position - SIZE/2)

def keypress(widget, event):
	global quit
	if event.keyval == gtk.keysyms.Escape:
		quit = True

window = gtk.Window(gtk.WINDOW_TOPLEVEL)
window.show()
window.connect("destroy", destroy)
window.connect("key-press-event", keypress)
window.fullscreen()

image = gtk.Image()
pixbuf = []
pixbuf.append(gtk.gdk.pixbuf_new_from_file("circle.png"))
pixbuf.append(gtk.gdk.pixbuf_new_from_file("circle1.png"))
pixbuf.append(gtk.gdk.pixbuf_new_from_file("circle2.png"))
pixbuf.append(gtk.gdk.pixbuf_new_from_file("circle3.png"))
pixbuf.append(gtk.gdk.pixbuf_new_from_file("circle4.png"))
pixbuf.append(gtk.gdk.pixbuf_new_from_file("circle5.png"))
pixbuf.append(gtk.gdk.pixbuf_new_from_file("circle6.png"))
pixbuf.append(gtk.gdk.pixbuf_new_from_file("circle7.png"))
pixbuf.append(gtk.gdk.pixbuf_new_from_file("circle8.png"))
pixbuf.append(gtk.gdk.pixbuf_new_from_file("circle9.png"))
pixbuf.append(gtk.gdk.pixbuf_new_from_file("circle10.png"))
pixbuf.append(gtk.gdk.pixbuf_new_from_file("circle11.png"))
for i in range(12):
	pixbuf[i] = pixbuf[i].scale_simple(SIZE,SIZE,gtk.gdk.INTERP_BILINEAR)
cur_buf = 0
count = 0
image.set_from_pixbuf(pixbuf[0])
image.show()

fixed = gtk.Fixed()
fixed.put(image, get_position(1680/2), get_position(1050/2))
fixed.show()

window.add(fixed)

bind_monitor(HOST, PORT)
tcpSocket = QtNetwork.QTcpSocket()
tcpSocket.connectToHost(QtNetwork.QHostAddress(HOST), PORT)
if tcpSocket.waitForConnected(-1):
	tcpSocket.write(unicode('SD\n'))
	tcpSocket.waitForBytesWritten()
	print "Stream requested"

	while not quit and tcpSocket.waitForReadyRead():
		data_line = str(tcpSocket.readAll())
		data = [float(x) for x in data_line.split(', ')]
		fixed.move(image, get_position(data[-2]), get_position(data[-1]))
		count += 1
		if count == 2:
			cur_buf = (cur_buf + 1) % 12
			image.set_from_pixbuf(pixbuf[cur_buf])
			count = 0
		gtk.main_iteration()

	tcpSocket.write(unicode('s\n'))
	tcpSocket.waitForBytesWritten()
	print "Stop stream requested"
	tcpSocket.disconnectFromHost()
	
unbind_monitor(HOST, PORT)
