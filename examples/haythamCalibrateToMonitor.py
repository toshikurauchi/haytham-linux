from mouseUbuntu import Mouse, Screen
import pyglet
from PyQt4 import QtCore
from PyQt4 import QtNetwork
from vrpnClient import VrpnClient
import numpy as np
import threading
import time
import math
import sys
from stimulus import CalibrationPoint, Circle

HOST = '192.168.32.93'  # Haytham server host
PORT = 3894              # Haytham server port

class DataClient(threading.Thread):
	def __init__(self, host = HOST):
		threading.Thread.__init__(self)
		self.client = VrpnClient("haytham@" + host)
		self.latest_scene_data = None
		self.client.set_eye_feature_handler(self.handle_eye_feature)
		self.client.set_scene_handler(self.handle_scene)
		self.quit = False
		self.reset_buffer()
		
	def handle_eye_feature(self, data):
		if not math.isnan(data[0]) and not math.isnan(data[1]):
			# Increase a position in buffers
			if len(self.latest_eye_x) == self.eye_count:
				self.latest_eye_x.append(0)
				self.latest_eye_y.append(0)
			self.latest_eye_x[self.eye_count] = data[0]
			self.latest_eye_y[self.eye_count] = data[1]
			self.eye_count = (self.eye_count + 1)%self.buffer_sz
		
	def handle_scene(self, data):
		self.latest_scene_data = data
		
	def filtered_feature(self):
		if len(self.latest_eye_x) < self.buffer_sz:
			return [float('NaN'), float('NaN')]
		return [np.median(self.latest_eye_x), np.median(self.latest_eye_y)]
		
	def reset_buffer(self):
		self.buffer_sz = 5
		self.latest_eye_x = []
		self.latest_eye_y = []
		self.eye_count = 0
		
	def update(self):
		self.client.update()

	def run(self):
		while not self.quit:
			self.update()

def clear_calibration_points(host=HOST, port=PORT):
	tcpSocket = QtNetwork.QTcpSocket()
	tcpSocket.connectToHost(QtNetwork.QHostAddress(host), port)
	if tcpSocket.waitForConnected(-1):
		tcpSocket.write(unicode('c\n'))
		tcpSocket.waitForBytesWritten()
		tcpSocket.disconnectFromHost()

def send_calibration_point(eye_point, scene_point, host=HOST, port=PORT):
	tcpSocket = QtNetwork.QTcpSocket()
	tcpSocket.connectToHost(QtNetwork.QHostAddress(host), port)
	if tcpSocket.waitForConnected(-1):
		tcpSocket.write(unicode('C ' + str(eye_point[0]) + ' ' + str(eye_point[1]) + ' ' + \
								str(scene_point[0]) + ' ' + str(scene_point[1]) + '\n'))
		tcpSocket.waitForBytesWritten()
		print "Eye Point: " + str(eye_point) + " Scene Point: " + str(scene_point) + " added"
		tcpSocket.disconnectFromHost()

if __name__ == "__main__":
	clear_calibration_points()
	client_thread = DataClient()
	client_thread.start()
	mouse = Mouse()
	screen = Screen()
	screen_id = 1
	w = screen.resolutions[screen_id].width
	h = screen.resolutions[screen_id].height
	positions = [(25, 25),  (w/2, 25),  (w-25, 25), 
				 (25, h/2), (w/2, h/2), (w-25, h/2),
				 (25, h-25), (w/2, h-25), (w-25, h-25)]
	for position in positions:
		mouse.move(position)
		raw_input("Press Enter to confirm...")
		feature = client_thread.filtered_feature()
		while math.isnan(feature[0]) or math.isnan(feature[1]):
			print "Sorry, an error occured. Please try again"
			raw_input("Press Enter to confirm...")
			feature = client_thread.filtered_feature()
		send_calibration_point(feature, position)

	client_thread.quit = True

	try:
		while True:
			client_thread.update()
			data = client_thread.latest_scene_data
			if not math.isnan(data[0]) and not math.isnan(data[1]):
				mouse.move(data)
	except KeyboardInterrupt:
		pass

