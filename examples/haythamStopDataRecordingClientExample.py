from PyQt4 import QtCore
from PyQt4 import QtNetwork

HOST = '192.168.33.4'    # Haytham server host
PORT = 33280             # Haytham server port

tcpSocket = QtNetwork.QTcpSocket()
tcpSocket.connectToHost(QtNetwork.QHostAddress(HOST), PORT)
if tcpSocket.waitForConnected(-1):
	tcpSocket.write(unicode('r\n'))
	tcpSocket.waitForBytesWritten()
	print "message sent"
	tcpSocket.disconnectFromHost()
