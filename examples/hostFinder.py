from PyQt4 import QtNetwork

detected_host = None

def detect_host():
	global detected_host
	if detected_host is None:
		# Default: Use IPv4 localhost
		detected_host = '127.0.0.1'
		# Use the first non-localhost IPv4 address
		for address in QtNetwork.QNetworkInterface.allAddresses():
			if address != QtNetwork.QHostAddress.LocalHost and address.toIPv4Address():
				detected_host = str(address.toString())
				break
	return detected_host
	
if __name__ == '__main__':
	print detect_host()
