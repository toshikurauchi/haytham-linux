from mouseUbuntu import Mouse, Screen
from vrpnClient import VrpnClient
import math

client = VrpnClient("haytham@localhost")
mouse = Mouse()
screen = Screen()
current_screen = 1

def move_mouse(pos):
	if len(pos) < 2:
		return
	if not math.isnan(pos[0]) and not math.isnan(pos[1]):
		x = pos[0]*int(screen.resolutions[current_screen].width)
		y = pos[1]*int(screen.resolutions[current_screen].height)
		pos = (x, y)
		mouse.move(pos)

client.set_monitor_handler(move_mouse)

try:
	while True:
		client.update()
except KeyboardInterrupt:
	pass
