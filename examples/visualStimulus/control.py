import sys
import pygame
import ConfigParser
import json
from stimulus import Image
from socketCommunication import Server
from animals import init_animals

# Define mouse constants
LEFT_BUTTON = 1
RIGHT_BUTTON = 3

# Read config file
config = ConfigParser.RawConfigParser()
config.read('config.txt')
exp_w, exp_h = config.getint('CONFIG', 'width'), config.getint('CONFIG', 'height')
app_port = config.getint('CONFIG', 'app_port')

# Socket setup
server = Server('', app_port)
server.start()

# Convert animal list to data to be transmited
def convert_to_data(animals):
	data = []
	for animal in animals:
		ratio = float(exp_h)/canvas_h
		pos = [int(animal.center[0]*ratio), int(animal.center[1]*ratio)]
		data.append({"id": animal.id, "pos": pos, "type": animal.flag})
	return json.dumps(data)

# Init pygame and window
pygame.init()
canvas_w, canvas_h = exp_w*600/exp_h, 600
menu_w = 70
screen = pygame.display.set_mode((canvas_w + menu_w, canvas_h))

# Init animals
animal_data = init_animals()
# Animals are separated between canvas and menu
x_pos = canvas_w + menu_w/2
menu_animals = [
	Image((x_pos, canvas_h/5), lambda: None, animal_data["pig"][0:2], 20, flag="pig"),
	Image((x_pos, 2*canvas_h/5), lambda: None, animal_data["goat"][0:2], 20, flag="goat"),
	Image((x_pos, 3*canvas_h/5), lambda: None, animal_data["sheep"][0:2], 20, flag="sheep"),
	Image((x_pos, 4*canvas_h/5), lambda: None, animal_data["cow"][0:2], 20, flag="cow"),
	]
canvas_animals = []
# Animal being dragged
selected_animal = None
# Initial position of the animal removed from the menu
menu_position = None
# Next available id for a new animal
next_id = 0

# Add id's
for animal in menu_animals:
	animal.id = next_id
	next_id += 1

while True:
	# Draw background
	screen.fill((0,0,0))
	pygame.draw.line(screen, (255, 255, 255), (canvas_w, 15), (canvas_w, canvas_h-15), 3)

	# Draw animals
	for animal in menu_animals:
		animal.draw(screen)
	for animal in canvas_animals:
		animal.draw(screen)

	# Draw on screen
	pygame.display.update()

	# Handle events
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			server.stop = True
			while not server.stopped:
				pass
			pygame.quit()
			sys.exit()
		elif event.type == pygame.MOUSEBUTTONDOWN:
			# Select an animal
			if event.button == LEFT_BUTTON:
				for animal in menu_animals:
					if animal.collides(event.pos):
						selected_animal = animal
						menu_position = animal.center
						break
				for animal in canvas_animals:
					if animal.collides(event.pos):
						selected_animal = animal
						menu_position = None
						break
			# Remove an animal
			elif event.button == RIGHT_BUTTON:
				for animal in canvas_animals:
					if animal.collides(event.pos):
						canvas_animals.remove(animal)
		elif event.type == pygame.MOUSEMOTION:
			# If left button is down and there is a selected animal
			if event.buttons[0] and selected_animal:
				selected_animal.center = event.pos
		elif event.type == pygame.MOUSEBUTTONUP:
			if event.button == LEFT_BUTTON:
				# If animal was on menu transfer it to canvas and create new animal
				if selected_animal is not None and selected_animal in menu_animals:
					# Transfer animal
					menu_animals.remove(selected_animal)
					canvas_animals.append(selected_animal)
					
					# Add new entry to menu
					new_animal = selected_animal.copy(menu_position)
					new_animal.id = next_id
					next_id += 1
					menu_animals.append(new_animal)
				selected_animal = None
				menu_position = None
				server.data = convert_to_data(canvas_animals)






