import socket
import threading

class Server(threading.Thread):
	def __init__(self, host, port):	
		threading.Thread.__init__(self)
		
		self.host = host
		self.port = port
		self.stop = False
		self.stopped = False
		self.data = '[]'
		
	def run(self):
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.bind((self.host, self.port))
		s.listen(1)
		conn, addr = s.accept()
		print 'Connected by', addr
	
		while not self.stop:
			# Send data
			data = conn.recv(1024)
			conn.send(self.data)
		conn.close()
		self.stopped = True
		
class Client(threading.Thread):
	def __init__(self, host, port, callback):
		threading.Thread.__init__(self)
		
		self.host = host
		self.port = port
		self.callback = callback
		self.stop = False
		self.stopped = False

	def run(self):
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((self.host, self.port))
		
		while not self.stop:
			# Update info
			s.send('d')
			data = s.recv(1024)
			self.callback(data)
		
