import pygame

class SoundPlayer(object):
	def __init__(self, sound):
		self.sound = sound
		
	def play(self):
		pygame.mixer.music.load(self.sound)
		pygame.mixer.music.play()
		
def init_animals():
	return {"pig":   [pygame.image.load("imgs/pig-small.png"), \
					 pygame.image.load("imgs/pig-small-selected.png"), \
					 SoundPlayer("snd/pig.mp3")],
			"goat":  [pygame.image.load("imgs/goat-small.png"), \
					 pygame.image.load("imgs/goat-small-selected.png"), \
					 SoundPlayer("snd/goat.mp3")],
			"sheep": [pygame.image.load("imgs/sheep-small.png"), \
					 pygame.image.load("imgs/sheep-small-selected.png"), \
					 SoundPlayer("snd/sheep.mp3")],
			"cow":   [pygame.image.load("imgs/cow-small.png"), \
					 pygame.image.load("imgs/cow-small-selected.png"), \
					 SoundPlayer("snd/cow.mp3")]}
