import math
import pygame

class Stimulus(object):
	def __init__(self, center, selection_callback, selection=15, max_distance=100):
		self.center = center
		self.max_distance = max_distance
		self.max_animation = 10
		self.count = 0
		self.animation_count = 0
		self.selection = selection
		self.selection_callback = selection_callback

	def collides(self, point):
		dist = math.sqrt((self.center[0]-point[0])**2 + (self.center[1]-point[1])**2)
		return dist < self.max_distance

	def gaze_collision(self, gaze):
		if self.collides(gaze):
			self.count += 1
			if self.count == self.selection:
				self.trigger_selection()
				self.count = 0
		else:
			self.count = 0
			
	def trigger_selection(self):
		self.animation_count = self.max_animation
		self.selection_callback()
		
class CalibrationConfirmation(Stimulus):
	def __init__(self, center, selection_callback):
		super(CalibrationConfirmation, self).__init__(center, selection_callback)
	
	def draw(self, screen):
		pygame.draw.circle(screen, (255, 255, 255), self.center, 20, 0)
		pygame.draw.circle(screen, (0, 0, 0), self.center, 2, 0)

class Image(Stimulus):
	def __init__(self, center, selection_callback, imgs, max_distance=100, flag=None):
		super(Image, self).__init__(center, selection_callback, selection=30, max_distance=max_distance)
		self.imgs = imgs
		self.flag = flag
		
	def calc_at(self, img):
		rect = img.get_rect()
		return [self.center[0]-rect[2]/2, self.center[1]-rect[3]/2]
		
	def draw(self, screen):
		if self.count:
			screen.blit(self.imgs[1], self.calc_at(self.imgs[1]))
			self.animation_count -= 1
		else:
			screen.blit(self.imgs[0], self.calc_at(self.imgs[0]))

	def copy(self, center=None):
		if center is None:
			center = self.center
		return Image(center, self.selection_callback, self.imgs, self.max_distance, self.flag)
			
