import pygame
import sys
import math
import ConfigParser
import json

from stimulus import CalibrationConfirmation, Image
from socketCommunication import Client
from animals import init_animals
sys.path.append('../')
from haythamCalibrateToMonitor import DataClient, send_calibration_point

# Read config file
config = ConfigParser.RawConfigParser()
config.read('config.txt')
HOST = config.get('CONFIG', 'host')
PORT = config.getint('CONFIG', 'port')
minX, maxX = config.getint('CONFIG', 'minX'), config.getint('CONFIG', 'maxX')
minY, maxY = config.getint('CONFIG', 'minY'), config.getint('CONFIG', 'maxY')
width, height = config.getint('CONFIG', 'width'), config.getint('CONFIG', 'height')
app_port = config.getint('CONFIG', 'app_port')

# Init pygame and window
pygame.init()
screen = pygame.display.set_mode((width, height))

# Define callback for calibration validation points
def next_calib_confirm():
	global cur_calib_confirm
	cur_calib_confirm += 1

# Init calibration positions
cur_calib_confirm = 0
calibPoint = 0
calibPositions = [(minX, minY), (maxX, minY), (minX, maxY), (maxX, maxY)]
calibConfirm = []
for position in calibPositions:
	calibConfirm.append(CalibrationConfirmation(position, next_calib_confirm))

# Init client
client = DataClient(HOST)
client.start()

# Init animals
animal_data = init_animals()
animals = {}

# Function that checks if server sent updated information
def check_new_data(json_data):
	data = json.loads(json_data)
	for ani_data in data:
		ani_id = ani_data["id"]
		ani_pos = ani_data["pos"]
		ani_type = ani_data["type"]
		if ani_id in animals:
			animals[ani_id].center = ani_pos
		else:
			animals[ani_id] = Image(ani_pos, animal_data[ani_type][2].play, animal_data[ani_type][0:2])

# Connect to server
sock_client = Client('localhost', app_port, check_new_data)
sock_client.start()

# Game loop
validated = False
while True:
	screen.fill((0,0,0))

	# If still calibrating, draw calibration target
	if calibPoint < 4:
		pygame.draw.circle(screen, (255, 255, 255), calibPositions[calibPoint], 20, 0)
		pygame.draw.circle(screen, (0, 0, 0), calibPositions[calibPoint], 2, 0)
	# Else draw current gaze position
	else:
		data = client.latest_scene_data
		if not math.isnan(data[0]) and not math.isnan(data[1]):
			gaze = (int(data[0]), int(data[1]))
			# If validating calibration
			if cur_calib_confirm < len(calibConfirm):
				pygame.draw.circle(screen, (255, 0, 0), gaze, 10, 0)
				calibConfirm[cur_calib_confirm].draw(screen)
				calibConfirm[cur_calib_confirm].gaze_collision(gaze)
			# Else interact
			else:
				keys = animals.keys()
				for ani_id in keys:
					animals[ani_id].draw(screen)
					animals[ani_id].gaze_collision(gaze)

	# Draw on screen
	pygame.display.update()

	# Handle events
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			client.quit = True
			sock_client.stop = True
			while not sock_client.stopped:
				pass
			pygame.quit()
			sys.exit()
		elif event.type == pygame.KEYDOWN:
			# Add calibration point
			if event.key == pygame.K_RETURN and calibPoint < 4:
				feature = client.filtered_feature()
				if not math.isnan(feature[0]) and not math.isnan(feature[1]):
					send_calibration_point(feature, 
										   calibPositions[calibPoint], 
										   HOST, PORT)
					client.reset_buffer()
					calibPoint += 1
			elif event.key == pygame.K_c:
				calibPoint = 0
				cur_calib_confirm = 0
	

