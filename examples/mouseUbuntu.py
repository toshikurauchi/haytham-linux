import sys, time
import subprocess

try: 
	from Xlib import X, display
	canMoveMouse = True
	disp = display.Display() 
	root = disp.screen().root
except:
	print "Xlib nao instalado"
	sys.exit(1)
	
class Mouse(object):
	def move(self, to):
		root.warp_pointer(to[0], to[1])
		disp.sync()

if __name__ == "__main__":
	mouse = Mouse()
	for i in range(50):
		mouse.move((i*10, i*10))
		time.sleep(0.1)

class Resolution(object):
	def __init__(self, width, height):
		self.width = int(width)
		self.height = int(height)

class Screen(object):
	def __init__(self):
		self.resolutions = []
		# This part was extracted from: http://stackoverflow.com/questions/8705814/get-display-count-and-resolution-for-each-display-in-python-without-xrandr
		output = subprocess.Popen('xrandr | grep "\*" | cut -d" " -f4',shell=True, stdout=subprocess.PIPE).communicate()[0]

		displays = output.strip().split('\n')
		for display in displays:
			values = display.split('x')
			width = values[0]
			height = values[1]
			self.resolutions.append(Resolution(width, height))
