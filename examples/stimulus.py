import math
import pyglet

class Stimulus(object):
	def __init__(self, center, selection_callback, selection=15, max_distance=100):
		self.center = center
		self.max_distance = max_distance
		self.max_animation = 10
		self.count = 0
		self.animation_count = 0
		self.selection = selection
		self.selection_callback = selection_callback

	def collides(self, point):
		dist = math.sqrt((self.center[0]-point[0])**2 + (self.center[1]-point[1])**2)
		return dist < self.max_distance

	def gaze_collision(self, gaze):
		if self.collides(gaze):
			self.count += 1
			if self.count == self.selection:
				self.trigger_selection()
				self.count = 0
		else:
			self.count = 0
			
	def trigger_selection(self):
		self.animation_count = self.max_animation
		self.selection_callback()

def create_vertices(center, radius, n_points, color):
	cx = center[0]
	cy = center[1]
	vertices = [cx, cy]
	step = 2*math.pi/n_points
	indices = [0]
	for i in range(n_points):
		indices.append(i + 1)
		if i % 2 == 1:
			indices.append(0)
		vertices.append(int(radius*math.cos(step*i) + cx))
		vertices.append(int(radius*math.sin(step*i) + cy))
	indices.append(1)
	return ('v2i', tuple(vertices)), ('c4B', color*(n_points + 1)), indices
		
class Circle(object):
	def __init__(self, center, radius):
		self.pts = 20
		red = (255, 0, 0, 255)
		self.verts, self.color, self.idx = create_vertices(center, radius, self.pts, red)
		
	def draw(self):
		pyglet.graphics.draw_indexed(self.pts + 1, pyglet.gl.GL_TRIANGLE_STRIP, 
									 self.idx, self.color, self.verts)
		
class CalibrationPoint(Stimulus):
	def __init__(self, center, selection_callback):
		super(CalibrationPoint, self).__init__(center, selection_callback)
		self.out_pt = 20
		self.inn_pt = 10
		wh = (255, 255, 255, 255)
		bl = (0, 0, 0, 255)
		self.outer, self.out_color, self.out_idx = create_vertices(center, 20, self.out_pt, wh)
		self.inner, self.inn_color, self.inn_idx = create_vertices(center, 4, self.inn_pt, bl)
	
	def draw(self):
		pyglet.graphics.draw_indexed(self.out_pt + 1, pyglet.gl.GL_TRIANGLE_STRIP, 
									 self.out_idx, self.out_color, self.outer)
		pyglet.graphics.draw_indexed(self.inn_pt + 1, pyglet.gl.GL_TRIANGLE_STRIP, 
									 self.inn_idx, self.inn_color, self.inner)

