/****************************************************************************
** Meta object code from reading C++ file 'SceneTab.h'
**
** Created: Thu Sep 19 18:00:53 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Ui/SceneTab.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SceneTab.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_haytham__SceneTab[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      29,   19,   18,   18, 0x0a,
      69,   55,   18,   18, 0x0a,
      94,   18,   18,   18, 0x0a,
     119,   18,   18,   18, 0x0a,
     156,  145,   18,   18, 0x0a,
     197,  179,   18,   18, 0x0a,
     233,  227,   18,   18, 0x0a,
     276,  266,   18,   18, 0x0a,
     298,   18,   18,   18, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_haytham__SceneTab[] = {
    "haytham::SceneTab\0\0threshold\0"
    "updateSceneThreshold(int)\0screenMinSize\0"
    "updateScreenMinSize(int)\0"
    "screenMinSliderPressed()\0"
    "screenMinSliderReleased()\0showScreen\0"
    "updateShowScreen(bool)\0showFilteredScene\0"
    "updateShowFilteredScene(bool)\0index\0"
    "updateScreenDetectionMethod(int)\0"
    "undistort\0updateUndistort(bool)\0"
    "calibrateCamera()\0"
};

void haytham::SceneTab::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SceneTab *_t = static_cast<SceneTab *>(_o);
        switch (_id) {
        case 0: _t->updateSceneThreshold((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->updateScreenMinSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->screenMinSliderPressed(); break;
        case 3: _t->screenMinSliderReleased(); break;
        case 4: _t->updateShowScreen((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->updateShowFilteredScene((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->updateScreenDetectionMethod((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->updateUndistort((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->calibrateCamera(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData haytham::SceneTab::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject haytham::SceneTab::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_haytham__SceneTab,
      qt_meta_data_haytham__SceneTab, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &haytham::SceneTab::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *haytham::SceneTab::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *haytham::SceneTab::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_haytham__SceneTab))
        return static_cast<void*>(const_cast< SceneTab*>(this));
    return QObject::qt_metacast(_clname);
}

int haytham::SceneTab::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}
static const uint qt_meta_data_haytham__VisualizationCheckHandler[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      44,   36,   35,   35, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_haytham__VisualizationCheckHandler[] = {
    "haytham::VisualizationCheckHandler\0\0"
    "checked\0visualizationCheck(bool)\0"
};

void haytham::VisualizationCheckHandler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        VisualizationCheckHandler *_t = static_cast<VisualizationCheckHandler *>(_o);
        switch (_id) {
        case 0: _t->visualizationCheck((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData haytham::VisualizationCheckHandler::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject haytham::VisualizationCheckHandler::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_haytham__VisualizationCheckHandler,
      qt_meta_data_haytham__VisualizationCheckHandler, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &haytham::VisualizationCheckHandler::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *haytham::VisualizationCheckHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *haytham::VisualizationCheckHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_haytham__VisualizationCheckHandler))
        return static_cast<void*>(const_cast< VisualizationCheckHandler*>(this));
    return QObject::qt_metacast(_clname);
}

int haytham::VisualizationCheckHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
