/*
 * EyeBlob_test.cpp
 *
 *  Created on: 01/08/2012
 *      Author: Andrew Kurauchi
 */

#include <gtest/gtest.h>
#include <vector>

#include "EyeBlob.h"
#include "Point.h"
#include "TestUtils.h"

namespace haytham {

	TEST(EyeBlob, FindBlobsOnROI) {
		cv::Mat before = cv::imread("images/glint_removed_thresh_damaged.jpg", 0);
		before = before(cv::Rect(2, 2, before.cols - 4, before.rows - 4));
		cv::threshold(before, before, 100, 255, CV_THRESH_BINARY);
		cv::Mat blobsImg = cv::Mat::zeros(before.size(), before.type());
		cv::Mat hull = blobsImg.clone();
		EyeBlob eyeBlob(before, 40, 60, 40, 60, 0.5, 5);

		std::vector<Blob> blobs = eyeBlob.getFilteredBlobs();
		size_t idx = 0;
		for(std::vector<Blob>::iterator it = blobs.begin(); it != blobs.end(); it++, idx++) {
			std::vector<std::vector<cv::Point> > lineStrip;
			lineStrip.push_back(it->getPoints());
			lineStrip.push_back(it->getHull());
			cv::drawContours(blobsImg, lineStrip, 0, cv::Scalar::all(255));
			cv::drawContours(hull, lineStrip, 1, cv::Scalar::all(255));
		}

		ASSERT_EQ(1, blobs.size());
		// Uncomment this lines to see the result
		// TestUtils::showBeforeAfterImages("Find blobs on ROI", before, blobsImg);
		// TestUtils::showImage("Convex Hull", 300, 250, hull);
		// cvWaitKey(0);
	}

	TEST(EyeBlob, SelectClosestBlobFromPoint) {
		cv::Mat img = cv::imread("images/six_circles.jpg", 0);
		EyeBlob eyeBlob(img, 80, 140, 80, 140, 0.5, 10);

		std::vector<Blob> blobs = eyeBlob.getFilteredBlobs();

        eyeBlob.selectClosestBlobFromPoint(Point(90, 90));
		Blob *closestBlob = eyeBlob.getSelectedBlob();
		ASSERT_EQ(6, blobs.size());
		ASSERT_LE(90, closestBlob->getCenterOfGravity().x);
		ASSERT_GE(110, closestBlob->getCenterOfGravity().x);
		ASSERT_LE(100, closestBlob->getCenterOfGravity().y);
		ASSERT_GE(120, closestBlob->getCenterOfGravity().y);
	}

	TEST(EyeBlob, ReturnZeroIfTherIsNoBlob) {
		cv::Mat img = cv::imread("images/six_circles.jpg", 0);
		EyeBlob eyeBlob(img, 70, 80, 70, 80, 0.5, 10);

		std::vector<Blob> blobs = eyeBlob.getFilteredBlobs();

        eyeBlob.selectClosestBlobFromPoint(Point(90, 90));
		Blob *closestBlob = eyeBlob.getSelectedBlob();
		ASSERT_EQ(0, blobs.size());
		ASSERT_EQ(0, closestBlob);
	}

} /* namespace haytham */
