/*
 * IntersectLine3d_test.cpp
 *
 *  Created on: 17/10/2013
 *      Author: Andrew Kurauchi
 */

#include <gtest/gtest.h>
#include <cv.h>

#include "../Common/IntersectLine3d.h"

namespace haytham {

    TEST(IntersectLine3d, intersect2linesAtOrigin) {
        cv::Mat PA(2, 3, CV_32FC1);
        cv::Mat PB(2, 3, CV_32FC1);

        PA.at<float>(0, 0) = -13;
        PA.at<float>(0, 1) = -43;
        PA.at<float>(0, 2) = -54;
        PA.at<float>(1, 0) = -57;
        PA.at<float>(1, 1) = 35;
        PA.at<float>(1, 2) = 68;

        PB.at<float>(0, 0) = 0;
        PB.at<float>(0, 1) = 0;
        PB.at<float>(0, 2) = 0;
        PB.at<float>(1, 0) = 0;
        PB.at<float>(1, 1) = 0;
        PB.at<float>(1, 2) = 0;

        IntersectLine3d intersectionFinder;
        cv::Mat intersection = intersectionFinder.intersectLines(PA, PB);

        ASSERT_FALSE(intersection.empty());
        ASSERT_NEAR(0, intersection.at<float>(0, 0), 1e-4);
        ASSERT_NEAR(0, intersection.at<float>(0, 1), 1e-4);
        ASSERT_NEAR(0, intersection.at<float>(0, 2), 1e-4);
    }

    TEST(IntersectLine3d, intersect2lines) {
        cv::Mat PA(2, 3, CV_32FC1);
        cv::Mat PB(2, 3, CV_32FC1);

        PA.at<float>(0, 0) = -1;
        PA.at<float>(0, 1) = -1;
        PA.at<float>(0, 2) = -1;
        PA.at<float>(1, 0) = -1;
        PA.at<float>(1, 1) = -1;
        PA.at<float>(1, 2) = 1;

        PB.at<float>(0, 0) = 3;
        PB.at<float>(0, 1) = 3;
        PB.at<float>(0, 2) = 3;
        PB.at<float>(1, 0) = 1;
        PB.at<float>(1, 1) = 1;
        PB.at<float>(1, 2) = -1;

        IntersectLine3d intersectionFinder;
        cv::Mat intersection = intersectionFinder.intersectLines(PA, PB);

        ASSERT_FALSE(intersection.empty());
        ASSERT_NEAR(0, intersection.at<float>(0, 0), 1e-4);
        ASSERT_NEAR(0, intersection.at<float>(0, 1), 1e-4);
        ASSERT_NEAR(0, intersection.at<float>(0, 2), 1e-4);
    }

    TEST(IntersectLine3d, intersect4linesAt1) {
        cv::Mat PA(4, 3, CV_32FC1);
        cv::Mat PB(4, 3, CV_32FC1);

        PA.at<float>(0, 0) = 0;
        PA.at<float>(0, 1) = 0;
        PA.at<float>(0, 2) = 0;
        PA.at<float>(1, 0) = 0;
        PA.at<float>(1, 1) = 0;
        PA.at<float>(1, 2) = 2;
        PA.at<float>(2, 0) = 0;
        PA.at<float>(2, 1) = 2;
        PA.at<float>(2, 2) = 0;
        PA.at<float>(3, 0) = 2;
        PA.at<float>(3, 1) = 0;
        PA.at<float>(3, 2) = 0;

        PB.at<float>(0, 0) = 2;
        PB.at<float>(0, 1) = 2;
        PB.at<float>(0, 2) = 2;
        PB.at<float>(1, 0) = 2;
        PB.at<float>(1, 1) = 2;
        PB.at<float>(1, 2) = 0;
        PB.at<float>(2, 0) = 2;
        PB.at<float>(2, 1) = 0;
        PB.at<float>(2, 2) = 2;
        PB.at<float>(3, 0) = 0;
        PB.at<float>(3, 1) = 2;
        PB.at<float>(3, 2) = 2;

        IntersectLine3d intersectionFinder;
        cv::Mat intersection = intersectionFinder.intersectLines(PA, PB);

        ASSERT_FALSE(intersection.empty());
        ASSERT_NEAR(1, intersection.at<float>(0, 0), 1e-4);
        ASSERT_NEAR(1, intersection.at<float>(0, 1), 1e-4);
        ASSERT_NEAR(1, intersection.at<float>(0, 2), 1e-4);
    }

    TEST(IntersectLine3d, intersectPlaneAtOrigin) {
        cv::Mat line(3, 2, CV_32FC1);
        cv::Mat plane(3, 3, CV_32FC1);

        line.at<float>(0, 0) = 1;
        line.at<float>(1, 0) = 1;
        line.at<float>(2, 0) = 1;
        line.at<float>(0, 1) = -1;
        line.at<float>(1, 1) = -1;
        line.at<float>(2, 1) = -1;

        plane.at<float>(0, 0) = -1;
        plane.at<float>(1, 0) = 1;
        plane.at<float>(2, 0) = 1;
        plane.at<float>(0, 1) = -1;
        plane.at<float>(1, 1) = -1;
        plane.at<float>(2, 1) = 1;
        plane.at<float>(0, 2) = 1;
        plane.at<float>(1, 2) = -1;
        plane.at<float>(2, 2) = -1;

        IntersectLine3d intersectionFinder;
        cv::Mat intersection = intersectionFinder.intersectPlane(line, plane);

        ASSERT_FALSE(intersection.empty());
        ASSERT_NEAR(0, intersection.at<float>(0, 0), 1e-4);
        ASSERT_NEAR(0, intersection.at<float>(0, 1), 1e-4);
        ASSERT_NEAR(0, intersection.at<float>(0, 2), 1e-4);
    }

    TEST(IntersectLine3d, intersectPlaneAt1) {
        cv::Mat line(3, 2, CV_32FC1);
        cv::Mat plane(3, 3, CV_32FC1);

        line.at<float>(0, 0) = 2;
        line.at<float>(1, 0) = 2;
        line.at<float>(2, 0) = 2;
        line.at<float>(0, 1) = 0;
        line.at<float>(1, 1) = 0;
        line.at<float>(2, 1) = 0;

        plane.at<float>(0, 0) = 0;
        plane.at<float>(1, 0) = 2;
        plane.at<float>(2, 0) = 2;
        plane.at<float>(0, 1) = 0;
        plane.at<float>(1, 1) = 0;
        plane.at<float>(2, 1) = 2;
        plane.at<float>(0, 2) = 2;
        plane.at<float>(1, 2) = 0;
        plane.at<float>(2, 2) = 0;

        IntersectLine3d intersectionFinder;
        cv::Mat intersection = intersectionFinder.intersectPlane(line, plane);

        ASSERT_FALSE(intersection.empty());
        ASSERT_NEAR(1, intersection.at<float>(0, 0), 1e-4);
        ASSERT_NEAR(1, intersection.at<float>(0, 1), 1e-4);
        ASSERT_NEAR(1, intersection.at<float>(0, 2), 1e-4);
    }

} /* namespace haytham */



