/*
 * MockMETState.h
 *
 *  Created on: 14/08/2012
 *      Author: Andrew Kurauchi
 */

#ifndef MOCKMETSTATE_H_
#define MOCKMETSTATE_H_

#include "../METState.h"

namespace haytham {

class MockMETState : public METState {
public:
	MockMETState();

	void setEyeImageOriginal(cv::Mat &eyeImageOriginal);
	void setEyeImageForShow(cv::Mat &eyeImageForShow);
	void setDilateErode(bool dilateErode);
	void setMinPupilScale(double minPupilScale);
	void setMaxPupilScale(double maxPupilScale);
	void setShowPupil(bool showPupil);
	void setPupilAdaptive(bool pupilAdaptive);
	void setRemoveGlint(bool removeGlint);
	void setGlintThreshold(int glintThreshold);
	void setIrisDiameter(int irisDiameter);
    void setShowIris(bool showIris);
    void setSceneThreshold(int sceneThreshold);
    void setScreenMinSize(float screenMinSize);
};

} /* namespace haytham */
#endif /* MOCKMETSTATE_H_ */
