/*
 * ShapeChecker_test.cpp
 *
 *  Created on: 02/08/2012
 *      Author: Andrew Kurauchi
 */

#include <gtest/gtest.h>
#include <vector>
#include <cv.h>

#include "ShapeChecker.h"
#include "EyeBlob.h"

namespace haytham {

	TEST(ShapeChecker, IsntCircleWithLessThan8Points) {
		ShapeChecker checker(0.5, 0.15);
		cv::Point pts[7] = {cv::Point(0,0), cv::Point(1,1), cv::Point(2,2),
							cv::Point(3,3), cv::Point(4,4), cv::Point(5,5),
							cv::Point(6,6)};
		std::vector<cv::Point> points(pts, pts + 7);
		Blob blob(points);
		ASSERT_FALSE(checker.isCircle(blob));
	}

	TEST(ShapeChecker, LineIsntCircle) {
		ShapeChecker checker(0.5, 0.15);
		cv::Point pts[9] = {cv::Point(0,0), cv::Point(5,5), cv::Point(10,10),
							cv::Point(15,15), cv::Point(20,20), cv::Point(25,25),
							cv::Point(30,30), cv::Point(35,35), cv::Point(40,40)};
		std::vector<cv::Point> points(pts, pts + 9);
		Blob blob(points);
		ASSERT_FALSE(checker.isCircle(blob));
	}

	TEST(ShapeChecker, RectangleIsntCircle) {
		ShapeChecker checker(0.5, 0.15);
		cv::Point pts[8] = {cv::Point(0,0), cv::Point(0,10), cv::Point(0,20),
							cv::Point(20,20), cv::Point(40,20), cv::Point(40,10),
							cv::Point(40,0), cv::Point(20,0)};
		std::vector<cv::Point> points(pts, pts + 8);
		Blob blob(points);
		ASSERT_FALSE(checker.isCircle(blob));
	}

	TEST(ShapeChecker, CircleIsCircle) {
		ShapeChecker checker(0.5, 0.15);
		// Points in a circle with radius=5 and center=(5,5)
		cv::Point pts[8] = {cv::Point(0,5), cv::Point(5,0), cv::Point(10,5),
							cv::Point(5,10), cv::Point(2.5,9.33012702), cv::Point(2.5,0.66987298),
							cv::Point(9.33012702,2.5), cv::Point(0.66987298,2.5)};
		std::vector<cv::Point> points(pts, pts + 8);
		Blob blob(points);
		ASSERT_TRUE(checker.isCircle(blob));
	}

} /* namespace haytham */
