/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Thu Sep 19 17:55:19 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QFormLayout>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QScrollArea>
#include <QtGui/QSlider>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "CalibrationPointsLabel.h"
#include "ImageLabel.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *camera;
    QFormLayout *formLayout_8;
    QGroupBox *groupBox_8;
    QGridLayout *gridLayout_10;
    QWidget *widget_9;
    QFormLayout *formLayout_9;
    QLabel *label_9;
    QComboBox *eye_camera;
    QLabel *label_10;
    QComboBox *eye_resolution;
    QWidget *widget_10;
    QHBoxLayout *horizontalLayout;
    QPushButton *settings_eye_camera;
    QPushButton *start_stop_eye_camera;
    QGroupBox *groupBox_9;
    QGridLayout *gridLayout_12;
    QWidget *widget_11;
    QFormLayout *formLayout_11;
    QLabel *label_13;
    QComboBox *scene_camera;
    QLabel *label_14;
    QComboBox *scene_resolution;
    QWidget *widget_12;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *settings_scene_camera;
    QPushButton *start_stop_scene_camera;
    QGroupBox *groupBox_10;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *start_both_cameras;
    QWidget *eye;
    QFormLayout *formLayout_5;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_9;
    QWidget *widget_5;
    QHBoxLayout *horizontalLayout_4;
    QCheckBox *show_pupil;
    QWidget *widget_7;
    QFormLayout *formLayout_6;
    QLabel *label_7;
    QSlider *pupil_threshold;
    QLabel *label_8;
    QSlider *pupil_sensitivity;
    QWidget *widget_8;
    QFormLayout *formLayout_7;
    QCheckBox *fill_gaps;
    QCheckBox *remove_glint;
    QWidget *widget_6;
    QHBoxLayout *horizontalLayout_5;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_2;
    QGroupBox *groupBox_7;
    QGridLayout *gridLayout_11;
    QWidget *widget_13;
    QHBoxLayout *horizontalLayout_8;
    QCheckBox *show_glint;
    QWidget *widget_15;
    QFormLayout *formLayout_10;
    QLabel *label_11;
    QSlider *glint_threshold;
    QLabel *label_12;
    QSlider *glint_sensitivity;
    QWidget *widget_14;
    QHBoxLayout *horizontalLayout_9;
    QRadioButton *radioButton_7;
    QRadioButton *radioButton_6;
    QGroupBox *groupBox_5;
    QFormLayout *formLayout_13;
    QLabel *label_6;
    QSlider *iris_size;
    QLabel *label_4;
    QCheckBox *show_iris_size;
    QComboBox *eye_detector;
    QWidget *scene;
    QFormLayout *formLayout_2;
    QGroupBox *screen_detection;
    QGridLayout *gridLayout_4;
    QWidget *widget_4;
    QFormLayout *formLayout_3;
    QCheckBox *show_screen;
    QCheckBox *show_filtered_image;
    QLabel *label_3;
    QSlider *scene_threshold_slider;
    QLabel *label_5;
    QSlider *screen_min_size;
    QComboBox *screen_detectio_method;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_5;
    QCheckBox *undistort;
    QPushButton *calibrate_camera;
    QGroupBox *visualization_group;
    QWidget *calibration;
    QFormLayout *formLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_3;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout_2;
    QRadioButton *pupil_center;
    QRadioButton *pupil_glint_vector;
    QCheckBox *gaze_smoothing;
    QPushButton *start_calibrating;
    QComboBox *calibration_method;
    haytham::CalibrationPointsLabel *remaining_calibration_points;
    QGroupBox *groupBox_15;
    QFormLayout *formLayout_14;
    QLabel *label_15;
    QSlider *calibPointDetectThresh;
    QWidget *data;
    QFormLayout *formLayout_12;
    QGroupBox *groupBox_11;
    QVBoxLayout *verticalLayout;
    QCheckBox *record_eye_video;
    QCheckBox *record_scene_video;
    QCheckBox *record_eye_data;
    QPushButton *start_stop_recording;
    QGroupBox *groupBox_12;
    QGridLayout *gridLayout_13;
    QLabel *label_16;
    QFrame *frame;
    QLabel *label_17;
    QFrame *frame_2;
    QLabel *label_18;
    QFrame *frame_3;
    QWidget *network;
    QFormLayout *formLayout_4;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_6;
    QScrollArea *scrollArea_2;
    QWidget *scrollAreaWidgetContents_2;
    QVBoxLayout *verticalLayout_5;
    QPlainTextEdit *network_messages;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_7;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_5;
    QRadioButton *radioButton;
    QWidget *widget;
    QGridLayout *gridLayout_2;
    QComboBox *comboBox_2;
    QComboBox *comboBox;
    QLabel *label;
    QLabel *label_2;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox_13;
    QVBoxLayout *verticalLayout_3;
    haytham::ImageLabel *eye_image;
    QGroupBox *groupBox_14;
    QVBoxLayout *verticalLayout_4;
    haytham::ImageLabel *scene_image;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1298, 963);
        MainWindow->setStyleSheet(QString::fromUtf8("QGroupBox {\n"
"	border: 1px solid lightgray;\n"
"	border-radius: 5px;\n"
"	margin-top: 1ex;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"	subcontrol-origin: margin;\n"
"	subcontrol-position: top left;\n"
"	margin-left: 10px;\n"
"}"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        tabWidget->setMaximumSize(QSize(16777215, 16777215));
        camera = new QWidget();
        camera->setObjectName(QString::fromUtf8("camera"));
        formLayout_8 = new QFormLayout(camera);
        formLayout_8->setSpacing(6);
        formLayout_8->setContentsMargins(11, 11, 11, 11);
        formLayout_8->setObjectName(QString::fromUtf8("formLayout_8"));
        groupBox_8 = new QGroupBox(camera);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        gridLayout_10 = new QGridLayout(groupBox_8);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        widget_9 = new QWidget(groupBox_8);
        widget_9->setObjectName(QString::fromUtf8("widget_9"));
        formLayout_9 = new QFormLayout(widget_9);
        formLayout_9->setSpacing(6);
        formLayout_9->setContentsMargins(11, 11, 11, 11);
        formLayout_9->setObjectName(QString::fromUtf8("formLayout_9"));
        formLayout_9->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_9 = new QLabel(widget_9);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        formLayout_9->setWidget(0, QFormLayout::LabelRole, label_9);

        eye_camera = new QComboBox(widget_9);
        eye_camera->setObjectName(QString::fromUtf8("eye_camera"));

        formLayout_9->setWidget(0, QFormLayout::FieldRole, eye_camera);

        label_10 = new QLabel(widget_9);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        formLayout_9->setWidget(1, QFormLayout::LabelRole, label_10);

        eye_resolution = new QComboBox(widget_9);
        eye_resolution->setObjectName(QString::fromUtf8("eye_resolution"));

        formLayout_9->setWidget(1, QFormLayout::FieldRole, eye_resolution);


        gridLayout_10->addWidget(widget_9, 0, 0, 1, 1);

        widget_10 = new QWidget(groupBox_8);
        widget_10->setObjectName(QString::fromUtf8("widget_10"));
        horizontalLayout = new QHBoxLayout(widget_10);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        settings_eye_camera = new QPushButton(widget_10);
        settings_eye_camera->setObjectName(QString::fromUtf8("settings_eye_camera"));

        horizontalLayout->addWidget(settings_eye_camera);

        start_stop_eye_camera = new QPushButton(widget_10);
        start_stop_eye_camera->setObjectName(QString::fromUtf8("start_stop_eye_camera"));

        horizontalLayout->addWidget(start_stop_eye_camera);


        gridLayout_10->addWidget(widget_10, 1, 0, 1, 1);


        formLayout_8->setWidget(0, QFormLayout::SpanningRole, groupBox_8);

        groupBox_9 = new QGroupBox(camera);
        groupBox_9->setObjectName(QString::fromUtf8("groupBox_9"));
        gridLayout_12 = new QGridLayout(groupBox_9);
        gridLayout_12->setSpacing(6);
        gridLayout_12->setContentsMargins(11, 11, 11, 11);
        gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
        widget_11 = new QWidget(groupBox_9);
        widget_11->setObjectName(QString::fromUtf8("widget_11"));
        formLayout_11 = new QFormLayout(widget_11);
        formLayout_11->setSpacing(6);
        formLayout_11->setContentsMargins(11, 11, 11, 11);
        formLayout_11->setObjectName(QString::fromUtf8("formLayout_11"));
        label_13 = new QLabel(widget_11);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        formLayout_11->setWidget(0, QFormLayout::LabelRole, label_13);

        scene_camera = new QComboBox(widget_11);
        scene_camera->setObjectName(QString::fromUtf8("scene_camera"));

        formLayout_11->setWidget(0, QFormLayout::FieldRole, scene_camera);

        label_14 = new QLabel(widget_11);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        formLayout_11->setWidget(1, QFormLayout::LabelRole, label_14);

        scene_resolution = new QComboBox(widget_11);
        scene_resolution->setObjectName(QString::fromUtf8("scene_resolution"));

        formLayout_11->setWidget(1, QFormLayout::FieldRole, scene_resolution);


        gridLayout_12->addWidget(widget_11, 0, 0, 1, 1);

        widget_12 = new QWidget(groupBox_9);
        widget_12->setObjectName(QString::fromUtf8("widget_12"));
        horizontalLayout_6 = new QHBoxLayout(widget_12);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        settings_scene_camera = new QPushButton(widget_12);
        settings_scene_camera->setObjectName(QString::fromUtf8("settings_scene_camera"));

        horizontalLayout_6->addWidget(settings_scene_camera);

        start_stop_scene_camera = new QPushButton(widget_12);
        start_stop_scene_camera->setObjectName(QString::fromUtf8("start_stop_scene_camera"));

        horizontalLayout_6->addWidget(start_stop_scene_camera);


        gridLayout_12->addWidget(widget_12, 1, 0, 1, 1);


        formLayout_8->setWidget(1, QFormLayout::SpanningRole, groupBox_9);

        groupBox_10 = new QGroupBox(camera);
        groupBox_10->setObjectName(QString::fromUtf8("groupBox_10"));
        horizontalLayout_7 = new QHBoxLayout(groupBox_10);
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        start_both_cameras = new QPushButton(groupBox_10);
        start_both_cameras->setObjectName(QString::fromUtf8("start_both_cameras"));

        horizontalLayout_7->addWidget(start_both_cameras);


        formLayout_8->setWidget(2, QFormLayout::SpanningRole, groupBox_10);

        tabWidget->addTab(camera, QString());
        eye = new QWidget();
        eye->setObjectName(QString::fromUtf8("eye"));
        formLayout_5 = new QFormLayout(eye);
        formLayout_5->setSpacing(6);
        formLayout_5->setContentsMargins(11, 11, 11, 11);
        formLayout_5->setObjectName(QString::fromUtf8("formLayout_5"));
        formLayout_5->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        groupBox_6 = new QGroupBox(eye);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setCheckable(true);
        gridLayout_9 = new QGridLayout(groupBox_6);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        widget_5 = new QWidget(groupBox_6);
        widget_5->setObjectName(QString::fromUtf8("widget_5"));
        horizontalLayout_4 = new QHBoxLayout(widget_5);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        show_pupil = new QCheckBox(widget_5);
        show_pupil->setObjectName(QString::fromUtf8("show_pupil"));

        horizontalLayout_4->addWidget(show_pupil);


        gridLayout_9->addWidget(widget_5, 0, 0, 1, 1);

        widget_7 = new QWidget(groupBox_6);
        widget_7->setObjectName(QString::fromUtf8("widget_7"));
        formLayout_6 = new QFormLayout(widget_7);
        formLayout_6->setSpacing(6);
        formLayout_6->setContentsMargins(11, 11, 11, 11);
        formLayout_6->setObjectName(QString::fromUtf8("formLayout_6"));
        formLayout_6->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_7 = new QLabel(widget_7);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        formLayout_6->setWidget(0, QFormLayout::LabelRole, label_7);

        pupil_threshold = new QSlider(widget_7);
        pupil_threshold->setObjectName(QString::fromUtf8("pupil_threshold"));
        pupil_threshold->setOrientation(Qt::Horizontal);

        formLayout_6->setWidget(0, QFormLayout::FieldRole, pupil_threshold);

        label_8 = new QLabel(widget_7);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        formLayout_6->setWidget(1, QFormLayout::LabelRole, label_8);

        pupil_sensitivity = new QSlider(widget_7);
        pupil_sensitivity->setObjectName(QString::fromUtf8("pupil_sensitivity"));
        pupil_sensitivity->setOrientation(Qt::Horizontal);

        formLayout_6->setWidget(1, QFormLayout::FieldRole, pupil_sensitivity);


        gridLayout_9->addWidget(widget_7, 2, 0, 1, 1);

        widget_8 = new QWidget(groupBox_6);
        widget_8->setObjectName(QString::fromUtf8("widget_8"));
        formLayout_7 = new QFormLayout(widget_8);
        formLayout_7->setSpacing(6);
        formLayout_7->setContentsMargins(11, 11, 11, 11);
        formLayout_7->setObjectName(QString::fromUtf8("formLayout_7"));
        fill_gaps = new QCheckBox(widget_8);
        fill_gaps->setObjectName(QString::fromUtf8("fill_gaps"));

        formLayout_7->setWidget(0, QFormLayout::LabelRole, fill_gaps);

        remove_glint = new QCheckBox(widget_8);
        remove_glint->setObjectName(QString::fromUtf8("remove_glint"));

        formLayout_7->setWidget(1, QFormLayout::LabelRole, remove_glint);


        gridLayout_9->addWidget(widget_8, 3, 0, 1, 1);

        widget_6 = new QWidget(groupBox_6);
        widget_6->setObjectName(QString::fromUtf8("widget_6"));
        horizontalLayout_5 = new QHBoxLayout(widget_6);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        radioButton_3 = new QRadioButton(widget_6);
        radioButton_3->setObjectName(QString::fromUtf8("radioButton_3"));

        horizontalLayout_5->addWidget(radioButton_3);

        radioButton_2 = new QRadioButton(widget_6);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));

        horizontalLayout_5->addWidget(radioButton_2);


        gridLayout_9->addWidget(widget_6, 1, 0, 1, 1);


        formLayout_5->setWidget(2, QFormLayout::SpanningRole, groupBox_6);

        groupBox_7 = new QGroupBox(eye);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        gridLayout_11 = new QGridLayout(groupBox_7);
        gridLayout_11->setSpacing(6);
        gridLayout_11->setContentsMargins(11, 11, 11, 11);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        widget_13 = new QWidget(groupBox_7);
        widget_13->setObjectName(QString::fromUtf8("widget_13"));
        horizontalLayout_8 = new QHBoxLayout(widget_13);
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        show_glint = new QCheckBox(widget_13);
        show_glint->setObjectName(QString::fromUtf8("show_glint"));

        horizontalLayout_8->addWidget(show_glint);


        gridLayout_11->addWidget(widget_13, 0, 0, 1, 1);

        widget_15 = new QWidget(groupBox_7);
        widget_15->setObjectName(QString::fromUtf8("widget_15"));
        formLayout_10 = new QFormLayout(widget_15);
        formLayout_10->setSpacing(6);
        formLayout_10->setContentsMargins(11, 11, 11, 11);
        formLayout_10->setObjectName(QString::fromUtf8("formLayout_10"));
        label_11 = new QLabel(widget_15);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        formLayout_10->setWidget(0, QFormLayout::LabelRole, label_11);

        glint_threshold = new QSlider(widget_15);
        glint_threshold->setObjectName(QString::fromUtf8("glint_threshold"));
        glint_threshold->setOrientation(Qt::Horizontal);

        formLayout_10->setWidget(0, QFormLayout::FieldRole, glint_threshold);

        label_12 = new QLabel(widget_15);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        formLayout_10->setWidget(1, QFormLayout::LabelRole, label_12);

        glint_sensitivity = new QSlider(widget_15);
        glint_sensitivity->setObjectName(QString::fromUtf8("glint_sensitivity"));
        glint_sensitivity->setOrientation(Qt::Horizontal);

        formLayout_10->setWidget(1, QFormLayout::FieldRole, glint_sensitivity);


        gridLayout_11->addWidget(widget_15, 2, 0, 1, 1);

        widget_14 = new QWidget(groupBox_7);
        widget_14->setObjectName(QString::fromUtf8("widget_14"));
        horizontalLayout_9 = new QHBoxLayout(widget_14);
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        radioButton_7 = new QRadioButton(widget_14);
        radioButton_7->setObjectName(QString::fromUtf8("radioButton_7"));

        horizontalLayout_9->addWidget(radioButton_7);

        radioButton_6 = new QRadioButton(widget_14);
        radioButton_6->setObjectName(QString::fromUtf8("radioButton_6"));

        horizontalLayout_9->addWidget(radioButton_6);


        gridLayout_11->addWidget(widget_14, 1, 0, 1, 1);


        formLayout_5->setWidget(3, QFormLayout::SpanningRole, groupBox_7);

        groupBox_5 = new QGroupBox(eye);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        formLayout_13 = new QFormLayout(groupBox_5);
        formLayout_13->setSpacing(6);
        formLayout_13->setContentsMargins(11, 11, 11, 11);
        formLayout_13->setObjectName(QString::fromUtf8("formLayout_13"));
        label_6 = new QLabel(groupBox_5);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout_13->setWidget(0, QFormLayout::LabelRole, label_6);

        iris_size = new QSlider(groupBox_5);
        iris_size->setObjectName(QString::fromUtf8("iris_size"));
        iris_size->setOrientation(Qt::Horizontal);

        formLayout_13->setWidget(0, QFormLayout::FieldRole, iris_size);

        label_4 = new QLabel(groupBox_5);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout_13->setWidget(1, QFormLayout::LabelRole, label_4);

        show_iris_size = new QCheckBox(groupBox_5);
        show_iris_size->setObjectName(QString::fromUtf8("show_iris_size"));

        formLayout_13->setWidget(1, QFormLayout::FieldRole, show_iris_size);


        formLayout_5->setWidget(1, QFormLayout::SpanningRole, groupBox_5);

        eye_detector = new QComboBox(eye);
        eye_detector->setObjectName(QString::fromUtf8("eye_detector"));

        formLayout_5->setWidget(0, QFormLayout::SpanningRole, eye_detector);

        tabWidget->addTab(eye, QString());
        scene = new QWidget();
        scene->setObjectName(QString::fromUtf8("scene"));
        formLayout_2 = new QFormLayout(scene);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        formLayout_2->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        screen_detection = new QGroupBox(scene);
        screen_detection->setObjectName(QString::fromUtf8("screen_detection"));
        screen_detection->setCheckable(true);
        gridLayout_4 = new QGridLayout(screen_detection);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        widget_4 = new QWidget(screen_detection);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        formLayout_3 = new QFormLayout(widget_4);
        formLayout_3->setSpacing(6);
        formLayout_3->setContentsMargins(11, 11, 11, 11);
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        formLayout_3->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        show_screen = new QCheckBox(widget_4);
        show_screen->setObjectName(QString::fromUtf8("show_screen"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, show_screen);

        show_filtered_image = new QCheckBox(widget_4);
        show_filtered_image->setObjectName(QString::fromUtf8("show_filtered_image"));

        formLayout_3->setWidget(2, QFormLayout::LabelRole, show_filtered_image);

        label_3 = new QLabel(widget_4);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout_3->setWidget(3, QFormLayout::LabelRole, label_3);

        scene_threshold_slider = new QSlider(widget_4);
        scene_threshold_slider->setObjectName(QString::fromUtf8("scene_threshold_slider"));
        scene_threshold_slider->setOrientation(Qt::Horizontal);

        formLayout_3->setWidget(3, QFormLayout::FieldRole, scene_threshold_slider);

        label_5 = new QLabel(widget_4);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout_3->setWidget(4, QFormLayout::LabelRole, label_5);

        screen_min_size = new QSlider(widget_4);
        screen_min_size->setObjectName(QString::fromUtf8("screen_min_size"));
        screen_min_size->setOrientation(Qt::Horizontal);

        formLayout_3->setWidget(4, QFormLayout::FieldRole, screen_min_size);


        gridLayout_4->addWidget(widget_4, 2, 0, 1, 1);

        screen_detectio_method = new QComboBox(screen_detection);
        screen_detectio_method->setObjectName(QString::fromUtf8("screen_detectio_method"));

        gridLayout_4->addWidget(screen_detectio_method, 0, 0, 1, 1);


        formLayout_2->setWidget(0, QFormLayout::SpanningRole, screen_detection);

        groupBox_2 = new QGroupBox(scene);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_5 = new QGridLayout(groupBox_2);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        undistort = new QCheckBox(groupBox_2);
        undistort->setObjectName(QString::fromUtf8("undistort"));

        gridLayout_5->addWidget(undistort, 0, 0, 1, 1);

        calibrate_camera = new QPushButton(groupBox_2);
        calibrate_camera->setObjectName(QString::fromUtf8("calibrate_camera"));

        gridLayout_5->addWidget(calibrate_camera, 0, 1, 1, 1);


        formLayout_2->setWidget(1, QFormLayout::FieldRole, groupBox_2);

        visualization_group = new QGroupBox(scene);
        visualization_group->setObjectName(QString::fromUtf8("visualization_group"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, visualization_group);

        tabWidget->addTab(scene, QString());
        calibration = new QWidget();
        calibration->setObjectName(QString::fromUtf8("calibration"));
        formLayout = new QFormLayout(calibration);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        groupBox = new QGroupBox(calibration);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout_3 = new QGridLayout(groupBox);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        widget_2 = new QWidget(groupBox);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        horizontalLayout_2 = new QHBoxLayout(widget_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        pupil_center = new QRadioButton(widget_2);
        pupil_center->setObjectName(QString::fromUtf8("pupil_center"));

        horizontalLayout_2->addWidget(pupil_center);

        pupil_glint_vector = new QRadioButton(widget_2);
        pupil_glint_vector->setObjectName(QString::fromUtf8("pupil_glint_vector"));

        horizontalLayout_2->addWidget(pupil_glint_vector);


        gridLayout_3->addWidget(widget_2, 0, 0, 1, 1);

        gaze_smoothing = new QCheckBox(groupBox);
        gaze_smoothing->setObjectName(QString::fromUtf8("gaze_smoothing"));

        gridLayout_3->addWidget(gaze_smoothing, 3, 0, 1, 1);

        start_calibrating = new QPushButton(groupBox);
        start_calibrating->setObjectName(QString::fromUtf8("start_calibrating"));

        gridLayout_3->addWidget(start_calibrating, 2, 0, 1, 1);

        calibration_method = new QComboBox(groupBox);
        calibration_method->setObjectName(QString::fromUtf8("calibration_method"));

        gridLayout_3->addWidget(calibration_method, 1, 0, 1, 1);


        formLayout->setWidget(0, QFormLayout::LabelRole, groupBox);

        remaining_calibration_points = new haytham::CalibrationPointsLabel(calibration);
        remaining_calibration_points->setObjectName(QString::fromUtf8("remaining_calibration_points"));

        formLayout->setWidget(2, QFormLayout::LabelRole, remaining_calibration_points);

        groupBox_15 = new QGroupBox(calibration);
        groupBox_15->setObjectName(QString::fromUtf8("groupBox_15"));
        formLayout_14 = new QFormLayout(groupBox_15);
        formLayout_14->setSpacing(6);
        formLayout_14->setContentsMargins(11, 11, 11, 11);
        formLayout_14->setObjectName(QString::fromUtf8("formLayout_14"));
        label_15 = new QLabel(groupBox_15);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        formLayout_14->setWidget(0, QFormLayout::LabelRole, label_15);

        calibPointDetectThresh = new QSlider(groupBox_15);
        calibPointDetectThresh->setObjectName(QString::fromUtf8("calibPointDetectThresh"));
        calibPointDetectThresh->setOrientation(Qt::Horizontal);

        formLayout_14->setWidget(0, QFormLayout::FieldRole, calibPointDetectThresh);


        formLayout->setWidget(1, QFormLayout::SpanningRole, groupBox_15);

        tabWidget->addTab(calibration, QString());
        data = new QWidget();
        data->setObjectName(QString::fromUtf8("data"));
        formLayout_12 = new QFormLayout(data);
        formLayout_12->setSpacing(6);
        formLayout_12->setContentsMargins(11, 11, 11, 11);
        formLayout_12->setObjectName(QString::fromUtf8("formLayout_12"));
        groupBox_11 = new QGroupBox(data);
        groupBox_11->setObjectName(QString::fromUtf8("groupBox_11"));
        verticalLayout = new QVBoxLayout(groupBox_11);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        record_eye_video = new QCheckBox(groupBox_11);
        record_eye_video->setObjectName(QString::fromUtf8("record_eye_video"));

        verticalLayout->addWidget(record_eye_video);

        record_scene_video = new QCheckBox(groupBox_11);
        record_scene_video->setObjectName(QString::fromUtf8("record_scene_video"));

        verticalLayout->addWidget(record_scene_video);

        record_eye_data = new QCheckBox(groupBox_11);
        record_eye_data->setObjectName(QString::fromUtf8("record_eye_data"));

        verticalLayout->addWidget(record_eye_data);

        start_stop_recording = new QPushButton(groupBox_11);
        start_stop_recording->setObjectName(QString::fromUtf8("start_stop_recording"));

        verticalLayout->addWidget(start_stop_recording);


        formLayout_12->setWidget(0, QFormLayout::SpanningRole, groupBox_11);

        groupBox_12 = new QGroupBox(data);
        groupBox_12->setObjectName(QString::fromUtf8("groupBox_12"));
        groupBox_12->setCheckable(true);
        gridLayout_13 = new QGridLayout(groupBox_12);
        gridLayout_13->setSpacing(6);
        gridLayout_13->setContentsMargins(11, 11, 11, 11);
        gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
        label_16 = new QLabel(groupBox_12);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout_13->addWidget(label_16, 0, 0, 1, 1);

        frame = new QFrame(groupBox_12);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);

        gridLayout_13->addWidget(frame, 1, 0, 1, 1);

        label_17 = new QLabel(groupBox_12);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        gridLayout_13->addWidget(label_17, 2, 0, 1, 1);

        frame_2 = new QFrame(groupBox_12);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);

        gridLayout_13->addWidget(frame_2, 3, 0, 1, 1);

        label_18 = new QLabel(groupBox_12);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        gridLayout_13->addWidget(label_18, 4, 0, 1, 1);

        frame_3 = new QFrame(groupBox_12);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);

        gridLayout_13->addWidget(frame_3, 5, 0, 1, 1);


        formLayout_12->setWidget(1, QFormLayout::SpanningRole, groupBox_12);

        tabWidget->addTab(data, QString());
        network = new QWidget();
        network->setObjectName(QString::fromUtf8("network"));
        formLayout_4 = new QFormLayout(network);
        formLayout_4->setSpacing(6);
        formLayout_4->setContentsMargins(11, 11, 11, 11);
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
        groupBox_3 = new QGroupBox(network);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_6 = new QGridLayout(groupBox_3);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        scrollArea_2 = new QScrollArea(groupBox_3);
        scrollArea_2->setObjectName(QString::fromUtf8("scrollArea_2"));
        scrollArea_2->setWidgetResizable(true);
        scrollAreaWidgetContents_2 = new QWidget();
        scrollAreaWidgetContents_2->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_2"));
        scrollAreaWidgetContents_2->setGeometry(QRect(0, 0, 96, 96));
        verticalLayout_5 = new QVBoxLayout(scrollAreaWidgetContents_2);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        network_messages = new QPlainTextEdit(scrollAreaWidgetContents_2);
        network_messages->setObjectName(QString::fromUtf8("network_messages"));
        network_messages->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        verticalLayout_5->addWidget(network_messages);

        scrollArea_2->setWidget(scrollAreaWidgetContents_2);

        gridLayout_6->addWidget(scrollArea_2, 0, 0, 1, 1);


        formLayout_4->setWidget(0, QFormLayout::SpanningRole, groupBox_3);

        groupBox_4 = new QGroupBox(network);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout_7 = new QGridLayout(groupBox_4);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        checkBox_4 = new QCheckBox(groupBox_4);
        checkBox_4->setObjectName(QString::fromUtf8("checkBox_4"));

        gridLayout_7->addWidget(checkBox_4, 0, 0, 1, 1);

        checkBox_5 = new QCheckBox(groupBox_4);
        checkBox_5->setObjectName(QString::fromUtf8("checkBox_5"));

        gridLayout_7->addWidget(checkBox_5, 0, 1, 1, 1);

        radioButton = new QRadioButton(groupBox_4);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));

        gridLayout_7->addWidget(radioButton, 1, 1, 1, 1);


        formLayout_4->setWidget(1, QFormLayout::SpanningRole, groupBox_4);

        tabWidget->addTab(network, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);

        widget = new QWidget(centralWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy1);
        gridLayout_2 = new QGridLayout(widget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        comboBox_2 = new QComboBox(widget);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));

        gridLayout_2->addWidget(comboBox_2, 0, 0, 1, 1);

        comboBox = new QComboBox(widget);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout_2->addWidget(comboBox, 0, 1, 1, 1);

        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy2);

        gridLayout_2->addWidget(label, 0, 2, 1, 1);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy2.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy2);

        gridLayout_2->addWidget(label_2, 0, 3, 1, 1);

        scrollArea = new QScrollArea(widget);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 907, 845));
        verticalLayout_2 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox_13 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_13->setObjectName(QString::fromUtf8("groupBox_13"));
        groupBox_13->setAlignment(Qt::AlignCenter);
        verticalLayout_3 = new QVBoxLayout(groupBox_13);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        eye_image = new haytham::ImageLabel(groupBox_13);
        eye_image->setObjectName(QString::fromUtf8("eye_image"));

        verticalLayout_3->addWidget(eye_image);


        verticalLayout_2->addWidget(groupBox_13);

        groupBox_14 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_14->setObjectName(QString::fromUtf8("groupBox_14"));
        groupBox_14->setAlignment(Qt::AlignCenter);
        verticalLayout_4 = new QVBoxLayout(groupBox_14);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        scene_image = new haytham::ImageLabel(groupBox_14);
        scene_image->setObjectName(QString::fromUtf8("scene_image"));

        verticalLayout_4->addWidget(scene_image);


        verticalLayout_2->addWidget(groupBox_14);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout_2->addWidget(scrollArea, 1, 0, 1, 4);


        gridLayout->addWidget(widget, 0, 1, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1298, 25));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::RightToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        groupBox_8->setTitle(QApplication::translate("MainWindow", "Eye Camera", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("MainWindow", "Camera", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("MainWindow", "Resolution", 0, QApplication::UnicodeUTF8));
        settings_eye_camera->setText(QApplication::translate("MainWindow", "Settings", 0, QApplication::UnicodeUTF8));
        start_stop_eye_camera->setText(QApplication::translate("MainWindow", "Start", 0, QApplication::UnicodeUTF8));
        groupBox_9->setTitle(QApplication::translate("MainWindow", "Scene Camera", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("MainWindow", "Camera", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("MainWindow", "Resolution", 0, QApplication::UnicodeUTF8));
        settings_scene_camera->setText(QApplication::translate("MainWindow", "Settings", 0, QApplication::UnicodeUTF8));
        start_stop_scene_camera->setText(QApplication::translate("MainWindow", "Start", 0, QApplication::UnicodeUTF8));
        groupBox_10->setTitle(QString());
        start_both_cameras->setText(QApplication::translate("MainWindow", "Start Both", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(camera), QApplication::translate("MainWindow", "Camera", 0, QApplication::UnicodeUTF8));
        groupBox_6->setTitle(QApplication::translate("MainWindow", "Pupil", 0, QApplication::UnicodeUTF8));
        show_pupil->setText(QApplication::translate("MainWindow", "Show Detected Pupil", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("MainWindow", "Threshold", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("MainWindow", "Sensitivity", 0, QApplication::UnicodeUTF8));
        fill_gaps->setText(QApplication::translate("MainWindow", "Fill Gaps", 0, QApplication::UnicodeUTF8));
        remove_glint->setText(QApplication::translate("MainWindow", "Remove Glint", 0, QApplication::UnicodeUTF8));
        radioButton_3->setText(QApplication::translate("MainWindow", "Auto", 0, QApplication::UnicodeUTF8));
        radioButton_2->setText(QApplication::translate("MainWindow", "Manual", 0, QApplication::UnicodeUTF8));
        groupBox_7->setTitle(QApplication::translate("MainWindow", "Glint", 0, QApplication::UnicodeUTF8));
        show_glint->setText(QApplication::translate("MainWindow", "Show Detected Glint", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("MainWindow", "Threshold", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("MainWindow", "Sensitivity", 0, QApplication::UnicodeUTF8));
        radioButton_7->setText(QApplication::translate("MainWindow", "Auto", 0, QApplication::UnicodeUTF8));
        radioButton_6->setText(QApplication::translate("MainWindow", "Manual", 0, QApplication::UnicodeUTF8));
        groupBox_5->setTitle(QString());
        label_6->setText(QApplication::translate("MainWindow", "Iris Size", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "Show Iris Region", 0, QApplication::UnicodeUTF8));
        show_iris_size->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(eye), QApplication::translate("MainWindow", "Eye", 0, QApplication::UnicodeUTF8));
        screen_detection->setTitle(QApplication::translate("MainWindow", "Screen Detection", 0, QApplication::UnicodeUTF8));
        show_screen->setText(QApplication::translate("MainWindow", "Show screen", 0, QApplication::UnicodeUTF8));
        show_filtered_image->setText(QApplication::translate("MainWindow", "Show filtered image", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "Threshold", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "Min Size", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Camera Calibration", 0, QApplication::UnicodeUTF8));
        undistort->setText(QApplication::translate("MainWindow", "Undistort", 0, QApplication::UnicodeUTF8));
        calibrate_camera->setText(QApplication::translate("MainWindow", "Calibrate Camera", 0, QApplication::UnicodeUTF8));
        visualization_group->setTitle(QApplication::translate("MainWindow", "Visualization", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(scene), QApplication::translate("MainWindow", "Scene", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("MainWindow", "Gaze Estimation", 0, QApplication::UnicodeUTF8));
        pupil_center->setText(QApplication::translate("MainWindow", "Pupil Center", 0, QApplication::UnicodeUTF8));
        pupil_glint_vector->setText(QApplication::translate("MainWindow", "Pupil-Glint Vector", 0, QApplication::UnicodeUTF8));
        gaze_smoothing->setText(QApplication::translate("MainWindow", "Gaze Smoothing", 0, QApplication::UnicodeUTF8));
        start_calibrating->setText(QApplication::translate("MainWindow", "Start Calibrating", 0, QApplication::UnicodeUTF8));
        remaining_calibration_points->setText(QString());
        groupBox_15->setTitle(QApplication::translate("MainWindow", "Calibration Points Detection", 0, QApplication::UnicodeUTF8));
        label_15->setText(QApplication::translate("MainWindow", "Threshold", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(calibration), QApplication::translate("MainWindow", "Calibration", 0, QApplication::UnicodeUTF8));
        groupBox_11->setTitle(QApplication::translate("MainWindow", "Record Videos and Eye Data", 0, QApplication::UnicodeUTF8));
        record_eye_video->setText(QApplication::translate("MainWindow", "Record Eye Video", 0, QApplication::UnicodeUTF8));
        record_scene_video->setText(QApplication::translate("MainWindow", "Record Scene Video", 0, QApplication::UnicodeUTF8));
        record_eye_data->setText(QApplication::translate("MainWindow", "Record Eye Data", 0, QApplication::UnicodeUTF8));
        start_stop_recording->setText(QApplication::translate("MainWindow", "Start recording", 0, QApplication::UnicodeUTF8));
        groupBox_12->setTitle(QApplication::translate("MainWindow", "Plot", 0, QApplication::UnicodeUTF8));
        label_16->setText(QApplication::translate("MainWindow", "Pupil X", 0, QApplication::UnicodeUTF8));
        label_17->setText(QApplication::translate("MainWindow", "Pupil Y", 0, QApplication::UnicodeUTF8));
        label_18->setText(QApplication::translate("MainWindow", "Pupil Diameter", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(data), QApplication::translate("MainWindow", "Data", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Messages", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "Clients", 0, QApplication::UnicodeUTF8));
        checkBox_4->setText(QApplication::translate("MainWindow", "Control", 0, QApplication::UnicodeUTF8));
        checkBox_5->setText(QApplication::translate("MainWindow", "Mouse Smoothing", 0, QApplication::UnicodeUTF8));
        radioButton->setText(QApplication::translate("MainWindow", "Auto Activation", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(network), QApplication::translate("MainWindow", "Network", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "No Pupil", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "No Glint", 0, QApplication::UnicodeUTF8));
        groupBox_13->setTitle(QApplication::translate("MainWindow", "Eye Image", 0, QApplication::UnicodeUTF8));
        eye_image->setText(QString());
        groupBox_14->setTitle(QApplication::translate("MainWindow", "Scene Image", 0, QApplication::UnicodeUTF8));
        scene_image->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
