qmake
make
if test $1 == '--all' -o $1 == '-A'
then
	./test --gtest_also_run_disabled_tests
else
	./test
fi
