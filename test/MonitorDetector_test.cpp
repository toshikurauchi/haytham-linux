/*
 * MonitorDetector_test.cpp
 *
 *  Created on: 18/01/2013
 *      Author: Andrew Kurauchi
 */

#include <gtest/gtest.h>
#include <cv.h>

#include "MonitorDetection/MonitorDetector.h"
#include "MockMETState.h"
#include "TestUtils.h"

namespace haytham {

TEST(MonitorDetector, FindMonitor) {
    MockMETState state;
    state.setScreenMinSize(0);
    state.setSceneThreshold(500);

    /*MonitorDetector detector(&state);

    cv::Mat img = cv::imread("images/monitor.jpg");
    ASSERT_TRUE(detector.detectScreen(img));
    detector.drawScreen(img);*/

    // Uncomment the following lines in order to see the result
    // TestUtils::showBeforeAfterImages("Image with monitor detected and canny edges image", img, detector.getCannyEdges());
    // cv::waitKey(0);
}

} // namespace haytham
