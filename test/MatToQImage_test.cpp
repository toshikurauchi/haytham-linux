/*
 * MatToQImage_test.cpp
 *
 *  Created on: 03/04/2013
 *      Author: Andrew Kurauchi
 */

#include <gtest/gtest.h>
#include <QImage>

#include "../MatToQImage.h"

namespace haytham {

TEST(QImageToMat, shouldConvertCorrectly) {
    QImage qImage(4, 3, QImage::Format_Indexed8);
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 3; j++) {
            qImage.setPixel(QPoint(i,j), 3*i+j);
        }
    }

    cv::Mat mat = QImageToMat(qImage);

    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 3; j++) {
            ASSERT_EQ(mat.at<float>(i,j), qImage.pixel(QPoint(i, j)));
        }
    }
}

} /* namespace haytham */
