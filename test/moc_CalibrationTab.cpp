/****************************************************************************
** Meta object code from reading C++ file 'CalibrationTab.h'
**
** Created: Thu Sep 19 18:00:44 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Ui/CalibrationTab.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CalibrationTab.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_haytham__CalibrationTab[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   24,   24,   24, 0x0a,
      50,   44,   24,   24, 0x0a,
      74,   44,   24,   24, 0x0a,
      96,   44,   24,   24, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_haytham__CalibrationTab[] = {
    "haytham::CalibrationTab\0\0startCalibrating()\0"
    "value\0smoothGazeChanged(bool)\0"
    "showGazeChanged(bool)\0updateCalibPointThresh(int)\0"
};

void haytham::CalibrationTab::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CalibrationTab *_t = static_cast<CalibrationTab *>(_o);
        switch (_id) {
        case 0: _t->startCalibrating(); break;
        case 1: _t->smoothGazeChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->showGazeChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->updateCalibPointThresh((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData haytham::CalibrationTab::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject haytham::CalibrationTab::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_haytham__CalibrationTab,
      qt_meta_data_haytham__CalibrationTab, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &haytham::CalibrationTab::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *haytham::CalibrationTab::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *haytham::CalibrationTab::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_haytham__CalibrationTab))
        return static_cast<void*>(const_cast< CalibrationTab*>(this));
    return QObject::qt_metacast(_clname);
}

int haytham::CalibrationTab::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
