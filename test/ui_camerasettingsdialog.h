/********************************************************************************
** Form generated from reading UI file 'camerasettingsdialog.ui'
**
** Created: Thu Sep 19 17:55:19 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CAMERASETTINGSDIALOG_H
#define UI_CAMERASETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_CameraSettingsDialog
{
public:
    QDialogButtonBox *buttonBox;
    QCheckBox *useLED;

    void setupUi(QDialog *CameraSettingsDialog)
    {
        if (CameraSettingsDialog->objectName().isEmpty())
            CameraSettingsDialog->setObjectName(QString::fromUtf8("CameraSettingsDialog"));
        CameraSettingsDialog->resize(298, 99);
        buttonBox = new QDialogButtonBox(CameraSettingsDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(30, 50, 251, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        useLED = new QCheckBox(CameraSettingsDialog);
        useLED->setObjectName(QString::fromUtf8("useLED"));
        useLED->setGeometry(QRect(20, 20, 97, 22));

        retranslateUi(CameraSettingsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), CameraSettingsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), CameraSettingsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(CameraSettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *CameraSettingsDialog)
    {
        CameraSettingsDialog->setWindowTitle(QApplication::translate("CameraSettingsDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        useLED->setText(QApplication::translate("CameraSettingsDialog", "Use LED", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CameraSettingsDialog: public Ui_CameraSettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CAMERASETTINGSDIALOG_H
