/****************************************************************************
** Meta object code from reading C++ file 'DataTab.h'
**
** Created: Thu Sep 19 18:00:48 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Ui/DataTab.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DataTab.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_haytham__DataTab[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_haytham__DataTab[] = {
    "haytham::DataTab\0\0startStopRecording()\0"
};

void haytham::DataTab::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DataTab *_t = static_cast<DataTab *>(_o);
        switch (_id) {
        case 0: _t->startStopRecording(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData haytham::DataTab::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject haytham::DataTab::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_haytham__DataTab,
      qt_meta_data_haytham__DataTab, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &haytham::DataTab::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *haytham::DataTab::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *haytham::DataTab::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_haytham__DataTab))
        return static_cast<void*>(const_cast< DataTab*>(this));
    return QObject::qt_metacast(_clname);
}

int haytham::DataTab::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
