/*
 * TestUtils.cpp
 *
 *  Created on: 31/07/2012
 *      Author: Andrew Kurauchi
 */

#include "TestUtils.h"

namespace haytham {

TestUtils::TestUtils() {
	// TODO Auto-generated constructor stub

}

void TestUtils::showImage(std::string windowName, int x, int y, cv::Mat img) {
	cv::namedWindow(windowName, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(windowName.c_str(), x, y);
	cv::imshow(windowName, img);
}

void TestUtils::showBeforeAfterImages(std::string testName, cv::Mat beforeImg, cv::Mat afterImg) {
	showImage(testName + " Original Image", 100, 100, beforeImg);
	showImage(testName + " Processed Image", 400, 100, afterImg);
}

MockMETState TestUtils::createMockMETStateForBlobDetection(double minPupilScale, double maxPupilScale, int irisDiameter, cv::Mat &eyeImageOriginal) {
	MockMETState state;
	state.setMinPupilScale(minPupilScale);
	state.setMaxPupilScale(maxPupilScale);
	state.setIrisDiameter(irisDiameter);
	state.setEyeImageOriginal(eyeImageOriginal);
	return state;
}

} /* namespace haytham */
