/****************************************************************************
** Meta object code from reading C++ file 'CameraTab.h'
**
** Created: Thu Sep 19 18:00:46 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Ui/CameraTab.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CameraTab.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_haytham__CameraWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_haytham__CameraWidget[] = {
    "haytham::CameraWidget\0"
};

void haytham::CameraWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData haytham::CameraWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject haytham::CameraWidget::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_haytham__CameraWidget,
      qt_meta_data_haytham__CameraWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &haytham::CameraWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *haytham::CameraWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *haytham::CameraWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_haytham__CameraWidget))
        return static_cast<void*>(const_cast< CameraWidget*>(this));
    return QObject::qt_metacast(_clname);
}

int haytham::CameraWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_haytham__CameraComponent[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   25,   25,   25, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_haytham__CameraComponent[] = {
    "haytham::CameraComponent\0\0openSettings()\0"
};

void haytham::CameraComponent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CameraComponent *_t = static_cast<CameraComponent *>(_o);
        switch (_id) {
        case 0: _t->openSettings(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData haytham::CameraComponent::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject haytham::CameraComponent::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_haytham__CameraComponent,
      qt_meta_data_haytham__CameraComponent, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &haytham::CameraComponent::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *haytham::CameraComponent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *haytham::CameraComponent::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_haytham__CameraComponent))
        return static_cast<void*>(const_cast< CameraComponent*>(this));
    return QObject::qt_metacast(_clname);
}

int haytham::CameraComponent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_haytham__CameraTab[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      33,   20,   19,   19, 0x0a,
      69,   19,   19,   19, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_haytham__CameraTab[] = {
    "haytham::CameraTab\0\0cameraObject\0"
    "connectOrDisconnectCamera(QObject*)\0"
    "startStopBothCameras()\0"
};

void haytham::CameraTab::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CameraTab *_t = static_cast<CameraTab *>(_o);
        switch (_id) {
        case 0: _t->connectOrDisconnectCamera((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 1: _t->startStopBothCameras(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData haytham::CameraTab::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject haytham::CameraTab::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_haytham__CameraTab,
      qt_meta_data_haytham__CameraTab, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &haytham::CameraTab::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *haytham::CameraTab::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *haytham::CameraTab::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_haytham__CameraTab))
        return static_cast<void*>(const_cast< CameraTab*>(this));
    return QObject::qt_metacast(_clname);
}

int haytham::CameraTab::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
