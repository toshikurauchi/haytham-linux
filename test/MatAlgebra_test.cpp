/*
 * MatAlgebra_test.cpp
 *
 *  Created on: 19/09/2013
 *      Author: Andrew Kurauchi
 */

#include <gtest/gtest.h>
#include <cv.h>

#include "MatAlgebra.h"

namespace haytham {

TEST(MatAlgebra, CumulativeSumByRow) {
    MatAlgebra algebra;
    cv::Mat matrix(3, 3, CV_32F);

    matrix.at<float>(0, 0) = 1;
    matrix.at<float>(0, 1) = 2;
    matrix.at<float>(0, 2) = 3;
    matrix.at<float>(1, 0) = 4;
    matrix.at<float>(1, 1) = 5;
    matrix.at<float>(1, 2) = 6;
    matrix.at<float>(2, 0) = 7;
    matrix.at<float>(2, 1) = 8;
    matrix.at<float>(2, 2) = 9;

    cv::Mat result = algebra.cumsum(matrix, true);
    ASSERT_FLOAT_EQ(1, result.at<float>(0, 0));
    ASSERT_FLOAT_EQ(3, result.at<float>(0, 1));
    ASSERT_FLOAT_EQ(6, result.at<float>(0, 2));
    ASSERT_FLOAT_EQ(4, result.at<float>(1, 0));
    ASSERT_FLOAT_EQ(9, result.at<float>(1, 1));
    ASSERT_FLOAT_EQ(15, result.at<float>(1, 2));
    ASSERT_FLOAT_EQ(7, result.at<float>(2, 0));
    ASSERT_FLOAT_EQ(15, result.at<float>(2, 1));
    ASSERT_FLOAT_EQ(24, result.at<float>(2, 2));
}

TEST(MatAlgebra, CumulativeSumByCol) {
    MatAlgebra algebra;
    cv::Mat matrix(3, 3, CV_32F);

    matrix.at<float>(0, 0) = 1;
    matrix.at<float>(0, 1) = 2;
    matrix.at<float>(0, 2) = 3;
    matrix.at<float>(1, 0) = 4;
    matrix.at<float>(1, 1) = 5;
    matrix.at<float>(1, 2) = 6;
    matrix.at<float>(2, 0) = 7;
    matrix.at<float>(2, 1) = 8;
    matrix.at<float>(2, 2) = 9;

    cv::Mat result = algebra.cumsum(matrix, false);
    ASSERT_FLOAT_EQ(1, result.at<float>(0, 0));
    ASSERT_FLOAT_EQ(2, result.at<float>(0, 1));
    ASSERT_FLOAT_EQ(3, result.at<float>(0, 2));
    ASSERT_FLOAT_EQ(5, result.at<float>(1, 0));
    ASSERT_FLOAT_EQ(7, result.at<float>(1, 1));
    ASSERT_FLOAT_EQ(9, result.at<float>(1, 2));
    ASSERT_FLOAT_EQ(12, result.at<float>(2, 0));
    ASSERT_FLOAT_EQ(15, result.at<float>(2, 1));
    ASSERT_FLOAT_EQ(18, result.at<float>(2, 2));
}

TEST(MatAlgebra, CumulativeSumByRowInt) {
    MatAlgebra algebra;
    cv::Mat matrix(3, 3, CV_8U);

    matrix.at<unsigned char>(0, 0) = 1;
    matrix.at<unsigned char>(0, 1) = 2;
    matrix.at<unsigned char>(0, 2) = 3;
    matrix.at<unsigned char>(1, 0) = 4;
    matrix.at<unsigned char>(1, 1) = 5;
    matrix.at<unsigned char>(1, 2) = 6;
    matrix.at<unsigned char>(2, 0) = 7;
    matrix.at<unsigned char>(2, 1) = 8;
    matrix.at<unsigned char>(2, 2) = 9;

    cv::Mat result = algebra.cumsum(matrix, true);
    ASSERT_FLOAT_EQ(1, result.at<unsigned char>(0, 0));
    ASSERT_FLOAT_EQ(3, result.at<unsigned char>(0, 1));
    ASSERT_FLOAT_EQ(6, result.at<unsigned char>(0, 2));
    ASSERT_FLOAT_EQ(4, result.at<unsigned char>(1, 0));
    ASSERT_FLOAT_EQ(9, result.at<unsigned char>(1, 1));
    ASSERT_FLOAT_EQ(15, result.at<unsigned char>(1, 2));
    ASSERT_FLOAT_EQ(7, result.at<unsigned char>(2, 0));
    ASSERT_FLOAT_EQ(15, result.at<unsigned char>(2, 1));
    ASSERT_FLOAT_EQ(24, result.at<unsigned char>(2, 2));
}

TEST(MatAlgebra, CumulativeSumByColInt) {
    MatAlgebra algebra;
    cv::Mat matrix(3, 3, CV_8U);

    matrix.at<unsigned char>(0, 0) = 1;
    matrix.at<unsigned char>(0, 1) = 2;
    matrix.at<unsigned char>(0, 2) = 3;
    matrix.at<unsigned char>(1, 0) = 4;
    matrix.at<unsigned char>(1, 1) = 5;
    matrix.at<unsigned char>(1, 2) = 6;
    matrix.at<unsigned char>(2, 0) = 7;
    matrix.at<unsigned char>(2, 1) = 8;
    matrix.at<unsigned char>(2, 2) = 9;

    cv::Mat result = algebra.cumsum(matrix, false);
    ASSERT_FLOAT_EQ(1, result.at<unsigned char>(0, 0));
    ASSERT_FLOAT_EQ(2, result.at<unsigned char>(0, 1));
    ASSERT_FLOAT_EQ(3, result.at<unsigned char>(0, 2));
    ASSERT_FLOAT_EQ(5, result.at<unsigned char>(1, 0));
    ASSERT_FLOAT_EQ(7, result.at<unsigned char>(1, 1));
    ASSERT_FLOAT_EQ(9, result.at<unsigned char>(1, 2));
    ASSERT_FLOAT_EQ(12, result.at<unsigned char>(2, 0));
    ASSERT_FLOAT_EQ(15, result.at<unsigned char>(2, 1));
    ASSERT_FLOAT_EQ(18, result.at<unsigned char>(2, 2));
}

TEST(MatAlgebra, SubtractRows) {
    MatAlgebra algebra;
    cv::Mat matrix(3, 3, CV_32F);

    matrix.at<float>(0, 0) = 1;
    matrix.at<float>(0, 1) = 2;
    matrix.at<float>(0, 2) = 3;
    matrix.at<float>(1, 0) = 4;
    matrix.at<float>(1, 1) = 5;
    matrix.at<float>(1, 2) = 6;
    matrix.at<float>(2, 0) = 7;
    matrix.at<float>(2, 1) = 8;
    matrix.at<float>(2, 2) = 9;

    cv::Mat cumsum = algebra.cumsum(matrix, false);
    cv::Mat result = cumsum.row(2) - cumsum.row(0);
    result /= float(2);
    ASSERT_FLOAT_EQ(5.5, result.at<float>(0, 0));
    ASSERT_FLOAT_EQ(6.5, result.at<float>(0, 1));
    ASSERT_FLOAT_EQ(7.5, result.at<float>(0, 2));
}

} // namespace haytham

