/*
 * ImageProcessing_test.cpp
 *
 *  Created on: 30/07/2012
 *      Author: Andrew Kurauchi
 */

#include <gtest/gtest.h>
#include <cv.h>
#include <highgui.h>
#include <stdio.h>
#include <string>
#include <vector>

#include "Point.h"
#include "ImageProcessing.h"
#include "TestUtils.h"

namespace haytham {
	cv::Mat createImage(int width, int height) {
		return cv::Mat(cv::Size(width, height), IPL_DEPTH_8U, 1);
	}

	TEST(Filter, DISABLED_Threshold) {
		ImageProcessing processor;
		cv::Mat grayImg = cv::imread("images/lenagray.png", 0);
		cv::Mat processedImg = processor.filterThreshold(grayImg, 100, false);

		TestUtils::showBeforeAfterImages("Simple threshold", grayImg, processedImg);
		cv::waitKey(0);
	}

	TEST(Filter, DISABLED_PupilAdaptiveThreshold) {
		ImageProcessing processor;
		cv::Mat grayImg = cv::imread("images/lenagray.png", 0);
		cv::Mat processedImg = processor.filterPupilAdaptiveThreshold(grayImg, 100, false, -1);

		TestUtils::showBeforeAfterImages("Pupil Adaptive Threshold", grayImg, processedImg);
		cv::waitKey(0);
	}

	TEST(Filter, DISABLED_Sobel) {
		ImageProcessing processor;
		cv::Mat grayImg = cv::imread("images/lenagray.png", 0);
		cv::Mat processedImg = processor.filterSobel(grayImg);

		TestUtils::showBeforeAfterImages("Sobel", grayImg, processedImg);
		cv::waitKey(0);
	}

	TEST(Circle, ShouldntDrawCircleWithNonPositiveCenterX) {
		ImageProcessing processor;
		cv::Mat img = createImage(10, 10);
        ASSERT_FALSE(processor.drawIrisCircle(img, 0, 5, 3, 0.1, 0.7));
	}

	TEST(Circle, ShouldntDrawCircleWithNonPositiveCenterY) {
		ImageProcessing processor;
		cv::Mat img = createImage(10, 10);
        ASSERT_FALSE(processor.drawIrisCircle(img, 5, 0, 3, 0.1, 0.7));
	}

	TEST(Circle, ShouldntDrawCircleWithCenterXGreaterThanImgWidth) {
		ImageProcessing processor;
		cv::Mat img = createImage(10, 10);
        ASSERT_FALSE(processor.drawIrisCircle(img, 10, 5, 3, 0.1, 0.7));
	}

	TEST(Circle, ShouldntDrawCircleWithCenterYGreaterThanImgHeight) {
		ImageProcessing processor;
		cv::Mat img = createImage(10, 10);
        ASSERT_FALSE(processor.drawIrisCircle(img, 10, 5, 3, 0.1, 0.7));
	}

	TEST(Circle, DrawIrisCircle) {
		ImageProcessing processor;
		cv::Mat img = cv::imread("images/eye.jpg", 1);
        ASSERT_TRUE(processor.drawIrisCircle(img, 270, 157, 160, 0.1, 0.7));
		// Uncomment the following lines in order to see the result
		// TestUtils::showImage("Iris Circle", 100, 100, img);
		// cv::waitKey(0);
	}

	TEST(Line, DISABLED_DrawLine) {
		ImageProcessing processor;
		cv::Mat img = cv::imread("images/eye.jpg", 1);
        processor.drawLine(img, Point(1, img.rows/2), Point(img.cols - 2, img.rows/2), cv::Scalar(0, 0, 255));
		TestUtils::showImage("Line", 100, 100, img);
		cv::waitKey(0);
	}

	TEST(Cross, ShouldntDrawCrossWithNonPositiveIntersectX) {
		ImageProcessing processor;
		cv::Mat img = createImage(10, 10);
		ASSERT_FALSE(processor.drawCross(img, 0, 5, cv::Scalar(0, 0, 0)));
	}

	TEST(Cross, ShouldntDrawCrossWithNonPositiveIntersectY) {
		ImageProcessing processor;
		cv::Mat img = createImage(10, 10);
		ASSERT_FALSE(processor.drawCross(img, 5, 0, cv::Scalar(0, 0, 0)));
	}

	TEST(Cross, ShouldntDrawCrossWithIntersectXGreaterThanImageWidth) {
		ImageProcessing processor;
		cv::Mat img = createImage(10, 10);
		ASSERT_FALSE(processor.drawCross(img, 10, 5, cv::Scalar(0, 0, 0)));
	}

	TEST(Cross, ShouldntDrawCrossWithIntersectYGreaterThanImageHeight) {
		ImageProcessing processor;
		cv::Mat img = createImage(10, 10);
		ASSERT_FALSE(processor.drawCross(img, 5, 10, cv::Scalar(0, 0, 0)));
	}

	TEST(Cross, DrawCross) {
		ImageProcessing processor;
		cv::Mat img = cv::imread("images/eye.jpg", 1);
        cv::resize(img, img, cv::Size(640, 480));
        ASSERT_TRUE(processor.drawCross(img, 352, 356, cv::Scalar(0, 0, 255)));
		// Uncomment the following lines in order to see the result
        // TestUtils::showImage("Cross", 100, 100, img);
        // cv::waitKey(0);
	}

	TEST(Ellipse, DISABLED_DrawEllipse) {
		ImageProcessing processor;
		cv::Mat img = cv::imread("images/eye.jpg", 1);
		cv::RotatedRect ellipse = cv::RotatedRect(cv::Point2f(255, 180), cv::Size2f(300, 150), 20);
		processor.drawEllipse(img, ellipse, cv::Scalar(0, 0, 255));
		// Uncomment the following lines in order to see the result
		 TestUtils::showImage("Ellipse", 100, 100, img);
		 cv::waitKey(0);
	}

	TEST(Median, MedianForEmptyVector) {
		ImageProcessing processor;
        std::vector<float> numbers;
		ASSERT_EQ(0, processor.findMedian(numbers));
	}

	TEST(Median, MedianForEvenNumberOfElements) {
		ImageProcessing processor;
        std::vector<float> numbers;
		numbers.push_back(1);
		numbers.push_back(2);
		numbers.push_back(3);
		numbers.push_back(4);
		ASSERT_EQ(2.5, processor.findMedian(numbers));
	}

	TEST(Median, MedianForOddNumberOfElements) {
		ImageProcessing processor;
        std::vector<float> numbers;
		numbers.push_back(1);
		numbers.push_back(2);
		numbers.push_back(3);
		ASSERT_EQ(2, processor.findMedian(numbers));
	}

	TEST(ROI, CreatesROIAsSpecified) {
		ImageProcessing processor;
		cv::Mat image = cv::Mat::zeros(cv::Size(20,20), 0);
		cv::Rect roi = processor.createROIForImage(image, 2, 3, 10, 15);
		ASSERT_EQ(2, roi.x);
		ASSERT_EQ(3, roi.y);
		ASSERT_EQ(10, roi.width);
		ASSERT_EQ(15, roi.height);
	}

	TEST(ROI, CreatesCroppedROI) {
		ImageProcessing processor;
		cv::Mat image = cv::Mat::zeros(cv::Size(20,20), 0);
		cv::Rect roi = processor.createROIForImage(image, -2, -3, 25, 30);
		ASSERT_EQ(0, roi.x);
		ASSERT_EQ(0, roi.y);
		ASSERT_EQ(20, roi.width);
		ASSERT_EQ(20, roi.height);
	}

	TEST(ROI, CreatesCroppedROIWithCorrectXAndY) {
		ImageProcessing processor;
		cv::Mat image = cv::Mat::zeros(cv::Size(20,20), 0);
		cv::Rect roi = processor.createROIForImage(image, 2, 3, 25, 30);
		ASSERT_EQ(2, roi.x);
		ASSERT_EQ(3, roi.y);
		ASSERT_EQ(18, roi.width);
		ASSERT_EQ(17, roi.height);
	}

	TEST(ROI, CreatesCroppedROIWithCorrectWidthAndHeight) {
		ImageProcessing processor;
		cv::Mat image = cv::Mat::zeros(cv::Size(20,20), 0);
		cv::Rect roi = processor.createROIForImage(image, -2, -3, 5, 10);
		ASSERT_EQ(0, roi.x);
		ASSERT_EQ(0, roi.y);
		ASSERT_EQ(3, roi.width);
		ASSERT_EQ(7, roi.height);
	}

    TEST(Rectangle, DISABLED_DrawRectangle) {
        ImageProcessing processor;
        cv::Mat img = cv::imread("images/eye.jpg", 1);
        processor.drawRectangle(img, Point(img.cols/2, img.rows/2), cv::Size2f(100, 100));
        // Uncomment the following lines in order to see the result
        TestUtils::showImage("Rectangle", 100, 100, img);
        cv::waitKey(0);
    }

    TEST(Rectangle, DISABLED_DrawFilledRectangleWithText) {
        ImageProcessing processor;
        Point corners[4];
        corners[0].x = 50;
        corners[0].y = 40;
        corners[1].x = 150;
        corners[1].y = 50;
        corners[2].x = 160;
        corners[2].y = 140;
        corners[3].x = 40;
        corners[3].y = 130;

        cv::Mat img = cv::imread("images/eye.jpg", 1);
        processor.drawRectangle(img, corners, 0, true, "Testing is cool");
        // Uncomment the following lines in order to see the result
        TestUtils::showImage("Rectangle", 100, 100, img);
        cv::waitKey(0);
    }

    TEST(Rectangle, DISABLED_DrawNotFilledRectangleWithText) {
        ImageProcessing processor;
        Point corners[4];
        corners[0].x = 50;
        corners[0].y = 40;
        corners[1].x = 150;
        corners[1].y = 50;
        corners[2].x = 160;
        corners[2].y = 140;
        corners[3].x = 40;
        corners[3].y = 130;

        cv::Mat img = cv::imread("images/eye.jpg", 1);
        processor.drawRectangle(img, corners, 0, false, "Testing is cool");
        // Uncomment the following lines in order to see the result
        TestUtils::showImage("Rectangle", 100, 100, img);
        cv::waitKey(0);
    }
} /* namespace haytham */
