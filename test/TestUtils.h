/*
 * TestUtils.h
 *
 *  Created on: 31/07/2012
 *      Author: Andrew Kurauchi
 */

#ifndef TESTUTILS_H_
#define TESTUTILS_H_

#include <cv.h>
#include <highgui.h>

#include "MockMETState.h"

#define ASSERT_BETWEEN(val1, val2, actual) \
	do { \
		ASSERT_LE(val1, actual); \
		ASSERT_GE(val2, actual); \
	} while(0)

namespace haytham {

class TestUtils {
public:
	TestUtils();
	static void showImage(std::string windowName, int x, int y, cv::Mat img);
	static void showBeforeAfterImages(std::string testName, cv::Mat beforeImg, cv::Mat afterImg);
	static MockMETState createMockMETStateForBlobDetection(double minPupilScale,
			double maxPupilScale, int irisDiameter, cv::Mat &eyeImageOriginal);
};

} /* namespace haytham */
#endif /* TESTUTILS_H_ */
