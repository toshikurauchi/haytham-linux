/*
 * Calibration_test.cpp
 *
 *  Created on: 20/11/2012
 *      Author: Andrew Kurauchi
 */

#include <gtest/gtest.h>

#include "../Calibration/Calibration.h"
#include "../Common/Point.h"

namespace haytham {

    TEST(PolynomialCalibration9Points, shouldntCalibrateWithLessThan9Points) {
        PolynomialCalibration9Points calibration;
        calibration.startCalibrating();
        calibration.addPoint(Point(1,1), Point(1,1));
        calibration.addPoint(Point(1,1), Point(1,1));
        calibration.addPoint(Point(1,1), Point(1,1));
        calibration.addPoint(Point(1,1), Point(1,1));
        calibration.addPoint(Point(1,1), Point(1,1));
        calibration.addPoint(Point(1,1), Point(1,1));
        calibration.addPoint(Point(1,1), Point(1,1));
        calibration.addPoint(Point(1,1), Point(1,1));
        ASSERT_FALSE(calibration.isReady());
    }

    TEST(PolynomialCalibration9Points, calibrate) {
        // Function used: f(x,y) = (1+2x+3y+4xy+5x^2+6y^2, 6+5x+4y+3xy+2x^2+y^2)
        PolynomialCalibration9Points calibration;
        calibration.startCalibrating();
        calibration.addPoint(Point(1,1), Point(21,21));
        calibration.addPoint(Point(1,2), Point(46,31));
        calibration.addPoint(Point(2,1), Point(42,35));
        calibration.addPoint(Point(3,4), Point(208,107));
        calibration.addPoint(Point(4,3), Point(200,115));
        calibration.addPoint(Point(2,5), Point(230,99));
        calibration.addPoint(Point(1,4), Point(132,57));
        calibration.addPoint(Point(4,1), Point(114,75));
        calibration.addPoint(Point(3,3), Point(151,87));
        ASSERT_TRUE(calibration.isReady());
        ASSERT_FALSE(calibration.isCalibrated());
        calibration.calibrate();
        ASSERT_TRUE(calibration.isCalibrated())
    }

    TEST(PolynomialCalibration9Points, calibrateAndMap) {
        // Function used: f(x,y) = (1+2x+3y+4xy+5x^2+6y^2, 6+5x+4y+3xy+2x^2+y^2)
        PolynomialCalibration9Points calibration;
        calibration.startCalibrating();
        calibration.addPoint(Point(1,1), Point(21,21));
        calibration.addPoint(Point(1,2), Point(46,31));
        calibration.addPoint(Point(2,1), Point(42,35));
        calibration.addPoint(Point(3,4), Point(208,107));
        calibration.addPoint(Point(4,3), Point(200,115));
        calibration.addPoint(Point(2,5), Point(230,99));
        calibration.addPoint(Point(1,4), Point(132,57));
        calibration.addPoint(Point(4,1), Point(114,75));
        calibration.addPoint(Point(3,3), Point(151,87));
        ASSERT_TRUE(calibration.isReady());
        ASSERT_FALSE(calibration.isCalibrated());
        calibration.calibrate();
        ASSERT_TRUE(calibration.isCalibrated());
        Point mapped = calibration.map(Point(5,5), Point(0,0));
        ASSERT_FLOAT_EQ(401, mapped.x);
        ASSERT_FLOAT_EQ(201, mapped.y);
    }

    TEST(PolynomialCalibration9Points, calibrateAndAddCorrection) {
        // Function used: f(x,y) = (1+2x+3y+4xy+5x^2+6y^2, 6+5x+4y+3xy+2x^2+y^2)
        PolynomialCalibration9Points calibration;
        calibration.startCalibrating();
        calibration.addPoint(Point(1,1), Point(21,21));
        calibration.addPoint(Point(1,2), Point(46,31));
        calibration.addPoint(Point(2,1), Point(42,35));
        calibration.addPoint(Point(3,4), Point(208,107));
        calibration.addPoint(Point(4,3), Point(200,115));
        calibration.addPoint(Point(2,5), Point(230,99));
        calibration.addPoint(Point(1,4), Point(132,57));
        calibration.addPoint(Point(4,1), Point(114,75));
        calibration.addPoint(Point(3,3), Point(151,87));
        ASSERT_TRUE(calibration.isReady());
        ASSERT_FALSE(calibration.isCalibrated());
        calibration.calibrate();
        ASSERT_TRUE(calibration.isCalibrated());
        Point mapped = calibration.map(Point(5,5), Point(1,2));
        ASSERT_FLOAT_EQ(400, mapped.x);
        ASSERT_FLOAT_EQ(199, mapped.y);
    }

    TEST(HomographyCalibration, shouldntCalibrateWithLessThan9Points) {
        HomographyCalibration calibration;
        calibration.startCalibrating();
        calibration.addPoint(Point(1,1), Point(1,1));
        calibration.addPoint(Point(1,1), Point(1,1));
        calibration.addPoint(Point(1,1), Point(1,1));
        ASSERT_FALSE(calibration.isReady());
    }

    TEST(HomographyCalibration, calibrate) {
        HomographyCalibration calibration;
        calibration.startCalibrating();
        //calibration.addPoint(Point(1,3), Point(1,1));
        //calibration.addPoint(Point(1,5), Point(1,5));
        //calibration.addPoint(Point(4,7), Point(5,5));
        //calibration.addPoint(Point(4,1), Point(5,1));
        calibration.addPoint(Point(1,1), Point(11,11));
        calibration.addPoint(Point(1,3), Point(11,13));
        calibration.addPoint(Point(3,3), Point(13,13));
        calibration.addPoint(Point(3,1), Point(13,11));
        ASSERT_TRUE(calibration.isReady());
        ASSERT_FALSE(calibration.isCalibrated());
        calibration.calibrate();
        ASSERT_TRUE(calibration.isCalibrated());
        Point mapped = calibration.map(Point(2,3), Point(0,0));
        ASSERT_FLOAT_EQ(12, mapped.x);
        ASSERT_FLOAT_EQ(13, mapped.y);
    }

} /* namespace haytham */


