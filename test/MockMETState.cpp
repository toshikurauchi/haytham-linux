/*
 * MockMETState.cpp
 *
 *  Created on: 14/08/2012
 *      Author: Andrew Kurauchi
 */

#include "MockMETState.h"

namespace haytham {

MockMETState::MockMETState() {
}

void MockMETState::setEyeImageOriginal(cv::Mat &eyeImageOriginal) {
	this->eyeImageOrginal = eyeImageOriginal;
}
void MockMETState::setEyeImageForShow(cv::Mat &eyeImageForShow) {
	this->eyeImageForShow = eyeImageForShow;
}
void MockMETState::setDilateErode(bool dilateErode) {
	this->dilateErode = dilateErode;
}
void MockMETState::setMinPupilScale(double minPupilScale) {
	this->minPupilScale = minPupilScale;
}
void MockMETState::setMaxPupilScale(double maxPupilScale) {
	this->maxPupilScale = maxPupilScale;
}
void MockMETState::setShowPupil(bool showPupil) {
	this->showPupil = showPupil;
}
void MockMETState::setPupilAdaptive(bool pupilAdaptive) {
	this->pupilAdaptive = pupilAdaptive;
}
void MockMETState::setRemoveGlint(bool removeGlint) {
	this->removeGlint = removeGlint;
}
void MockMETState::setGlintThreshold(int glintThreshold) {
	this->glintThreshold = glintThreshold;
}
void MockMETState::setIrisDiameter(int irisDiameter) {
	this->irisDiameter = irisDiameter;
}
void MockMETState::setShowIris(bool showIris) {
    this->showIris = showIris;
}
void MockMETState::setSceneThreshold(int sceneThreshold) {
    this->sceneThreshold = sceneThreshold;
}
void MockMETState::setScreenMinSize(float screenMinSize) {
    this->screenMinSize = screenMinSize;
}

} /* namespace haytham */
