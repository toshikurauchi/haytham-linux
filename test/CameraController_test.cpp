/*
 * CameraController_test.cpp
 *
 *  Created on: 01/08/2012
 *      Author: Andrew Kurauchi
 */

#include <gtest/gtest.h>
#include <iostream>
#include <vector>

#include "CameraController.h"

namespace haytham {

    TEST(CameraController, getCameraInfoList) {
        CameraController *controller = CameraController::newController();
        std::vector<CameraInfo> cameraInfoList = controller->getCameraInfoList();
        for(std::vector<CameraInfo>::iterator it = cameraInfoList.begin(); it != cameraInfoList.end(); it++) {
            std::cout << *it << std::endl;
        }
        delete controller;
    }

} /* namespace haytham */

