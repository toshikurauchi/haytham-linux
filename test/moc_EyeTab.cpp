/****************************************************************************
** Meta object code from reading C++ file 'EyeTab.h'
**
** Created: Thu Sep 19 18:00:50 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Ui/EyeTab.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'EyeTab.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_haytham__EyeTab[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   17,   16,   16, 0x0a,
      46,   17,   16,   16, 0x0a,
      71,   17,   16,   16, 0x0a,
      93,   17,   16,   16, 0x0a,
     119,   17,   16,   16, 0x0a,
     140,   17,   16,   16, 0x0a,
     164,   17,   16,   16, 0x0a,
     196,  190,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_haytham__EyeTab[] = {
    "haytham::EyeTab\0\0newValue\0updateIrisSize(int)\0"
    "updateShowIrisSize(bool)\0updateShowPupil(bool)\0"
    "updatePupilThreshold(int)\0"
    "updateFillGaps(bool)\0updateRemoveGlint(bool)\0"
    "updateGlintThreshold(int)\0index\0"
    "updateEyeDetector(int)\0"
};

void haytham::EyeTab::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        EyeTab *_t = static_cast<EyeTab *>(_o);
        switch (_id) {
        case 0: _t->updateIrisSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->updateShowIrisSize((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->updateShowPupil((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->updatePupilThreshold((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->updateFillGaps((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->updateRemoveGlint((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->updateGlintThreshold((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->updateEyeDetector((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData haytham::EyeTab::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject haytham::EyeTab::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_haytham__EyeTab,
      qt_meta_data_haytham__EyeTab, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &haytham::EyeTab::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *haytham::EyeTab::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *haytham::EyeTab::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_haytham__EyeTab))
        return static_cast<void*>(const_cast< EyeTab*>(this));
    return QObject::qt_metacast(_clname);
}

int haytham::EyeTab::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
